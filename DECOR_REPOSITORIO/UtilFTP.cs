﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net;
using System.IO;

namespace DECOR_REPOSITORIO
{
    public class UtilFTP
    {
        public static void crearDirectorio(String path)
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(path);
                request.Method = WebRequestMethods.Ftp.MakeDirectory;
                request.Credentials = new NetworkCredential(Constantes.FTPUser, Constantes.FTPUserPass);
                var resp = (FtpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                throw e;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static FtpWebResponse crearArchivo(String path, byte[] fileContents)
        {
            try
            {

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(path);
                request.Method = WebRequestMethods.Ftp.UploadFile;

                // This example assumes the FTP site uses anonymous logon.  
                request.Credentials = new NetworkCredential(Constantes.FTPUser, Constantes.FTPUserPass);

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(fileContents, 0, fileContents.Length);
                requestStream.Close();

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                Console.WriteLine("Upload File Complete, status {0}", response.StatusDescription);
                //response.ResponseUri.AbsoluteUri;
                response.Close();

                return response;
            }
            catch (WebException e)
            {
                Console.WriteLine(e.Message.ToString());
                String status = ((FtpWebResponse)e.Response).StatusDescription;
                Console.WriteLine(status);
                throw e;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                throw ex;
                
            }
        }

        public static byte[] leerArchivo(String path)
        {
            WebClient request = new WebClient();
            request.Credentials = new NetworkCredential(Constantes.FTPUser, Constantes.FTPUserPass);
            try
            {
                return request.DownloadData(path);
            }
            catch (WebException e)
            {
                Console.WriteLine(e.Message.ToString());
                //String status = ((FtpWebResponse)e.Response).StatusDescription;
                //Console.WriteLine(status);
                throw e;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                throw ex;

            }
        }

        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}
