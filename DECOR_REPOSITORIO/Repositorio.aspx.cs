﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Newtonsoft.Json.Linq;

namespace DECOR_REPOSITORIO
{
    public partial class Repositorio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string Body = "";
                Directorios.crearDirectorioUbicaciones();

                if (Request.Params.Get("Accion") == "Ver")
                {
                    String fotoId = Request.Params.Get("FotoId");
                    byte[] imagen = Archivos.leerFotos(fotoId);
                    string base64 = Convert.ToBase64String(imagen);
                    Response.Buffer = true;
                    Response.AddHeader("Content-Type", "image/jpeg");
                    Response.AddHeader("Content-Length", imagen.Length.ToString());
                    Response.AddHeader("Content-Disposition", "inline; filename=" + fotoId + ".jpg");
                    Response.BinaryWrite(imagen);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                JObject jsonResponse = new JObject();
                Response.Buffer = false;
                //Response.ContentType = "application/json";
                jsonResponse.Add("Estado", "ex");
                jsonResponse.Add("Mensaje", ex.Message);
                //Response.ContentType = "application/json";
                Response.Write(jsonResponse.ToString());

            }
        }
    }
}
