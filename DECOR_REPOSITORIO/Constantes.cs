﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Configuration;

namespace DECOR_REPOSITORIO
{
    public class Constantes
    {
        public static String serverFTP = WebConfigurationManager.AppSettings["serverFTP"];
        public static String ftpPathUbicaciones = Constantes.serverFTP + WebConfigurationManager.AppSettings["FTPPath"];
        public static String FTPUser = WebConfigurationManager.AppSettings["FTPUser"];
        public static String FTPUserPass = WebConfigurationManager.AppSettings["FTPUserPass"];

        // SOAP WEBSERVICES
        public static String serverWebDistpacher = WebConfigurationManager.AppSettings["serverWebDistpacher"];
        public static String WebDistpacherUser = WebConfigurationManager.AppSettings["WebDistpacherUser"];
        public static String WebDistpacherPass = WebConfigurationManager.AppSettings["WebDistpacherPass"];
    }
}
