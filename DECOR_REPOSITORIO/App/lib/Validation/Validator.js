/*global sap */

sap.ui.define([
	'sap/ui/core/message/Message',
	'sap/ui/core/MessageType',
	"../../validaciones/RulesSAP"
], function (Message, MessageType, RulesSAP) {
	"use strict";

	/**
	 * @name        nl.qualiture.plunk.demo.utils.Validator
	 *
	 * @class       
	 * @classdesc   Validator class.<br/>
	 *
	 * @version     Oktober 2015
	 * @author      Robin van het Hof
	 */
	var Validator = function () {
		this._isValid = true;
		this._isValidationPerformed = false;
	};

	/**
	 * Returns true _only_ when the form validation has been performed, and no validation errors were found
	 * @memberof nl.qualiture.plunk.demo.utils.Validator
	 *
	 * @returns {boolean}
	 */
	Validator.prototype.isValid = function () {
		return this._isValidationPerformed && this._isValid;
	};

	/**
	 * Recursively validates the given oControl and any aggregations (i.e. child controls) it may have
	 * @memberof nl.qualiture.plunk.demo.utils.Validator
	 *
	 * @param {(sap.ui.core.Control|sap.ui.layout.form.FormContainer|sap.ui.layout.form.FormElement)} oControl - The control or element to be validated.
	 * @return {boolean} whether the oControl is valid or not.
	 */
	Validator.prototype.validate = function (oControl,model,oEvent, oMessageManager, oProcessor) {
		this._isValid = true;
		//oMessageManager.removeAllMessages();
		//sap.ui.getCore().getMessageManager().removeAllMessages();
		this.oMessageManager = oMessageManager;
		this.oProcessor = oProcessor;
		this._validate(oControl,model,oEvent);
		return this.isValid();
	};

	/**
	 * Recursively validates the given oControl and any aggregations (i.e. child controls) it may have
	 * @memberof nl.qualiture.plunk.demo.utils.Validator
	 *
	 * @param {(sap.ui.core.Control|sap.ui.layout.form.FormContainer|sap.ui.layout.form.FormElement)} oControl - The control or element to be validated.
	 */
	function addMessage(mensaje, type, oControl, objAValidateProperties, oMessageManager, oProcessor) {
		oMessageManager.addMessages(
			new Message({
				message: mensaje,
				type: type,
				controlIds: [],
				target: oControl.getId() + "/" + objAValidateProperties,
				processor: oProcessor,
				technical: false,
				persistent: false,
				validation: true
			})
		);
	}
	Validator.prototype._validate = function (oControl,model,oEvent) {

		var aPossibleAggregations = ["items", "content", "form", "formContainers", "formElements", "fields"],
			aControlAggregation = null,
			oControlBinding = null,
			aValidateProperties = ["value", "selectedKey", "text"], // yes, I want to validate Select and Text controls too
			isValidatedControl = false,
			oExternalValue, oInternalValue,
			i, j;

		// only validate controls and elements which have a 'visible' property
		if (oControl instanceof sap.ui.core.Control ||
			oControl instanceof sap.ui.layout.form.FormContainer ||
			oControl instanceof sap.ui.layout.form.FormElement) {

			// only check visible controls (invisible controls make no sense checking)
			if (oControl.getVisible()) {
				// check control for any properties worth validating 
				
				for (i = 0; i < aValidateProperties.length; i += 1) {
					if (oControl.getBinding(aValidateProperties[i])) {
						// check if a data type exists (which may have validation constraints)
						if (oControl.getBinding(aValidateProperties[i]).getType()) {
							// try validating the bound value
							///Inicio Rules
							var findRule;
							if(oEvent){
								findRule = RulesSAP.GeneralRule(oControl,model,oEvent);
							}
							var nameCampo;

							if (findRule) {
								nameCampo = findRule.name;
								addMessage(findRule.stateText, findRule.state, oControl, aValidateProperties[i], this.oMessageManager, this.oProcessor);
								return isValidatedControl = true;
							}
							///End Rules
							try {
								oControlBinding = oControl.getBinding(aValidateProperties[i]);
								oExternalValue = oControl.getProperty(aValidateProperties[i]);
								oInternalValue = oControlBinding.getType().parseValue(oExternalValue, oControlBinding.sInternalType);
								oControlBinding.getType().validateValue(oInternalValue);
								//

								if (oControl.getDateValue) {
									if (oControl.getDateValue() === null || oControl.getDateValue() === "") {
										addMessage("Falta Llenar Fecha", MessageType.Error, oControl, aValidateProperties[i], this.oMessageManager, this.oProcessor);
									} else {
										addMessage("Entrada Correcta", MessageType.Success, oControl, aValidateProperties[i], this.oMessageManager, this.oProcessor);
									}
								} else {
									addMessage("Entrada Correcta", MessageType.Success, oControl, aValidateProperties[i], this.oMessageManager, this.oProcessor);
								}
								//
							}
							// catch any validation errors
							catch (ex) {
								this._isValid = false;

								/*if ((nameCampo === "ConsignatarioTipoDocumento" || nameCampo === "ConsignatarioCodigo") && $.FlagMotivo) {
									//oControl.setRequired(false);
									addMessage("Entrada Correcta", MessageType.Success, oControl, aValidateProperties[i], this.oMessageManager, this.oProcessor);
								} else {*/
									//
									//oControl.setRequired(true);
									addMessage(ex.message, MessageType.Error, oControl, aValidateProperties[i], this.oMessageManager, this.oProcessor);
									//
								/*}*/

							}

							isValidatedControl = true;
						}
					}
				}

				// if the control could not be validated, it may have aggregations
				if (!isValidatedControl) {
					for (i = 0; i < aPossibleAggregations.length; i += 1) {
						aControlAggregation = oControl.getAggregation(aPossibleAggregations[i]);

						if (aControlAggregation) {
							// generally, aggregations are of type Array
							if (aControlAggregation instanceof Array) {
								for (j = 0; j < aControlAggregation.length; j += 1) {
									this._validate(aControlAggregation[j]);
								}
							}
							// ...however, with sap.ui.layout.form.Form, it is a single object *sigh*
							else {
								this._validate(aControlAggregation);
							}
						}
					}
				}
			}
		}
		this._isValidationPerformed = true;
	};

	return Validator;
});