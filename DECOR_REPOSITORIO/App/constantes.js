/* global moment:true */
sap.ui.define([

], function () {
    "use strict";
    return {
        URLSOAP: "../SOAP.aspx",
        URLFTPService: ".", //"http://localhost:51988",
        ClientSOAP: "300",
        usernameSOAP: "TTAPIA",
        passwordSOAP: "Seidor03$",
        
        
        ambiente: "DEV",
        ActivarLocal: false,
        ActivarMockData: false,
        idProyecto: "app.reporteStock.",
        PaginaHome: "appHome",
        IdApp: "ReporteStock",
        userApi: "/userapi",
        sTokenMockData: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzVXN1YXJpbyI6IkNaQVBBVEEiLCJpSWQiOjMxNCwic05vbWJyZSI6ImNpbnRoeWEiLCJzQXBlbGxpZG8iOiJ6YXBhdGEiLCJpYXQiOjE2MzE3NDE0MzQsImV4cCI6MTYzMTc3MDIzNH0.dOvErfa7gHsZ1CDb53NgqlJeLZoGW3cupQO-hZ05L5I",
        destApiGateway: jQuery.sap.getModulePath("app.reporteStock") + "/" + "dest-cajawebapigateway",
        destSecurity: "http://srvdesarr.decorcenter.corp:8080",
        services: {
            login: "../SOAP.aspx?Accion=Login"
        }
    };
});