
sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	'sap/m/Button',
	'sap/m/Dialog',
	'sap/m/Text',
	"sap/ui/layout/VerticalLayout",
	"sap/ui/core/routing/History",
	'sap/ui/model/Filter',
	"./utilController",
	"../dataPass",
	"../estructuras/enviarDatos"
], function (JSONModel, MessageBox, MessageToast, Button, Dialog, Text, VerticalLayout, History, Filter, utilController, dataPass,
	Estructura) {
	"use strict";
	var coordDocX = 5;
	var coordDocY = 65;
	var img = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPYAAADaCAYAAACVfL+FAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAADORSURBVHhe7Z0HfFvV2f9/V3vLlmXLe8ojiZ2dMEvfFigvlPZPGU0hBMpsaFklQAphE6AQApSZMgtNoVBm6FtGoYsMyJ62472nLMuyrD3+V86lZPheX1mWdCXO9/NRfI9sKVdX93fOc57zPM+hvF5vCAQCIamQSqXM0cSImJ8EAiFJmEzUYYiwCYQUhAibQEhBiLAJhBSECJtASEGIsAmEFIQIm0BIQYiwCYQUhAibQEgyfD4fc8QOETaBkIIQYRMISQaJPCMQvqUQYRMIKQgRNoGQghBhEwgpCBE2gZCCEGETCCkIETaBkIIQYRMIKQgRNoGQghBhEwgpCBE2gZCCEGETCEkEnzjxMETYBEIKQoRNIKQgZCcQQsSEAn74A0H67gnB6xzFmMsHXyCAkNeJYesovKEgfG4nRocH0Gdz0y/wwGGzYXh4GJq55+GCH85HjoR5M0JE8DXFibAJDCGEgkEExgULBP0++OnDkG8MNuswLcoRuAKAy9KOtv4RuEeH0NvTivp9DRhwhcVOi9tLv2ayu4lKx/xfv4BnLshjniBEAhE24RjCInWMeeChFRty9aNhTzMGPF4E6DtgrGMXvjxoQRB+uB02DHT3YWTCW4PuAEIUrX36J/NMpFB5Z+O+p1biNDJsRwwRdqoTCgsrRI+sbth6W9Bp9SHk98BpH0BbQzvoJjy2PvT29qBn0EHLNYSAn/6XEoMKeOGmBe0PjGLUEf+vX158Opav/DWWzE0PGweECCDCTjpC8LlGYbONwRfy0aauHbbhIVgdtEK9NvR29mFodAwe2hz2j1kxaPdCTA94PpcTDrsdA53tGPIkyVdJlWDZk09i+UIj8d5GCBF2IgnRBq3XDS8twvFR1DlGP3wI0nexd9QKGy1QJ33ZxXCgt7kTtoAEIY8dI7SQB/va0dTUjaEx9/gcNzWR4+S7XsNDZxaCGOORQYQ9jYRos/fQzyACvvCcVAQq6MGY00MfB+HzuDFq6UHfiDc8nMI6MAiLdQj9Xe1oa2tDx4AdHq+fnr+G4PfTaieg5LJ1ePnqOVAwbQI/iLCnA/sBfLG1DQNDNoyO9KK5tgm9NgfcfiAw1IE22+T1nQkTk7PkCbx243HQMG0CP4iwo8KDutfvxZo3NqHRLobc58IYuUrTStHlr+KNqyqI8ywC+Io6DBH2MQTR/fHjuOext7F/lHmKMP3I0lFcXYUcOYX00gWYU6SDSJmJ0vIcaCgKyjQj0nVKMgc/DCLsqBjCp3f+HHd/ZmHaBL6IJAqodJkoKDfDJPdhYP8W1Foj9ClINMgqLkdJjgHpeg1UmjRkZKRBq1JBl56B9DQddDo9dFoN1CoFZFIJlHIZ8+LUhgg7Kqz44tFLcOs7Q0z724tIlY2KwjRQYgW06TooRPTNpaJFpVFDRY+0fq8CObNmIZ+eKIuVehjS9dCo5eOjbDDgQ8sfb8CK97oPvdk0QollUGrpc1swD6UGJXTGXORlpUGtNSIn3wgVbXVJ1WnQqaQQiaWQySTjy2oUbQkkM0TYUeFF+6ercNndG+FinkkZJHqk6STjN4hUQt/sIjGkMhkk9F0v0WYhLzcH2SY9FJQS2ZU1KE4XIUSLWqM3IT9HG7FZbPvbjTjn/q/gYdpxRaJDTqYaUGRjxsJZKDIYkFNYioIsPdR6I7IzdVDKpBDTYk8WvRNhR4UbbZ+9gDVPvI6dAh60xRJxeLkclCgc3slAiSAWhRDwUdCVzENllggitRH5+QXIL8iEln6NPq8cxVn06KuQQyFTQiGPXYhIaNe9OO2XH8PJtBML3ZnRuqBE4WvEfGZKTM/lTcjJyUZObh59nfLpRw4ydFoY84qRn6EaF5NYIMInwo6KIGz738Dq257GJoFMs8dNT40KMlq4+qLZmF2eD5NRD5U6A3kl+Uinp5hyelQ1pasPmZ3030nCI3L4hgyPSIfeJu6Edt+H0675SCDC5gndEWaqaPFLZZDJ5ZDTFo1Mpkf+zLn43o/OxSll2oRdTyLsaBn4O+5bfhc+6mXaMYISy6Gl54eFxWUoKcxFTlY6dGpapQElTOXlMCkDCNKmsFpFj6wKGaS06SxTKiET06KhZ40iMf1g3kuQNL+Aiy5+Ga1MM7mRQZddiPlXP4aHzsxknosvRNjRYt+ENVfejHc7mTZfZBmomJEPFRUWbAY09Pcgkqmg1mih1ylA+SkYimegME2EgEgOjVaHtDQtFLRApUoNVHJxlKNBCMFAOL6NHnGEYD/2fICbL/otNiVkkh0bJDXXYv3zS1HEtONFJKIOQ4Q9Ee5tWLvserzdxbSPhpJBW1CDU06Zg/x0DeS0hjSmUhSG5670iKpWGel5mwb0wBpzgo5+dPdZYHPSo7u9E7X1XbB5ZMitmY/v0ueXxvxdQuj7CHctuw9/dzDtY5AhraASeWo33L5w8rcfHpcH/lAA1r5BeJm/EhLqWUtw+2M34vs65ok4QYQ9LbTgmZ8sxfo+pnkUxrMexvpVp0CfUDvYj45PHsbN9/8VnUHxuNNsPKQ9FESQ/qmdcTZ+s3YVvp9+6K8TwuCnuPuiu/Epi7BFc2/Be8+diyzaxnC76WE94ILNMoD+vn6M0E13zz58VdsPH1yw9vSil37e5jqUGXMofj9soQQQougvwuOJS0egm3MRbltzHf5HyzwRJ4iwp4QPjoFOtHX2YbC3A80N/8b77+zGEEt2lfGEX+Du2y/BQmNiZ7jOlr/grl8+hk0jzBOHozsVD7y9Gt+P8w14BK46vHntFXiiluUWyzwHT/5lJRbJmfYkfJ2MQwXG0NvWhzH6e7N1t6Br0AGHtQftrc1obOqBPVzpZXgAI25a9OOvmD7SvnML1j1yLjHFBYXfBfuIDUMDfejt6UZ3dxe6OnvQP2SF1TqM/tYWDPDp9rN+hEeevQXfyYvsYk83vr4PserSB/GFnXniCKTInHkKTpuTBUqqQYYpC0aDAQb6kZFlgsmUDlWsp+Hedny26hLcuZHlokoW4q6/PYUzo+18aMEHw6KnTXgvUxEm4HXB5fbCR/90jo3A0ttHT1HoazbSi44+G9yeATTtqUVLjxVjvvBr+VWEMZ56J55ffRZymHa8IMIOm2d+L9zOMYy5XBizhdMnW9HY3AXLiBUDvb2o37ULvW7mz6eC7GTc/9oDOK0oMaGM4WKC3uFGfLHhRTz90mb0R5C3TanyMLOqEJk5RSivnIU582pQWZQFjTQGKvd14z9rr8PKD9iWF2Zi5Scv4Zw4z1cDHhfc4fps9HV02YcwEjbvg/Q1te/Dhy+9gA/3sS/QZZx6B15Y/UMi7JgS7mXD+dB0rzxms6C/twddHW1obdqDLzftQduQA85wtYNppxLXvvgcls5SMu04EPLA2rofe/fX4sD+A6jduRU7u6chNk6Wiarjv4czzjoTpy6uRKZyGgUeHMbePz+EO5/6AgPMU0dSiRs3/AFLErN6NCGOPU/h4uWvo59pH036qffij6t/gAymHS9SW9i0qeW2W9Db2YbmxoM4eLCBHonrsL+2B2MRjFrRo8UZ972Oe043Mu1YEcBo515s/vR9vPX+RjRYnPAzv5l+xFDkLcK5V67AL/83f5o8+iE4tz+By69/C+0T3mXFWLruVVw7RyhJHEFYtv1u/HwHmWeOJuPHT+Ld2xYh3mecGsIOeuEYGsCAxYLOg7uwq66NnhN3oLWtHw5/EH4fPXfyBxCk51OJWRKJtbC96N/zCf747DpsqB1BIPxZmd/EHJEO5d/7GX6xfClOyo/+9nXteg6/vP411E/YIxXi/MdfworjhVJuwYeGN67FZU/uZb3emRf9CRuuK2VaiYGPyIUVuOTtwL9eX4ObrvwFrr35N1h113248+Hn8eaGT/Gf7fXotAxj2DaC0TF6jpQwUYdxoK1njDmeXtytH+PRKy7ClbesxTt7rYc6MOZ3cSFoR+Pnr+DB21fhD7ujT0iXysXhGRMLPjjH3NPuuZ46PowOj3Beb4lEAIE/PBCOsIND2PLSA1j91LvYUlePgw3NaOvqpS+1EAnBah2b5hvSg95NL+GOG+7FO7XdsIwmMlzLB2vjRqxf+yhe3zOhy503EmM6pBzCHunrw0hcey4uAvD5uCc77J2UsBCMsH31H+CP7+xFbMZBHojTUFA2A3MWnYTv/eAE1JRmQclxdTzuaXTKefux5YVbcdWtL2IT2+QuAYw1fYrf//ZxvFMXxbei1XHMR4fR1dEHe+wcB9OOSsNz0T3BCEbYowMD6Ii1qsVKqLV6pKWlw5CRCVPxPJx24RX41Yo7sPrh1bjv4cfwuycexYP3PoZnnnkQF1awZyAHpqs2sLMef7r1Etz88lbWgJhE4m37GE+sehbbpro8SInBbr0G0N1zaCuh5EALc56KORY2ghG2QqdDLC6ZWJWJkhnzcOLpF+CqFavwwOPr8PL6P+PtDRvw/hvP4v7rr8TF5/8Qp560AFV5afg6PVlKiz/bxO7UCVkOooM5njp2eqR+HH/ebo/vPDpC/L3/wCsvfTU1a0qcjlyOsFZ/fxeSp9irEhqhOPAnQTDClpsqsKB46ossIqkCGRWn4GeXX4Nf33YPHly7Dm98+Dk+//g9rH/xGTx63024/Cen4rhZxcjJ0HGa2YdQQiVj/yMx/atwhNPUCaBtw5N4+t29sMSs1LgI5lOvxIOPrsTlPyxDZAsmh2PDrjd+jw09TDMSRFqYsjgcTtYuDAqmVI1okmW+5Km2Ihhhiw1mVFdlT3pCoowaLDz5NJx9/lJc8asVWHX/Y1j32pt46623sP7Z+3DtFctwwY/PwPdOnINiowpyqRiiw6uM8EaPLB372fg9LniicctbPsbTT/0fWqb4HprCBTj7soux5CfHgz1wKwSq6GR896RzcNWq5/DCXRfi+9W5mNIsMdCFndunkKBOSSDnqtLip6+jYObYfth6h5ljFsL7KiUBghE2lNnIy5zEGNcuxGV3P4T77/oNbl1xLa68+HycfdoJmFNeiLzsTKSp5fRIOl0VQ0TQqdgvT8AxhAHrVJVtx5733sd21nRGLuQw/+A6PLjmQay8+kpceOZ3cXw+2zgTwrBLeuhLprSoPPN63PPAnbj2jOIpiNuFjh2b0RTpR6YUSDNw/G/eYdgFs+eYG5Y+DvOBykBOuFxNEiAcYUOBLK4bIEzAiIULM5CmVUdhVvJHxBEI4KVNyPZex9SWvGxf4o2P6iIv8kfJUbX0t3jinouwqFAHeiyEKccIo579ugU9ziPm79KsuTj/19fhxzUG/l++TEJ3mFIEbW3oi7TOEd2h5JlNTGMCQt3otgvFw+CDy83xjRpmosJIRuyIMRVMkubjakejlTmOA0pTLnsHMtRCzzz5ZQQdjWPLB/iiN9KJtRqLr38R6649HhmHmyTGYhSks3dAfqeTns0fhf5EXHfj6TgmRHu8YidFT11E4w95WgGqTzgDP730Bty55gk8dOtlOCHiyg0iKPRqDivKA4czadziCAWS41wFJWxxwULM4LKjQw709sTPhSpLz+GYv1pQ3zx8rGgmxYO2tokSqLmhKi7CdUvME5jQ9AXjuGbB0T5MVGxVOvNq3H3picijLUuxQofcmafg3EuuwS33rMEzr7yJD//+b/zjb2/ihcfuwa8vPx9nnDgb5jzDFGLI6U5CWogC1oHOj6BkeiLTY01GuWG840sGBCVsaPJRUcJlZLvQc7AtbkEsYtmh4vcTE4DVYo88Ms7ZiebeSN3ACsz+n8UomPCeSkcGV2J1wMNi8qswb/ka/PGdt/D222/htWdW46ZfLMNPfnAS5lbkw6AKVzmdjptYDkNhAUxqpjkBoz3JsDmDDBqdFsoYlmueToR1lqpsmGeW0EYnGxYc2NuIwTgN2rL0PJg4rpDP44t4/Tk40om6ut7IXqefi5MXF7BEcClhULOfZNDrxBirw0sEpbEA2Rl6qBWxqp8tQnpmFvQcPif3kGUKlk8sCG+PzBxOgDxcg1xPRuzIURSgqsjA6Riz1LbBEqcwaklGGapzmcYEOIaPdEzxwWfrRkN3hDNztQmFeWzzVAoqJbtdEXRaMTCQyLhz+gxlSihZhR3e4MAbw3TUSPByLL2Jock00fZRciAwu0KJguIs7s3Q++vRFSdbPBzwn1HJvjRk31kHG3PMF4+1F5ZIPW7eMYw62cc0mV7PHE2AdwT9lulOWIkQ+i4LsAomCKe1HwPRVLSZLgYPgt2FI4ZkfAeG5EBgwga0uRncS1n+etTHyTMu0piQpRWxj8r0MBSXr9reij27ellHNXW+mdXJ5245iOjys6aB9CoUsK7I0dfXMYgBR6S2TywI76DCHB5DAL7oQg3jiuCEDWMujJxOUi8Gu6YU2RE5iixkGZTsFyngQaQDjUyfGbk5523Gxx/8G60sgzYllXN0hr1o7HZEPGWYXsSQsgrGh46mQYGIhitkNASpRidAwUyM8M5TlYsizrXSEEa7uiL3Rk8JKZQKKfuo7BuFNUJlS9NyUZbFNCLAV/sJPtg4OKFAZXoTDMzxsXjQ32dP8ByWglTBYdv0tGFwTBjuMy6UBo4pj8AQnrDFJsyYwRVaGsJYf0d0VUZ5I6KFze6YQnAMwxHauSJtDipr8iPekhaBVrz76CPYMMF207KMIpjZlQ23y51gYcuRlsERVejvRrs1fvEJU0MEeZLkYocRnrApA6pO/h5KWb2oPgz3dEQe2jhFjFkcC7D0+BmIcKChtNlTjjcOWTbikWtuwoub+49YHhLJNSiew7YcRl8xbzA+vgBWZMgs5ggrhQsOR2I994dgdlOZEBHEyVI+hUaAUwYZ0nON0HAMacPb/oGdXfG5EWRqDlPcM4SO1onNY1ZEmajIV055zhsa3IJX7rgKP7/rdewcPLTc5gsqoZKEWN+z96s6RF+9LMaIEh99Fgj54Ga1BAMIJEnUWRgBCluMzII86Di/5wF0xcl0oxQ6sFYPFwXgHBmJ2IGWvWARoikAGnQNoumzdbj18qtwwz2P4YUPdqKh3cpubo+GYlLEgj8S6LMyOcJRh7DjYKQLh9OND/2NrRhivYh5KEwjwo6OrCJkcAp7DO0dkcdbTwWKnhPnsn2f9g70OKewP1TxqTjnpCnMsw+HHl3GLC3Y/slfsP6l5/BhA8fcxLYBazb8B1/trsPBplZ0Wexwef2IXz6DCOrsPHCl+Izao0lunw48sPe3oJVtwUVmgoEzwEJYCFPYoly6d2SOWRjq7ImPZ1xhgIl1yLajqWsk8jLIEjPO/9kp7B3GdBPqxd8fWokbr7kcP7/kYiy94ib89qlwiai38bd/bsXBnpFxoYd36YwNFGTaLM7wXFHQG3kHOZ2E6A7aZwXrQqpYjljsghQrhClsyJFbzl1EPmDrZtk2ZpoRycLpyKwM7m+AdQojn3T2T3HewjzuKLtYEArCO3AAn779J7z83Frcf/sN+Pl5Z+HM867AykdexodbGjHIbFU7fVCQyNJRVszuVQ45hhAnf+iUSSJdC0PYrq792L1tCzZt2oRNmzdjyz/ew6ZJCoF5hzrQ0h+HtU9tAWYVcn2lAUxtezATzrvzGpyeLxfADROEx9KAje+/iIduuRrLLroCtz/zAbZ3OadtFFXnlSFHw/5Jg67hiMNz40u4c0oeaSdY2B7sfeV+rLhxJe56cC3uW3kzbl6xAjeteg5/3cWd2hgc7UJDexx8vZQCeh17XFdozDXlNWJx5qm49o7l+EFpHDf34yREW6RujPTV45/rH8GtVyzBBbf/CbumIetGpNFCz1XiMzS1ohXxQlS+EKWJ3Gs8QhIr7OBB/N/bf8OubisG+7rHNyzni7drHxqmWpooEkRiSDJN7KNqXzusUUR/6Ob8DLfcfQuWfYcrXTURBOGyW9D9z6exYsXdeGbDbvRH49+iryNXWmggHC8er0T7iaDEnJaTVJ/GmgwkRBIrbO8g+qacoWDDgYODsc/jlWhhKi09tozQ17iHMBrlSagrzsTyu9fgkRXnYE6eAWrOLJj442r4N9avvQd3PvA8Ng9OtStVj6+1sxFyWdHXm8hZthfDPRbWgUKnGTcqkoYEj9ihqOaXQwdaYl9NhdKjkDOVNAjvNLjnReo8zD9/JZ5+5iGsWPoDzM5VQy4VhAvkEN5+7Pv0Fdx20yP4pHsqzjU9MjXsn8czaofD5UucOR60ormWpZILZUJZNm25JT6GhjeJvXMoP6LaAsvVjY44BEFTnNvUuNHfM30nITHNxpm/uBe///N6PHn7z3F6pT7RjpAj8Da9j3t+9Si2RWxpiaFSsS8vBHqaMEpNV+noKeD3sVt/oSDECi046lkIjsTeM7ISLDQzx1PBP4ohe+z7eKVBx54WSYkQcMcguEKajdn/exXu+8MGfPj6s3jgpktw1nFm6BJ25x/G4Od44olP0Mc0+UEh6OFywnVh696B2E+tpgKlgqmkBAYhXHueJHjElkOnj6IbdPajrTNcBDi2UIY01gSL8Cb93pjuKieDoWQevn/BNbjj0efx+uvr8MjdN2P5JefjrFPMSEuEeRi0o+U/b+PtjYMRXfv00hKOGy6IsVFn4rLQZBqoNBkTWwyhUYwFhGQ3TQ7l9XoT6BLox2f3LcOdH0112UqOOcufwmOX1sQ2Ftr5Ea457T7snvBKmXDabb/DbT8uim88dsgPz9gILH0DsPtFaP3zbVj9SW9c56iS0mX4/Wu/xEyenYv1H6uxdNX/sa5XF1z4NF69fgF7bH6MGWvdic1f7kVfSAnZ15+J7ridjix8d9kZKBVISKmUYyOLr0lwN6RGjrmIIzlgMjyo3d0c+9I/tGXBPj0cRntDM4biHeoc3hOLHmHyzDMwo6oSZy3/EfKYX02MAsayhZgzqxKl+VlI1yggifLb97d8gr/ujcBzKBZz3nA2yxhrhlo8UJfMx+kX/hzLLlqCJUuYx4XLcNlVwhE1XxIsbBHkhlmoiaL0o69vMPaecakJ5fnM8TF40d5Ld+wJtHvGCRpRxnHzyY6/Ba+ufwrrXvwDXvvD83ji3uvws+9VIj0qU96Blh37wTcdR6FN41yrH23ujjhTjjAxCRa2DGqtHKJouuneg+iI9TasFH2eHEs1focdCd9XTq6BjiOCwmvtQT8zgRWrTag48Vz8avUf8O5bT+KGs6qQNqU7wY3W/7yHnTxNJmlWFcxCCbJLcRIsbAlyCvMR1QaGATt6B5njWCFSQKtld/IFuxqQ0NiKMMpslOdxuG07mzE4gWdKkbsIP7tlNX5zfsUUIquCsDfuxPY2fj0zJZYjt4TDrPANR16amTAhCRY2jUYDZTTmYMiBno4Ypw9Qapi48kidwxhNdGUfsQqGQtb4OMBjh4tNNIo8fPey5bhggXEKN8QYejsn2VOaQazUISvXyOFTcWJY6CleSULiha1OA8c21JMTGkNfczOsMe3p5cgoLGIf0Xx2jPkSPNRINcgoKUMG0zwWP9xcnU/aCVh+3Y9RGvF3IYbPNsTL6UXpw3XaJeye+6B/WqL4CEIQtqwYudFE1wdH0dPUCEtMbwgxJKoilLGliAc60DmSYGGLDDCXmTjMaQ8GJtm6V1x5Ia4+eyYi255KDCrg5RdYQqUhI42jTrvbCbefKHs6SLyw6VsiulrxLrTu74EtppENcpjMBdCz2pBuOKa9OEGk0GoUSdjLLYlECHgmE40GJ1x6NqoiXNqRKJTRlXn6Gt8A6vf1Rl6RhnAMCQ5QCWPHxzeejXu/iqanLsTyd9/EpTlMMxb0f4J7fnEPPuln2kegwOlPf477FiS4n2x9GcuWvoCmib5RqgQ/e/JF3LBwsjCaDjx/3hK80sM0J0WHE29ah0cvKOEV5z3w7i9x3ppdLBFmKpx0x/O4/4dlEQepuPoPYMu/tmDPwU6MhMQQ4jZbAZ8bgfQKLF68CPNrZiF/ivsP8AlQEYCwXfjq4SW48X1217ZETI82gSBHVJUWZz39Ee5cENWiLDcWWrhX3oGPJhS2HMff9T4eP3OSQm28CSHo88DpsGNkdAxulwtjzkxUz8vkNrGG3sblP1qLugkvVDZOW7kWK88ppcdlLmx486Iz8UQr05wUepS/5RU8di7rQv8RjH12E866cwvLqExhxk0v46kLqiLITfeh6/Nn8MAr/0JLW39EOf2JQpJmQl72Yiy762b8sCTyJaEkiDwLI4bWyCWIEpx34aW46txq1o3nwssuI90W5jhG6NKhZB0FKLgH+qaQwBBCwOuGy+mAfdiCvtb9+Opfn+Kv7/4JLz6zFrf98hIsvfgSXHL5L3DtjbfhldpJXO/KNKSxfucWtDbTo9mkhpENwxFFiQTGixDwRcJVQI6+HkM9wxGZ4n0f3oFr7ngTu5uTQ9Rh/LZ+tNd/iNXXrsIHvC2jyBCEsFXp6RxzNBus4mxUzy2GnvVsgxizDCCmiV6yQhiZw2OhR9ggl0VxJJ6B/fjkr+/hrdeewgO3rcCN11+HKy/+KS645BrcfOf9ePjx3+O1t/+G7W0j8Pj89Hv74fPW4Z21v8M2Lm3LwmvZzPEx+NFnDU7qvfZ2bEVtRKuHEhjyuW2Aw5HIZZw3nb2tFw6eFzLQ9xnWvfAfustKUqwb8dprm6ZUDHMyBCBsEfSlM8G1AYw4vQCllSUc0VFeWNoa0RPjCDRrePteSjpuCn3zCJfU8aCLNgP5/veejWtwzwNr8Lvn/4K/b9uH+qY2DIwFIZZIIJPJxh9yuQJKhZz+ST9kUno6EsJw7Xt4/PUO5l0mQGKEqaAc6RIR/V6HneN4MrkMWYpR0NNPDgbxz1c+QiPdeVCiw14/4UMCsViJTPNcLOAOUj8Ccek8zCoxQC6WQHL4+9HTLUpiQLFGA46ah0fQ88XnqIt5okAskUBkb0N7DFZUBDDHBkb2voHVq57ERpauN++Kv+DtK3149pyL8McJ57g0ssVY8erjOL84ln1VAD6P96gtXymIxGJagJHtlR30e+i5sw9B+kXU11vHsBYaCCFE/6FYJofiv2lH7AS9Lnjoc/ymlA/9viIZ3VHw80EE3PS8nv7/OD8Pfa4iupOTKyQRfe5DBOFze+E/2sqRKKCS8f3+mvDs+cvwxwk2KUwepMj/n6vxm1uWYkEEyd7hjnAyBDBiA6qsAhRksq+xON3h0zSisoo9/ALeNrRFU1WQF2JI5UqoVKrDHkooIhR1GJFEDpWWHp3oEUqtVh96HPG+hz/o32lUvEQdRiRTQqk8/PV0m6eow4gVXOfCPJT0556SqMOIIFXQFsnR78lb1HTX0L4Ne2MdShxzgpDos5GtndpV5EIQwpZmmJChZj+VoGOI7tkpZC88DuWsqzVWdPTFOhuEIAzGsOezrehOgVgWnXk28iYfgCNGEMKGVAW1nCPJYrgTFtpsTaupRgGrn8aPntYB3g4sQhITaMI/Pt6VAgkjchRVZzHH04swhA0FFFy9lt8Nd0AKfV4JctTsZovP2h37oguExDOwG9vjtI1yTFGfhOOymeNpRiDCNiCfIyHY73LQxpeSno9Wwaxm76Z91g60s+6qRkgNQhjc05gCHbgIed85jj3/IEoEImwKEo7wjtDYADqt4d8rUD6XPW7UZ+9DZ38K9OQEdoKd+HJ7C/uumElDGsqqZ8A0LUH2xyKI5a4wna9ejJ+ua2ZaR6FagF89tQYXz1TCtfUxXHLDX9DF/OoIJGYs+e2TuPGkSGoteTHcuh+ttgTWtCbwgO785UrYt76Kp1/+J9qS3nGWhyW/fwM3zo7cc8ZnuUswwrb9+0Es/c2HCMeAHEs6fvK7t3DrYtpuGfoSz6y8HesPTOwBL7/6T3j1slKeIvVj3zPX4Nfr98e+bhqBcDiiubj9b8/hR1NIBEmadewwlEjEIcZh7N7dechYT5+JmXnsa97NO5m/40U9Xn+diJqQAMzHoZo9+SFqBCNseUYeMrmG2aDvUKqfSIeaEo7KDK5BuhvgR6h7PyzE/ibEHT0WnFSNjBjee4IRtlSfhxnFHKkgg/b/5vDqZ85nz/Ry96OHV3aSF517GzBMFr4J8UaRh8pZBTHdNlkwwhYp05FXmMV6QvYeK772l0iLjsMJLKlWofC2P908KuKFBrH3iy2Y0saRBEI0SI0oqzSBf5Bv5Ahnjp2WBaOOPeY6MOL474iNjFn4zsklE6Z6hvpqsbdxaPJ5tqcTe3fHuLopgTARuiKUs+cATwuCETZEJuSb1OwnFDosE0iSi5pKljK2oW7sqO+ZPIVytANNRNeEBKCdUT3JdkzRIxxhTwY9wtb/N9yIQlZ1LqspYznYhcmMcWf9AXST+TUh3khzUF2Rw1FNdnoQkLAlMBk5StiFAvAfnpWZNQ9zWcpFBTo6wb1/pxON9KhOYtQI8YZKL8P8OdkxnV+HEdSIraHn2Kz4nLA7DvN0qStw4gKWQFvPILq4huzReuw4OPjNnJ1AiBOidDPmVmmZVuwQTORZmMD2e3HGdR9PHDCStghX3n8HLl2YxTjN3Gh5/378+uF/YGC8fRjqE/Gb9Wvx/1gyZ0Ktf8K1y5/msZkchbTS43HqgkJIJVwBNIRvH+HKOfS4GPBjcPcn+LxumFdglObUh7Fh9SlR7QGeVCGlYYINz2PZpa+ghWkfgaIc/++Wu3D9Web/bjAfbHkZP7/4BTQe/Qk0p+KRDavxHZar5950N35886eTmOs0ihOw6q3HcDbHllgEAuwf4Mr//S0O8FBS+a/exWsXR1cAP6lCSsNQqixks1nj7ha0++hekmmGERXPgIE5PgJDCYo4usTufbWTi5r+n3JOPAGlsYwiIKQEjm2b0cJreNSjvCj2ZngYYQk7XKmSOT6WAA5ubcARK1SiIhw38+hXyFE6t2piwYcJ9qOhm08JJSkMRRXIi8ZmInwL8KGnc4iXv0aUfzIWl062E8v0IChhQ1uAGRwVJbwjo3Af0TMaUHNyzZFLB2ozFiwq+a+5fjTBgQY08crZFiOjtDTCDeoI3zrcbTjQwm9+nVm9GLNN8ZGcsIQtNaCggnWsRcDhgOeIEFAFihafjDmHWzfaYsyZm8v6wZwdu7Ctjk+afgZKS+NjNhGSl2DfHmza1jPpRgxhZ5veXI2cGBVWOBphCZsSQ5FnZt9xY6APo0ddQW15NQoPmweL0ktRxRGuZzmwFY187KbMeajh2sWAQKBxtezATl4RjFLkmON3QwlL2LI05GRw7OE60gvb0TaPtALmw5PVaXOe/fKN4sBWPrvNaVG5qAq5EW4nS/j24Wip47cDDGXGrDiZ4WGEJWxoYMxK4wi3oxBeOjwSGQpmfr0eRWHG4uIJk0PGcbeifXJ3OH1VtCieOwfZsQ4PIiQ5DtS18atlr59/PKqz4uewEZiwAaVayRFu50TPMVv8SFEwbxEKwp9EOQcn1bDb4c7OJnTz2fFNpEe+uZCewRMI7ISGalHby8cRq0HZguMQJ4f4OIITtsKop6XKhgvWwWN7yPQZJ2KxWQ5dNf2zhG19KoCBA19hRz8PYStyYC6Kk5eDkLSMNm7Bxno+wlYg21yKKe5zPyUEJ2ykcwg75MXY6Nh/Cy58jSSvGuVGCeQFNahkdWQ70bR5K0aYFieZZSiJY+9KSEZC6N21Ca28CnWkobAoRgXEWRCesOlTErFNRUIOdNbWYfBorzaVhcpCen4+q4p9fg0Ldu/kt6O7tqIkrr0rIRlxoXF7J3M8CWlVmBVdFGnECE/Y8lxUsk6TbWik59jfbA/7NRQqz7sMSxezz4pDlv3o5hNFIC7Cd483g6xgEzgJdYFXACNkKD5+LnLjPLMTnrDFKqRxlGUdbW7B0ATF4qnsRZjDGtsSQN/+BgzxMZvSzJg3N/b5soTkJthVh1Y+87pwtZ/Fs2JakXQiBChs2qQ2cmSvWHth8zLHh0NfQCPrpxlB8/av0DTR645GlYtS0+TZM4RvMz607djHnfP/NVIjSqsKY14x5WiEJ2xKA1MehyHstGIk0tInQQsO7urktcWuJLsYuSQ+nMCJAy07dqONj8tGmYuyvPjLTIDCliE9N4s9y8vdCwu/mIBvcLdjXwdzzAmFsgVVZH5NmIQhNDd080r8oHJnwMxRGChWCE/Y9ClRqmIUsV6MfvSO8hl7v8HfeABdPOLDKeNinDzLyN6pEAhhbHXY28McT0L+/NKYbgzAhgCFrUbhrHJksgo7CIeV37LVIQLoa+ubtGppGHlWGWYWkMoKBG4cza2w8XHESkqxqCqbI+AqdghQ2BRUWi2UrG7pEFzDERQE93ViX103T2GXo9JE/OEELuxoOdgFXpHJOdVYMMOQEAtQgMKmpa3mEnYQbvs3+3hNykgDtnzVzKvUsLbQjAzmmECYEP8AarfvxQCf2aDOjOrsxIQmC1LY0GZBxylsG0Z4KjvYfwB7+vh8CzLkl5IEbMIkOFuwYyevwGRIciuRxRzHG2EKm1JDwWq/hODsa0EHTwfawP5aWJhjTqgyzI9n+g0hKQm27cIBXsutchx/Qqw38mFHmMKmR08da/SZDx2Nw7zWpIExdPXYef2tctYiVBjJ/JrAzXDXIL8dZDJPwgnliVs4FaiwFTBxeae796PZymMV0d6I2g4HD2FLkFc9H+Uk84PARXAQDU2Dx2QXToTeXIOZOQlYwGYQqLCl0GZwpLkF+tEzPPnl9XZux8bdVqbFhQgZFTNgEOjVIAiEkYPYvLmRl7AVOWYUJXDlVKC3sghyNVf9Eg+c7sm9Z7b67TjAa8lbhQIzR+YJgUAT6N6JTe18JnYUDMXFcY8PPxyBClsOU0UFa21wwIrm3skU60XTrgYeZWFp5DNRlc4cEwgsjNVvRS9zzE0hFlUn9oYS7Igt0xRhZhrTPIYQRkYnMYjo+VD3MJ9oXhGyTzgOlaz/F4EQJojOSQeTQyjKZ8OckVhHrECFTUFlzIEpjX1x3+/ijiXz9dajeYiPsA2oXDgfeaTEGYGLQAfq2vlsNCFDfvVCVLHvexEXBOsuUmWZYFCyn15g1Ar2/jOEoQNfYHM7D2FTGuSXF0W1rSkh9fG27sABnnu+ZVbUoCDBA4VghQ2VDlo5++kFx6xg397ai7atX2GQaXEiMaIonxRWIHAz0rgdezr5VOpQo6A88RGMwhU2JZlgc4DDCAY48mFtqOW37wqQPQPlxHFG4IS2AJsb0MtnZqesQk1h4mUlYGEXoYKjUJRnqANtFpalB087+vksNtIGeGlVadzrURGSjSEc2NvHHHMhRmZ1hSC2hhKusEGxlyGmEQWcsNkmnvN4Wur41aNS5KF6cSWIQ5zAyWgT2kZ4rF9TaSibNxd5ApjZCVjY9Gwlk91Gtre0Ysg1UZCKEw3b69DHZzokN8E8syghifCE5MFGm+E9YzyELTLAPHeWIGrSC3rE1uQWsIvO2YSOIfcEceCD2P/lbvTwSeuUZ6OiSNB9GyHh+NC3fwf2DzFNLiSZKDcLY8c3Qd/VUpUO7GH0drR22o51oHm7sL+eX74sVEUoJQldBE5G0bR9F8cKzGHIMlEokEqYgha2TKONfMfLgRZ08axiWnhyJdlRk8CNvwW7anl5YqFatBDZzHGiEbSwFdlVKGIdUUOo396Oo6fSQw1tsPOJ05eUYn6ViWOvLwKBxtqFQT7+GmThxOPLBVO6WtDCFiu0KMhhd437Ha4jTfFgL/bubaeNp8mRFMzGwsrEFJojJAsBDBxsBq/IZGM1jpuXI5itoQQtbInWhJKyTKZ1LFTIhSOsbkcTvtxShzGmyYUkswKzSMQZgRMb6jZ/hVY+jlhdKWryE1dY4WgELWxKX4iSLI5ZsGcIfYdPfyz12NPBK1ETytyyhBWaIyQJvk5s38xvayhxlhm5zLEQELSww0telFjCbi4HPXAdNv9xdTTwzJcVo6giV+gfnpBoRg/gywHmeBKKZ+UKyl8j8HtbCYOO3VwOuYcxaP3aTvKgvcnCET/+DZRhHhYmsNAcITnwd/Yd45ydEPVsnDg7W1D+GsGP2IY0jnmwbxQDfbZDVVIczdjbwhxzQiG9LLGF5gjJQAAd9Z28dpBRlc3HcWaOGn0JQPDWqEKnZvc0DjWgsXt0XMwhaz227erjMR+ioC2YgRkk84PAhbsJOw708io1LDeZUWYQ1v0keGGLtZkca4NW1HeNjos5OLAXu3llalJIKyGJH4RJsNLz6z1dvCqS6vKLBXc/CV/YhmJwpbfauizjveronq/Ap3BNWNj6QiNzTCBMjL9nP/YN8Flh0aKiOp85Fg6CF/ZkeIb2YXvvML6s5+XmoHuKGiwSQCI8QdgMtXTwioeQFi7GPAGtX3+N8EdsfT5KOPLgQnWb8O+t+7C/h5+w1fNO5Kh+SiCEsaOxlY8jVowsczXKBeivEf7QpchAYRnHZnmBTny85h6808InPEiLGQvmIC+RldwJgic0uB/7O/hM7ETQFc9CeQJ3/GBD+MKWKGEoLeNOXg/wTOeCHFllJdAThziBA1f7Tuw4yCf1V46cKnNCd/xgQ/jCluugV4l5mEV8UKOgTFjrjQThYW+rRR2fCTZyUVMlzMLVwhe2KBMlhcbp6RWzFmI2cYgTOPGiaVstrwhGmOYh3nEpUim/xCXhC5tGLBEjeutZhNyaSmSThC4CF4EutFj4+GtEKFg0E7kC9dckhbAN+enRB9hLCzFnYRUEFiBEEBjeniZ08qnUQZlQvWAWMgWqoKQQtsiYFn0Cu9QIc3UxKYVE4CAEa8MO7O7mF5hSWpMn2Aq3SSFsBKnoR2xFFoqFUPCZIGC86Ni2EV28ErBNqBBSAvZRJIewaVFGO5ehTJUoJzvvETixY+8mK3PMjWT+8cgX8LQuOYQtTUNOlNFi+rICkIUuAifOg+ji4zejzfDZs0sEsTEAG8khbIkG6VHVRTBg/txCMr8mcOJpbcYgn3QuTTnmLygGRzxkwkkOYYu1yM6eui1OZS/ACbOM07BkRkhdPGiro4XNKwE7FzMq0gV9PyWHsEVa5JpzmEbkhPw+eJwj4+mdoaAffj95kMc3j0DYWTZ6AF/8awc/UzxcZFPIwzUN5fV6+fgAE0vIht1/XovfrvsM7TyzM4+Fglgmh0wi5tzFk/DtI+jzwOPzI8hTCTlL1uHNG+ckZKmLb+RZcggbfgxt/T1uuXU96viYSgRCrJAW4YyVj+PuH+bE3RTnK+owyWGKQwJ9uh7KJDlbQuoiza/BopkZgvfXJI1UJDodlGRnTEKCkWZWYHax8CvcJs8YqEsHiS8hJBplbjlyk8BHkzzCVhqgIk4vQkIRwVSWL5iN97hIHmGHAvDxSpIlEGIDlVaN+ebk2EEmeYRNZaCyPHlOl5B66M0LsKBQoAnYR5FESlHAmEuivQmJQoT0srmoMTBNgZNEwpZDrdExxwRC/NGZqyDAgqQTkiQBKocY/PcLeGzdX7DboYZKRsxyQrzwYGTQhpNe3oh7zcxTCSCSAJWkEjaB8G0mBSPPCARCJBBhEwgpCBE2gZCCEGETCCkIETaBkIIQYRMIKQgRNoGQJPh8fCotHoIIm0BIQYiwCYQkgu+oTYRNIKQgRNgEQgpChE0gpCBE2ARCCkKETSCkIETYBEIKQoRNIKQgRNgEQgpChE0gpCBE2ARCCkKETSCkIETYBEIKQoRNIKQgRNgEQgpChE0gpCBE2ARCygH8f2gVHWolShXGAAAAAElFTkSuQmCC";
	return {
		loadingPage: function (self, valor) {
			self.getView().getModel("ui").setProperty("/loadingPage", valor);
			self.getView().getModel("ui").refresh();
		},
		validarEmail: function (email) {
			var rexMail = /^\w+[\w-+\.]*\@\w+([-\.]\w+)*\.[a-zA-Z]{2,}$/; ///^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
			var indicador = "";
			if (!email.match(rexMail)) {
				indicador = false;
			} else {
				indicador = true;
			}
			return indicador;
		},
		onFiltrarTablaLive: function (self, idInput, idTabla, fields) {
			var oView = self.getView();
			var Input = oView.byId(idInput);
			var oTable = oView.byId(idTabla);
			var valor = Input.getValue();
			this.filterSearchBoxList(oTable, valor, fields);
		},
		filterSearchBoxList: function (tablaLista, valor, params) {
			var oBinding = tablaLista.mBindingInfos.items.binding; //tablaLista.getBinding();
			// apply filters to binding
			var aFilters = [];
			params.forEach(function (item) {
				aFilters.push(new Filter(item.key, item.query, valor));
			});
			var filter = new Filter({
				filters: aFilters,
				and: false
			});
			oBinding.filter(filter);
		},
		filterSearchBoxList2: function (tablaLista, valor, params) {
			var oBinding = tablaLista.getBinding();
			// apply filters to binding
			var aFilters = [];
			params.forEach(function (item) {
				aFilters.push(new Filter(item.key, item.query, valor));
			});
			var filter = new Filter({
				filters: aFilters,
				and: false
			});
			oBinding.filter(filter);
		},
		FiltrarCampos2: function (self, modelo, tabla, arrayFiltros, nuevoModel, resolve, reject) {
			var oDataModel = self.getView().getModel(modelo);
			//new Filter("CodigoTabla", "EQ", "tipo_usuario");
			var onSuccess = function (oDataModel) {
				var results = oDataModel.results;
				var filtrado = new JSONModel(results);
				self.getView().setModel(filtrado, nuevoModel);
				self.getView().getModel(nuevoModel);
				self.getView().getModel(nuevoModel).refresh();
				resolve(results);
				sap.ui.core.BusyIndicator.hide();

			};
			//Handler en caso de error
			var onError = function (error) {
				jQuery.sap.log.info("--cargaTablaClasePedido error--", error);
				reject();
				sap.ui.core.BusyIndicator.hide();
			};
			var reqHeaders = {
				filters: arrayFiltros,
				context: this, // mention the context you want
				success: onSuccess, // success call back method
				error: onError, // error call back method
				async: false // flage for async true
			};

			oDataModel.read("/" + tabla, reqHeaders);
		},
		FiltrarCampos: function (self, modelo, tabla, arrayFiltros, nuevoModel, nuevoTabla) {
			var oDataModel = self.getView().getModel(modelo);
			//new Filter("CodigoTabla", "EQ", "tipo_usuario");
			var onSuccess = function (oDataModel) {
				var results = oDataModel.results;
				self.getView().getModel(nuevoModel).setProperty("/" + nuevoTabla, results);
				self.getView().getModel(nuevoModel).refresh();
				sap.ui.core.BusyIndicator.hide();
			};
			//Handler en caso de error
			var onError = function (error) {
				jQuery.sap.log.info("--cargaTablaClasePedido error--", error);
				sap.ui.core.BusyIndicator.hide();
			};
			var reqHeaders = {
				filters: arrayFiltros,
				context: this, // mention the context you want
				success: onSuccess, // success call back method
				error: onError, // error call back method
				async: false // flage for async true
			};
			oDataModel.read("/" + tabla, reqHeaders);
		},
		onMensajeGeneral: function (self, mensaje) {
			var dialog = new sap.m.Dialog({
				title: 'Mensaje',
				type: 'Message',
				content: new sap.m.Text({
					text: mensaje
				}),
				beginButton: new sap.m.Button({
					text: 'OK',
					press: function () {
						dialog.close();
					}.bind(self)
				}),
				afterClose: function () {
					dialog.destroy();
				}
			});

			dialog.open();
		},
		onMessageErrorDialogPress: function (idTransaccion, message) {
			var mensaje = message ? message : 'Ocurrió un error en el servicio';
			var that = this;
			var dialog = new sap.m.Dialog({
				//id: 'dglExitoso',
				title: 'Error',
				type: 'Message',
				state: 'Error',
				contentWidth: '25rem',
				content: new sap.ui.layout.VerticalLayout({
					//id: 'idVertical',
					content: [new sap.m.Text({
							text: mensaje + '\n' + '\n',
							textAlign: 'Center'
							//id: 'txtMensaje'
						}),
						new sap.m.Input({
							value: idTransaccion,
							enabled: false,
							//id: 'inputTranscaccion',
							class: 'tamaniotexto'
						})
					]
				}),
				beginButton: new sap.m.Button({
					icon: 'sap-icon://copy',
					//id: 'btnidTransaccion',
					tooltip: 'Copiar código de transaccion al portapapeles',
					press: function () {
						that.copyToClipboard(idTransaccion);
					}
				}),
				endButton: new sap.m.Button({
					text: 'OK',
					press: function () {
						dialog.destroy();
					}
				}),
				afterClose: function () {
					dialog.destroy();
				}
			});
			dialog.open();
		},

		onMessageWarningDialogPress: function (idTransaccion, mensaje) {
			var that = this;
			var dialog = new Dialog({
				//id: 'dglExitoso',
				title: 'Alerta',
				type: 'Message',
				state: 'Warning',
				contentWidth: '25rem',
				content: new sap.ui.layout.VerticalLayout({
					//id: 'idVertical',
					content: [new sap.m.Text({
							text: mensaje + '\n' + '\n',
							textAlign: 'Center'
							//id: 'txtMensaje'
						}),
						new sap.m.Input({
							value: idTransaccion,
							enabled: false,
							//id: 'inputTranscaccion',
							class: 'tamaniotexto'
						})
					]
				}),
				beginButton: new sap.m.Button({
					visible: false,
					icon: 'sap-icon://copy',
					//id: 'btnidTransaccion',
					tooltip: 'Copiar código de transaccion al portapapeles',
					press: function () {
						that.copyToClipboard(idTransaccion);
					}
				}),
				endButton: new sap.m.Button({
					text: 'OK',
					press: function () {
						dialog.destroy();
					}
				}),
				afterClose: function () {
					dialog.destroy();
				}
			});

			dialog.open();
		},

		onMessageWarningDialogPressExit: function (idTransaccion, mensaje) {
			var that = this;
			var dialog = new sap.m.Dialog({
				//id: 'dglExitoso',
				title: 'Alerta',
				type: 'Message',
				state: 'Warning',
				contentWidth: '25rem',
				content: new sap.ui.layout.VerticalLayout({
					//id: 'idVertical',
					content: [new sap.m.Text({
							text: mensaje + '\n' + '\n',
							textAlign: 'Center'
							//id: 'txtMensaje'
						}),
						new sap.m.Input({
							value: idTransaccion,
							enabled: false,
							//id: 'inputTranscaccion',
							class: 'tamaniotexto'
						})
					]
				}),
				beginButton: new sap.m.Button({
					visible: false,
					icon: 'sap-icon://copy',
					//id: 'btnidTransaccion',
					tooltip: 'Copiar código de transaccion al portapapeles',
					press: function () {
						that.copyToClipboard(idTransaccion);
					}
				}),
				endButton: new sap.m.Button({
					text: 'Salir',
					press: function () {
						var aplicacion = "#";
						var accion = "";
						that.regresarAlLaunchpad(aplicacion, accion);
						dialog.destroy();
					}
				}),
				afterClose: function () {
					dialog.destroy();
				}
			});

			dialog.open();
		},
		onMessageWarningDialogPress2: function (idTransaccion, mensaje) {
			var that = this;
			var dialog = new sap.m.Dialog({
				//id: 'dglExitoso',
				title: 'Alerta',
				type: 'Message',
				state: 'Warning',
				contentWidth: '25rem',
				content: new sap.ui.layout.VerticalLayout({
					//id: 'idVertical',
					content: [new Text({
						text: mensaje + '\n' + '\n',
						textAlign: 'Center'
						//id: 'txtMensaje'
					})]
				}),
				beginButton: new sap.m.Button({
					visible: false,
					icon: 'sap-icon://copy',
					//id: 'btnidTransaccion',
					tooltip: 'Copiar código de transaccion al portapapeles',
					press: function () {
						that.copyToClipboard(idTransaccion);
					}
				}),
				endButton: new sap.m.Button({
					text: 'OK',
					press: function () {
						dialog.destroy();
					}
				}),
				afterClose: function () {
					dialog.destroy();
				}
			});

			dialog.open();
		},
		onMessageSuccessDialogPress: function (idTransaccion, mensaje) {
			var that = this;
			var dialog = new sap.m.Dialog({
				//id: 'dglExitoso',
				title: 'Éxito',
				type: 'Message',
				state: 'Success',
				contentWidth: '25rem',
				content: new sap.ui.layout.VerticalLayout({
					//id: 'idVertical',
					content: [new sap.m.Text({
							text: mensaje + '\n' + '\n',
							textAlign: 'Center'
							//id: 'txtMensaje'
						}),
						new sap.m.Input({
							value: idTransaccion,
							enabled: false,
							//id: 'inputTranscaccion',
							class: 'tamaniotexto'
						})
					]
				}),
				beginButton: new sap.m.Button({
					icon: 'sap-icon://copy',
					//id: 'btnidTransaccion',
					tooltip: 'Copiar código de transaccion al portapapeles',
					press: function () {
						that.copyToClipboard(idTransaccion);
					}
				}),
				endButton: new sap.m.Button({
					text: 'Cerrar',
					press: function () {
						dialog.destroy();
					}
				}),
				afterClose: function () {
					dialog.destroy();
				}
			});

			dialog.open();
		},

		messageStrip: function (controller, id, message, tipo) {

			var control = controller.getView().byId(id);
			control.setText(message);
			control.setShowIcon(true);
			control.setShowCloseButton(false);

			if (!tipo) {
				control.setType("Information");
			}

			if (tipo.toUpperCase() === "E") {
				control.setType("Error");
			}

			if (tipo.toUpperCase() === "S") {
				control.setType("Success");
			}

			if (tipo.toUpperCase() === "W") {
				control.setType("Warning");
			}

			if (tipo.toUpperCase() === "I") {
				control.setType("Information");
			}
		},
		objectListItemSelectedItem: function (event, model) {
			return event.getSource().getBindingContext(model) == undefined ? false : event.getSource().getBindingContext(model).getObject();
		},
		messageBox: function (mensaje, tipo, callback) {
			if (tipo.toUpperCase() === "C") {
				MessageBox.show(mensaje, {
					icon: MessageBox.Icon.QUESTION,
					title: "Confirmación",
					actions: [MessageBox.Action.YES, MessageBox.Action.NO],
					onClose: function (sAnswer) {
						return callback(sAnswer === MessageBox.Action.YES);
					}
				});
			}

			if (tipo.toUpperCase() === "E") {
				MessageBox.error(mensaje, {
					onClose: function (sAnswer) {
						return callback(sAnswer === MessageBox.Action.YES);
					}
				});
			}

			if (tipo.toUpperCase() === "S") {
				MessageBox.success(mensaje, {
					onClose: function (sAnswer) {
						return callback(sAnswer === MessageBox.Action.YES);
					}
				});
			}

		},
		messageToast: function (data) {
			return MessageToast.show(data, {
				duration: 3000
			});
		},
		imageResize: function (srcData, width, height) {
			var imageObj = new Image(),
				canvas = document.createElement("canvas"),
				ctx = canvas.getContext('2d'),
				xStart = 0,
				yStart = 0,
				aspectRadio,
				newWidth,
				newHeight;

			imageObj.src = srcData;
			canvas.width = width;
			canvas.height = height;

			aspectRadio = imageObj.height / imageObj.width;

			if (imageObj.height < imageObj.width) {
				//horizontal
				aspectRadio = imageObj.width / imageObj.height;
				newHeight = height,
					newWidth = aspectRadio * height;
				xStart = -(newWidth - width) / 2;
			} else {
				//vertical
				newWidth = width,
					newHeight = aspectRadio * width;
				yStart = -(newHeight - height) / 2;
			}

			ctx.drawImage(imageObj, xStart, yStart, newWidth, newHeight);

			return canvas.toDataURL("image/jpeg", 0.75);
		},
		resizeImg: function (img, maxWidth, maxHeight, degrees) {
			var imgWidth = img.width,
				imgHeight = img.height;

			var ratio = 1,
				ratio1 = 1,
				ratio2 = 1;
			ratio1 = maxWidth / imgWidth;
			ratio2 = maxHeight / imgHeight;

			// Use the smallest ratio that the image best fit into the maxWidth x maxHeight box.
			if (ratio1 < ratio2) {
				ratio = ratio1;
			} else {
				ratio = ratio2;
			}
			var canvas = document.createElement("canvas");
			var canvasContext = canvas.getContext("2d");
			var canvasCopy = document.createElement("canvas");
			var copyContext = canvasCopy.getContext("2d");
			var canvasCopy2 = document.createElement("canvas");
			var copyContext2 = canvasCopy2.getContext("2d");
			canvasCopy.width = imgWidth;
			canvasCopy.height = imgHeight;
			copyContext.drawImage(img, 0, 0);

			// init
			canvasCopy2.width = imgWidth;
			canvasCopy2.height = imgHeight;
			copyContext2.drawImage(canvasCopy, 0, 0, canvasCopy.width, canvasCopy.height, 0, 0, canvasCopy2.width, canvasCopy2.height);

			var rounds = 1;
			var roundRatio = ratio * rounds;
			for (var i = 1; i <= rounds; i++) {

				// tmp
				canvasCopy.width = imgWidth * roundRatio / i;
				canvasCopy.height = imgHeight * roundRatio / i;

				copyContext.drawImage(canvasCopy2, 0, 0, canvasCopy2.width, canvasCopy2.height, 0, 0, canvasCopy.width, canvasCopy.height);

				// copy back
				canvasCopy2.width = imgWidth * roundRatio / i;
				canvasCopy2.height = imgHeight * roundRatio / i;
				copyContext2.drawImage(canvasCopy, 0, 0, canvasCopy.width, canvasCopy.height, 0, 0, canvasCopy2.width, canvasCopy2.height);

			} // end for

			canvas.width = imgWidth * roundRatio / rounds;
			canvas.height = imgHeight * roundRatio / rounds;
			canvasContext.drawImage(canvasCopy2, 0, 0, canvasCopy2.width, canvasCopy2.height, 0, 0, canvas.width, canvas.height);

			if (degrees == 90 || degrees == 270) {
				canvas.width = canvasCopy2.height;
				canvas.height = canvasCopy2.width;
			} else {
				canvas.width = canvasCopy2.width;
				canvas.height = canvasCopy2.height;
			}

			canvasContext.clearRect(0, 0, canvas.width, canvas.height);
			if (degrees == 90 || degrees == 270) {
				canvasContext.translate(canvasCopy2.height / 2, canvasCopy2.width / 2);
			} else {
				canvasContext.translate(canvasCopy2.width / 2, canvasCopy2.height / 2);
			}
			canvasContext.rotate(degrees * Math.PI / 180);
			canvasContext.drawImage(canvasCopy2, -canvasCopy2.width / 2, -canvasCopy2.height / 2);

			var dataURL = canvas.toDataURL();
			return dataURL;
		},
		loading: function (bShow) {
			if (bShow === true) {
				sap.ui.core.BusyIndicator.show(10);
			} else {
				sap.ui.core.BusyIndicator.hide();
			}
		},
		getValueIN18: function (context, nameField) {
			return context.getView().getModel("i18n").getResourceBundle().getText(nameField);
			//{i18n>Description_TEXT}
		},
		crearExcel1: function (self, model, tabla, arrayFiltros, newModel, newTabla, arrayColumTabla, tituloExcel, nameFile) {
			var results = self.getView().getModel(model).getProperty("/" + tabla);
			this.onCreateExcel(self, results, newModel, arrayColumTabla, tituloExcel, nameFile);
		},
		crearExcelOdata1: function (self, model, tabla, arrayFiltros, newModel, newTabla, arrayColumTabla, tituloExcel, nameFile) {
			var that = this;
			var odata = "/" + tabla;
			var oDataModel = self.getView().getModel(model);
			//new Filter("CodigoTabla", "EQ", "tipo_usuario");
			var onSuccess = function (oDataModel) {
				var results = oDataModel.results;
				self.getView().getModel(newModel).setProperty("/" + newTabla, results);
				that.onCreateExcel(self, results, newModel, arrayColumTabla, tituloExcel, nameFile);
			};
			//Handler en caso de error
			var onError = function (error) {
				jQuery.sap.log.info("--cargaTablaClasePedido error--", error);
			};
			var reqHeaders = {
				filters: arrayFiltros,
				context: self, // mention the context you want
				success: onSuccess, // success call back method
				error: onError, // error call back method
				async: false // flage for async true
			};
			oDataModel.read(odata, reqHeaders);
		},
		onCreateExcelFormat: function (self, resultados, newModel, arrayColumTabla, tituloExcel, nameFile) {
			/*for (var i = 0; i < resultados.length; i++) {
				resultados[i].FechaCreacion = resultados[i].FechaCreacion.getDate() + "/" + (resultados[i].FechaCreacion.getMonth() + 1) + "/" +
					resultados[i].FechaCreacion.getFullYear();
			}*/
			/*
			á -> &aacute;
			é -> &eacute;
			í -> &iacute;
			ó -> &oacute;
			ú -> &uacute;
			ñ -> &ntilde;
			*/
			var rows = resultados;
			//////Inicio Fecha Actual/////////////////////////////////////////////////////////////////////////
			var date = new Date();
			var yyyy = date.getFullYear().toString();
			var mm = (date.getMonth() + 1).toString(); // getMonth() is zero-based
			var dd = date.getDate().toString();

			var dia = (dd[1] ? dd : "0" + dd[0]);
			var mes = (mm[1] ? mm : "0" + mm[0]);
			var anio = yyyy;
			var fechaActual = dia + "/" + mes + "/" + anio; // padding 
			//var fechaAct = (dd[1] ? dd : "0" + dd[0]) + (mm[1] ? mm : "0" + mm[0]) + yyyy; // padding 
			///////Fin Fecha Actual///////////////////////////////////////////////////////////////////////////
			var datefull = fechaActual; //this.formatter.date_full(new Date());
			var html = "";
			var tr = "";
			var th = "";
			var td = "";
			html = '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>';
			/*html = html + '<h4> ' + 'SERVICIOS AEROPORTUARIOS ANDINOS S.A.' + ' </h4>';
			//html = html + '<a href='+imgSaasaString+'><img src='+imgSaasaString+' width="200" height="150"/></a>'; 
			//html = html + '<img src="data:image/jpg;base64," width="200" height="150"/>'; 
			html = html + '<h6> ' + 'Av. Mariscal José de la Mar N° 1253 Int. 604 Urb. Santa Cruz, Miraflores - Lima' + ' </h6>';
			html = html + '<h6> ' + 'R.U.C. 20550083613' + ' </h6>';*/
			//html = html + '<h6> ' + 'Av. Elmer Faucett N° S/N Fnd. San Agustín Valle Bocanegra (Sub Parcela N° 2) - Callao' + ' </h6>';
			html = html + '<h2><center> ' + tituloExcel + ' </center></h2>';
			html = html + '<h6 align="right">Fecha Reporte: ' + datefull + '</h6>';
			var columnas = [];
			columnas = arrayColumTabla;
			/*[{
			        						name: "Id",
			        						model: "Id"
			        					   }];*/
			tr += '<tr>';
			for (var l = 0; l < columnas.length; l++) {
				var text = columnas[l].name;
				th += '<th style="background-color: #404040; color: #fff">' + text + '</th>';
			}
			tr += th + '</tr>';

			for (var j = 0; j < rows.length; j++) {
				tr += '<tr>';
				td = "";
				for (var k = 0; k < columnas.length; k++) {
					var item = rows[j][columnas[k].model];
					item = item ? item : '';
					if (columnas[k].model == "FirmaRepresentanteLineaAerea") {
						td += '<td style="text-align: center;"><a href="' + item + '">' + item + '</a></td>';
					} else {
						td += '<td style="text-align: center;">' + item + '</td>';
					}
				}
				tr += td + '</tr>';
			}
			html = html + '<table style="width:100%"  border="1">' + tr + '</table>';
			var fileName = nameFile + fechaActual + ".xls";
			//var data_type = 'data:application/vnd.ms-excel';
			var ua = window.navigator.userAgent;
			var msie = ua.indexOf("MSIE");
			if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
				if (window.navigator.msSaveBlob) {
					var blob = new Blob([html], {
						type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;"
					});
					navigator.msSaveBlob(blob, fileName);
				}
			} else {
				var blob2 = new Blob([html], {
					type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;"
				});
				var filename = fileName;
				var elem = window.document.createElement('a');
				elem.href = window.URL.createObjectURL(blob2);
				elem.download = filename;
				document.body.appendChild(elem);
				elem.click();
				document.body.removeChild(elem);
			}
			self.getView().getModel(newModel).refresh(true);
		},
		///Inicio Export Excel///
		onExportExcelLocalModel: function (self, model, tabla, arrayFiltros, newModel, arrayColumTabla, tituloExcel, nameFile) {
			var that = this;
			var results = self.getView().getModel(model).getProperty(tabla);
			that.onCreateExcelFormat(self, results, newModel, arrayColumTabla, tituloExcel, nameFile);
		},
		onExportExcel: function (self, model, tabla, arrayFiltros, newModel, newTabla, nameFile, nameHoja, mapEstructura) {
			var results = self.getView().getModel(model).getProperty("/" + tabla);
			this.onCreateExcel(self, results, newModel, nameFile, nameHoja, mapEstructura);
		},
		onExportExcelOdata: function (self, model, tabla, arrayFiltros, newModel, arrayColumTabla, tituloExcel, nameFile) {
			var that = this;
			var odata = "/" + tabla;
			var oDataModel = self.getView().getModel(model);
			//new Filter("CodigoTabla", "EQ", "tipo_usuario");
			var onSuccess = function (oDataModel) {
				var results = oDataModel.results;
				//self.getView().getModel(newModel).setProperty("/" + newTabla, results);
				that.onCreateExcelFormat(self, results, newModel, arrayColumTabla, tituloExcel, nameFile);
			};
			//Handler en caso de error
			var onError = function (error) {
				jQuery.sap.log.info("--cargaTablaClasePedido error--", error);
			};
			var reqHeaders = {
				sorters: [
					new sap.ui.model.Sorter("Id", false)
				],
				filters: arrayFiltros,
				context: self, // mention the context you want
				success: onSuccess, // success call back method
				error: onError, // error call back method
				async: false // flage for async true
			};
			oDataModel.read(odata, reqHeaders);
		},
		onExportExcelOdata1: function (self, model, tabla, arrayFiltros, newModel, newTabla, nameFile, nameHoja, mapEstructura) {
			var that = this;
			var odata = "/" + tabla;
			var oDataModel = self.getView().getModel(model);
			//new Filter("CodigoTabla", "EQ", "tipo_usuario");
			var onSuccess = function (oDataModel) {
				var results = oDataModel.results;
				//self.getView().getModel(newModel).setProperty("/" + newTabla, results);
				that.onCreateExcel(self, results, newModel, nameFile, nameHoja, mapEstructura);
			};
			//Handler en caso de error
			var onError = function (error) {
				jQuery.sap.log.info("--cargaTablaClasePedido error--", error);
			};
			var reqHeaders = {
				filters: arrayFiltros,
				context: self, // mention the context you want
				success: onSuccess, // success call back method
				error: onError, // error call back method
				async: false // flage for async true
			};
			oDataModel.read(odata, reqHeaders);
		},
		onCreateExcel: function (self, results, newModel, nameFile, nameHoja, mapEstructura) {
			var data = mapEstructura(results);
			/* this line is only needed if you are not adding a script tag reference */
			if (typeof XLSX == 'undefined') XLSX = require('xlsx');
			/* make the worksheet */
			var ws = XLSX.utils.json_to_sheet(data);
			//Inicio Filtrar//
			var AutoFilter = {
				ref: "A1:E1"
			};
			ws['!autofilter'] = AutoFilter;
			//End Filtrar//
			//Inicio Tamaño de la Columna//
			/*var wscols = [{wpx: '100'},{wpx: '100'}];
			ws['!cols'] = wscols ;*/
			//End Tamaño de la Columna//
			//Inicio Agregar Comentario//
			/*ws.A2.c.hidden = true;
			ws.A2.c.push({a:"SheetJS", t:"This comment will be hidden"});*/
			//End Agregar Comentario//
			/* add to workbook */
			var wb = XLSX.utils.book_new();
			//Inicio Agregar Otra Hoja//
			/*wb.SheetNames.push("Test Sheet");
			var ws1 = XLSX.utils.aoa_to_sheet(data);
			wb.Sheets["Test Sheet"] = ws1;*/
			//End Agregar Otra Hoja//
			XLSX.utils.book_append_sheet(wb, ws, nameHoja);
			/* generate an XLSX file */
			//////Inicio Fecha Actual/////////////////////////////////////////////////////////////////////////
			var date = new Date();
			var yyyy = date.getFullYear().toString();
			var mm = (date.getMonth() + 1).toString(); // getMonth() is zero-based
			var dd = date.getDate().toString();
			var fechaActual = (dd[1] ? dd : "0" + dd[0]) + "/" + (mm[1] ? mm : "0" + mm[0]) + "/" + yyyy;
			//////End Fecha Actual////////////////////////////////////////////////////////////////////////////
			XLSX.writeFile(wb, nameFile + fechaActual + ".xlsx");
		},
		///End Export Excel///
		///Inicio Import Excel///
		onImportExcel: function (self, oEvent, libro1, libro2, idFileUploader, mapEstructura, callback) {
			var file = oEvent.getParameter("files")[0];
			if (file && window.FileReader) {
				var reader = new FileReader();
				reader.onload = this.onReadFile.bind(self, self, libro1, libro2, idFileUploader, mapEstructura, callback);
				reader.readAsArrayBuffer(file);
			}
		},
		onReadFile: function (self, libro1, libro2, idFileUploader, mapEstructura, callback, evt) {
			var result = {};
			var data;
			var arr;
			var xlsx;
			data = evt.target.result;
			//var xlsx = XLSX.read(data, {type: 'binary'});
			arr = String.fromCharCode.apply(null, new Uint8Array(data));
			xlsx = XLSX.read(btoa(arr), {
				type: 'base64'
			});
			result = xlsx.Strings;
			result = {};
			xlsx.SheetNames.forEach(function (sheetName) {
				var rObjArr = XLSX.utils.sheet_to_row_object_array(xlsx.Sheets[sheetName]);
				if (rObjArr.length > 0) {
					result[sheetName] = rObjArr;
				}
			});
			var oResponse = {};
			oResponse.success = true;
			var contenido1 = result[libro1];
			var formatContenido1 = mapEstructura(contenido1);

			var contenido2 = result[libro2];
			var formatContenido2 = mapEstructura(contenido2);
			//self.getView().getModel(model).getProperty(tabla1)

			if ((!contenido1 || !contenido2) || (formatContenido1.length == 0 && formatContenido2.length == 0)) {
				oResponse.success = false;
			}
			self.getView().byId(idFileUploader).setValue("");
			oResponse.DocumentoTransporteMadre = formatContenido1;
			oResponse.DocumentoTransporteHija = formatContenido2;

			callback(oResponse);
		},
		///Inicio Import con MockData
		onImportExcel1: function (self, oEvent, model, tabla, idFileUploader, mapEstructura) {
			var file = oEvent.getParameter("files")[0];
			if (file && window.FileReader) {
				var reader = new FileReader();
				reader.onload = this.onReadFile.bind(self, self, model, tabla, idFileUploader, mapEstructura);
				reader.readAsArrayBuffer(file);
			}
		},
		onReadFile1: function (self, model, tabla, idFileUploader, mapEstructura, evt) {
			var result = {};
			var data;
			var arr;
			var xlsx;
			data = evt.target.result;
			//var xlsx = XLSX.read(data, {type: 'binary'});
			arr = String.fromCharCode.apply(null, new Uint8Array(data));
			xlsx = XLSX.read(btoa(arr), {
				type: 'base64'
			});
			result = xlsx.Strings;
			result = {};
			xlsx.SheetNames.forEach(function (sheetName) {
				var rObjArr = XLSX.utils.sheet_to_row_object_array(xlsx.Sheets[sheetName]);
				if (rObjArr.length > 0) {
					result[sheetName] = rObjArr;
				}
			});
			var contenido = result["SAP Document Export"]; //this.onRemoverHeader(data);
			//var localModel = this.getView().getModel("modelProductos");
			var formatContenido = mapEstructura(contenido);
			var modelTb = self.getView().getModel(model).getProperty(tabla) === undefined ? [] : self.getView().getModel(model).getProperty(tabla);
			var modelConcat = modelTb.concat(formatContenido);
			self.getView().getModel(model).setProperty(tabla, modelConcat);
			self.getView().byId(idFileUploader).setValue("");
		},
		onRemoverHeader1: function (data) {
			var excelKeys;
			var primerLibro = {};
			excelKeys = Object.keys(data);
			primerLibro.nombre = excelKeys[0];
			primerLibro.contenido = data[primerLibro.nombre];
			var shiftedInfo = primerLibro.contenido.shift();
			return primerLibro.contenido;
		},
		///End Import Mock Data
		///End Import Excel///

		/////INICIO ROY JsChart////////
		excecuteUtilsChart: function (callback) {
			window.chartColors = {
				red: 'rgb(255, 99, 132)',
				orange: 'rgb(255, 159, 64)',
				yellow: 'rgb(255, 205, 86)',
				green: 'rgb(75, 192, 192)',
				blue: 'rgb(54, 162, 235)',
				purple: 'rgb(153, 102, 255)',
				grey: 'rgb(201, 203, 207)'
			};

			(function (global) {
				var MONTHS = [
					'January',
					'February',
					'March',
					'April',
					'May',
					'June',
					'July',
					'August',
					'September',
					'October',
					'November',
					'December'
				];

				var COLORS = [
					'#4dc9f6',
					'#f67019',
					'#f53794',
					'#537bc4',
					'#acc236',
					'#166a8f',
					'#00a950',
					'#58595b',
					'#8549ba'
				];

				var Samples = global.Samples || (global.Samples = {});
				//var Color = global.Color;
				var Color = window.Color;

				Samples.utils = {
					// Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
					srand: function (seed) {
						this._seed = seed;
					},

					rand: function (min, max) {
						var seed = this._seed;
						min = min === undefined ? 0 : min;
						max = max === undefined ? 1 : max;
						this._seed = (seed * 9301 + 49297) % 233280;
						return min + (this._seed / 233280) * (max - min);
					},

					numbers: function (config) {
						var cfg = config || {};
						var min = cfg.min || 0;
						var max = cfg.max || 1;
						var from = cfg.from || [];
						var count = cfg.count || 8;
						var decimals = cfg.decimals || 8;
						var continuity = cfg.continuity || 1;
						var dfactor = Math.pow(10, decimals) || 0;
						var data = [];
						var i, value;

						for (i = 0; i < count; ++i) {
							value = (from[i] || 0) + this.rand(min, max);
							if (this.rand() <= continuity) {
								data.push(Math.round(dfactor * value) / dfactor);
							} else {
								data.push(null);
							}
						}

						return data;
					},

					labels: function (config) {
						var cfg = config || {};
						var min = cfg.min || 0;
						var max = cfg.max || 100;
						var count = cfg.count || 8;
						var step = (max - min) / count;
						var decimals = cfg.decimals || 8;
						var dfactor = Math.pow(10, decimals) || 0;
						var prefix = cfg.prefix || '';
						var values = [];
						var i;

						for (i = min; i < max; i += step) {
							values.push(prefix + Math.round(dfactor * i) / dfactor);
						}

						return values;
					},

					months: function (config) {
						var cfg = config || {};
						var count = cfg.count || 12;
						var section = cfg.section;
						var values = [];
						var i, value;

						for (i = 0; i < count; ++i) {
							value = MONTHS[Math.ceil(i) % 12];
							values.push(value.substring(0, section));
						}

						return values;
					},

					color: function (index) {
						return COLORS[index % COLORS.length];
					},

					transparentize: function (color, opacity) {
						var alpha = opacity === undefined ? 0.5 : 1 - opacity;
						return Color(color).alpha(alpha).rgbString();
					}
				};

				// DEPRECATED
				window.randomScalingFactor = function () {
					return Math.round(Samples.utils.rand(-100, 100));
				};

				// INITIALIZATION

				Samples.utils.srand(Date.now());

				// Google Analytics
				/* eslint-disable */
				if (document.location.hostname.match(/^(www\.)?chartjs\.org$/)) {
					(function (i, s, o, g, r, a, m) {
						i['GoogleAnalyticsObject'] = r;
						i[r] = i[r] || function () {
							(i[r].q = i[r].q || []).push(arguments)
						}, i[r].l = 1 * new Date();
						a = s.createElement(o),
							m = s.getElementsByTagName(o)[0];
						a.async = 1;
						a.src = g;
						m.parentNode.insertBefore(a, m)
					})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
					ga('create', 'UA-28909194-3', 'auto');
					ga('send', 'pageview');
				}
				/* eslint-enable */
				callback(Samples);
			}(this));
		},
		createBar: function () {

		},
		createBarDoble: function (chartData, tituloChart) {
			var ctx = window.canvas.getContext('2d');
			window.myMixedChart = new Chart(ctx, {
				type: 'bar',
				data: chartData,
				options: {
					showAllTooltips: true,
					maintainAspectRatio: true
				},
				plugins: [{
					beforeRender: function (chart) {
						if (chart.config.options.showAllTooltips) {
							// create an array of tooltips
							// we can't use the chart tooltip because there is only one tooltip per chart
							chart.pluginTooltips = [];
							chart.config.data.datasets.forEach(function (dataset, i) {
								chart.getDatasetMeta(i).data.forEach(function (sector, j) {
									chart.pluginTooltips.push(new Chart.Tooltip({
										_chart: chart.chart,
										_chartInstance: chart,
										_data: chart.data,
										_options: chart.options.tooltips,
										_active: [sector]
									}, chart));
								});
							});

							// turn off normal tooltips
							chart.options.tooltips.enabled = false;
						}
					},
					afterDraw: function (chart, easing) {
						if (chart.config.options.showAllTooltips) {
							// we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
							if (!chart.allTooltipsOnce) {
								if (easing !== 1)
									return;
								chart.allTooltipsOnce = true;
							}

							// turn on tooltips
							chart.options.tooltips.enabled = true;
							Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
								tooltip.initialize();
								tooltip.update();
								// we don't actually need this since we are not animating tooltips
								tooltip.pivot();
								tooltip.transition(easing).draw();
							});
							chart.options.tooltips.enabled = false;
						}
					}
				}]
			});
		},
		createLine: function (chartData, tituloChart) {
			var dataSet = chartData.datasets;
			dataSet.forEach(function (item) {
				item.type = null;
				item.fill = false;
			});
			var config = {
				type: 'line',
				data: {
					labels: chartData.labels,
					datasets: dataSet
				},
				options: {
					responsive: true,
					title: {
						display: true,
						text: tituloChart
					},
					scales: {
						xAxes: [{
							gridLines: {
								display: true
							}
						}],
						yAxes: [{
							gridLines: {
								display: true
							},
							ticks: {
								min: 0,
								max: 100,
								stepSize: 10
							}
						}]
					}
				}
			};

			var ctx = window.canvas.getContext('2d');
			window.myLine = new Chart(ctx, config);
			var colorNames = Object.keys(window.chartColors);
		},
		createPie: function (chartData, tituloChart, that) {
			var COLORS = [
				'#4dc9f6',
				'#f67019',
				'#f53794',
				'#537bc4',
				'#acc236',
				'#166a8f',
				'#00a950',
				'#58595b',
				'#8549ba'
			];

			function color(index) {
				return COLORS[index % COLORS.length];
			}
			var colorDeg = Chart.helpers.color;
			var modData = chartData.datasets;
			var aDatos = [];
			var dataSets = [];
			var aColor = [];
			var backColor = [];
			var labels = [];
			modData.forEach(function (itemData, y) {
				var datosConteo = 0;
				var datos = itemData.data;
				datos.forEach(function (item, x) {
					datosConteo = datosConteo + item;
				});
				aDatos.push(datosConteo);
				//aColor.push(color(y));
				backColor.push(colorDeg(color(y)).alpha(0.5).rgbString());
				labels.push(itemData.label);
			});
			var obj = {
				data: aDatos,
				//backgroundColor: colorDeg(color(x)).alpha(0.5).rgbString(),
				borderColor: aColor,
				backgroundColor: backColor
			};
			dataSets.push(obj);
			var config = {
				type: 'doughnut',
				data: {
					datasets: dataSets,
					labels: labels
				},
				options: {
					showAllTooltips: true,
					maintainAspectRatio: true,
					responsive: true,
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						//text: 'Chart.js Doughnut Chart'
					},
					animation: {
						animateScale: true,
						animateRotate: true
					}
				},
				plugins: [{
					beforeRender: function (chart) {
						if (chart.config.options.showAllTooltips) {
							// create an array of tooltips
							// we can't use the chart tooltip because there is only one tooltip per chart
							chart.pluginTooltips = [];
							chart.config.data.datasets.forEach(function (dataset, i) {
								chart.getDatasetMeta(i).data.forEach(function (sector, j) {
									chart.pluginTooltips.push(new Chart.Tooltip({
										_chart: chart.chart,
										_chartInstance: chart,
										_data: chart.data,
										_options: chart.options.tooltips,
										_active: [sector]
									}, chart));
								});
							});

							// turn off normal tooltips
							chart.options.tooltips.enabled = false;
						}
					},
					afterDraw: function (chart, easing) {
						if (chart.config.options.showAllTooltips) {
							// we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
							if (!chart.allTooltipsOnce) {
								if (easing !== 1)
									return;
								chart.allTooltipsOnce = true;
							}

							// turn on tooltips
							chart.options.tooltips.enabled = true;
							Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
								tooltip.initialize();
								tooltip.update();
								// we don't actually need this since we are not animating tooltips
								tooltip.pivot();
								tooltip.transition(easing).draw();
							});
							chart.options.tooltips.enabled = false;
						}
					}
				}]
			};

			var ctx = window.canvas.getContext('2d');
			var hh = color();
			window.myDoughnut = new Chart(ctx, config);

			var colorNames = Object.keys(window.chartColors);
		},
		createPolar: function (Samples, chartData, tituloChart, that) {
			var DATA_COUNT = 7;

			var utils = Samples.utils;

			utils.srand(110);

			function colorize(opaque, hover, ctx) {
				var v = ctx.dataset.data[ctx.dataIndex];
				var c = v < 35 ? '#D60000' : v < 55 ? '#F46300' : v < 75 ? '#0358B6' : '#44DE28';

				var opacity = hover ? 1 - Math.abs(v / 150) - 0.2 : 1 - Math.abs(v / 150);

				return opaque ? c : utils.transparentize(c, opacity);
			}

			function hoverColorize(ctx) {
				return colorize(false, true, ctx);
			}
			var COLORS = [
				'#4dc9f6',
				'#f67019',
				'#f53794',
				'#537bc4',
				'#acc236',
				'#166a8f',
				'#00a950',
				'#58595b',
				'#8549ba'
			];

			function color(index) {
				return COLORS[index % COLORS.length];
			}
			var colorDeg = Chart.helpers.color;
			var modData = chartData.datasets;
			var aDatos = [];
			var dataSets = [];
			var aColor = [];
			var backColor = [];
			var labels = [];
			modData.forEach(function (itemData, y) {
				var datosConteo = 0;
				var datos = itemData.data;
				datos.forEach(function (item, x) {
					datosConteo = datosConteo + item;
				});
				aDatos.push(datosConteo);
				//aColor.push(color(y));
				backColor.push(colorDeg(color(y)).alpha(0.5).rgbString());
				labels.push(itemData.label);
			});
			var obj = {
				data: aDatos,
				//backgroundColor: colorDeg(color(x)).alpha(0.5).rgbString(),
				borderColor: aColor,
				backgroundColor: backColor
			};
			dataSets.push(obj);

			var data = {
				datasets: dataSets
			};

			var options = {
				legend: false,
				tooltips: false,
				elements: {
					arc: {
						backgroundColor: colorize.bind(null, false, false),
						hoverBackgroundColor: hoverColorize
					}
				}
			};
			var ctx = window.canvas;
			var chart = new Chart(ctx, {
				type: 'polarArea',
				data: data,
				options: options
			});
		},
		inicializarDatos: function (that, nameTable, nameTablaModel) {
			var columnasGeneral = that.columnaTable(nameTable);
			var columnasFechas = columnasGeneral.filter(function (el) {
				return el.model.includes("Fecha");
			});
			var columnas = columnasGeneral.filter(function (el) {
				return !el.model.includes("Fecha");
			});
			that.getView().getModel("localModel").setProperty("/ColumnaTablaFechas", columnasFechas);
			that.getView().getModel("localModel").setProperty("/ColumnaTabla", columnas);
			var agDatos = that.getView().getModel("localModel").getProperty("/AgregarDatos");
			that.getView().getModel("localModel").setProperty("/AgregarDatos", agDatos ? agDatos : {});
			var dataSet = that.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet");
			dataSet = dataSet ? (dataSet.length > 0 ? dataSet : []) : [];

			var aDataSet = [];
			if (dataSet.length > 0) {
				dataSet.forEach(function (item, x) {
					if (item["Columna"] !== "" && item["Data"].length > 0) {
						aDataSet.push(item);
					}
				});
				that.getView().getModel("localModel").setProperty("/AgregarDatos/DataSet", aDataSet);
				that.getView().getModel("localModel").refresh(true);
				this.createFormDinamico(that);
			}
			that.getView().getModel("localModel").setProperty("/AgregarDatos/DataSet", aDataSet);
			that.getView().getModel("localModel").refresh(true);
		},
		createGrafico: function (that, tituloChart) {
			var filtros = that.getView().getModel("localModel").getProperty("/AgregarDatos"); //DataSet
			var datosTabla = that.getView().getModel("localModel").getProperty("/VChartDataSet");
			if (filtros && datosTabla) {
				var dataSet = that.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet");
				if (dataSet) {
					var dataSetNew = dataSet.length > 0 ? dataSet : [];
					dataSetNew.forEach(function (dataItemSuperior, i) {
						var nameSelectedColumna = "Columna";
						var nameSelectedData = "Data";
						var nameColumnaDataTabla = "ColumnaDataTabla";

						var nameContainer0 = "Container0";
						var nameContainer1 = "Container1";
						var nameContainer2 = "Container2";
						var nameBtnEliminar = "btnEliminar";
						var nameBtnMultiSelect = "btnMultiSelect";
						var namePressedMultiSelect = "PressedMultiSelect"
						var dataSelectedColumna = "";
						var dataSelectedData = [];
						var dataCmbColumnaDataTabla = [];
						var dataBtnPressed = false;
						var nameValidEnabled = "EnabledCampos"

						var nameCmbColumna = "cmbColumna";
						var nameCmbColumnaDatos = "cmbColumnaDatos";
						var dataEnabled = false;

						if (dataSetNew[i][nameSelectedColumna] !== "") {
							dataSelectedColumna = dataSetNew[i][nameSelectedColumna];
						}
						if (dataSetNew[i][nameSelectedData].length > 0) {
							dataSelectedData = dataSetNew[i][nameSelectedData];
						}
						if (dataSetNew[i][nameColumnaDataTabla].length > 0) {
							dataCmbColumnaDataTabla = dataSetNew[i][nameColumnaDataTabla];
						}
						if (dataSetNew[i][namePressedMultiSelect]) {
							dataBtnPressed = dataSetNew[i][namePressedMultiSelect];
						}
						if (dataSetNew[i][nameValidEnabled]) {
							dataEnabled = dataSetNew[i][nameValidEnabled];
						}
						var objGeneral = {
							[nameSelectedColumna]: dataSelectedColumna,
							[nameSelectedData]: dataSelectedData,
							[nameColumnaDataTabla]: dataCmbColumnaDataTabla,
							[namePressedMultiSelect]: dataBtnPressed,
							[nameValidEnabled]: dataEnabled,

							[nameBtnEliminar]: dataItemSuperior[nameBtnEliminar],
							[nameBtnMultiSelect]: dataItemSuperior[nameBtnMultiSelect],

							[nameContainer0]: dataItemSuperior[nameContainer0],
							[nameContainer1]: dataItemSuperior[nameContainer1],
							[nameContainer2]: dataItemSuperior[nameContainer2],
							[nameCmbColumna]: dataItemSuperior[nameCmbColumna],
							[nameCmbColumnaDatos]: dataItemSuperior[nameCmbColumnaDatos]
						};
						that.getView().getModel("localModel").setProperty("/AgregarDatos/DataSet/" + i + "/", objGeneral);
						that.getView().getModel("localModel").refresh();

					});
				}
				var self = this;
				sap.ui.getCore().byId("dlg_dialogGraficoChart").destroyContent();
				var cmbTipoChartSelected = sap.ui.getCore().byId("cmbTipoChart").getSelectedKey();
				//////////////////////////////////////////
				var dataSet = that.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet");
				dataSet = dataSet ? (dataSet.length > 0 ? dataSet : []) : [];

				var aDataSet = [];
				if (dataSet.length > 0) {
					dataSet.forEach(function (item, x) {
						if (item["Columna"] !== "" && item["Data"].length > 0) {
							aDataSet.push(item);
						}
					});
					that.getView().getModel("localModel").setProperty("/AgregarDatos/DataSet", aDataSet);
				}
				//////////////////////////////////////////
				if (filtros.DataSet) {
					if (!(cmbTipoChartSelected && filtros.DataSet.length > 0 && filtros.FechaSelected && filtros.FechaInicio && filtros.FechaFin)) {
						return sap.m.MessageToast.show("Falta llenar Data y/o seleccionar el tipo de Chart");
					}
				} else {
					return sap.m.MessageToast.show("Falta llenar Data y/o seleccionar el tipo de Chart");
				}
				filtros.MonthsSelected.forEach(function (MonthSelect) {
					var dataSet = filtros.DataSet;
					dataSet.forEach(function (objData) {
						var datosSelected = objData.Data;
						var contador = 0;
						datosSelected.forEach(function (item) {
							var match = item.split("|")[0];
							var nameCampo = objData.Columna.split("|")[0];
							var nameFecha = filtros.FechaSelected.split("|")[0].replace("String", "");
							var cantidad = datosTabla.filter(function (el) {
								return el[nameCampo] == match && (new Date(el[nameFecha]).getFullYear() + (new Date(el[nameFecha]).getMonth() >= 12 ? 0 : new Date(el[nameFecha]).getMonth() +
									1) == MonthSelect.Anio + MonthSelect.id);
							});
							contador = contador + cantidad.length;
						});
						objData.aDatos = objData.aDatos ? objData.aDatos : [];
						objData.EtiquetaColumna = objData.Columna.split("|")[1];
						objData.aDatos.push(contador);
					});

				});

				var content = new sap.ui.core.HTML({
					//content: "<div class='contentDiv' style='width:100%;'><canvas id='canvas'></canvas></div><br></br><button id='randomizeData'>Randomize Data</button><button id='addDataset'>Add Dataset</button><button id='removeDataset'>Remove Dataset</button><button id='addData'>Add Data</button><button id='removeData'>Remove Data</button><button id='changeCircleSize'>Semi/Full Circle</button>";
					content: "<div class='contentDiv' style='width:100%;'><canvas id='canvas'></canvas>"
				});
				sap.ui.getCore().byId("dlg_dialogGraficoChart").addContent(content);
				sap.ui.core.BusyIndicator.show(0);
				jQuery.sap.delayedCall(300, self, function () {
					sap.ui.core.BusyIndicator.hide();
					window.canvas = document.getElementById('canvas');
					window.canvas.getContext('2d').clearRect(0, 0, window.canvas.width, window.canvas.height);
					this.excecuteUtilsChart(function (Samples) {
						///////////////////////
						var COLORS = [
							'#4dc9f6',
							'#f67019',
							'#f53794',
							'#537bc4',
							'#acc236',
							'#166a8f',
							'#00a950',
							'#58595b',
							'#8549ba'
						];

						function color(index) {
							return COLORS[index % COLORS.length];
						}
						var colorDeg = Chart.helpers.color;
						///////////////////////
						var chartData = {
							"labels": [],
							"datasets": []
						};
						filtros.MonthsSelected.forEach(function (Mes) {
							chartData.labels.push(Mes.name + " " + Mes.Anio);
						});
						/*filtros.DataSet.forEach(function (DataSet, x) {
							//DataSet.aDatos.push(randomScalingFactor());
							var obj = {
								type: "bar",
								label: DataSet.Columna.split("|")[1],
								backgroundColor: colorDeg(color(x)).alpha(0.5).rgbString(),
								//borderColor: window.chartColors.red,
								borderColor: color(x),

								//borderWidth:2,
								//fill:false,
								data: DataSet.aDatos
							};
							chartData.datasets.push(obj);
						});*/ ///Comentado Roy Anterior 28-06-2020

						filtros.DataSet[0].Data.forEach(function (DataSet, x) {
							var nameFecha = filtros.FechaSelected.split("|")[0].replace("String", "");
							var modDatos2 = [];
							filtros.MonthsSelected.forEach(function (item, z) {
								var dataFecha = datosTabla.filter(function (el) {
									return el[filtros.DataSet[0].Columna.split("|")[0]] == DataSet.split("|")[0] && (new Date(el[nameFecha]).getFullYear() + (new Date(el[nameFecha]).getMonth() >= 12 ? 0 : new Date(el[nameFecha]).getMonth() +
										1) == filtros.MonthsSelected[z].Anio + filtros.MonthsSelected[z].id);
								});
								modDatos2.push(dataFecha.length);
							});
							var obj = {
								type: "bar",
								label: DataSet.split("|")[0],
								backgroundColor: colorDeg(color(x)).alpha(0.5).rgbString(),
								borderColor: color(x),
								data: modDatos2
							};
							chartData.datasets.push(obj);
						}); ///Roy Nuevo 28-06-2020
						///////////////////////
						switch (cmbTipoChartSelected) {
							case "bar":
								self.createBar(chartData, tituloChart);
								break;
							case "barDoble":
								self.createBarDoble(chartData, tituloChart);
								break;
							case "line":
								self.createLine(chartData, tituloChart);
								break;
							case "pie":
								self.createPie(chartData, tituloChart, that);
								break;
							case "polar":
								self.createPolar(Samples, chartData, tituloChart, that);
								break;
							default:
								break;
						};
					});
				});
			}
		},
		createFormDinamico: function (self) {
			//var self = this;
			var dataSet = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet");
			var dataSetNew = dataSet.length > 0 ? dataSet : [];
			var contenedor = sap.ui.getCore().byId("formDinamico");
			contenedor.destroyFormContainers();
			var modDataSet = [];
			dataSetNew.forEach(function (dataItemSuperior, i) {
				var nameSelectedColumna = "Columna";
				var nameSelectedData = "Data";
				var nameColumnaDataTabla = "ColumnaDataTabla";

				var nameContainer0 = "Container0";
				var nameContainer1 = "Container1";
				var nameContainer2 = "Container2";
				var nameBtnEliminar = "btnEliminar";
				var nameBtnMultiSelect = "btnMultiSelect";
				var namePressedMultiSelect = "PressedMultiSelect"
				var dataSelectedColumna = "";
				var dataSelectedData = [];
				var dataCmbColumnaDataTabla = [];
				var dataBtnPressed = false;
				var nameValidEnabled = "EnabledCampos"

				var nameCmbColumna = "cmbColumna";
				var nameCmbColumnaDatos = "cmbColumnaDatos";
				var dataEnabled = false;

				if (dataSetNew[i][nameSelectedColumna] !== "") {
					dataSelectedColumna = dataSetNew[i][nameSelectedColumna];
				}
				if (dataSetNew[i][nameSelectedData].length > 0) {
					dataSelectedData = dataSetNew[i][nameSelectedData];
				}
				if (dataSetNew[i][nameColumnaDataTabla].length > 0) {
					dataCmbColumnaDataTabla = dataSetNew[i][nameColumnaDataTabla];
				}
				if (dataSetNew[i][namePressedMultiSelect]) {
					dataBtnPressed = dataSetNew[i][namePressedMultiSelect];
				}
				if (dataSetNew[i][nameValidEnabled]) {
					dataEnabled = dataSetNew[i][nameValidEnabled];
				}

				var comboColumna = new sap.m.ComboBox({
					width: "100%",
					selectedKey: "{localModel>/AgregarDatos/DataSet/" + i + "/" + nameSelectedColumna + "}",
					change: function (oEvent) {
						if (self.getView) {
							var path;
							var nameModel;

							if (oEvent.getSource().getBindingInfo("selectedKey").binding) {
								path = oEvent.getSource().getBindingInfo("selectedKey").binding.getPath();
								nameModel = oEvent.getSource().getBindingInfo("selectedKey").binding.getValue();
							} else {
								path = oEvent.getSource().getBindingInfo("selectedKey").parts[0].path;
								nameModel = oEvent.getSource().getSelectedKey();
							}

							var campoModel = nameModel.split("|")[0];
							var posicion = parseInt(path.split("/")[3]);
							var datosTabla = self.getView().getModel("localModel").getProperty("/VChartDataSet");
							var newArray = datosTabla ? datosTabla : [];
							var aDatos = [];

							if (newArray.length > 0) {
								newArray.forEach(function (item) {
									item.Id = item.Id;
									item.model = item[campoModel];
									item.name = item[campoModel];
									aDatos.push(utilController.unlink(item));
								});
							}
							aDatos = utilController.removeDuplicatesGeneralConteo(aDatos, campoModel, campoModel);
							self.getView().getModel("localModel").setProperty("/AgregarDatos/DataSet/" + posicion + "/" + nameSelectedData, []);
							self.getView().getModel("localModel").setProperty("/AgregarDatos/DataSet/" + posicion + "/" + nameColumnaDataTabla, aDatos);
							self.getView().getModel("localModel").refresh();
							/////////////////////////
							if (nameModel && nameModel !== "") {
								self.getView().getModel("localModel").setProperty("/AgregarDatos/DataSet/" + posicion + "/" +
									nameValidEnabled, true);
							} else {
								self.getView().getModel("localModel").setProperty("/AgregarDatos/DataSet/" + posicion + "/" +
									nameValidEnabled, false);
							}
							var cmbColumnaDatos = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + posicion + "/" +
								nameCmbColumnaDatos);
							var selected = cmbColumnaDatos.getSelectedItems();
							var items = cmbColumnaDatos.getItems();
							var btnMultiSelect = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + posicion + "/" +
								nameBtnMultiSelect);
							if (selected.length == items.length) {
								self.getView().getModel("localModel").setProperty("/AgregarDatos/DataSet/" + posicion + "/" + namePressedMultiSelect, true);
								btnMultiSelect.setPressed(true);
							} else {
								self.getView().getModel("localModel").setProperty("/AgregarDatos/DataSet/" + posicion + "/" + namePressedMultiSelect, false);
								btnMultiSelect.setPressed(false);
							}
							////////////////////////
						}
					},
					items: {
						path: "localModel>/ColumnaTabla",
						template: new sap.ui.core.Item({
							key: "{localModel>model}|{localModel>name}",
							text: "{localModel>name}"
						})
					}
				});
				var formContainer = new sap.ui.layout.form.FormContainer({
					formElements: [new sap.ui.layout.form.FormElement({
						label: "Columna",
						fields: [comboColumna]
					})]
				});
				var comboColumnaData = new sap.m.MultiComboBox({
					width: "200px",
					enabled: "{localModel>/AgregarDatos/DataSet/" + i + "/" + nameValidEnabled + "}",
					selectedKeys: "{localModel>/AgregarDatos/DataSet/" + i + "/" + nameSelectedData + "}",
					selectionFinish: function (oEvent) {
						if (self.getView) {
							var selected = oEvent.getSource().getSelectedItems();
							var items = oEvent.getSource().getItems();
							var path, posicion;
							if (oEvent.getSource().getBindingInfo("selectedKeys").binding) {
								path = oEvent.getSource().getBindingInfo("selectedKeys").binding.getPath();
								posicion = parseInt(path.split("/")[3]);
							} else {
								path = oEvent.getSource().getBindingInfo("selectedKeys").parts[0].path;
								posicion = parseInt(path.split("/")[3]);
							}
							var btnMultiSelect = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + posicion + "/" +
								nameBtnMultiSelect);
							if (selected.length == items.length) {
								self.getView().getModel("localModel").setProperty("/AgregarDatos/DataSet/" + posicion + "/" + namePressedMultiSelect, true);
								btnMultiSelect.setPressed(true);
							} else {
								self.getView().getModel("localModel").setProperty("/AgregarDatos/DataSet/" + posicion + "/" + namePressedMultiSelect, false);
								btnMultiSelect.setPressed(false);
							}
						}
					},
					items: {
						path: "localModel>/AgregarDatos/DataSet/" + i + "/" + nameColumnaDataTabla,
						template: new sap.ui.core.Item({
							key: "{localModel>name}",
							text: "{localModel>name}"
						})
					}
				});
				var formContainer1 = new sap.ui.layout.form.FormContainer({
					formElements: [
						new sap.ui.layout.form.FormElement({
							label: "Datos",
							fields: [comboColumnaData]
						})
					]
				});
				var btnEliminar = new sap.m.Button({
					width: "20%",
					type: "Transparent",
					icon: "sap-icon://sys-cancel",
					tooltip: i.toString(),
					//id:"btnElminar"+i,
					press: function (oEvent) {
						if (self.getView) {
							var nameId = oEvent.getSource().getTooltip();
							var posicion = parseInt(nameId);
							var cont0 = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + posicion + "/" + nameContainer0);
							var cont1 = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + posicion + "/" + nameContainer1);
							var cont2 = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + posicion + "/" + nameContainer2);
							cont0.destroy();
							cont1.destroy();
							cont2.destroy();
							var dataSetMod = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet");
							dataSetMod.splice(posicion, 1);
							var modDataSet = dataSetMod;
							var newDataSet = [];
							modDataSet.forEach(function (item, x) {
								if (x >= posicion) {
									item[nameBtnEliminar].setAggregation("tooltip", x.toString());
									modDataSet[x][nameBtnEliminar] = item[nameBtnEliminar];

									item[nameBtnMultiSelect].setAggregation("tooltip", x.toString());
									modDataSet[x][nameBtnMultiSelect] = item[nameBtnMultiSelect];
								}
								//////////////
								var dataSelectedColumna = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + x + "/" +
									nameSelectedColumna);
								var dataSelectedData = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + x + "/" +
									nameSelectedData);
								var dataCmbColumnaDataTabla = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + x + "/" +
									nameColumnaDataTabla);

								var objBtnEliminar = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + x + "/" +
									nameBtnEliminar);

								var objBtnMultiSelect = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + x + "/" +
									nameBtnMultiSelect);
								var pressedBtnMultiSelect = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + x + "/" +
									namePressedMultiSelect);
								var objDataEnabled = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + x + "/" +
									nameValidEnabled);

								var objContainer0 = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + x + "/" + nameContainer0);
								var objContainer1 = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + x + "/" + nameContainer1);
								var objContainer2 = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + x + "/" + nameContainer2);

								var objCmbColumna = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + x + "/" + nameCmbColumna);
								var objCmbColumnaData = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + x + "/" +
									nameCmbColumnaDatos);
								//////////////
								newDataSet.push({
									[nameSelectedColumna]: dataSelectedColumna,
									[nameSelectedData]: dataSelectedData,
									[nameColumnaDataTabla]: dataCmbColumnaDataTabla,
									[nameBtnEliminar]: objBtnEliminar,
									[nameBtnMultiSelect]: objBtnMultiSelect,
									[nameContainer0]: objContainer0,
									[nameContainer1]: objContainer1,
									[nameContainer2]: objContainer2,
									[nameCmbColumna]: objCmbColumna,
									[nameCmbColumnaDatos]: objCmbColumnaData,
									[namePressedMultiSelect]: pressedBtnMultiSelect,
									[nameValidEnabled]: objDataEnabled
								});
							});
							modDataSet.forEach(function (item, x) {
								var objBtnMultiSelect = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + x + "/" +
									nameBtnMultiSelect);
								var cmbCol = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + x + "/" + nameCmbColumna);
								cmbCol.bindProperty("selectedKey", "localModel>/AgregarDatos/DataSet/" + x + "/" + nameSelectedColumna);
								var cmbColData = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + x + "/" +
									nameCmbColumnaDatos);
								cmbColData.bindAggregation("items", {
									path: "localModel>/AgregarDatos/DataSet/" + x + "/" + nameColumnaDataTabla,
									template: new sap.ui.core.Item({
										key: "{localModel>name}",
										text: "{localModel>name}"
									})
								});
								cmbColData.bindProperty("selectedKeys", "localModel>/AgregarDatos/DataSet/" + x + "/" + nameSelectedData);
								cmbColData.bindProperty("enabled", "localModel>/AgregarDatos/DataSet/" + x + "/" + nameValidEnabled);

								objBtnMultiSelect.bindProperty("enabled", "localModel>/AgregarDatos/DataSet/" + x + "/" + nameValidEnabled);
								objBtnMultiSelect.bindProperty("pressed", "localModel>/AgregarDatos/DataSet/" + x + "/" + namePressedMultiSelect);
							});
							self.getView().getModel("localModel").setProperty("/AgregarDatos/DataSet", newDataSet);
							self.getView().getModel("localModel").refresh();
						}
					}
				});
				var btnMultiSelect = new sap.m.ToggleButton({
					width: "100%",
					type: "Transparent",
					enabled: "{localModel>/AgregarDatos/DataSet/" + i + "/" + nameValidEnabled + "}",
					text: "Seleccionar Todos",
					// icon: "sap-icon://multi-select",
					pressed: "{localModel>/AgregarDatos/DataSet/" + i + "/" + namePressedMultiSelect + "}",
					tooltip: i.toString(),
					//id:"btnElminar"+i,
					press: function (oEvent) {
						if (self.getView) {
							var pressed = oEvent.getSource().getPressed();
							var nameId = oEvent.getSource().getTooltip();
							var posicion = parseInt(nameId);
							var multiComboData = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + posicion + "/" +
								nameCmbColumnaDatos);
							if (pressed) {
								self.getView().getModel("localModel").setProperty("/AgregarDatos/DataSet/" + posicion + "/" + namePressedMultiSelect, true);
								multiComboData.setSelectedItems(multiComboData.getItems());
							} else {
								self.getView().getModel("localModel").setProperty("/AgregarDatos/DataSet/" + posicion + "/" + namePressedMultiSelect, false);
								multiComboData.setSelectedItems([]);
							}
						}
					}
				});
				var label = new sap.m.Label({
					text: "1"
				});
				label.addStyleClass("classLabelBorrarFormDinamico");
				var formContainer2 = new sap.ui.layout.form.FormContainer({
					formElements: [
						new sap.ui.layout.form.FormElement({
							label: label,
							fields: [btnEliminar, btnMultiSelect]
						})
					]
				})

				var objGeneral = {
					[nameSelectedColumna]: dataSelectedColumna,
					[nameSelectedData]: dataSelectedData,
					[nameColumnaDataTabla]: dataCmbColumnaDataTabla,

					[nameBtnEliminar]: btnEliminar,
					[nameBtnMultiSelect]: btnMultiSelect,
					[namePressedMultiSelect]: dataBtnPressed,
					[nameContainer0]: formContainer,
					[nameContainer1]: formContainer1,
					[nameContainer2]: formContainer2,
					[nameCmbColumna]: comboColumna,
					[nameCmbColumnaDatos]: comboColumnaData,
					[nameValidEnabled]: dataEnabled
				};
				self.getView().getModel("localModel").setProperty("/AgregarDatos/DataSet/" + i + "/", objGeneral);
				self.getView().getModel("localModel").refresh();
				contenedor.addFormContainer(formContainer);
				contenedor.addFormContainer(formContainer1);
				contenedor.addFormContainer(formContainer2);
			});
		},
		eliminarSoloCuerpoDataSet: function (that) {
			that.onBtnLimpiarDataSet(true);
			var cmbFechaSelected = sap.ui.getCore().byId("cmbFechaSelected").getSelectedKey();
			var drRangoFechaValue = sap.ui.getCore().byId("drRangoFecha").getValue();
			if (cmbFechaSelected && drRangoFechaValue) {
				sap.m.MessageToast.show("No hay datos dentro de este rango de fechas");
			}
		},
		onChangeFiltroFecha: function (that, nameModelTabla) {
			this.onEliminarItemsFormDinamico(that);
			var agregarDatos = that.getView().getModel("localModel").getProperty("/AgregarDatos");
			var getInicioRangoFecha = agregarDatos.FechaInicio;
			var getFinRangoFecha = agregarDatos.FechaFin;
			if (!(agregarDatos.FechaSelected && getInicioRangoFecha && getFinRangoFecha)) {
				return;
			}
			var splitFechaSelected = agregarDatos.FechaSelected.split("|");

			var datosTabla = that.getView().getModel("localModel").getProperty(nameModelTabla);
			var getSelectedCmbFechas = splitFechaSelected[0].replace("String", "");
			var filterFechas = datosTabla.filter(function (el) {
				return new Date(el[getSelectedCmbFechas]) >= getInicioRangoFecha && new Date(el[getSelectedCmbFechas]) <= getFinRangoFecha;
			});
			var fechaSelected = [];
			var MonthsData = Estructura.maestro.mapMonth();
			var newAgregarDatos = that.getView().getModel("localModel").getProperty("/AgregarDatos");
			var items = utilController.removeDuplicatesGeneral(filterFechas, splitFechaSelected[0]);
			items.forEach(function (item) {
				var fechaDate = item[splitFechaSelected[0].replace("String", "")];
				fechaDate = new Date(fechaDate);
				var idMonth = fechaDate.getMonth() >= 12 ? 0 : fechaDate.getMonth() + 1;
				var idAnio = fechaDate.getFullYear();
				var findDate = MonthsData.find(function (el) {
					return el.id === idMonth;
				});
				findDate.Anio = idAnio;
				fechaSelected.push(findDate);
			});
			fechaSelected = utilController.orderByFecha(fechaSelected);
			fechaSelected = utilController.removeDuplicatesGeneral2Campos(fechaSelected, "Anio", "id");
			that.getView().getModel("localModel").setProperty("/AgregarDatos/MonthsSelected", fechaSelected);
			if (filterFechas) {
				if (filterFechas.length > 0) {

				} else {
					this.eliminarSoloCuerpoDataSet(that);
				}
			} else {
				this.eliminarSoloCuerpoDataSet(that);
			}

			that.getView().getModel("localModel").setProperty("/AgregarDatos/EtiquetaFecha", splitFechaSelected[1]);
			that.getView().getModel("localModel").setProperty("/VChartDataSet", filterFechas);
		},
		onBtnAddFormDinamico: function (that) {
			var datosFilter = that.getView().getModel("localModel").getProperty("/VChartDataSet");
			if (datosFilter) {
				if (datosFilter.length > 0) {
					this.addItemFormDinamico(that);
				} else {
					this.eliminarSoloCuerpoDataSet(that);
				}
			} else {
				this.eliminarSoloCuerpoDataSet(that);
			}
		},
		addItemFormDinamico: function (that) {
			var dataSet = that.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet");
			var dataSetNew = dataSet.length > 0 ? dataSet : [];
			dataSetNew.push({
				["Columna"]: "",
				["Data"]: [],
				["ColumnaDataTabla"]: []
			});
			that.getView().getModel("localModel").setProperty("/AgregarDatos/DataSet", dataSetNew);
			this.createFormDinamico(that);
		},
		onEliminarItemsFormDinamico: function (self) {
			var dataSet = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet");
			var nameContainer0 = "Container0";
			var nameContainer1 = "Container1";
			var nameContainer2 = "Container2";
			var objContainer0;
			var objContainer1;
			var objContainer2;
			if (dataSet.length > 0) {
				dataSet.forEach(function (item, x) {
					objContainer0 = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + x + "/" + nameContainer0);
					objContainer1 = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + x + "/" + nameContainer1);
					objContainer2 = self.getView().getModel("localModel").getProperty("/AgregarDatos/DataSet/" + x + "/" + nameContainer2);
					objContainer0.destroy();
					objContainer1.destroy();
					objContainer2.destroy();
				});
			}
			//if (!flagCabecera) {
			//self.getView().getModel("localModel").setProperty("/AgregarDatos", {});
			//}
			self.getView().getModel("localModel").setProperty("/AgregarDatos/DataSet", []);
		},
		/////END ROY JsChart////////
		//Generar PDF
		setPDFCabecera:function(self,doc,value,fechaActual,horaActual){
			var cord_X = 12;
					var cord_y = 8;
					///Inicio Cabecera
					doc.setFontSize(10);
					doc.setFont('arial', 'bold');
					var pic = img;
							doc.addImage(pic, 'JPG', cord_X, cord_y+9, 10, 10);
					doc.text(cord_X+61, cord_y, self.utilController.i18n("txtNombreApp")+value.sFechaApertura+" "+value.sRangoFecha);
					doc.text(cord_X, cord_y+8, value.sSociedad);
					doc.text(cord_X+15, cord_y+13, value.sNombreColaborador);
					doc.text(cord_X+15, cord_y+17, value.sIdUsuario);

					doc.setFontSize(7);
					doc.setFont('arial', 'normal');
					doc.text(cord_X, cord_y+24, self.utilController.i18n("txtSede")+": "+value.sSede);
					doc.text(cord_X, cord_y+28, self.utilController.i18n("txtArea")+": "+value.sArea);
					doc.setFontSize(8);


					doc.text(cord_X+145, cord_y+13, fechaActual);//fechaActual
					doc.text(cord_X+145, cord_y+17, horaActual);
					doc.setFontSize(7);
					doc.text(cord_X+145, cord_y+24, value.sEstado);
					doc.text(cord_X+145, cord_y+28, self.utilController.i18n("txtSupervisor")+": ");
					self.utilController.alignTextJsPDFV2( doc, value.sSupervisor ,cord_X+161,cord_y+28,20,"justify")
					doc.setFont('arial', 'bold');
					///End Cabecera
		},
		setPDFFooter:function(doc,data,value){
			var coordX = data.settings.margin.left;
			var coordY = doc.internal.pageSize.height-15;
			doc.setDrawColor(0, 0, 0);
			doc.line(coordX+22, coordY-15, doc.internal.pageSize.width-(data.settings.margin.right+100), coordY-15);
			doc.setFontSize(8);
			doc.setFont('sans-serif', 'normal');
			doc.setTextColor(0, 0, 0);
			var widthPageMargin	=	doc.internal.pageSize.width - (data.settings.margin.left + data.settings.margin.right);
			doc.text((doc.internal.pageSize.width/2)-62, coordY-12 , value.sFirmaCajaCentral);
			doc.text((doc.internal.pageSize.width/2)-62, coordY-9 , 'NOMBRE');
			doc.text((doc.internal.pageSize.width/2)-47, coordY-9 , ': ' + value.sFirmaNombre);
			doc.text((doc.internal.pageSize.width/2)-62, coordY-6 , value.sTipoDocumento+': '+value.sDNI);//'D.N.I.');
			//doc.text((doc.internal.pageSize.width/2)-47 + value.sTipoDocumento.length + 1, coordY-6 , ': ' + value.sDNI);
			doc.line(coordX+105, coordY-15, doc.internal.pageSize.width-(data.settings.margin.right+20), coordY-15);
			doc.text((doc.internal.pageSize.width/2)+30, coordY-12 , value.sFirmaCajaGeneral);
		},
		setPDFPiePagina:function(doc,totalPagesExp){
			// Contador
			var str = "Pág     " + doc.internal.getNumberOfPages()

			if (typeof doc.putTotalPages === 'function') {
				str = str + " de " + totalPagesExp;
			}
			doc.setFontSize(6);
			var pageSize = doc.internal.pageSize;
			var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
			//doc.text(str, data.settings.margin.left+216, pageHeight - 5, 'right');
		},
		setPDF_FFS_ES_ED_Detalle: function (self,doc,value,fechaActual,horaActual,totalPagesExp) {
			var that = this;
			var arrayLinea	=	[];
			var arrayCampo	=	[];
			var arrayFooter	=	[];
			var arrayFooterTotal	=	[];
			var aData	=	value.aFondoFijoSoles;
			
			// Selecciona solo los campos que se van a mostrar. Autotable pinta todos los campos del arreglo
			aData.forEach(function(obj){
				arrayCampo	=	[];
				arrayCampo.push(obj.sDenominacion);
				arrayCampo.push(obj.iCantidad);
				arrayCampo.push(obj.fImporte);
				arrayCampo.push("");
				arrayLinea.push(arrayCampo);
			});
			// Arma el footer con las sumatorias. Van en "" para que no muestre nada. El autotable requiere la misma cantidad de campos
			
			arrayFooter.push("Total: ");
			arrayFooter.push(value.FFSTotalCantidad);
			arrayFooter.push(value.FFSTotalImporte);
			arrayFooter.push("");
			
			var finalY = 70;
			const headerTable = [
				{ title: "", dataKey: 1 },
				{ title: "", dataKey: 2 },
				{ title: "", dataKey: 3 },
				{ title: "", dataKey: 4 }
			];
			arrayFooterTotal.push(arrayFooter);
			//LEFT
			doc.autoTable(headerTable,arrayLinea,{
				startY: finalY,
				body: arrayLinea,
				theme: 'plain',
				foot: arrayFooterTotal,
				showFoot: 'lastPage',
				showHead: 'never',
				bodyStyles: {valign: 'top'},
				footStyles: {halign: 'right', cellPadding: {top: 3.5, bottom: 1.5, left: 0.1, right: 1}, cellWidth: 0},
				margin: {top: 70, right: 11, bottom: 22, left: 14},
				styles: {
							rowPageBreak: 'auto',
							fontSize:5.8,
							textColor:[0, 0, 0],
							valign: 'top',
							minCellWidth: 1,
							minCellHeight: 7,
							cellPadding: {top: 1.5}
				},
				didDrawPage: function (data) {	// Dibuja la estructura en todas las páginas
					that.setPDFCabecera(self,doc,value,fechaActual,horaActual);

					var coordX = 14;
					var coordY = 65;
					
					// Cabecera
					doc.setFontSize(10);
					doc.setDrawColor(0, 0, 0);
					doc.text(coordX + 32, coordY-16, "FONDO FIJO SOLES");
					doc.setFontSize(7);
					doc.text(coordX, coordY-8, self.utilController.i18n("txtNumFondoAsignado") );
					//doc.rect(coordX+23, coordY-13, 25, coordY-57);
					doc.text(coordX + 30, coordY-8, ': ' + value.sNumeroFondoAsignado);
					doc.setFont('sans-serif', 'bold');
					
					doc.rect(coordX-1, coordY-5, 92, coordY-56);
					doc.text(coordX + 6, coordY, self.utilController.i18n("txtFFSColumna1"));
					doc.text(coordX + 40, coordY, self.utilController.i18n("txtFFSColumna2"));
					doc.text(coordX + 67, coordY, self.utilController.i18n("txtFFSColumna3"));

					//Inicio Línea Foot para el total Dinámico
					var footX = data.table.foot.length>0 ? data.table.foot[0].x+2:null;
					var footY = data.table.foot.length>0 ? data.table.foot[0].y:null;
					if(footX && footY){
						doc.line(footX-2, footY, 105, footY);
					}
					//End Línea Foot para el total Dinámico
					// Footer
				},
				columnStyles:	{
									1: {cellWidth: 17, halign: 'left',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
									2: {cellWidth: 16, halign: 'right',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
									3: {cellWidth: 16, halign: 'right',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
					4: {cellWidth: 51, halign: 'center',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}}
									
									
								}
			});
			///RIGTH
			var arrayLinea2	=	[];
			var arrayCampo2	=	[];
			var arrayFooter2	=	[];
			var arrayFooterTotal2	=	[];
			var aData2	=	value.aEfectivoSoles;
			
			// Selecciona solo los campos que se van a mostrar. Autotable pinta todos los campos del arreglo				
			aData2.forEach(function(obj){
				arrayCampo2	=	[];
				arrayCampo2.push("");
				arrayCampo2.push(obj.sDenominacion);
				arrayCampo2.push(obj.iCantidad);
				arrayCampo2.push(obj.fImporte);
				arrayLinea2.push(arrayCampo2);
			});
			
			// Arma el footer con las sumatorias. Van en "" para que no muestre nada. El autotable requiere la misma cantidad de campos
			
			arrayFooter2.push("");
			arrayFooter2.push("Total: ");
			arrayFooter2.push(value.ESTotalCantidad);
			arrayFooter2.push(value.ESTotalImporte);
			var finalY2 = 70;
			const headerTable2 = [
				{ title: "", dataKey: 1 },
				{ title: "", dataKey: 2 },
				{ title: "", dataKey: 3 },
				{ title: "", dataKey: 4 }
			];
			arrayFooterTotal2.push(arrayFooter2);
			doc.autoTable(headerTable2,arrayLinea2,{
				startY: finalY2,
				body: arrayLinea2,
				theme: 'plain',
				foot: arrayFooterTotal2,
				showFoot: 'lastPage',
				showHead: 'never',
				bodyStyles: {valign: 'top'},
				footStyles: {halign: 'right', cellPadding: {top: 3.5, bottom: 1.5, left: 0.1, right: 1}, cellWidth: 0},
				margin: {top: 70, right: 11, bottom: 22, left: 14},
				styles: {
							rowPageBreak: 'auto',
							fontSize:5.8,
							textColor:[0, 0, 0],
							valign: 'top',
							minCellWidth: 1,
							minCellHeight: 7,
							cellPadding: {top: 1.5}
				},
				didDrawPage: function (data) {	// Dibuja la estructura en todas las páginas
					that.setPDFCabecera(self,doc,value,fechaActual,horaActual);

					var coordX = 108;
					var coordY = 65;
					
					// Cabecera
					doc.setFontSize(10);
					doc.text(coordX + 30, coordY-16, "EFECTIVO SOLES");
					doc.setFontSize(7);
					doc.setFont('sans-serif', 'bold');
					doc.setDrawColor(0, 0, 0);
					doc.rect(coordX-1, coordY-5, 92, 9);
					doc.text(coordX + 6, coordY, self.utilController.i18n("txtFFSColumna1"));
					doc.text(coordX + 40, coordY, self.utilController.i18n("txtFFSColumna2"));
					doc.text(coordX + 67, coordY, self.utilController.i18n("txtFFSColumna3"));

					//Inicio Línea Foot para el total Dinámico
					var footX = data.table.foot.length>0 ? data.table.foot[0].x+2:null;
					var footY = data.table.foot.length>0 ? data.table.foot[0].y:null;
					if(footX && footY){
						doc.line(footX+92, footY, 198, footY);
					}
					//End Línea Foot para el total Dinámico
					// Footer
				},
				columnStyles:	{
					1: {cellWidth: 51, halign: 'center',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
									2: {cellWidth: 17, halign: 'left',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
									3: {cellWidth: 16, halign: 'right',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
									4: {cellWidth: 16, halign: 'right',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}}
					
									
									
								}
			});
			
			///CENTER
			var arrayLinea3	=	[];
			var arrayCampo3	=	[];
			var arrayFooter3	=	[];
			var arrayFooterTotal3	=	[];
			var aData3	=	value.aEfectivoDolares;
			
			// Selecciona solo los campos que se van a mostrar. Autotable pinta todos los campos del arreglo
			aData3.forEach(function(obj){
				arrayCampo3	=	[];
				arrayCampo3.push("");
				arrayCampo3.push(obj.sDenominacion);
				arrayCampo3.push(obj.iCantidad);
				arrayCampo3.push(obj.fImporte);
				arrayCampo3.push("");
				arrayLinea3.push(arrayCampo3);
			});
			// Arma el footer con las sumatorias. Van en "" para que no muestre nada. El autotable requiere la misma cantidad de campos
			
			arrayFooter3.push("");
			arrayFooter3.push("Total: ");
			arrayFooter3.push(value.EDTotalCantidad);
			arrayFooter3.push(value.EDTotalImporte);
			arrayFooter3.push("");
			var finalY3 = 184;
			const headerTable3 = [
				{ title: "", dataKey: 1 },
				{ title: "", dataKey: 2 },
				{ title: "", dataKey: 3 },
				{ title: "", dataKey: 4 },
				{ title: "", dataKey: 5 }
			];
			arrayFooterTotal3.push(arrayFooter3);
			doc.autoTable(headerTable3,arrayLinea3,{
				startY: finalY3,
				body: arrayLinea3,
				theme: 'plain',
				foot: arrayFooterTotal3,
				showFoot: 'lastPage',
				showHead: 'never',
				bodyStyles: {valign: 'top'},
				footStyles: {halign: 'right', cellPadding: {top: 3.5, bottom: 1.5, left: 0.1, right: 1}, cellWidth: 0},
				margin: {top: 70, right: 11, bottom: 22, left: 14},
				styles: {
							rowPageBreak: 'auto',
							fontSize:5.8,
							textColor:[0, 0, 0],
							valign: 'top',
							minCellWidth: 1,
							minCellHeight: 7,
							cellPadding: {top: 1.5}
				},
				didDrawPage: function (data) {	// Dibuja la estructura en todas las páginas
					that.setPDFCabecera(self,doc,value,fechaActual,horaActual);

					var coordX = 60;
					var coordY = 179;
					
					// Cabecera
					doc.setFontSize(10);
					doc.text(coordX + 30, coordY-12, "EFECTIVO DOLARES");
					doc.setFontSize(7);
					doc.setFont('sans-serif', 'bold');
					doc.setDrawColor(0, 0, 0);
					doc.rect(coordX, coordY-5, 92, 9);
					doc.text(coordX + 10, coordY, self.utilController.i18n("txtEDColumna1"));
					doc.text(coordX + 42, coordY, self.utilController.i18n("txtEDColumna2"));
					doc.text(coordX + 70, coordY, self.utilController.i18n("txtEDColumna3"));

					//Inicio Línea Foot para el total Dinámico
					var footX = data.table.foot.length>0 ? data.table.foot[0].x+2:null;
					var footY = data.table.foot.length>0 ? data.table.foot[0].y:null;
					if(footX && footY){
						doc.line(footX+45, footY, 152, footY);
					}
					//End Línea Foot para el total Dinámico
					
					// Footer
					that.setPDFFooter(doc,data,value);
					that.setPDFPiePagina(doc,totalPagesExp);
				},
				columnStyles:	{
					1: {cellWidth: 25.5, halign: 'center',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
									2: {cellWidth: 17, halign: 'left',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
									3: {cellWidth: 16, halign: 'right',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
									4: {cellWidth: 16, halign: 'right',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
					5: {cellWidth: 25.5, halign: 'center',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}}	
									
								}
			});
			// Imprime el numero total de paginas del PDF
			if (typeof doc.putTotalPages === 'function') {
				doc.putTotalPages(totalPagesExp);
			}
		},
		setPDFResumenDetalle : function (self,doc,value,fechaActual,horaActual,totalPagesExp) {
			var that = this;
			var arrayLinea	=	[];
			var arrayCampo	=	[];
			var arrayFooter	=	[];
			var arrayFooterTotal	=	[];
			var aData	=	value.aResumenTipoDocumento;
			
			// Selecciona solo los campos que se van a mostrar. Autotable pinta todos los campos del arreglo	
			aData.forEach(function(obj){
				arrayCampo	=	[];
				arrayCampo.push(obj.sDenominacion);
				arrayCampo.push(obj.iCantidad);
				arrayCampo.push(obj.fImporte);
				arrayCampo.push("");
				arrayLinea.push(arrayCampo);
			});
			// Arma el footer con las sumatorias. Van en "" para que no muestre nada. El autotable requiere la misma cantidad de campos
			
			arrayFooter.push("Total: ");
			arrayFooter.push(value.RTDTotalCantidad);
			arrayFooter.push(value.RTDTotalImporte);
			arrayFooter.push("");
			
			var finalY = 70;
			const headerTable = [
				{ title: "", dataKey: 1 },
				{ title: "", dataKey: 2 },
				{ title: "", dataKey: 3 },
				{ title: "", dataKey: 4 }
			];
			arrayFooterTotal.push(arrayFooter);
			//LEFT
			doc.autoTable(headerTable,arrayLinea,{
				startY: finalY,
				body: arrayLinea,
				theme: 'plain',
				foot: arrayFooterTotal,
				showFoot: 'lastPage',
				showHead: 'never',
				bodyStyles: {valign: 'top'},
				footStyles: {halign: 'right', cellPadding: {top: 3.5, bottom: 1.5, left: 0.1, right: 1}, cellWidth: 0},
				margin: {top: 70, right: 11, bottom: 22, left: 14},
				styles: {
							rowPageBreak: 'auto',
							fontSize:5.8,
							textColor:[0, 0, 0],
							valign: 'top',
							minCellWidth: 1,
							minCellHeight: 2,
							cellPadding: {top: 1.5}
				},
				didDrawPage: function (data) {	// Dibuja la estructura en todas las páginas
					that.setPDFCabecera(self,doc,value,fechaActual,horaActual);

					var coordX = 14;
					var coordY = 65;
					
					// Cabecera
					doc.setFontSize(10);
					doc.setDrawColor(0, 0, 0);
					doc.text(coordX + 13, coordY-16, "INGRESOS POR TIPO DE DOCUMENTO");
					doc.setFontSize(7);
					doc.setFont('sans-serif', 'bold');
					
					doc.rect(coordX-1, coordY-5, 92, coordY-56);
					doc.text(coordX + 6, coordY, self.utilController.i18n("txtTDColumna1"));
					doc.text(coordX + 40, coordY, self.utilController.i18n("txtTDColumna2"));
					doc.text(coordX + 69, coordY, self.utilController.i18n("txtTDColumna3"));

					//Inicio Línea Foot para el total Dinámico
					var footX = data.table.foot.length>0 ? data.table.foot[0].x+2:null;
					var footY = data.table.foot.length>0 ? data.table.foot[0].y:null;
					if(footX && footY){
						doc.line(footX-2, footY, 105, footY);
					}
					//End Línea Foot para el total Dinámico

					// Footer
					
				},
				columnStyles:	{
									1: {cellWidth: 17, halign: 'left',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
									2: {cellWidth: 16, halign: 'right',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
									3: {cellWidth: 16, halign: 'right',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
					4: {cellWidth: 51, halign: 'center',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}}
									
									
								}
			});
			///RIGTH
			var arrayLinea2	=	[];
			var arrayCampo2	=	[];
			var arrayFooter2	=	[];
			var arrayFooterTotal2	=	[];
			var aData2	=	value.aResumenMedioPago;
			
			// Selecciona solo los campos que se van a mostrar. Autotable pinta todos los campos del arreglo	
			aData2.forEach(function(obj){
				arrayCampo2	=	[];
				arrayCampo2.push("");
				arrayCampo2.push(obj.sDenominacion);
				arrayCampo2.push(obj.iCantidad);
				arrayCampo2.push(obj.fImporte);
				arrayLinea2.push(arrayCampo2);
			});
			// Arma el footer con las sumatorias. Van en "" para que no muestre nada. El autotable requiere la misma cantidad de campos
			
			arrayFooter2.push("");
			arrayFooter2.push("Total: ");
			arrayFooter2.push(value.RMPTotalCantidad);
			arrayFooter2.push(value.RMPTotalImporte);
			var finalY2 = 70;
			const headerTable2 = [
				{ title: "", dataKey: 1 },
				{ title: "", dataKey: 2 },
				{ title: "", dataKey: 3 },
				{ title: "", dataKey: 4 }
			];
			arrayFooterTotal2.push(arrayFooter2);
			doc.autoTable(headerTable2,arrayLinea2,{
				startY: finalY2,
				body: arrayLinea2,
				theme: 'plain',
				foot: arrayFooterTotal2,
				showFoot: 'lastPage',
				showHead: 'never',
				bodyStyles: {valign: 'top'},
				footStyles: {halign: 'right', cellPadding: {top: 3.5, bottom: 1.5, left: 0.1, right: 1}, cellWidth: 0},
				margin: {top: 70, right: 11, bottom: 22, left: 14},
				styles: {
							rowPageBreak: 'auto',
							fontSize:5.8,
							textColor:[0, 0, 0],
							valign: 'top',
							minCellWidth: 1,
							minCellHeight: 2,
							cellPadding: {top: 1.5}
				},
				didDrawPage: function (data) {	// Dibuja la estructura en todas las páginas
					that.setPDFCabecera(self,doc,value,fechaActual,horaActual);

					var coordX = 108;
					var coordY = 65;
					
					// Cabecera
					doc.setFontSize(10);
					doc.text(coordX + 17, coordY-16, "INGRESOS POR MEDIO DE PAGO");
					doc.setFontSize(7);
					doc.setFont('sans-serif', 'bold');
					doc.setDrawColor(0, 0, 0);
					doc.rect(coordX-1, coordY-5, 92, 9);
					doc.text(coordX + 6, coordY, self.utilController.i18n("txtMPColumna1"));
					doc.text(coordX + 40, coordY, self.utilController.i18n("txtMPColumna2"));
					doc.text(coordX + 69, coordY, self.utilController.i18n("txtMPColumna3"));

					//Inicio Línea Foot para el total Dinámico
					var footX = data.table.foot.length>0 ? data.table.foot[0].x+2:null;
					var footY = data.table.foot.length>0 ? data.table.foot[0].y:null;
					if(footX && footY){
						doc.line(footX+92, footY, 198, footY);
					}
					//End Línea Foot para el total Dinámico
					
					// Ubica donde se va a pintar el contador de paginas
					doc.setFontSize(6);
					// Footer
					var middle = (doc.internal.pageSize.width/2);
					var coordY = doc.internal.pageSize.height-15;
					doc.text(middle-62, coordY-60 , self.utilController.i18n("txtAjusteRedondeo"));
					doc.text(middle-40, coordY-60 , ': ' + value.ResumenTotalAjusteRedondeo);
					doc.text(middle-62, coordY-56 , self.utilController.i18n("txtSobrante"));
					doc.text(middle-47, coordY-56 , ': ' + value.ResumenTotalSobrante);
					doc.text(middle-62, coordY-52 , self.utilController.i18n("txtFaltante"));
					doc.text(middle-47, coordY-52 , ': ' + value.ResumenTotalFaltante);
					doc.text(middle-62, coordY-48 , self.utilController.i18n("txtReposicionFondo"));
					doc.text(middle-47, coordY-48 , '             : ' + value.ResumenReposicionFondo);

					that.setPDFFooter(doc,data,value);
					that.setPDFPiePagina(doc,totalPagesExp);
					
				},
				columnStyles:	{
					1: {cellWidth: 51, halign: 'center',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
									2: {cellWidth: 17, halign: 'left',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
									3: {cellWidth: 16, halign: 'right',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
									4: {cellWidth: 16, halign: 'right',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}}
					
									
									
								}
			});
			
			// Imprime el numero total de paginas del PDF
			if (typeof doc.putTotalPages === 'function') {
				doc.putTotalPages(totalPagesExp);
			}
		},
		setPDFCustodiosDetalle : function (self,doc,value,fechaActual,horaActual,totalPagesExp) {
			var that = this;
			///CENTER
			var arrayLinea	=	[];
			var arrayCampo	=	[];
			var arrayFooter	=	[];
			var arrayFooterTotal	=	[];
			var aData	=	value.aCustodios;
			
			// Selecciona solo los campos que se van a mostrar. Autotable pinta todos los campos del arreglo			
			aData.forEach(function(obj){
				arrayCampo	=	[];
				arrayCampo.push(obj.iItem);
				arrayCampo.push(obj.sCodigo);
				arrayCampo.push(obj.sFecha);
				arrayCampo.push(obj.sEstado);
				arrayCampo.push(obj.sConcepto);
				arrayCampo.push(obj.sUsuarioCustodia);
				arrayCampo.push(obj.fImportePEN);
				arrayCampo.push(obj.fImporteUSD);
				arrayLinea.push(arrayCampo);
			});
			// Arma el footer con las sumatorias. Van en "" para que no muestre nada. El autotable requiere la misma cantidad de campos
			
			var finalY = 70;
			const headerTable = [
				{ title: "", dataKey: 1 },
				{ title: "", dataKey: 2 },
				{ title: "", dataKey: 3 },
				{ title: "", dataKey: 4 },
				{ title: "", dataKey: 5 },
				{ title: "", dataKey: 6 },
				{ title: "", dataKey: 7 },
				{ title: "", dataKey: 8 }
			];
			doc.autoTable(headerTable,arrayLinea,{
				startY: finalY,
				body: arrayLinea,
				theme: 'plain',
				foot: arrayFooterTotal,
				showFoot: 'lastPage',
				showHead: 'never',
				bodyStyles: {valign: 'top'},
				footStyles: {halign: 'center', cellPadding: {top: 3.5, bottom: 1.5, left: 0.2, right: 0.2}, cellWidth: 0},
				margin: {top: 70, right: 11, bottom: 22, left: 14},
				styles: {
							rowPageBreak: 'auto',
							fontSize:5.8,
							textColor:[0, 0, 0],
							valign: 'top',
							minCellWidth: 1,
							minCellHeight: 7,
							cellPadding: {top: 1.5}
				},
				didDrawPage: function (data) {	// Dibuja la estructura en todas las páginas
					that.setPDFCabecera(self,doc,value,fechaActual,horaActual);

					var coordX = 14;
					var coordY = 65;
					
					// Cabecera
					doc.setFontSize(10);
					doc.text(coordX + 83, coordY-16, "CUSTODIOS");
					doc.setFontSize(7);
					doc.setFont('sans-serif', 'bold');
					doc.setDrawColor(0, 0, 0);
					doc.rect(coordX, coordY-5, 185, 9);
					doc.text(coordX+3, coordY, self.utilController.i18n("txtCColumna1"));
					doc.text(coordX + 19, coordY, self.utilController.i18n("txtCColumna2"));
					doc.text(coordX + 42, coordY, self.utilController.i18n("txtCColumna3"));
					doc.text(coordX + 65, coordY, self.utilController.i18n("txtCColumna4"));
					doc.text(coordX + 95, coordY, self.utilController.i18n("txtCColumna5"));
					doc.text(coordX + 124, coordY, self.utilController.i18n("txtCColumna6"));
					doc.text(coordX + 156, coordY-1.5, (self.utilController.i18n("txtCColumna7")).split(" ")[0]);
					doc.text(coordX + 157, coordY+1.5, (self.utilController.i18n("txtCColumna7")).split(" ")[1]);
					doc.text(coordX + 172, coordY-1.5, (self.utilController.i18n("txtCColumna8")).split(" ")[0]);
					doc.text(coordX + 173, coordY+1.5, (self.utilController.i18n("txtCColumna8")).split(" ")[1]);

					// Footer
					that.setPDFFooter(doc,data,value);
					that.setPDFPiePagina(doc,totalPagesExp);
				},
				columnStyles:	{
					1: {cellWidth: 9, halign: 'left',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 4,
						right: 1
					}},
					2: {cellWidth: 8, halign: 'left',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
					3: {cellWidth: 11.5, halign: 'left',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
					4: {cellWidth: 12, halign: 'left',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
					5: {cellWidth: 13, halign: 'left',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
					6: {cellWidth: 12, halign: 'left',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
					7: {cellWidth: 7, halign: 'right',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
					8: {cellWidth: 7, halign: 'right',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}}
					
									
									
								}
			});
			// Imprime el numero total de paginas del PDF
			if (typeof doc.putTotalPages === 'function') {
				doc.putTotalPages(totalPagesExp);
			}
		},

		setPDFTrasladosDetalle : function (self,doc,value,fechaActual,horaActual,totalPagesExp) {
			var that = this;
			///CENTER
			var arrayLinea	=	[];
			var arrayCampo	=	[];
			var arrayFooter	=	[];
			var arrayFooterTotal	=	[];
			var aData	=	value.aTraslados;
			
			// Selecciona solo los campos que se van a mostrar. Autotable pinta todos los campos del arreglo			
			aData.forEach(function(obj){
				arrayCampo	=	[];
				arrayCampo.push(obj.iItem);
				arrayCampo.push(obj.sCodigo);
				arrayCampo.push(obj.sFecha);
				arrayCampo.push(obj.fImportePEN);
				arrayCampo.push(obj.fImporteUSD);
				arrayLinea.push(arrayCampo);
			});
			// Arma el footer con las sumatorias. Van en "" para que no muestre nada. El autotable requiere la misma cantidad de campos
			
			var finalY = 70;
			const headerTable = [
				{ title: "", dataKey: 1 },
				{ title: "", dataKey: 2 },
				{ title: "", dataKey: 3 },
				{ title: "", dataKey: 4 },
				{ title: "", dataKey: 5 }
			];
			doc.autoTable(headerTable,arrayLinea,{
				startY: finalY,
				body: arrayLinea,
				theme: 'plain',
				foot: arrayFooterTotal,
				showFoot: 'lastPage',
				showHead: 'never',
				bodyStyles: {valign: 'top'},
				footStyles: {halign: 'center', cellPadding: {top: 3.5, bottom: 1.5, left: 0.2, right: 0.2}, cellWidth: 0},
				margin: {top: 70, right: 11, bottom: 22, left: 14},
				styles: {
							rowPageBreak: 'auto',
							fontSize:5.8,
							textColor:[0, 0, 0],
							valign: 'top',
							minCellWidth: 1,
							minCellHeight: 7,
							cellPadding: {top: 1.5}
				},
				didDrawPage: function (data) {	// Dibuja la estructura en todas las páginas
					that.setPDFCabecera(self,doc,value,fechaActual,horaActual);

					var coordX = 14;
					var coordY = 65;
					
					// Cabecera
					doc.setFontSize(10);
					doc.text(coordX + 83, coordY-16, "TRASLADOS");
					doc.setFontSize(7);
					doc.setFont('sans-serif', 'bold');
					doc.setDrawColor(0, 0, 0);
					doc.rect(coordX, coordY-5, 185, 9);
					doc.text(coordX+3, coordY, self.utilController.i18n("txtTColumna1"));
					doc.text(coordX + 19, coordY, self.utilController.i18n("txtTColumna2"));
					doc.text(coordX + 42, coordY, self.utilController.i18n("txtTColumna3"));
					doc.text(coordX + 75, coordY, self.utilController.i18n("txtTColumna4"));
					doc.text(coordX + 112, coordY, self.utilController.i18n("txtTColumna5"));
					// Footer
					that.setPDFFooter(doc,data,value);
					that.setPDFPiePagina(doc,totalPagesExp);
				},
				columnStyles:	{
					1: {cellWidth: 3, halign: 'center',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
					2: {cellWidth: 4, halign: 'left',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
					3: {cellWidth: 7, halign: 'left',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
					4: {cellWidth: 7, halign: 'left',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}},
					5: {cellWidth: 12, halign: 'left',cellPadding: {
						top: 1.5,
						bottom: 1.5,
						left: 0.1,
						right: 1
					}}
					
									
									
								}
			});
			// Imprime el numero total de paginas del PDF
			if (typeof doc.putTotalPages === 'function') {
				doc.putTotalPages(totalPagesExp);
			}
		},
		setCabeceraTablaDocumentos:function(self,doc,pageCoordY){
			var coordX = coordDocX-5;
			var coordY = pageCoordY;
				
			// Cabecera
			doc.setFontSize(10);
			//doc.text(coordX + 5, coordY-8, self.utilController.i18n("txtD1"));
			doc.setFontSize(7);
			doc.setFont('sans-serif', 'bold');
			doc.setDrawColor(0, 0, 0);
			doc.rect(coordX+8, coordY-5, 195, 9);
			//doc.text(coordX+3, coordY, self.utilController.i18n("txtDColumna1"));

			doc.text(coordX + 14, coordY-1.5, (self.utilController.i18n("txtDColumna2").split(" ")[0]));
			doc.text(coordX + 14, coordY+1.5, (self.utilController.i18n("txtDColumna2").split(" ")[1]));

			doc.text(coordX + 28, coordY-1.5, (self.utilController.i18n("txtDColumna3").split(" ")[0]));
			doc.text(coordX + 27, coordY+1.5, (self.utilController.i18n("txtDColumna3").split(" ")[1]));

			doc.text(coordX + 47, coordY, self.utilController.i18n("txtDColumna4"));
			doc.text(coordX + 73, coordY, self.utilController.i18n("txtDColumna5"));
			
			doc.text(coordX + 96, coordY, self.utilController.i18n("txtDColumna6"));

			doc.text(coordX + 109, coordY-1.5, (self.utilController.i18n("txtDColumna7").split(" ")[0]));
			doc.text(coordX + 109, coordY+1.5, (self.utilController.i18n("txtDColumna7").split(" ")[1]));

			doc.text(coordX + 124, coordY-1.5, (self.utilController.i18n("txtDColumna8").split(" ")[0]));
			doc.text(coordX + 124, coordY+1.5, (self.utilController.i18n("txtDColumna8").split(" ")[1]));

			doc.text(coordX + 138, coordY, self.utilController.i18n("txtDColumna9"));

			doc.text(coordX + 150, coordY-1.5, (self.utilController.i18n("txtDColumna10").split(" ")[0]));
			doc.text(coordX + 149, coordY+1.5, (self.utilController.i18n("txtDColumna10").split(" ")[1]));

			doc.text(coordX + 161, coordY, self.utilController.i18n("txtDColumna11"));

			doc.text(coordX + 178, coordY-1.5, (self.utilController.i18n("txtDColumna12").split(" ")[0]));
			doc.text(coordX + 178, coordY+1.5, (self.utilController.i18n("txtDColumna12").split(" ")[1]));

			doc.text(coordX + 192, coordY-1.5, (self.utilController.i18n("txtDColumna13").split(" ")[0]));
			doc.text(coordX + 193, coordY+1.5, (self.utilController.i18n("txtDColumna13").split(" ")[1]));
		},
		setHeaderAutoTable:function(totalImporte){
			var modData = {};
			const headerTable = [
				{ title: "", dataKey: 1 },
				{ title: "", dataKey: 2 },
				{ title: "", dataKey: 3 },
				{ title: "", dataKey: 4 },
				{ title: "", dataKey: 5 },
				{ title: "", dataKey: 6 },
				{ title: "", dataKey: 7 },
				{ title: "", dataKey: 8 },
				{ title: "", dataKey: 9 },
				{ title: "", dataKey: 10 },
				{ title: "", dataKey: 11 },
				{ title: "", dataKey: 12 }
				//{ title: "", dataKey: 13 }
			];
			var arrayFooter = [];
			var arrayFooterTotal = [];
			arrayFooter.push("Total: ");
			//arrayFooter.push("");
			arrayFooter.push("");
			arrayFooter.push("");
			arrayFooter.push("");
			arrayFooter.push("");
			arrayFooter.push("");
			arrayFooter.push("");
			arrayFooter.push("");
			arrayFooter.push("");
			arrayFooter.push(totalImporte);
			arrayFooter.push("");
			arrayFooter.push("");
			arrayFooterTotal.push(arrayFooter);

			modData.headerTable = headerTable;
			modData.arrayFooterTotal = arrayFooterTotal;
			return modData;
		},
		setTamanioCellDocumentos:function(){
			return {
				/*1: {cellWidth: 10, halign: 'center',
					cellPadding: {top: 1.5,bottom: 1.5,left: 0.1,right: 1}},*/
				1: {cellWidth: 12, halign: 'left',
					cellPadding: {top: 1.5,bottom: 1.5,left: 0.1,right: 1}},
				2: {cellWidth: 16, halign: 'left',
					cellPadding: {top: 1.5,bottom: 1.5,left: 0.1,right: 1}},
				3: {cellWidth: 27, halign: 'left',
					cellPadding: {top: 1.5,bottom: 1.5,left: 0.1,right: 1}},
				4: {cellWidth: 27, halign: 'left',
					cellPadding: {top: 1.5,bottom: 1.5,left: 0.1,right: 1}},
				5: {cellWidth: 13, halign: 'left',
					cellPadding: {top: 1.5,bottom: 1.5,left: 0.1,right: 1}},
				6: {cellWidth: 10, halign: 'left',
					cellPadding: {top: 1.5,bottom: 1.5,left: 0.1,right: 1}},
				7: {cellWidth: 20, halign: 'left',
					cellPadding: {top: 1.5,bottom: 1.5,left: 0.1,right: 1}},
				8: {cellWidth: 10, halign: 'left',
					cellPadding: {top: 1.5,bottom: 1.5,left: 0.1,right: 1}},
				9: {cellWidth: 10, halign: 'right',
					cellPadding: {top: 1.5,bottom: 1.5,left: 0.1,right: 1}},
				10: {cellWidth: 15, halign: 'right',
					cellPadding: {top: 1.5,bottom: 1.5,left: 0.1,right: 1}},
				11: {cellWidth: 15, halign: 'left',
					cellPadding: {top: 1.5,bottom: 1.5,left: 0.1,right: 1}},
				12: {cellWidth: 15, halign: 'left',
					cellPadding: {top: 1.5,bottom: 1.5,left: 0.1,right: 1}}
				};

		},
		setRowDocumentos:function(aData){
			var arrayLinea = [];
			aData.forEach(function(obj){
				var arrayCampo	=	[];
				//arrayCampo.push(obj.iItem);
				arrayCampo.push(obj.sTipoDocumento);
				arrayCampo.push(obj.sNumeroSunat);
				arrayCampo.push(obj.sPaciente);
				arrayCampo.push(obj.sCliente);
				arrayCampo.push(obj.sEstado);
				arrayCampo.push(obj.sTipoDocumentoIdentidad);
				arrayCampo.push(obj.sNumeroIdentidad);
				arrayCampo.push(obj.sMoneda);
				arrayCampo.push(obj.fTipoCambio);
				arrayCampo.push(obj.fImporte);
				arrayCampo.push(obj.sMotivoAnulacion);
				arrayCampo.push(obj.sFormaPago);
				arrayLinea.push(arrayCampo);
			});
			return arrayLinea;
		},
		setPDFDocumentosGlobalDetalle : function (self,doc,value,fechaActual,horaActual,totalPagesExp,aValue,fTotalImporte,tituloTabla,ultimoTipoDocumento) {
			var that = this;
			var coordDocYFinal= 0;
			///CENTER
			var arrayLinea	=	[];
			var arrayCampo	=	[];
			var arrayFooter	=	[];
			var arrayFooterTotal	=	[];
			var aData	=	aValue;
			
			// Selecciona solo los campos que se van a mostrar. Autotable pinta todos los campos del arreglo				
			arrayLinea = that.setRowDocumentos(aData);
			
			// Arma el footer con las sumatorias. Van en "" para que no muestre nada. El autotable requiere la misma cantidad de campos
			
			var finalY = coordDocY+5;
			var headerAutotable = that.setHeaderAutoTable(fTotalImporte);
			
			const headerTable = headerAutotable.headerTable;
			arrayFooterTotal = headerAutotable.arrayFooterTotal;
			doc.autoTable(headerTable,arrayLinea,{
				startY: finalY,
				body: arrayLinea,
				theme: 'plain',
				foot: arrayFooterTotal,
				showFoot: 'lastPage',
				showHead: 'never',
				bodyStyles: {valign: 'top'},
				footStyles: {halign: 'right', cellPadding: {top: 3.5, bottom: 1.5, left: 0.2, right: 0.2}, cellWidth: 0},
				margin: {top: 70, right: 8, bottom: 70, left: 11},
				styles: {
							rowPageBreak: 'auto',
							fontSize:5.8,
							textColor:[0, 0, 0],
							valign: 'top',
							minCellWidth: 1,
							minCellHeight: 7,
							cellPadding: {top: 1.5}
				},
				didDrawPage: function (data) {	// Dibuja la estructura en todas las páginas
					//coordDocYFinal = coordDocY;
					//if(coordDocY>205){
					//	coordDocY = 65;
					//}			
					var bodyX = data.table.body.length>0 ? data.table.body[data.table.body.length-1].x:null;
					var bodyY = data.table.body.length>0 ? ( (finalY + (6*(data.table.body.length+1)) ) % 145) :null;
					var filas = [];
					
					if(data.pageNumber==1){
						if(coordDocY<=228){//205
							doc.text(coordDocX + 5, data.table.pageStartY-12, tituloTabla);
							that.setCabeceraTablaDocumentos(self,doc,data.table.pageStartY-4);
						}else{
							if(bodyX && bodyY){
								doc.text(bodyX + 5, bodyY-8, tituloTabla);
								that.setCabeceraTablaDocumentos(self,doc,bodyY);	
							}
						}
					}else if(data.pageNumber>1){
						bodyY = null;
						var cont = 1;

						//var modBody = utilController.unlink(data.table.body);
						//modBody = utilController.orderByGeneral(modBody,["index"],"DESC","Numeric");

						if(data.table.body.length>0){

							var coordYAnterior = data.table.body[0].y;
							data.table.body.forEach(function(item){
								if(coordYAnterior>item.y){
									cont = cont + 1;
								}
								var obj = {y:item.y,page:cont};
								if(item.y){
								    filas.push(obj);
								}
								if(item.y){
								    coordYAnterior = item.y;
								}
							});
						}
						var modNumeroFilasPage = filas.filter(function(el){
							return el.page == data.pageNumber;
						});
						if(data.table.body.length>0){
							if(modNumeroFilasPage.length>0){
								bodyY = (modNumeroFilasPage[0].y - 8) ;//+ (6*(modNumeroFilasPage.length)) );	
							}
						} 
						if(bodyY){
							var modCoordY = bodyY;
							//doc.text(coordDocX + 5, coordDocY-8, tituloTabla);
						    that.setCabeceraTablaDocumentos(self,doc,modCoordY);	
						}
					}
					that.setPDFCabecera(self,doc,value,fechaActual,horaActual);
					// Footer
					that.setPDFFooter(doc,data,value);
					that.setPDFPiePagina(doc,totalPagesExp);

					//Inicio Línea Foot para el total Dinámico
					var footX = data.table.foot.length>0 ? data.table.foot[0].x+2:null;
					var footY = data.table.foot.length>0 ? data.table.foot[0].y:null;
					if(footX && footY){
						doc.line(footX-4, footY, 202, footY);
					}
					if(footY){
					    coordDocY = footY+25;
					}
					//coordDocYFinal = coordDocY;
					//if(coordDocY>213){
				   if(coordDocY>228){//205
						if(!ultimoTipoDocumento){
							coordDocY = 65;
							doc.addPage();
						}
					}
				},
				columnStyles:	that.setTamanioCellDocumentos()
			});
			// Imprime el numero total de paginas del PDF
			if (typeof doc.putTotalPages === 'function') {
				doc.putTotalPages(totalPagesExp);
			}
		},
		/*setPDFDocumentosNotaCreditoAplicadaDetalle : function (self,doc,value,fechaActual,horaActual,totalPagesExp) {
			var that = this;
			///CENTER
			var arrayLinea	=	[];
			var arrayCampo	=	[];
			var arrayFooter	=	[];
			var arrayFooterTotal	=	[];
			var aData	=	value.aDocumentosNotaCreditoAplicada;
			
			// Selecciona solo los campos que se van a mostrar. Autotable pinta todos los campos del arreglo				
			arrayLinea = that.setRowDocumentos(aData);
			
			// Arma el footer con las sumatorias. Van en "" para que no muestre nada. El autotable requiere la misma cantidad de campos
			
			var finalY = coordDocY+5;
			var headerAutotable = that.setHeaderAutoTable(value.TotalImporteNotaCreditoAplicada);
			const headerTable = headerAutotable.headerTable;
			arrayFooterTotal = headerAutotable.arrayFooterTotal;
			doc.autoTable(headerTable,arrayLinea,{
				startY: finalY,
				body: arrayLinea,
				theme: 'plain',
				foot: arrayFooterTotal,
				showFoot: 'lastPage',
				showHead: 'never',
				bodyStyles: {valign: 'top'},
				footStyles: {halign: 'right', cellPadding: {top: 3.5, bottom: 1.5, left: 0.2, right: 0.2}, cellWidth: 0},
				margin: {top: 70, right: 11, bottom: 70, left: 5},
				styles: {
							rowPageBreak: 'auto',
							fontSize:5.8,
							textColor:[0, 0, 0],
							valign: 'top',
							minCellWidth: 1,
							minCellHeight: 7,
							cellPadding: {top: 1.5}
				},
				didDrawPage: function (data) {	// Dibuja la estructura en todas las páginas
					that.setPDFCabecera(self,doc,value,fechaActual,horaActual);

					that.setCabeceraTablaDocumentos(self,doc);
			
					// Footer
					that.setPDFFooter(doc,data,value);
					that.setPDFPiePagina(doc,totalPagesExp);
					//Inicio Línea Foot para el total Dinámico
					var footX = data.table.foot.length>0 ? data.table.foot[0].x+2:null;
					var footY = data.table.foot.length>0 ? data.table.foot[0].y:null;
					if(footX && footY){
						doc.line(footX+2, footY, 204, footY);
					}
					coordDocY = footY+25;
					if(coordDocY>214){
						coordDocY = 70;
						doc.addPage();
					}
				},
				columnStyles:	that.setTamanioCellDocumentos()
			});
			// Imprime el numero total de paginas del PDF
			if (typeof doc.putTotalPages === 'function') {
				doc.putTotalPages(totalPagesExp);
			}
		},
		setPDFDocumentosOtrosDetalle : function (self,doc,value,fechaActual,horaActual,totalPagesExp) {
			var that = this;
			///CENTER
			var arrayLinea	=	[];
			var arrayCampo	=	[];
			var arrayFooter	=	[];
			var arrayFooterTotal	=	[];
			var aData	=	value.aDocumentosOtros;
			
			// Selecciona solo los campos que se van a mostrar. Autotable pinta todos los campos del arreglo				
			arrayLinea = that.setRowDocumentos(aData);
			
			// Arma el footer con las sumatorias. Van en "" para que no muestre nada. El autotable requiere la misma cantidad de campos
			
			var finalY = coordDocY+5;
			var headerAutotable = that.setHeaderAutoTable(value.TotalImporteOtros);
			const headerTable = headerAutotable.headerTable;
			arrayFooterTotal = headerAutotable.arrayFooterTotal;
			doc.autoTable(headerTable,arrayLinea,{
				startY: finalY,
				body: arrayLinea,
				theme: 'plain',
				foot: arrayFooterTotal,
				showFoot: 'lastPage',
				showHead: 'never',
				bodyStyles: {valign: 'top'},
				footStyles: {halign: 'right', cellPadding: {top: 3.5, bottom: 1.5, left: 0.2, right: 0.2}, cellWidth: 0},
				margin: {top: 70, right: 11, bottom: 70, left: 5},
				styles: {
							rowPageBreak: 'auto',
							fontSize:5.8,
							textColor:[0, 0, 0],
							valign: 'top',
							minCellWidth: 1,
							minCellHeight: 7,
							cellPadding: {top: 1.5}
				},
				didDrawPage: function (data) {	// Dibuja la estructura en todas las páginas
					that.setPDFCabecera(self,doc,value,fechaActual,horaActual);

					that.setCabeceraTablaDocumentos(self,doc);
			
					// Footer
					that.setPDFFooter(doc,data,value);
					that.setPDFPiePagina(doc,totalPagesExp);
					//Inicio Línea Foot para el total Dinámico
					var footX = data.table.foot.length>0 ? data.table.foot[0].x+2:null;
					var footY = data.table.foot.length>0 ? data.table.foot[0].y:null;
					if(footX && footY){
						doc.line(footX+2, footY, 204, footY);
					}
					coordDocY = footY+25;
					if(coordDocY>214){
						coordDocY = 70;
						doc.addPage();
					}
				},
				columnStyles:	that.setTamanioCellDocumentos()
			});
			// Imprime el numero total de paginas del PDF
			if (typeof doc.putTotalPages === 'function') {
				doc.putTotalPages(totalPagesExp);
			}
		},*/
		onGenerarPDFArqueoCajaResumen: function (self,value) {
			var that = this;
			////INICIO FECHA Y HORA
			var date = new Date();
			/////////////////////////////
			var yyyy = date.getFullYear().toString();
			var mm = utilController.pad((date.getMonth() + 1).toString(),"0",2); // getMonth() is zero-based
			var dd = utilController.pad(date.getDate().toString(),"0",2);
			var h = utilController.pad(date.getHours(),"0", 2);
			var m = utilController.pad(date.getMinutes(),"0", 2);
			var s = utilController.pad(date.getSeconds(),"0", 2);
			var fechaActual =   dd + "/" + mm + "/" + yyyy; // padding 
			var horaActual = h+":"+m+":"+s;
			/////////////////////////////
			////END FECHA Y HORA
			////INICIO CONSTANTES DOC///
			coordDocX = 5;
			coordDocY = 65;
			////END CONSTANTES DOC///
			debugger;
			var test1  = jQuery.sap.require("app/arqueocaja/lib/jsPDF/jspdf");
			var test = jQuery.sap.require("app/arqueocaja/lib/jsPDF/autotable");
			$.generarOrdenCompraPDF = function (value) {
				var doc = new jsPDF('p', 'mm', 'a4');//Vertical
				//var doc = JSPDFInstancia('l', 'mm', 'a4');//Horizontal
				// Contador para el número de páginas
				var totalPagesExp = "{total_pages_count_string}";
				var setDetalle = function (Detalle) {
					that.setPDFResumenDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);
					if(value.aDocumentos.length>0){
							doc.addPage();
							doc.setFontSize(10);
							doc.setFont('sans-serif', 'bold');
							doc.text(coordDocX + 88, coordDocY-16, "DOCUMENTOS");
					}
					var ultimoTipoDocumento;
					if(value.aDocumentosBoletaElectronica.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD1") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosBoletaElectronica,value.TotalImporteBoletaElectronica,self.utilController.i18n("txtD1"),ultimoTipoDocumento);	
					}
					if(value.aDocumentosFacturaElectronica.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD2") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosFacturaElectronica,value.TotalImporteFacturaElectronica,self.utilController.i18n("txtD2"),ultimoTipoDocumento);	
					}
					if(value.aDocumentosBoletaManual.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD3") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosBoletaManual,value.TotalImporteBoletaManual,self.utilController.i18n("txtD3"),ultimoTipoDocumento);	
					}	
					if(value.aDocumentosFacturaManual.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD4") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosFacturaManual,value.TotalImporteFacturaManual,self.utilController.i18n("txtD4"),ultimoTipoDocumento);	
					}	
					if(value.aDocumentosBoletaCredito.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD5") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosBoletaCredito,value.TotalImporteBoletaCredito,self.utilController.i18n("txtD5"),ultimoTipoDocumento);	
					}	
					if(value.aDocumentosFacturaCredito.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD6") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosFacturaCredito,value.TotalImporteFacturaCredito,self.utilController.i18n("txtD6"),ultimoTipoDocumento);	
					}	
					if(value.aDocumentosAnulado.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD7") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosAnulado,value.TotalImporteAnulado,self.utilController.i18n("txtD7"),ultimoTipoDocumento);	
					}
					if(value.aDocumentosNotaCreditoAplicada.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD8") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosNotaCreditoAplicada,value.TotalImporteNotaCreditoAplicada,self.utilController.i18n("txtD8"),ultimoTipoDocumento);	
					}
					if(value.aDocumentosNotaCreditoEmitidaPagaNo.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD9") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosNotaCreditoEmitidaPagaNo,value.TotalImporteNotaCreditoEmitidaPagaNo,self.utilController.i18n("txtD9"),ultimoTipoDocumento);	
					}	
					if(value.aDocumentosComprobantesOtrasSedes.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD10") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosComprobantesOtrasSedes,value.TotalImporteComprobantesOtrasSedes,self.utilController.i18n("txtD10"),ultimoTipoDocumento);	
					}
					if(value.aDocumentosOtrosComprobantes.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD11") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosOtrosComprobantes,value.TotalImporteOtrosComprobantes,self.utilController.i18n("txtD11"),ultimoTipoDocumento);	
					}
					/*if(value.aDocumentosBoletaElectronica.length>0){
						    doc.text(coordDocX + 5, coordDocY-8, self.utilController.i18n("txtD1"));
						    that.setPDFDocumentosBoletaElectronicaDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);	
					}
					if(value.aDocumentosFacturaElectronica.length>0){
							doc.text(coordDocX + 5, coordDocY-8, self.utilController.i18n("txtD2"));
						    that.setPDFDocumentosFacturaElectronicaDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);	
					}
					if(value.aDocumentosBoletaManual.length>0){
							doc.text(coordDocX + 5, coordDocY-8, self.utilController.i18n("txtD3"));
						    that.setPDFDocumentosBoletaManualDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);	
					}
					if(value.aDocumentosFacturaManual.length>0){
							doc.text(coordDocX + 5, coordDocY-8, self.utilController.i18n("txtD4"));
						    that.setPDFDocumentosFacturaManualDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);	
					}
					if(value.aDocumentosNotaCreditoEmitida.length>0){
							doc.text(coordDocX + 5, coordDocY-8, self.utilController.i18n("txtD5"));
						    that.setPDFDocumentosNotaCreditoEmitidaDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);	
					}
					if(value.aDocumentosBoletaOtraSede.length>0){
							doc.text(coordDocX + 5, coordDocY-8, self.utilController.i18n("txtD6"));
						    that.setPDFDocumentosBoletaOtraSedeDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);	
					}
					if(value.aDocumentosFacturaOtraSede.length>0){
							doc.text(coordDocX + 5, coordDocY-8, self.utilController.i18n("txtD7"));
						    that.setPDFDocumentosFacturaOtraSedeDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);	
					}
					if(value.aDocumentosDevolucionTotalEfectivo.length>0){
							doc.text(coordDocX + 5, coordDocY-8, self.utilController.i18n("txtD8"));
						    that.setPDFDocumentosDevolucionTotalEfectivoDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);	
					}
					if(value.aDocumentosNotaCreditoAplicada.length>0){
							doc.text(coordDocX + 5, coordDocY-8, self.utilController.i18n("txtD9"));
						    that.setPDFDocumentosNotaCreditoAplicadaDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);	
					}
					if(value.aDocumentosOtros.length>0){
							doc.text(coordDocX + 5, coordDocY-8, self.utilController.i18n("txtD10"));
						    that.setPDFDocumentosOtrosDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);	
					}*/
					doc.addPage();//GN 27-12-2020
					that.setPDF_FFS_ES_ED_Detalle(self,doc,value,fechaActual,horaActual,totalPagesExp);
				};
				// DETALLE
				var Detalle = value.aDetalle;
				setDetalle(Detalle);
				
				sap.ui.core.BusyIndicator.hide();
				
				doc.save("DOCUMENTO-" + value.Titulo.trim() + '.pdf');
				var winprint = window.open(doc.output('bloburl'), '_blank'); 
				//winprint.focus();
			};
			$.generarOrdenCompraPDF(value);
		},
		onGenerarPDFArqueoCajaDetalle: function (self,value) {
			var that = this;
			////INICIO FECHA Y HORA
			var date = new Date();
			/////////////////////////////
			var yyyy = date.getFullYear().toString();
			var mm = utilController.pad((date.getMonth() + 1).toString(),"0",2); // getMonth() is zero-based
			var dd = utilController.pad(date.getDate().toString(),"0",2);
			var h = utilController.pad(date.getHours(),"0", 2);
			var m = utilController.pad(date.getMinutes(),"0", 2);
			var s = utilController.pad(date.getSeconds(),"0", 2);
			var fechaActual =   dd + "/" + mm + "/" + yyyy; // padding 
			var horaActual = h+":"+m+":"+s;
			/////////////////////////////
			////END FECHA Y HORA
			////INICIO CONSTANTES DOC///
			coordDocX = 5;
			coordDocY = 65;
			////END CONSTANTES DOC///
			var test1  = jQuery.sap.require("app/arqueocaja/lib/jsPDF/jspdf");
			var test = jQuery.sap.require("app/arqueocaja/lib/jsPDF/autotable");
			$.generarOrdenCompraPDF = function (value) {
				var doc = new jspdf('p', 'mm', 'a4');//Vertical
				//var doc = JSPDFInstancia('l', 'mm', 'a4');//Horizontal
				// Contador para el número de páginas
				var totalPagesExp = "{total_pages_count_string}";
				var setDetalle = function (Detalle) {
					that.setPDF_FFS_ES_ED_Detalle(self,doc,value,fechaActual,horaActual,totalPagesExp);
					doc.addPage();
					that.setPDFResumenDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);
					if(value.aDocumentos.length>0){
							doc.addPage();
							doc.setFontSize(10);
							doc.setFont('sans-serif', 'bold');
							doc.text(coordDocX + 88, coordDocY-16, "DOCUMENTOS");
					}
					var ultimoTipoDocumento;
					if(value.aDocumentosBoletaElectronica.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD1") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosBoletaElectronica,value.TotalImporteBoletaElectronica,self.utilController.i18n("txtD1"),ultimoTipoDocumento);	
					}
					if(value.aDocumentosFacturaElectronica.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD2") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosFacturaElectronica,value.TotalImporteFacturaElectronica,self.utilController.i18n("txtD2"),ultimoTipoDocumento);	
					}
					if(value.aDocumentosBoletaManual.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD3") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosBoletaManual,value.TotalImporteBoletaManual,self.utilController.i18n("txtD3"),ultimoTipoDocumento);	
					}	
					if(value.aDocumentosFacturaManual.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD4") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosFacturaManual,value.TotalImporteFacturaManual,self.utilController.i18n("txtD4"),ultimoTipoDocumento);	
					}	
					if(value.aDocumentosBoletaCredito.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD5") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosBoletaCredito,value.TotalImporteBoletaCredito,self.utilController.i18n("txtD5"),ultimoTipoDocumento);	
					}	
					if(value.aDocumentosFacturaCredito.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD6") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosFacturaCredito,value.TotalImporteFacturaCredito,self.utilController.i18n("txtD6"),ultimoTipoDocumento);	
					}	
					if(value.aDocumentosAnulado.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD7") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosAnulado,value.TotalImporteAnulado,self.utilController.i18n("txtD7"),ultimoTipoDocumento);	
					}
					if(value.aDocumentosNotaCreditoAplicada.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD8") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosNotaCreditoAplicada,value.TotalImporteNotaCreditoAplicada,self.utilController.i18n("txtD8"),ultimoTipoDocumento);	
					}
					if(value.aDocumentosNotaCreditoEmitidaPagaNo.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD9") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosNotaCreditoEmitidaPagaNo,value.TotalImporteNotaCreditoEmitidaPagaNo,self.utilController.i18n("txtD9"),ultimoTipoDocumento);	
					}	
					if(value.aDocumentosComprobantesOtrasSedes.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD10") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosComprobantesOtrasSedes,value.TotalImporteComprobantesOtrasSedes,self.utilController.i18n("txtD10"),ultimoTipoDocumento);	
					}
					if(value.aDocumentosOtrosComprobantes.length>0){
						ultimoTipoDocumento =  value.ultimoTipoDocumento == self.utilController.i18n("txtD11") ? true : false;
						that.setPDFDocumentosGlobalDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp,value.aDocumentosOtrosComprobantes,value.TotalImporteOtrosComprobantes,self.utilController.i18n("txtD11"),ultimoTipoDocumento);	
					}
					/*if(value.aDocumentosBoletaElectronica.length>0){
						    doc.text(coordDocX + 5, coordDocY-8, self.utilController.i18n("txtD1"));
						    that.setPDFDocumentosBoletaElectronicaDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);	
					}
					if(value.aDocumentosFacturaElectronica.length>0){
							doc.text(coordDocX + 5, coordDocY-8, self.utilController.i18n("txtD2"));
						    that.setPDFDocumentosFacturaElectronicaDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);	
					}
					if(value.aDocumentosBoletaManual.length>0){
							doc.text(coordDocX + 5, coordDocY-8, self.utilController.i18n("txtD3"));
						    that.setPDFDocumentosBoletaManualDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);	
					}
					if(value.aDocumentosFacturaManual.length>0){
							doc.text(coordDocX + 5, coordDocY-8, self.utilController.i18n("txtD4"));
						    that.setPDFDocumentosFacturaManualDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);	
					}
					if(value.aDocumentosNotaCreditoEmitida.length>0){
							doc.text(coordDocX + 5, coordDocY-8, self.utilController.i18n("txtD5"));
						    that.setPDFDocumentosNotaCreditoEmitidaDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);	
					}
					if(value.aDocumentosBoletaOtraSede.length>0){
							doc.text(coordDocX + 5, coordDocY-8, self.utilController.i18n("txtD6"));
						    that.setPDFDocumentosBoletaOtraSedeDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);	
					}
					if(value.aDocumentosFacturaOtraSede.length>0){
							doc.text(coordDocX + 5, coordDocY-8, self.utilController.i18n("txtD7"));
						    that.setPDFDocumentosFacturaOtraSedeDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);	
					}
					if(value.aDocumentosDevolucionTotalEfectivo.length>0){
							doc.text(coordDocX + 5, coordDocY-8, self.utilController.i18n("txtD8"));
						    that.setPDFDocumentosDevolucionTotalEfectivoDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);	
					}
					if(value.aDocumentosNotaCreditoAplicada.length>0){
							doc.text(coordDocX + 5, coordDocY-8, self.utilController.i18n("txtD9"));
						    that.setPDFDocumentosNotaCreditoAplicadaDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);	
					}
					if(value.aDocumentosOtros.length>0){
							doc.text(coordDocX + 5, coordDocY-8, self.utilController.i18n("txtD10"));
						    that.setPDFDocumentosOtrosDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);	
					}*/
					if(value.aCustodios.length>0){
						doc.addPage();
						that.setPDFCustodiosDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);
					}

					if(value.aTraslados.length>0){
						doc.addPage();
						that.setPDFTrasladosDetalle(self,doc,value,fechaActual,horaActual,totalPagesExp);
					}
				};
				// DETALLE
				var Detalle = value.aDetalle;
				setDetalle(Detalle);
				
				sap.ui.core.BusyIndicator.hide();
				var winprint = window.open(doc.output('bloburl'), '_blank'); 
				//winprint.focus();
				doc.save("DOCUMENTO-" + value.Titulo.trim() + '.pdf');
			};
			$.generarOrdenCompraPDF(value);
		},
		////////////////////////////////////
		setPDFDocumentosGlobalDetallePrueba1 : function (self,doc,value,fechaActual,horaActual,totalPagesExp,aValue,fTotalImporte,tituloTabla) {
			var that = this;
			var coordDocYFinal= 0;
			///CENTER
			var arrayLinea	=	[];
			var arrayCampo	=	[];
			var arrayFooter	=	[];
			var arrayFooterTotal	=	[];
			var aData	=	aValue;
			
			// Selecciona solo los campos que se van a mostrar. Autotable pinta todos los campos del arreglo				
			arrayLinea = that.setRowDocumentos(aData);
			
			// Arma el footer con las sumatorias. Van en "" para que no muestre nada. El autotable requiere la misma cantidad de campos
			
			var finalY = coordDocY+5;
			var headerAutotable = that.setHeaderAutoTable(fTotalImporte);
			
			const headerTable = headerAutotable.headerTable;
			arrayFooterTotal = headerAutotable.arrayFooterTotal;
			//////
			
			doc.text(coordDocX + 5, coordDocY-8, tituloTabla);
			that.setCabeceraTablaDocumentos(self,doc);
			/////
			doc.autoTable(headerTable,arrayLinea,{
				startY: finalY,
				body: arrayLinea,
				theme: 'plain',
				foot: arrayFooterTotal,
				showFoot: 'lastPage',
				showHead: 'never',
				bodyStyles: {valign: 'top'},
				footStyles: {halign: 'right', cellPadding: {top: 3.5, bottom: 1.5, left: 0.2, right: 0.2}, cellWidth: 0},
				margin: {top: 70, right: 11, bottom: 70, left: 5},
				styles: {
							rowPageBreak: 'auto',
							fontSize:5.8,
							textColor:[0, 0, 0],
							valign: 'top',
							minCellWidth: 1,
							minCellHeight: 7,
							cellPadding: {top: 1.5}
				},
				didDrawPage: function (data) {	// Dibuja la estructura en todas las páginas
					//coordDocYFinal = coordDocY;
					//if(coordDocY>205){
					//	coordDocY = 65;
					//}		
					if(data.pageNumber>1){
						var bodyX = data.table.body.length>0 ? data.table.body[data.table.body.length-1].x:null;
					    var bodyY = data.table.body.length>0 ? data.table.body[data.table.body.length-1].y-6:null;
						if(bodyX && bodyY){
						    that.setCabeceraTablaDocumentosManyPages(self,doc,bodyX,bodyY);	
						}
					}	
					that.setPDFCabecera(self,doc,value,fechaActual,horaActual);
					// Footer
					that.setPDFFooter(doc,data,value);
					that.setPDFPiePagina(doc,totalPagesExp);

					//Inicio Línea Foot para el total Dinámico
					var footX = data.table.foot.length>0 ? data.table.foot[0].x+2:null;
					var footY = data.table.foot.length>0 ? data.table.foot[0].y:null;
					if(footX && footY){
						doc.line(footX+2, footY, 206, footY);
					}
					coordDocY = footY+25;
					//coordDocYFinal = coordDocY;
					//if(coordDocY>213){
				   if(coordDocY>232){//205
						coordDocY = 65;
						doc.addPage();
					}
				},
				columnStyles:	that.setTamanioCellDocumentos()
			});
			// Imprime el numero total de paginas del PDF
			if (typeof doc.putTotalPages === 'function') {
				doc.putTotalPages(totalPagesExp);
			}
		},
		////////////////////////////////////
	};
});