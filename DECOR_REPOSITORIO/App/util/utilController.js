
sap.ui.define([
	'sap/m/MessageBox',
	'sap/m/MessageToast',
	"../dataPass",
	"./utilPopUps",
	"./utilValidation",
	"../formatters/formatter",
	"../constantes",
	'sap/ui/model/Filter',
	'sap/ui/model/FilterOperator',
], function(MessageBox,MessageToast,dataPass,utilPopUps,utilValidation,formatter,constantes,Filter,FilterOperator) {
	"use strict";
	return {
        initModelView: function (controller) {
			controller.getView().setModel(new JSONModel({}));
		},
		i18n: function (name) {
            var source = dataPass.viewDetail.getView().getModel('i18n').getResourceBundle();
            return source.getText(name);
        },
		leftNumberUndefined: function (valor) {
			if (!valor) {
				valor = "0";
			} else {
				valor = valor.toString();
			}
			return valor;
		},
		valueUndefinedPDF: function (valor) {
			var newValor = valor;
			if (!valor) {
				newValor = "";
			}
			if (newValor.includes) {
				if (newValor.includes("XNA") || newValor.includes("NO APLICA")) {
					newValor = "";
				}
			}
			return newValor;
		},
		alignTextJsPDF: function (Doc, Value, CoordX, CoordY, WidthString, Align) {
			var doc = Doc;
			var coordX = CoordX;
			var coordY = CoordY;
			var widthString = WidthString;
			var paragraph = Value;
			var lines = doc.splitTextToSize(paragraph, widthString);
			var dim = doc.getTextDimensions('Text');
			var lineHeight = dim.h;
			var maximoWidth = 0;
			for (var l = 0; l < lines.length; l++) {
				if (doc.getTextWidth(lines[l]) > maximoWidth) {
					maximoWidth = doc.getTextWidth(lines[l]);
				}
			}
			for (var i = 0; i < lines.length; i++) {
				var lineTop = i == 0 ? coordY : ((lineHeight / 3.3) * (i)) + coordY;
				lines[i] = lines[i].toString().replace(/ /g, "|");
				while (doc.getTextWidth(lines[i]) < maximoWidth) {
					if (doc.getTextWidth(lines[i]) * 1.2 <= maximoWidth) {
						break;
					}
					if (!lines[i].toString().includes("|")) {
						lines[i] = lines[i].toString().replace(/ /g, "|");
					}
					lines[i] = lines[i].toString().replace("|", "  ");
				}
				while (lines[i].includes("|")) {
					lines[i] = lines[i].toString().replace("|", " ");
				}
				doc.text(lines[i], coordX, lineTop, {
					maxWidth: widthString,
					align: Align //justify,center,left,rihgt
				}); //see this line
			}
		},
		alignTextJsPDF2: function (Doc, Value, CoordX, CoordY, WidthString, Align) {
			var doc = Doc;
			var coordX = CoordX;
			var coordY = CoordY;
			var widthString = WidthString;
			var paragraph = Value;
			var lines = doc.splitTextToSize(paragraph, widthString);
			var dim = doc.getTextDimensions('Text');
			var lineHeight = dim.h;
			var maximoWidth = 0;
			for (var l = 0; l < lines.length; l++) {
				if (doc.getTextWidth(lines[l]) > maximoWidth) {
					maximoWidth = doc.getTextWidth(lines[l]);
				}
			}
			for (var i = 0; i < lines.length; i++) {
				var lineTop = i == 0 ? coordY : ((lineHeight / 2.1) * (i)) + coordY;
				lines[i] = lines[i].toString().replace(/ /g, "|");
				while (doc.getTextWidth(lines[i]) < maximoWidth) {
					if (doc.getTextWidth(lines[i]) * 1.2 <= maximoWidth) {
						break;
					}
					if (!lines[i].toString().includes("|")) {
						lines[i] = lines[i].toString().replace(/ /g, "|");
					}
					lines[i] = lines[i].toString().replace("|", "  ");
				}
				while (lines[i].includes("|")) {
					lines[i] = lines[i].toString().replace("|", " ");
				}
				doc.text(lines[i], coordX, lineTop, {
					maxWidth: widthString,
					align: Align //justify,center,left,rihgt
				}); //see this line
			}
		},
		validarPosDeliveryDocumentos : function(self,modelo,property) {
			
            var errorPOS_Delivery = false;
            var aDocumentosBoletaElectronica = self.getView().getModel(modelo).getProperty(property);
            console.log(aDocumentosBoletaElectronica);
			var aEncontrado =  $.grep(aDocumentosBoletaElectronica, function(element, index) {
                                        return element.flagCamposCorrectosPOS_Delivery == false
                                    });

                                    if(aEncontrado.length>0){
                                          errorPOS_Delivery = true  
                                    }

                                    return errorPOS_Delivery;
		},
		verificarYpintarObligatoriedadCamposValidarPOS_Delivery : function(self,aDocumentos,aDocumentosTabla,aPagos,idTabla,estiloCSS){
            
            var aPosicionesErronea=[];
			

			if(aDocumentosTabla){
				
				aDocumentosTabla.forEach(function(item,index){
                var pago = aPagos.find(function(el){
                        return el.iIdDocumento == item.iIdDocumento;
                    });
                    if(pago){
                            if(pago.sFormaPago == this.constantes.DesPosDelivery){
							  var aObjKeys = Object.keys(pago);
									console.log(aObjKeys);
									var cantidadCamposFaltantes = 0;
									for(var i=0;i<this.constantes.aCamposValidarPOS_Delivery.length;i++){
											var element = this.constantes.aCamposValidarPOS_Delivery[i];
											if(!aObjKeys.includes(element)){
												   cantidadCamposFaltantes++;
												   break; 
											}else{
												  if(pago[element]==""){
													  cantidadCamposFaltantes++;
															break;     
												  }  
											}
									}
									if(cantidadCamposFaltantes>0){
										///INICIO NUEVO AJUSTE PARA FACTURA
										if(item.sCodTipoDocumento=="01"){
											var NC = aDocumentos.aDocumentosNotaCreditoEmitida.filter(function(el){
												return item.sReferenciaNC == el.sReferencia;
											});
											if(NC.length==0){
											    aPosicionesErronea.push(index);
										        item.flagCamposCorrectosPOS_Delivery=false;	
											}
										  }else{///END NUEVO AJUSTE PARA FACTURA
											aPosicionesErronea.push(index);
											item.flagCamposCorrectosPOS_Delivery=false;
										  }
									}else{
										 item.flagCamposCorrectosPOS_Delivery=true;       
									}     
						   }else{
							   item.flagCamposCorrectosPOS_Delivery=true;    
						   } 
                    }else{
						   
                            item.flagCamposCorrectosPOS_Delivery=true;
                       }

                    }.bind(self))
                    
            }
            if(aPosicionesErronea.length>0){
            	self.utilController.pintarFilaxTablaById(aPosicionesErronea,idTabla,estiloCSS)
            }
            console.log("----")
            

          
		},
		pintarFilaxTablaById : function(aPosicionesErronea,idTabla,color){
			for(var j=0;j<aPosicionesErronea.length;j++){
                    const posi = aPosicionesErronea[j];
                    const item = sap.ui.getCore().byId(idTabla).getItems()[posi];
                    item.addStyleClass(color); 
                 }
		},
		alignTextJsPDFV2: function (Doc, Value, CoordX, CoordY, WidthString, Align) {
			var doc = Doc;
			var coordX = CoordX;
			var coordY = CoordY;
			var widthString = WidthString;
			var paragraph = Value;
			var lines = doc.splitTextToSize(paragraph, widthString);
			var dim = doc.getTextDimensions('Text');
			var lineHeight = dim.h;
			var maximoWidth = 0;
			for (var l = 0; l < lines.length; l++) {
				if (doc.getTextWidth(lines[l]) > maximoWidth) {
					maximoWidth = doc.getTextWidth(lines[l]);
				}
			}
			for (var i = 0; i < lines.length; i++) {
				var lineTop = i == 0 ? coordY : ((lineHeight / 3.3) * (i)) + coordY;
				lines[i] = lines[i].toString().replace(/ /g, "|");
				while (doc.getTextWidth(lines[i]) < maximoWidth) {
					//////INICIO Roy 16-09-2020
					var palabras = lines[i].split(" ").length+ lines[i].split("|").length;
					var palabraEspacio = lines[i].includes(" ");
					var palabraBarra = lines[i].includes("|");
					if(palabras<=2 && !(palabraEspacio || palabraBarra)){
                        break;
					}
					//////END Roy 16-09-2020
					if (doc.getTextWidth(lines[i]) * 1.2 <= maximoWidth) {
						break;
					}
					if (!lines[i].toString().includes("|")) {
						lines[i] = lines[i].toString().replace(/ /g, "|");
					}
					lines[i] = lines[i].toString().replace("|", "  ");
				}
				while (lines[i].includes("|")) {
					lines[i] = lines[i].toString().replace("|", " ");
				}
				doc.text(lines[i], coordX, lineTop, {
					maxWidth: widthString,
					align: Align //justify,center,left,rihgt
				}); //see this line
			}
		},
		alignTextJsPDFV5: function (Doc, Value, CoordX, CoordY, WidthString, Align) {
			var doc = Doc;
			var coordX = CoordX;
			var coordY = CoordY;
			var widthString = WidthString;
			var paragraph = Value;
			var lines = doc.splitTextToSize(paragraph, widthString);
			var dim = doc.getTextDimensions('Text');
			var lineHeight = dim.h;
			var maximoWidth = 0;
			for (var l = 0; l < lines.length; l++) {
				if (doc.getTextWidth(lines[l]) > maximoWidth) {
					maximoWidth = doc.getTextWidth(lines[l]);
				}
			}
			for (var i = 0; i < lines.length; i++) {
			var lineTop = i == 0 ? coordY : ((lineHeight / 3.3) * (i)) + coordY;
			lines[i] = lines[i].toString().replace(/ /g, "|");
			while (doc.getTextWidth(lines[i]) < maximoWidth) {
			//////INICIO Roy 16-09-2020
			var palabras = lines[i].split(" ").length+ lines[i].split("|").length;
			var palabraEspacio = lines[i].includes(" ");
			var palabraBarra = lines[i].includes("|");
			    if(palabras<=2 && !(palabraEspacio || palabraBarra)){
			        break;
			    }
			//////END Roy 16-09-2020
			if (doc.getTextWidth(lines[i]) * 1.2 <= maximoWidth) {
			    break;
			}
			    if (!lines[i].toString().includes("|")) {
			        lines[i] = lines[i].toString().replace(/ /g, "|");
			    }
			        lines[i] = lines[i].toString().replace("|", " ");
			    }
			    while (lines[i].includes("|")) {
			        lines[i] = lines[i].toString().replace("|", " ");
			    }
			    doc.text(lines[i], coordX, lineTop, {
			        maxWidth: widthString,
			        align: Align //justify,center,left,rihgt
			        }); //see this line
			    }
			},
		setPadString: function (self, model, nameCampo, oEvent) {
			var path;
			var valorPast
			if (oEvent.getSource().getBindingContext(model)) {
				path = oEvent.getSource().getBindingContext(model).getPath();
				valorPast = self.getView().getModel(model).getProperty(path + "/" + nameCampo + "Past");
			} else {
				path = oEvent.getSource().getBindingInfo("value").binding.getPath();
				model = "miData";
				nameCampo = "";
				valorPast = self.getView().getModel(model).getProperty(path + "Past");
			}

			var value = oEvent.getSource().getValue();
			var modValue = value.split("");
			for (var i in modValue) {
				if (!modValue[i].includes("0")) {
					break;
				} else {
					modValue[i] = modValue[i].replace("0", "");
				}
			}
			var valor = modValue.join("");

			if (valor) {
				var valorPad = this.pad(valor, '0', 11);
				if (valorPast && valor && (valor.length == 0 || valor.length == 12) && valor.substring(0, 1) !== "0") {
					return self.getView().getModel(model).setProperty(path + "/" + nameCampo, valorPast);
				}
				if (oEvent.getSource().getBindingContext(model)) {
					self.getView().getModel(model).setProperty(path + "/" + nameCampo, valorPad);
					self.getView().getModel(model).setProperty(path + "/" + nameCampo + "Past", valorPad);
				} else {
					self.getView().getModel(model).setProperty(path, valorPad);
					self.getView().getModel(model).setProperty(path + "Past", valorPad);
				}
			}
		},
		setPadNumber: function (self, model, nameCampo, oEvent) {
			var object = oEvent.getSource().getBindingContext(model).getObject();
			var path = oEvent.getSource().getBindingContext(model).getPath();
			var value = oEvent.getSource().getValue();
			var objParseInt = parseInt(value);
			if (!objParseInt) {
				return self.getView().getModel(model).setProperty(path + "/" + nameCampo, "");;
			}
			var valor = value ? objParseInt.toString() : "";

			var valorPast = self.getView().getModel(model).getProperty(path + "/" + nameCampo + "Past");

			if (valor) {
				var valorPad = this.pad(valor, '0', 11);
				if (valorPast && valor && (valor.length == 0 || valor.length == 12) && valor.substring(0, 1) !== "0") {
					return self.getView().getModel(model).setProperty(path + "/" + nameCampo, valorPast);
				}
				self.getView().getModel(model).setProperty(path + "/" + nameCampo, valorPad);
				self.getView().getModel(model).setProperty(path + "/" + nameCampo + "Past", valorPad)
			}
		},
		setComboGeneral: function (self, nameModel, nameTable, campoKey, campoDescripcion, servDatos) {
			var model = self.getView().getModel(nameModel).getProperty("/" + nameTable);
			var modelSet = model ? model : [];
			var modDatos = servDatos;
			modDatos.Codigo = campoKey;
			if (campoDescripcion.includes("XNA")) {
				modDatos.Descripcion = "--NO APLICA--";
			} else {
				modDatos.Descripcion = campoDescripcion;
			}

			modelSet.push(modDatos);
			self.getView().getModel(nameModel).setProperty("/" + nameTable, modelSet);
			self.getView().getModel(nameModel).refresh(true);
		},
		listaToArbol: function (list) {
			var map = {};
			var node, roots = [];
			var i = 0;
			for (i = 0; i < list.length; i += 1) {
				map[list[i].id] = i; // initialize the map
				list[i].children = []; // initialize the children
			}
			for (i = 0; i < list.length; i += 1) {
				node = list[i];
				if (node.parentId !== "0") {
					// if you have dangling branches check that map[node.parentId] exists
					if (map[node.parentId]) {
						list[map[node.parentId]].children.push(node);
					} else {
						roots.push(node);
					}
				} else {
					roots.push(node);
				}
			}
			return roots;
		},
		uploadFile: function (oEvent, events) {
			var self = this;
			var fileUploader = oEvent.getSource();
			var domRef = fileUploader.getFocusDomRef();
			var file = domRef.files[0];
			var reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function (ev) {
				var data = {};
				data.resultTotal = ev.target.result;
				data.resultado = data.resultTotal.split(',')[1];
				data.formato = data.resultTotal.split(',')[0];
				data.nombreArchivo = file.name.split('.').slice(0, -1).join('.');
				data.type = file.type;
				data.extension = file.type.split('/')[1];
				//fileUploader.setValue("");
				self.convertirImgToPDF(data.resultado, data.extension, data.formato,
					function (dataPdf) {
						events.uploadFileComplete(data, dataPdf);
					});
			};
		},
		convertirImgToPDF: function (SBase64File, extension, formato, callback) {
			formato = formato + ",";
			var dataPdf = {};
			if (!(extension == "pdf" || extension == "PDF")) {
				var doc = new jsPDF('p', 'pt', 'a4');
				var width = doc.internal.pageSize.width;
				var height = doc.internal.pageSize.height;
				var options = {
					pagesplit: true
				};
				var h1 = 50;
				var aspectwidth1 = (height - h1) * (9 / 16);
				doc.addImage(formato + SBase64File, extension, 10, h1, aspectwidth1, (height - h1), 'monkey');
				dataPdf.ruta = doc.output('datauri');
				dataPdf.extension = "application/pdf";
			}
			callback(dataPdf);
		},
		itemsSelectedTable: function (self, idTable, model, table, event) {
			var tabla = self.getView().byId(idTable);
			var itemsTabla = tabla.getSelectedIndices().length;
			var aItems = [];
			if (itemsTabla > 0) {
				for (var i = itemsTabla - 1; i >= 0; i--) {
					var item = tabla.getSelectedIndices()[i];
					var obj = self.getView().getModel(model).getProperty(table + "/" + item);
					aItems.push(obj);
					//self.removeSelection(idTable);
				}
				event.Seleccionados(aItems);
			} else {
				MessageToast.show('No ha seleccionado ningun item', {
					duration: 3000
				});
				return;
			}
		},
		removeDuplicates: function (originalArray) {
			var modoriginalArray = originalArray;
			var newArray = [];
			for (var i in modoriginalArray) {
				var filterArray = newArray.filter(function (el) {
					return el == modoriginalArray[i];
				});
				if (filterArray.length == 0) {
					newArray.push(modoriginalArray[i]);
				}
			}
			return newArray;
		},
		removeDuplicatesId: function (originalArray) {
			var modoriginalArray = originalArray;
			var newArray = [];
			for (var i in modoriginalArray) {
				var filterArray = newArray.filter(function (el) {
					return el.DTId == modoriginalArray[i].DTId;
				});
				if (filterArray.length == 0) {
					newArray.push(modoriginalArray[i]);
				}
			}
			return newArray;
		},
		removeDuplicatesGeneral: function (originalArray, campoFiltro) {
			var modoriginalArray = originalArray;
			var newArray = [];
			modoriginalArray.forEach(function (item) {
				if (campoFiltro) {
					var filterArray = newArray.filter(function (el) {
						return el[campoFiltro] == item[campoFiltro];
					});
				} else {
					var filterArray = newArray.filter(function (el) {
						return el == item;
					});
				}

				if (filterArray.length == 0) {
					newArray.push(item);
				}
			});
			return newArray;
		},
		removeDuplicatesGeneral2Campos: function (originalArray, campoFiltro1,campoFiltro2) {
			var modoriginalArray = originalArray;
			var newArray = [];
			modoriginalArray.forEach(function (item) {
				if (campoFiltro1 && campoFiltro2) {
					var filterArray = newArray.filter(function (el) {
						return el[campoFiltro1] + el[campoFiltro2] == item[campoFiltro1] + item[campoFiltro2];
					});
				} else {
					var filterArray = newArray.filter(function (el) {
						return el == item;
					});
				}

				if (filterArray.length == 0) {
					newArray.push(item);
				}
			});
			return newArray;
		},
		removeDuplicatesGeneralConteo: function (originalArray, campoFiltro,campoConteo) {
			var modoriginalArray = originalArray;
			var newArray = [];
			modoriginalArray.forEach(function (item) {
				if (campoFiltro) {
					var filterArray = newArray.filter(function (el) {
						return el[campoFiltro] == item[campoFiltro];
					});
				} else {
					var filterArray = newArray.filter(function (el) {
						return el == item;
					});
				}

				if (filterArray.length == 0) {
					///Conteo
					var filterConteo = modoriginalArray.filter(function (el) {
						return el[campoConteo] == item[campoConteo];
					});
					item.name = item.name+"|"+filterConteo.length;
					///
					newArray.push(item);
				}
			});
			return newArray;
		},
		/*removeDuplicates: function (originalArray) {
			var newArray = [];
			var lookupObject = {};

			for (var i in originalArray) {
				lookupObject[originalArray[i]] = originalArray[i];
			}

			for (i in lookupObject) {
				newArray.push(lookupObject[i]);
			}
			return newArray;
		},*/
		formatterComboBox: function (self, campo, dato, newCampo, nameModel, nametable) {
			var modData = "";
			try {
				if (dato != undefined) {
					var table = self.getView().getModel(nameModel).getProperty(nametable);
					var findObject = table.find(function (el) {
						return el[campo] == dato;
					});
					modData = findObject[newCampo];
				}
			} catch (e) {}
			return modData;
		},
		convertDateToString: function (date, formmat) {
			var modDate = {},
				anio, mes, dia;
			anio = date.getFullYear();
			if (date.getMonth() == 12) {
				mes = 1;
			} else {
				mes = date.getMonth() + 1;
			}
			dia = date.getDate();
			modDate.anio = anio;
			modDate.mes = mes;
			modDate.dia = dia;
			var separador = formmat.search('-') > 0  ? '-' : (formmat.search('/') > 0 ? '/' : false);
			var modFormat;
			if (separador) {
				var modSplit = formmat.split(separador);
				modFormat = this.getFormateDate(modDate, modSplit, separador)
			}
		
			return modFormat;
		},
		getFormateDate: function (modDate, modSplit, separador) {
			var modData = "";
			modSplit.forEach(function(item, x) {
				var separa;
				if (x + 1 === modSplit.length) {
					separa = "";
				} else {
					separa = separador;
				}
		
				if (item === "mm" || item === "MM") {
					modData = modData + this.pad(modDate.mes,"0",2) + separa;
				}
				if (item === "dd" || item === "DD") {
					modData = modData + this.pad(modDate.dia,"0",2) + separa;
				}
				if (item === "YYYY" || item === "yyyy") {
					modData = modData + modDate.anio + separa;
				}
			});
			return modData;
		},
		pad: function (n, padString, length) { //Añadir ceros a la izquierda, length tamaño final que debe tener
			var n = n.toString();
			while (n.length < length) {
				n = padString + n;
			}
			return n;
		},
		isObject: function (a) {
			return (!!a) && (a.constructor === Object);
		},
		formatFechaDDMMAAAAHHMMSS: function (value) {
			var fechaString;
			/////////////////////////////
			if (value) {
				var date = value;
				var yyyy = date.getFullYear().toString();
				var mm = this.pad((date.getMonth() + 1).toString(), "0", 2); // getMonth() is zero-based
				var dd = this.pad(date.getDate().toString(), "0", 2);
				var h = this.pad(date.getHours(), "0", 2);
				var m = this.pad(date.getMinutes(), "0", 2);
				var s = this.pad(date.getSeconds(), "0", 2);
				fechaString = dd + "/"+ mm + "/" + yyyy + " " + h + ":" + m + ":" + s; // padding 
			} else {
				fechaString = "";
			}
			////////////////////////////
			return fechaString;
		},
		formatFechaStringToDDMMAAAAHHMMSS: function (value) {
			var fechaString;
			/////////////////////////////
			if (value) {
				fechaString = value.split("T")[0].split("-")[2] + "/" + value.split("T")[0].split("-")[1] + "/" +value.split("T")[0].split("-")[0]
			    fechaString = fechaString + " " + value.split("T")[1].split(".")[0];
			} else {
				fechaString = "";
			}
			////////////////////////////
			return fechaString;
		},
		formatFechaDDMMAAAAHora: function (date) {
			if (!date) {
				return date;
			}
			var fecha = this.pad(date.getDate(), "0", 2) + "/" + this.pad((date.getMonth() + 1), "0", 2) + "/" + this.pad(date.getFullYear(), "0",
					4) + " " +
				this.pad(date.getHours(), "0", 2) + ":" + this.pad(date.getMinutes(), "0", 2) + ":" + this.pad(date.getSeconds(), "0", 2);
			return fecha;
		},
		formatFechaAAAAMMDDHora: function (date) {
			if (!date) {
				return date;
			}
			var fecha = this.pad(date.getFullYear(), "0",
					4) +"-"+this.pad((date.getMonth() + 1), "0", 2)+"-" +this.pad(date.getDate(), "0", 2) + " " +
				this.pad(date.getHours(), "0", 2) + ":" + this.pad(date.getMinutes(), "0", 2) + ":" + this.pad(date.getSeconds(), "0", 2);
			return fecha;
		},
		formatFechaDDMMAAAA: function (date) {
			if (!date) {
				return date;
			}
			var fecha = this.pad(date.getDate(), "0", 2) + "/" + this.pad((date.getMonth() + 1), "0", 2) + "/" + this.pad(date.getFullYear(), "0",
				4);
			return fecha;
		},
		formatFechaDD_MM_AAAA: function (date) {
			if (!date) {
				return date;
			}
			var fecha = this.pad(date.getDate(), "0", 2) + "-" + this.pad((date.getMonth() + 1), "0", 2) + "-" + this.pad(date.getFullYear(), "0",
				4);
			return fecha;
		},
		formatFechaDate: function (date) {
			if (!date) {
				return date;
			}
			var iFecha = date.split("/");
			var fecha = new Date(iFecha[2].split(" ")[0] + "/" + iFecha[1] + "/" + iFecha[0] + " " + iFecha[2].split(" ")[1]);
			return fecha;
		},
		valorDefault: function (table, objectDefault) {
			var tableInicial = "/Detail/";
			var valorDefault = "";
			if (table == tableInicial + "Contenedor") {
				valorDefault = "XNA_AEREO";
				objectDefault.Tipo = valorDefault;
				objectDefault.Tamanio = valorDefault;
				objectDefault.Clasificacion = valorDefault;
			} else if (table == tableInicial + "Paquete") {

			} else if (table == tableInicial + "Precinto") {

			} else if (table == tableInicial + "Adjunto") {

			} else if (table == tableInicial + "Flete") {
				valorDefault = "XNAX";
				objectDefault.TipoPago = valorDefault;
				objectDefault.TipoFlete = valorDefault;
				objectDefault.MonedaFlete = valorDefault;
				//objectDefault.DestinacionCarga = valorDefault;
			} else { //DocumentoTransporte Directa, Master e Hija
				valorDefault = "XNAX";
				objectDefault.TipoDocumentoTransporte = valorDefault;
				objectDefault.Indicador = valorDefault;
				objectDefault.TipoBulto = valorDefault;
				//objectDefault.LugarEmision = valorDefault;//Comentado 20-09-2019
				objectDefault.CodModalidad = null;
				objectDefault.PuntoLlegada = 4372;
				objectDefault.DepositoTemporal = valorDefault;
				objectDefault.TerminalOrigen = valorDefault;
				objectDefault.TerminalDestino = valorDefault;
				objectDefault.NaturalezaCarga = valorDefault;
				//objectDefault.CodigoUN = valorDefault; //Comentado Obs 31-07-2019
				objectDefault.IdTipoCondicion = valorDefault;
				objectDefault.AgenteCarga = null;
				//objectDefault.EmbarcadorCodigo = valorDefault; //Comentado Obs 31-07-2019
				//objectDefault.ConsignatarioCodigo = valorDefault; //Comentado Obs 31-07-2019
				///objectDefault.NotificadoCodigo = valorDefault; //Comentado Obs 31-07-2019
				//objectDefault.ConsolidadorCodigo = valorDefault; //Comentado Obs 31-07-2019
				objectDefault.ValorMercanciaMoneda = valorDefault;
				objectDefault.EntidadReguladoraMercPeligrosa = valorDefault;
				objectDefault.DestinacionCarga = valorDefault; //Nuevo

				valorDefault = "XNA_AEREO";
				//objectDefault.Origen = valorDefault;//Comentado 20-09-2019
				//objectDefault.Destino = valorDefault;//Comentado 20-09-2019
				//objectDefault.CodTipoAlmacenamiento = valorDefault;
				//objectDefault.PuertoEmbarque = valorDefault;//Comentado 20-09-2019
				objectDefault.ConsolidadorPuertoDescarga = valorDefault;
			}
			return objectDefault;
		},
		btnMenosTable2: function (self, idTable, model, table) {
			var tabla = self.getView().byId(idTable);
			var listTotalGuia = self.getView().getModel(model).getProperty(table);
			var itemsTabla = tabla.getSelectedIndices().length;
			if (itemsTabla > 0) {
				for (var i = itemsTabla - 1; i >= 0; i--) {
					var item = tabla.getSelectedIndices()[i];
					listTotalGuia.splice(item, 1);
					self.getView().getModel(model).setProperty(table, listTotalGuia);
					self.removeSelection(idTable);
				}
				MessageToast.show('Se ha(n) Eliminado el(los) item(s) seleccionado(s)', {
					duration: 3000
				});
				return;
			} else {
				MessageToast.show('No ha seleccionado ningun item', {
					duration: 3000
				});
				return;
			}
		},
		btnMenosTable: function (self, idTable, model, table) {
			//var tabla = self.getView().byId(idTable);
			var tabla = sap.ui.getCore().byId(idTable);
			var itemsTabla = tabla.getSelectedIndices().length;
			var mensaje = 'Usted tiene registros editables sin validar.';
				var aRegistrosEditados =$.grep(self.getView().getModel(model).getProperty(table), function( n, i ) {
					return n["enabledInput"]===true;
				  });
				  if(aRegistrosEditados.length>0){
					return self.utilPopUps.messageBoxOk(mensaje,"WARNING","Alerta");
				  }
			if(itemsTabla==0){
				return sap.m.MessageToast.show("No ha seleccionado ningun item.", {duration: 3000});
			}
			var fechaHoy = new Date();
			fechaHoy.setHours(0, 0, 0);
			var semanas="";
			var modEliminar = [];
			tabla.getSelectedIndices().forEach(function(item,x){
				var obj = self.getView().getModel(model).getProperty(table + "/" + item);
				modEliminar.push(obj);
			});
			var validNoEliminar=modEliminar.filter(function (el) {
				return Date.parse(el.dFechaInicio)<Date.parse(fechaHoy);
			});
			validNoEliminar.forEach(function(item,x){
					if(validNoEliminar.length==x+1){
						semanas = semanas+item.iSemana;
					}else{
						semanas = semanas+item.iSemana+", ";
					}
			});
			if(semanas!=""){
				return sap.m.MessageToast.show(this.i18n("mensajeValidacionElimnarFueraHorario").replace("{0}",semanas), {duration: 3000});
			}
			var rs = self.utilPopUps.messageBox('¿Desea eliminar los items seleccionados?', 'c');
			rs.then(function(confirmation){
				if (confirmation) {  
					var listTotalGuia = self.getView().getModel(model).getProperty(table);
					var aItems = [];
					if (itemsTabla > 0) {
						for (var i = itemsTabla - 1; i >= 0; i--) {
							var item = tabla.getSelectedIndices()[i];
							var posReal = tabla.getBinding().aIndices;
							var obj = self.getView().getModel(model).getProperty(table + "/" + posReal[item]);
							var iId = obj.iId;
								if(iId){
									self.getView().getModel(model).setProperty(table + "/" + posReal[item]+"/EstadoLocal",'E');
								}else{
									listTotalGuia.splice(posReal[item], 1);
								}
								self.getView().getModel(model).refresh(true);
						}
						sap.ui.getCore().byId(idTable).clearSelection();
						var modListTotalGuia = self.getView().getModel(model).getProperty(table);
						var cont = 0;
						modListTotalGuia.forEach(function(item,x){
							if(item.EstadoLocal!=='E'){
								cont = cont+1;
								item.iSemana = cont;
							}
						});
						self.getView().getModel(model).setProperty(table,modListTotalGuia);
						self.getView().getModel(model).refresh(true);
						console.log(self.getView().getModel(model).getProperty(table));
					} else {
						MessageToast.show('No ha seleccionado ningun item', {
							duration: 3000
						});
						return;
					}
				}
			});
		},
		btnMasTable: function (self,idTable, model, table, callback) {
			sap.ui.getCore().byId(idTable).clearSelection();
			var that = this;
			var items = self.getView().getModel(model).getProperty(table);
			items = items ? items : [];
			items = items.filter(function (el) {
				return el.EstadoLocal !== 'E';
			});
			var contenedor = this.unlink(items);
			var otrasFilas = this.unlink(items);
			contenedor = contenedor ? contenedor : [];
			
			var objectDefault = {
				EstadoLocal:'P',
				enabledInput: true,
				validMostrarPDF: false,
				icon: this.i18n("iconAceptar"),
				iSemana:contenedor.length+1,
				anterior: {}
			};
			/////////////////////
			var validPeriodo = self.utilController.validPeriodo(self.getView().getModel(model).getData());
			if(!validPeriodo){
				return;
			}
			self.getView().getModel(model).setProperty("/dPeriodo",validPeriodo.modFechaPeriodo);
			self.getView().getModel(model).setProperty("/sPeriodo",validPeriodo.modFechaPeriodoString);
			self.getView().getModel(model).setProperty("/iAnioPeriodo",validPeriodo.modAnio);
			self.getView().getModel(model).setProperty("/iMesPeriodo",validPeriodo.modMes);
			/////////////////////
			var dataModel = self.getView().getModel(model).getData();
			var ultimoDiaPeriodo = new Date(dataModel.dPeriodo);
			ultimoDiaPeriodo.setMonth(ultimoDiaPeriodo.getMonth()+1);
			ultimoDiaPeriodo.setDate(ultimoDiaPeriodo.getDate()-1);
			if(items.length>0){
				if(items[items.length-1].dFechaFin.toLocaleDateString() == ultimoDiaPeriodo.toLocaleDateString()){
					sap.m.MessageToast.show("No se puede registrar mas horario, ya que ha registrado el último día del período.", { duration: 3000, width: "25em" 	});  
					return; 
				}
			}
			var validHorario = self.utilController.validHorario(objectDefault,dataModel);
			objectDefault.minDateInicio = validHorario.modMinMes;
			objectDefault.maxDateInicio = validHorario.modMaxDateInicio;
			objectDefault.minDateFin = validHorario.modMminDateFin;
			objectDefault.maxDateFin = validHorario.modMaxDateFin;
			if (contenedor.length > 0) {
				var ultimoObj = contenedor[contenedor.length - 1];
				if (ultimoObj.anterior && jQuery.isEmptyObject(ultimoObj.anterior)) {
					return;
				}
				for (var i in contenedor) {
					contenedor[i].icon = this.i18n("iconEditar");
					contenedor[i].enabledInput = false;
				}
			}
			contenedor.push(objectDefault);
			self.getView().getModel(model).setProperty(table, contenedor);
			self.utilController.reordenarUltimoMinFecha(self,model,table);
			self.getView().getModel(model).refresh(true);
		},
		changeEditSaveRowTable: function (self, oEvent, model, campoAnterior, campoIcon, campoEnabled, event) {
			var objDepend = oEvent.getSource().getBindingContext(model).getObject();
			var obj = this.unlink(objDepend);
			var path = oEvent.getSource().getBindingContext(model).getPath();
			if (obj.icon === this.i18n("iconEditar")) {
				var contenedor = obj; //this.unlink(self.getView().getModel(model).getProperty(path));
				self.getView().getModel(model).setProperty(path + "/" + campoAnterior, contenedor);
				self.getView().getModel(model).setProperty(path + "/" + campoIcon, this.i18n("iconAceptar"));
				self.getView().getModel(model).setProperty(path + "/" + campoEnabled, true);
				self.getView().getModel(model).refresh(true);
				self.mostrarTablas(false);
				self.mostrarTablas(true);
			} else if (obj.icon === this.i18n("iconAceptar")) {
				obj.path = path;
				if (obj.Id && obj.Id != "") {
					////Editar
					event.Editar(obj);
				} else {
					////Registrar
					event.Registrar(obj);
				}
				////
			}
		},
		changeCancelRowTable: function (self, oEvent, model, table) {
			var campoAnterior="anterior";
			var campoIcon="icon";
			var campoEnabled="enabledInput";
			var objDepend = oEvent.getSource().getBindingContext(model).getObject();
			var obj = this.unlink(objDepend);
			var path = oEvent.getSource().getBindingContext(model).getPath();
			if (obj.icon === this.i18n("iconAceptar")) {
				if (obj.Id && obj.Id != "") {//if(obj.anterior && jQuery.isEmptyObject(obj.anterior)){//
					////Editar
					var objAnterior = obj[campoAnterior];
					self.getView().getModel(model).setProperty(path, objAnterior);
					self.getView().getModel(model).setProperty(path + "/" + campoIcon, this.i18n("iconEditar"));
					self.getView().getModel(model).setProperty(path + "/" + campoEnabled, false);
				} else {
					////Registrar
					if(obj.anterior && jQuery.isEmptyObject(obj.anterior)){
					    var contenedor = self.getView().getModel(model).getProperty(table);
						contenedor.pop();
						self.getView().getModel(model).setProperty(table, contenedor);
					}else{
						var objAnterior = obj[campoAnterior];
					    self.getView().getModel(model).setProperty(path, objAnterior);
						self.getView().getModel(model).setProperty(path + "/" + campoIcon, this.i18n("iconEditar"));
					    self.getView().getModel(model).setProperty(path + "/" + campoEnabled, false);
					}
					
				}
				self.getView().getModel(model).refresh(true);
			}
		},
		btnMasTableUltimo: function (self, model, table) {
			var contenedor = self.getView().getModel(model).getProperty(table);
			contenedor = contenedor ? contenedor : [];
			var valorDefault = {
				enabledInput: true,
				icon: "sap-icon://save",
				anterior: {}
			};
			if (jQuery.isEmptyObject(contenedor)) {
				contenedor = [valorDefault];
			} else {
				var ultimoObj = contenedor[contenedor.length - 1];
				if (ultimoObj.anterior && jQuery.isEmptyObject(ultimoObj.anterior)) {
					return;
				}
				for (var i in contenedor) {
					contenedor[i].icon = "sap-icon://edit";
					contenedor[i].enabledInput = false;
				}
				contenedor.push(valorDefault);

			}
			self.getView().getModel(model).setProperty(table, contenedor);
		},
		
		changeEditSaveRowTableLocal: function (self, oEvent, model,nameFieldGroup) {
			var campoAnterior="anterior";
			var campoIcon="icon";
			var campoEnabled="enabledInput";
			var that = this;
			var objDepend = oEvent.getSource().getBindingContext(model).getObject();
			var obj = this.unlink(objDepend);
			var path = oEvent.getSource().getBindingContext(model).getPath();
			var contenedor = this.unlink(self.getView().getModel(model).getProperty(path));
			if (obj.icon === this.i18n("iconEditar")) {
				////Editar
				var pathLista = "/"+path.split("/")[1];
				var totalFilasInicial = this.unlink(self.getView().getModel(model).getProperty(pathLista));
				var otrasFilas = this.unlink(self.getView().getModel(model).getProperty(pathLista));
                otrasFilas = otrasFilas.filter(function (el) {
					return !jQuery.isEmptyObject(el[campoAnterior]);
				});
				var flag,flagX;
				otrasFilas.forEach(function(item,x){
					if(item[campoIcon]==that.i18n("iconAceptar")){
					    flag=true;	
					    flagX=x;
					}
				});
				var aFilasDidabled = otrasFilas.filter(function (el) {
					return el[campoEnabled] === false;
				});
                
                
				if(totalFilasInicial.length!==aFilasDidabled.length){
					var rs = utilPopUps.messageBox("Hay una fila pendiente de grabar. ¿Desea continuar?", "C");
					rs.then(function(){
						if (confirmation) { 
							var modFilas = []; 
							otrasFilas.forEach(function(item){
								var obj = self.utilController.unlink(item[campoAnterior]);
								var objAnterior = self.utilController.unlink(item[campoAnterior]);
								obj[campoAnterior] = objAnterior;
								obj[campoIcon] = that.i18n("iconEditar");
								obj[campoEnabled] = false;
								modFilas.push(obj);
							});
							self.getView().getModel(model).setProperty(pathLista,modFilas);
							self.getView().getModel(model).setProperty(path + "/" + campoAnterior, contenedor);
							self.getView().getModel(model).setProperty(path + "/" + campoIcon, that.i18n("iconAceptar"));
							self.getView().getModel(model).setProperty(path + "/" + campoEnabled, true);
							var iId = self.getView().getModel(model).getProperty(path + "/iId");
							if(iId){
								self.getView().getModel(model).setProperty(path + "/" + "EstadoLocal", 'M');
							}else{
								self.getView().getModel(model).setProperty(path + "/" + "EstadoLocal", 'P');
							}
						}
					});
				}else{
					/*var fechaHoy = new Date();
					var objValid = self.getView().getModel(model).getProperty(path); 
					if(objValid.dFechaInicio<fechaHoy){
						return sap.m.MessageToast.show(that.i18n("mensajeValidacionEditarFueraHorario").replace("{0}",objValid.iSemana), {
							duration: 3000
						});
					}*/
						if(flag){
							var valid = self.utilValidation.validatorSAPUI5(self, [nameFieldGroup]); // para Formulario
							if (!valid) {
								self.getView().getModel(model).setProperty(pathLista+"/"+flagX + "/" + campoAnterior, contenedor);
								self.getView().getModel(model).setProperty(pathLista+"/"+flagX + "/" + campoIcon, that.i18n("iconAceptar"));
								self.getView().getModel(model).setProperty(pathLista+"/"+flagX + "/" + campoEnabled, true);	
								return sap.m.MessageToast.show(that.i18n("mensajeValidacionError"), {
									duration: 3000
								});
							}
						}else{
							self.getView().getModel(model).setProperty(pathLista,otrasFilas);
							self.getView().getModel(model).setProperty(path + "/" + campoAnterior, contenedor);
							self.getView().getModel(model).setProperty(path + "/" + campoIcon, that.i18n("iconAceptar"));
							self.getView().getModel(model).setProperty(path + "/" + campoEnabled, true);
							var iId = self.getView().getModel(model).getProperty(path + "/iId");
							if(iId){
								self.getView().getModel(model).setProperty(path + "/" + "EstadoLocal", 'M');
							}else{
								self.getView().getModel(model).setProperty(path + "/" + "EstadoLocal", 'P');
							}	
						}
				}
				that.reordenarMinfechaSelected(self,model,path);
				////
			} else if (obj.icon === this.i18n("iconAceptar")) {
				////Registrar
				var objAnterior = obj[campoAnterior];
				var objAnteriorIsEmpty = jQuery.isEmptyObject(objAnterior);
				var valid = self.utilValidation.validatorSAPUI5(self, [nameFieldGroup], oEvent, model); ///Para tabla por ello tiene el  
					if (!valid) {
						return sap.m.MessageToast.show(that.i18n("mensajeValidacionError"), {
							duration: 3000
						});
					}else {
                        self.getView().getModel(model).setProperty(path + "/" + campoIcon, self.utilController.i18n("iconEditar"));
                        self.getView().getModel(model).setProperty(path + "/" + campoEnabled, false);
						self.getView().getModel(model).setProperty(path + "/" + campoAnterior, contenedor);
						var iId = self.getView().getModel(model).getProperty(path + "/iId");
						if(iId){
							self.getView().getModel(model).setProperty(path + "/" + "EstadoLocal", 'M');
						}else{
							self.getView().getModel(model).setProperty(path + "/" + "EstadoLocal", 'P');
						}
                    }
			}
		},
		changeCancelRowTable2: function (self, oEvent, model, table, campoAnterior, campoIcon, campoEnabled) {
			var objDepend = oEvent.getSource().getBindingContext(model).getObject();
			var obj = this.unlink(objDepend);
			var path = oEvent.getSource().getBindingContext(model).getPath();
			if (obj.icon === "sap-icon://edit") {
				////
				self.getView().getModel(model).setProperty(path + "/" + campoIcon, "sap-icon://save");
				self.getView().getModel(model).setProperty(path + "/" + campoEnabled, true);
				////
			} else if (obj.icon === "sap-icon://save") {
				////
				var objAnterior = obj[campoAnterior];
				self.getView().getModel(model).setProperty(path + "/" + campoIcon, "sap-icon://edit");
				self.getView().getModel(model).setProperty(path + "/" + campoEnabled, false);
				self.getView().getModel(model).setProperty(path, objAnterior);
				var objAnteriorIsEmpty = jQuery.isEmptyObject(objAnterior);
				if (objAnteriorIsEmpty) {
					var contenedor = this.unlink(self.getView().getModel(model).getProperty(table));
					contenedor.pop();
					self.getView().getModel(model).setProperty(table, contenedor);
				}
				////
			}
		},
		toStringIE:function(data){
			var modData = data.replace(/[^\x00-\x7F]/g, "");
			return modData;
		},
		addObjectEntries:function(){
			if (!Object.entries){
				Object.entries = function( obj ){
					var ownProps = Object.keys( obj ),
						i = ownProps.length,
						resArray = new Array(i); // preallocate the Array
					while (i--)
					resArray[i] = [ownProps[i], obj[ownProps[i]]];
					return resArray;
				};
			}
			return Object;
		},
		unlink: function (object) {
			Object = this.addObjectEntries();
			var modData = object;
			var returnJSON = modData ? JSON.parse(JSON.stringify(modData)) : null;
			if(returnJSON){
				var keys = Object.entries(modData);
				var keysFormat = keys.filter(function (el) {
					return typeof el[1] == "object" && el[1] != null;
				});
				if(keysFormat){
					keysFormat.forEach(function(item){
						returnJSON[item[0]] = item[1];
					});
				}
			}
			return returnJSON;
		},
		convertArrayToStringPDF:function(aData){
			Object = this.addObjectEntries();
			var modData = [];
			if(aData){
				aData.forEach(function(item){
					var entries = Object.entries(item);
					var obj = {};
					entries.forEach(function(ent){
						obj[ent[0]]=typeof ent[1]=="object"?ent[1]:(ent[1]===null || ent[1]===undefined?"":( ent[0].includes("Importe") ? formatter.FormatterDinero(ent[1]) : ent[1].toString() ));
					})
					modData.push(obj);
				});
			}
			return modData;
		},
		convertObjectToStringPDF:function(aData){
			Object = this.addObjectEntries();
			var modData = {};
			if(aData){
				var entries = Object.entries(aData);
				entries.forEach(function(ent){
					modData[ent[0]]=typeof ent[1]=="object" && ent[1]?ent[1]:(ent[1]===null || ent[1]===undefined?"":ent[1].toString());
				})
			}
			return modData;
		},
		createDialog: function (self, idFrag, rutaFrag) {
			try {
				sap.ui.getCore().byId(idFrag).destroy();
			} catch (e) {}
			if (!sap.ui.getCore().byId(idFrag)) {
				self[idFrag] = sap.ui.xmlfragment(
					constantes.idProyecto + rutaFrag,
					self.getView().getController() // associate controller with the fragment            
				);
				self.getView().addDependent(self[idFrag]);
			}
			sap.ui.getCore().byId(idFrag).open();
			sap.ui.core.BusyIndicator.hide();
		},
		createTable: function (self, idFrag, rutaFrag, idPanel, create, bHideLoading) {
			try {
				sap.ui.getCore().byId(idFrag).destroy();
			} catch (e) {}
			if (create) {
				if (!sap.ui.getCore().byId(idFrag)) {
					self[idFrag] = sap.ui.xmlfragment(
						constantes.idProyecto + rutaFrag,
						self.getView().getController() // associate controller with the fragment            
					);
					self.getView().byId(idPanel).addContent(self[idFrag]);
				}
				if (bHideLoading) {
					jQuery.sap.delayedCall(3000, self, function () {
						sap.ui.core.BusyIndicator.hide();
					});
				}
			}
		},
		createPage: function (view, splitId, viewId, rutaController) {
			var viewDefault = view.getView().byId(viewId).getId();
			view.getView().byId(splitId).to(view.createId(viewDefault));
			//this.callController(rutaController).onRouteMatched();
			/*$.Home.getView().byId(splitId).setMode("ShowMode");
			$.Home.getView().byId(splitId).setMode("HideMode");*/
		},
		callController: function (rutaController) {
			return sap.ui.controller(constantes.idProyecto + ".controller." + rutaController);
		},
		controllerFromView: function (controller, id) {
			var idController = this.byId(controller, id).getId();
			return sap.ui.getCore().byId(idController).getController();
		},
		createPageMaster: function (self, splitId, viewId) {
			var viewDefault = self.getView().byId(viewId).getId();
			self.getView().byId(splitId).toMaster(self.createId(viewDefault));
		},
		getControllerView: function (nameController) {
			if (!this.controllerView) {
				this.controllerView = sap.ui.getCore().byId(sap.ui.controller(constantes.idProyecto + nameController).getId())
					.getController();
			}
			return this.controllerView;
		},
		property: function (controller, root, data, bRefresh) {
			if (data) {
				controller.getView().getModel().setProperty(root, data);
				if (bRefresh === true) {
					this.refreshModel(controller);
				}
				return null;
			} else {
				return controller.getView().getModel().getProperty(root);
			}
		},
		refreshModel: function (controller) {
			controller.getView().getModel().refresh();
		},
		byId: function (controller, id) {
			return controller.getView().byId(id);
		},
		navToPage: function (self, page) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(self);
			oRouter.navTo(page);
			this.getControllerView(page);
		},
		navTo: function (controller, page, data) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(controller);
			//oRouter.navTo(null, data ? data : null, true);
			oRouter.navTo(page, {Id:0}, false);
			/////////////////////////////////////////////////////////////////////
			var oRouterSecond = sap.ui.core.UIComponent.getRouterFor(controller);
			//oRouter.navTo(null, data ? data : null, true);
			oRouterSecond.navTo(page, data ? data : null, false);
		},
		navBack: function (controller) {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(controller);
				oRouter.navTo(constantes.PaginaHome, true);
			}
		},
		filtroInicialFechasOC: function (self) {
			// Setea los Date Picker de OC a los ultimos 3 meses
			var date = new Date(new Date().getFullYear(), 0, 1);
			var date2 = new Date();
			var dpFin = self.getView().byId("dpFin");
			dpFin.setDateValue(date2);
			var dpIni = self.getView().byId("dpIni");
			dpIni.setDateValue(date);
		},
		filtroInicialFechasComprobantes: function (self) {
			// Setea los Date Picker de Comprobantes a los ultimos 3 meses
			var dateC = new Date(new Date().getFullYear(), 0, 1);
			var dateC2 = new Date();
			var dpFinC = self.getView().byId("dpFinDOA");
			dpFinC.setDateValue(dateC2);
			var dpIniC = self.getView().byId("dpIniDOA");
			dpIniC.setDateValue(dateC);
		},
		getParameter: function (self, oEvent, nameParam, nameView, data) {
			var parametro = oEvent.mParameters ? oEvent.getParameter("arguments")[nameParam] : null;
			if (!parametro) {
				if (nameView) {
					this.navTo(self, nameView, data);
				}
			}
			return parametro;
		},
		alignTextCutPhraseJsPDF: function (Doc, Value, CoordX, CoordY, WidthString, Align) {
			var doc = Doc;
			var coordX = CoordX;
			var coordY = CoordY;
			var widthString = WidthString;
			var paragraph = Value;
			var lines = doc.splitTextToSize(paragraph, widthString);
			var dim = doc.getTextDimensions('Text');
			var lineHeight = dim.h;
			var maximoWidth = 0;
			for (var l = 0; l < lines.length; l++) {
				if (doc.getTextWidth(lines[l]) > maximoWidth) {
					maximoWidth = doc.getTextWidth(lines[l]);
				}
			}
			for (var i = 0; i < lines.length; i++) {
				var lineTop = i == 0 ? coordY : ((lineHeight / 3.3) * (i)) + coordY;
				lines[i] = lines[i].toString().replace(/ /g, "|");
				while (doc.getTextWidth(lines[i]) < maximoWidth) {
					if (doc.getTextWidth(lines[i]) * 1.2 <= maximoWidth) {
						break;
					}
					if (!lines[i].toString().includes("|")) {
						lines[i] = lines[i].toString().replace(/ /g, "|");
					}
					lines[i] = lines[i].toString().replace("|", "  ");
				}
				while (lines[i].includes("|")) {
					lines[i] = lines[i].toString().replace("|", " ");
				}

			}
			doc.text(lines[0], coordX, lineTop, {
				maxWidth: widthString,
				align: Align //justify,center,left,rihgt
			}); //see this line
		},
		orderByAscMejoraxMultipleCondiciones : function(data,firstKey,secondKey,thirdKey){
           
         var modData = [];
            if(data){
            	if(data.length>0){
            		 modData = data.sort(function (a, b) {
					if (b[firstKey] > a[firstKey]) {  
						return -1;  
					} else if (b[firstKey] < a[firstKey]) {  
						return 1;  
					}  
					else {
						 if (b[secondKey] > a[secondKey]) {  
						return -1;  
						} else if (b[secondKey] < a[secondKey]) {  
							return 1;  
						}else{
							if (b[thirdKey] > a[thirdKey]) {  
								return -1;  
							} else if (b[thirdKey] < a[thirdKey]) {  
								return 1;  
							}
						}  
					}
				  });
            	}
            }
           
            return modData;


		},
		orderByGeneral: function (data, campoOrder, order,campoType) {
			var modData = data;
			if (campoOrder) {
				if (campoOrder.length > 0) {
					modData.forEach(function(item, index) {
						item.keyOrder = null;
						campoOrder.forEach(function(campo) {
							item.keyOrder = (item.keyOrder ? item.keyOrder : (campoType=="Numeric" ? 0 :"")) + item[campo];
						});
					});
				}
			}
			if (order == "ASC") {
				modData = modData.sort(function(a, b) {
					if (a.keyOrder > b.keyOrder) {
						return 1;
					}
					if (a.keyOrder < b.keyOrder) {
						return -1;
					}
					// a must be equal to b
					return 0;
				});
			} else if (order == "DESC") {
				modData = modData.sort(function(a, b) {
					if (a.keyOrder > b.keyOrder) {
						return -1;
					}
					if (a.keyOrder < b.keyOrder) {
						return 1;
					}
					// a must be equal to b
					return 0;
				});
			}
			
			return modData;
		},
		orderByNew:function(data,name,order){
			var modData = [];
			if(order=="ASC"){
				modData = data.sort(function (a, b) {
					if (a[name] > b[name]) {
					  return 1;
					}
					if (a[name] < b[name]) {
					  return -1;
					}
					// a must be equal to b
					return 0;
				  });
			}
			else if(order=="DESC"){
				modData = data.sort(function (a, b) {
					if (a[name] > b[name]) {
					  return -1;
					}
					if (a[name] < b[name]) {
					  return 1;
					}
					// a must be equal to b
					return 0;
				  });
			}
			
          return modData;
		},
		orderBy:function(data,name){
			var modData = data.sort(function (a, b) {
            if (a[name] > b[name]) {
              return 1;
            }
            if (a[name] < b[name]) {
              return -1;
            }
            // a must be equal to b
            return 0;
          });
          return modData;
		},
		orderById:function(data){
			var modData = data.sort(function (a, b) {
            if (a.iId > b.iId) {
              return 1;
            }
            if (a.iId < b.iId) {
              return -1;
            }
            // a must be equal to b
            return 0;
          });
          return modData;
		},
		orderByFecha:function(data){
			var modData = data.sort(function (a, b) {
            if ((a.Anio+(a.id)*0.0833) > (b.Anio+(b.id)*0.0833)) {
              return 1;
            }
            if ((a.Anio+(a.id)*0.0833) < (b.Anio+(b.id)*0.0833)) {
              return -1;
            }
            // a must be equal to b
            return 0;
          });
          return modData;
        },
        createDialogController: function (self,rutaFrag) {
			sap.ui.core.BusyIndicator.show(0);
			return new Promise(function (resolve, reject) {
				var modRutaFrag = rutaFrag.replace("view","controller");
				var oCtrl = sap.ui.controller(constantes.idProyecto+modRutaFrag);		
				var idFrag = rutaFrag.split(".")[rutaFrag.split(".").length-1];	
				var idFragEdit = rutaFrag.split(".")[rutaFrag.split(".").length-1].includes("Add") ? rutaFrag.split(".")[rutaFrag.split(".").length-1].replace("Add","Edit") : rutaFrag.split(".")[rutaFrag.split(".").length-1].replace("Edit","Add");		
				try {
					sap.ui.getCore().byId(idFrag).destroy();
				} catch (e) {}
				try {
					self[idFrag].destroy();
				} catch (e) {}
				self[idFrag]=null;
				try {
					sap.ui.getCore().byId(idFragEdit).destroy();
					
				} catch (e) {}
				try {
					self[idFragEdit].destroy();
					
				} catch (e) {}
				if (!sap.ui.getCore().byId(idFrag)) {
					self[idFrag] = sap.ui.xmlfragment(
						constantes.idProyecto+rutaFrag,
						oCtrl //self.getView().getController() // Asociar Controlador al fragment        
					);
					self.getView().addDependent(self[idFrag]);// Asociar Vista al fragment
				}
				sap.ui.getCore().byId(idFrag).open();
				oCtrl.onInit();
				sap.ui.core.BusyIndicator.hide();
				resolve(oCtrl);
			});
		},
        createTableController: function (self, rutaFrag, idContent, create, bHideLoading) {
			sap.ui.core.BusyIndicator.show(0);
			return new Promise(function (resolve, reject) {
				var modRutaFrag = rutaFrag.replace("view","controller");
				var oCtrl = sap.ui.controller(constantes.idProyecto+modRutaFrag);	
				var idFrag = rutaFrag.split(".")[rutaFrag.split(".").length-1];	
				var idFragEdit = rutaFrag.split(".")[rutaFrag.split(".").length-1].includes("Add") ? rutaFrag.split(".")[rutaFrag.split(".").length-1].replace("Add","Edit") : rutaFrag.split(".")[rutaFrag.split(".").length-1].replace("Edit","Add");		
				try {
					sap.ui.getCore().byId(idFrag).destroy();
				} catch (e) {}
				try {
					self[idFrag].destroy();
				} catch (e) {}
				self[idFrag]=null;
				try {
					sap.ui.getCore().byId(idFragEdit).destroy();
					
				} catch (e) {}
				try {
					self[idFragEdit].destroy();
					
				} catch (e) {}
				self[idFragEdit]=null;
				if (create) {
					if (!self[idFrag]) {
						self[idFrag] = sap.ui.xmlfragment(
							constantes.idProyecto + rutaFrag,
							oCtrl//self.getView().getController() // Asociar Controlador al fragment      
						);
						self.getView().byId(idContent).addContent(self[idFrag]);//Asociar Vista al fragment
						console.log("name: "+idFrag ,"table: "+self[idFrag] +" creation: "+create);
						oCtrl.onInit();
						sap.ui.core.BusyIndicator.hide();
						resolve(oCtrl);
					}
				}
			});
		},
		createTabController: function (self, rutaFrag, idContent, create, bHideLoading) {
			sap.ui.core.BusyIndicator.show(0);
			return new Promise(function (resolve, reject) {
				var modRutaFrag = rutaFrag.replace("view","controller");
				var oCtrl = sap.ui.controller(constantes.idProyecto+modRutaFrag);	
				var idFrag = rutaFrag.split(".")[rutaFrag.split(".").length-1];	
				var idFragEdit = rutaFrag.split(".")[rutaFrag.split(".").length-1].includes("Add") ? rutaFrag.split(".")[rutaFrag.split(".").length-1].replace("Add","Edit") : rutaFrag.split(".")[rutaFrag.split(".").length-1].replace("Edit","Add");		
				try {
					sap.ui.getCore().byId(idFrag).destroy();
				} catch (e) {}
				try {
					self[idFrag].destroy();
				} catch (e) {}
				self[idFrag]=null;
				try {
					sap.ui.getCore().byId(idFragEdit).destroy();
					
				} catch (e) {}
				try {
					self[idFragEdit].destroy();
					
				} catch (e) {}
				self[idFragEdit]=null;
				if (create) {
					if (!self[idFrag]) {
						self[idFrag] = sap.ui.xmlfragment(
							constantes.idProyecto + rutaFrag,
							oCtrl//self.getView().getController() // Asociar Controlador al fragment      
						);
						self.getView().byId(idContent).addItem(self[idFrag]);//Asociar Vista al fragment
						console.log("name: "+idFrag ,"tab: "+self[idFrag] +" creation: "+create);
						oCtrl.onInit();
						sap.ui.core.BusyIndicator.hide();
						resolve(oCtrl);
					}
				}
			});
		},
		createListController: function (self, rutaFrag, idContent, create, bHideLoading) {
			sap.ui.core.BusyIndicator.show(0);
			return new Promise(function (resolve, reject) {
				var modRutaFrag = rutaFrag.replace("view","controller");
				var oCtrl = sap.ui.controller(constantes.idProyecto+modRutaFrag);	
				var idFrag = rutaFrag.split(".")[rutaFrag.split(".").length-1];	
				var idFragEdit = rutaFrag.split(".")[rutaFrag.split(".").length-1].includes("Add") ? rutaFrag.split(".")[rutaFrag.split(".").length-1].replace("Add","Edit") : rutaFrag.split(".")[rutaFrag.split(".").length-1].replace("Edit","Add");		
				try {
					sap.ui.getCore().byId(idFrag).destroy();
				} catch (e) {}
				try {
					self[idFrag].destroy();
				} catch (e) {}
				self[idFrag]=null;
				try {
					sap.ui.getCore().byId(idFragEdit).destroy();
					
				} catch (e) {}
				try {
					self[idFragEdit].destroy();
					
				} catch (e) {}
				
				if (create) {
					if (!self[idFrag]) {
						self[idFrag] = sap.ui.xmlfragment(
							constantes.idProyecto + rutaFrag,
							oCtrl//self.getView().getController() // Asociar Controlador al fragment      
						);
						self.getView().byId(idContent).addContent(self[idFrag]);//Asociar Vista al fragment
						console.log("List: "+self.getView().byId(idContent).getContent() +" creation: "+create);
						oCtrl.onInit();
						sap.ui.core.BusyIndicator.hide();
						resolve(oCtrl);
					}
				}
			});
		},
		obtenerSemanasMesAnio : function(date){
            var year=date  .getFullYear();
            var mes=date  .getMonth()+1;
            var primerdia = ((new Date(year, mes, 1).getDay()-1)%7+7)%7;
            var dias=Math.round(((new Date(year, mes))-(new Date(year, mes-1)))/86400000);
             return Math.ceil(dias/7);
		},
		obtenerNombreDia : function(index){
            var weekday=new Array(7);
            weekday[0]="Lunes";
            weekday[1]="Martes";
            weekday[2]="Miercoles";
            weekday[3]="Juergues";
            weekday[4]="Viernes";
            weekday[5]="Sábado";
            weekday[6]="Domingo";
            return weekday[index]
        },
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		newModel: function (sName, oData, oRules, oEventChange) {
			var oModel = dataPass.viewMaster.getView().getModel(sName) ? dataPass.viewMaster.getView().getModel(sName) : new sap.ui.model.json.JSONModel();
			var uValidation = dataPass.viewMaster.utilValidation;
			function setValueStateModel(oModelSV, bValidUndefined) {
				var oBodyCurrent = oModelSV.getData();
				var oBodyInit = oModelSV.initData ? oModelSV.initData : {};
				var bHasChanges = false;
				var errores = uValidation.validate(oModelSV.getData(), oRules);
				var error = errores[0] ? errores[0] : null;
				var SubfijoState = "State";
				var SubfijoMessage = "Message";

				if (Object.values(oBodyInit).length < 1) {
					bHasChanges = true;
				} else {
					Object.keys(oBodyInit).forEach(function (itemKeyInit) {
						var valorInit = oBodyInit[itemKeyInit];
						var valorCurrent = oBodyCurrent[itemKeyInit];
						if (valorCurrent != valorInit) {
							bHasChanges = true;
						}
					});
				}

				/*Object.keys(oRules).forEach(function (itemKey) {
					if (!oModelSV.getProperty("/" + itemKey + SubfijoState)) {
						oModelSV.setProperty("/" + itemKey + SubfijoState, "None");
						oModelSV.setProperty("/" + itemKey + SubfijoMessage, null);
					}
				});*/

				Object.keys(oRules).forEach(function (itemKeyBody) {
					var valor = oBodyCurrent[itemKeyBody];
					var isValid = true;
					if (bValidUndefined) {
						errores.forEach(function (itemError) {
							var errorText = itemError.error;
							var attribute = itemError.attribute;
							if (itemKeyBody === attribute) {
								isValid = false;
								if (_.isObject(errorText)) {
									var errorsArray = errorText.errors;

									valor.forEach(function (itemValor, indexValor) {
										Object.keys(errorsArray).forEach(function (indexdata) {
											if (parseInt(indexValor, 10) === parseInt(indexdata, 10)) {
												var itemErrorArray = errorsArray[indexdata];
												var error1 = itemErrorArray.error;
												Object.keys(itemValor).forEach(function (itemValorKey) {
													var isValidItem = true;
													Object.keys(error1).forEach(function (itemKeyInit2) {
														if (itemValorKey === itemKeyInit2) {
															isValidItem = false;
															valor[indexdata][itemKeyInit2 + SubfijoState] = "Error";
															valor[indexdata][itemKeyInit2 + SubfijoMessage] = error1[itemKeyInit2][0];
														}
													});

													if (isValidItem) {
														valor[indexdata][itemValorKey + SubfijoState] = "Success";
														valor[indexdata][itemValorKey + SubfijoMessage] = null;
													}

												});
											}
										});

									});

									oModelSV.setProperty("/" + attribute, valor);
								} else {

									oModelSV.setProperty("/" + attribute + SubfijoState, "Error");
									oModelSV.setProperty("/" + attribute + SubfijoMessage, errorText);
								}

							}
						});

						if (isValid) {
							oModelSV.setProperty("/" + itemKeyBody + SubfijoState, "Success");
							oModelSV.setProperty("/" + itemKeyBody + SubfijoMessage, null);
						}
					} else {
						if (valor !== undefined) {
							errores.forEach(function (itemError) {
								var errorText = itemError.error;
								var attribute = itemError.attribute;

								if (itemKeyBody === attribute) {
									isValid = false;
									if (_.isObject(errorText)) {
										var errorsArray = errorText.errors;

										valor.forEach(function (itemValor, indexValor) {
											Object.keys(errorsArray).forEach(function (indexdata) {
												if (parseInt(indexValor, 10) === parseInt(indexdata, 10)) {
													var itemErrorArray = errorsArray[indexdata];
													var error1 = itemErrorArray.error;
													Object.keys(itemValor).forEach(function (itemValorKey) {
														var isValidItem = true;
														Object.keys(error1).forEach(function (itemKeyInit2) {
															if (itemValorKey === itemKeyInit2) {
																isValidItem = false;
																if (valor[indexdata][itemKeyInit2] !== undefined) {
																	valor[indexdata][itemKeyInit2 + SubfijoState] = "Error";
																	valor[indexdata][itemKeyInit2 + SubfijoMessage] = error1[itemKeyInit2][0];
																} else {
																	valor[indexdata][itemKeyInit2 + SubfijoState] = "None";
																	valor[indexdata][itemKeyInit2 + SubfijoMessage] = null;
																}
															}
														});

														if (isValidItem) {
															valor[indexdata][itemValorKey + SubfijoState] = "Success";
															valor[indexdata][itemValorKey + SubfijoMessage] = null;
														}

													});
												}
											});

										});

										oModelSV.setProperty("/" + attribute, valor);
									} else {

										oModelSV.setProperty("/" + attribute + SubfijoState, "Error");
										oModelSV.setProperty("/" + attribute + SubfijoMessage, errorText);
									}
								}
							});

							if (isValid) {
								oModelSV.setProperty("/" + itemKeyBody + SubfijoState, "Success");
								oModelSV.setProperty("/" + itemKeyBody + SubfijoMessage, null);
							}
						}
					}
				});

				var oBodyValidado = oModelSV.getData();
				Object.keys(oBodyValidado).forEach(function (KeyBody) {
					if (KeyBody.indexOf(SubfijoState) < 0 && KeyBody.indexOf(SubfijoMessage) < 0) {
						if (Array.isArray(oBodyValidado[KeyBody])) {
							oBodyValidado[KeyBody].forEach(function (itemArray) {
								Object.keys(itemArray).forEach(function (KeyBody2) {
									if (KeyBody2.indexOf(SubfijoState) < 0 && KeyBody2.indexOf(SubfijoMessage) < 0) {
										if (itemArray[KeyBody2 + SubfijoState] !== "Error" && itemArray[KeyBody2]) {
											itemArray[KeyBody2 + SubfijoState] = "Success";
											itemArray[KeyBody2 + SubfijoMessage] = null;
										}

									}
								});
							})
						} else {
							if (oBodyValidado[KeyBody + SubfijoState] !== "Error" && oBodyValidado[KeyBody]) {
								oBodyValidado[KeyBody + SubfijoState] = "Success";
								oBodyValidado[KeyBody + SubfijoMessage] = null;
							}
						}
					}
				});
				oModelSV.setProperty("/", oBodyValidado);
				oModelSV.setProperty("/bTieneCambios", bHasChanges);
				oModelSV.setProperty("/bTieneErrores", (errores.length > 0));
				oModelSV.setProperty("/bNoTieneErrores", (errores.length < 1));
				oModelSV.setProperty("/bNoTieneErroresConCambios", ((errores.length < 1) && bHasChanges));

				return {
					oError: error,
					aErrores: errores,
					oDataInicial: oBodyInit,
					oDataActual: oBodyValidado,
					bTieneCambios: bHasChanges,
					bTieneErrores: (errores.length > 0),
					bNoTieneErrores: (errores.length < 1)
				};
			};

			if (!oData) {
				oData = {};
			}

			Object.keys(oRules).forEach(function (itemKey) {
				if (!oData[itemKey] && oData[itemKey] !== false && oData[itemKey] !== 0) {
					oData[itemKey] = undefined;
				}
			});

			oModel.initData = JSON.parse(JSON.stringify(oData));
			oModel.setData(oData);
			setValueStateModel(oModel);

			oModel.attachPropertyChange(function (oEvent) {
				var resultChange = setValueStateModel(this);
				if (oEventChange) {
					return oEventChange(resultChange, oEvent);
				}
			});

			oModel.resetData = function () {
				oModel.setData(JSON.parse(JSON.stringify(oModel.initData)));
				setValueStateModel(oModel);
			};

			oModel.validate = function (bValidateUndefined, aAtributos) {
				if (!aAtributos) {
					var resultado = setValueStateModel(oModel, bValidateUndefined);
					console.log(resultado);
					return !resultado.bTieneErrores;
				} else {
					var self = this;
					var errores = self.getErrores();
					var erroresOrigen = [];
					errores.forEach(function (item) {
						aAtributos.forEach(function (sAtributo) {
							if (item.attribute === sAtributo) {
								self.setProperty("/" + item.attribute + "State", "Error");
								self.setProperty("/" + item.attribute + "Message", item.error);
								erroresOrigen.push(item.error);
							}
						})
					});
					return erroresOrigen ? false : true;
				}
			};

			oModel.getErrores = function () {
				var resultado = setValueStateModel(oModel);
				return resultado.aErrores;
			};

			oModel.getCleanData = function (bValidateUndefined) {
				var whitelist = {};
				Object.keys(oRules).forEach(function (itemKey) {
					whitelist[itemKey] = true;
				});
				var resultado = validate.cleanAttributes(this.getData(), whitelist);;
				return resultado;
			};

			oModel.getCleanDataExclude = function (dataExcludes) {
				var whitelist = {};
				Object.keys(oRules).forEach(function (itemKey) {
					var include = true;
					dataExcludes.forEach(function (keyExclude) {
						if (itemKey === keyExclude) {
							include = false;
						}
					});

					if (include) {
						whitelist[itemKey] = true;
					}
				});



				var resultado = validate.cleanAttributes(this.getData(), whitelist);;
				return resultado;
			};

			dataPass.viewMaster.getView().setModel(oModel, sName);
		},
		onCopiarTable: function(self, model,idTable,table){
			var indexes    = sap.ui.getCore().byId(idTable).getSelectedIndices();
			var items = self.getView().getModel(model).getProperty(table);
			items = items.filter(function (el) {
				return el.EstadoLocal !== 'E';
			});
			var mensaje;
            if(indexes.length>0){
				mensaje = 'Usted tiene registros editables sin validar.';
				var aRegistrosEditados =$.grep(items, function( n, i ) {
					return n["enabledInput"]===true;
				  });
				  if(aRegistrosEditados.length>0){
					return self.utilPopUps.messageBoxOk(mensaje,"WARNING","Alerta");
				  }
				mensaje = "Desea copiar las líneas seleccionadas?"
				var rs = self.utilPopUps.messageBoxMejorado(mensaje, 'c');
				rs.then(function(confirmation){
					if (confirmation) {  
						var aDataSource = [];
						var oInicial = self.utilController.unlink(items);
						if(sap.ui.getCore().byId(idTable).getSelectedIndices().length>0){
							sap.ui.getCore().byId(idTable).getSelectedIndices().forEach(function(item,x){
								var indice = item;
								var obj = self.getView().getModel(model).getProperty(table+"/" 
								+ sap.ui.getCore().byId(idTable).getBinding("rows").aIndices[indice]);
								oInicial.push(obj);
							});
						}
						self.getView().getModel(model).setProperty(table,oInicial);
						var modTable = [];
						items.forEach(function(item,x){
							var obj = self.utilController.unlink(item);
							obj.iSemana = x+1;
							modTable.push(obj);
						});
						self.getView().getModel(model).setProperty(table,modTable);
						self.getView().getModel(model).refresh();
						console.log(aDataSource);
						sap.ui.getCore().byId(idTable).clearSelection();
					}else{
					}
				});
			}else{
				mensaje = 'Para copiar se debe seleccionar 1 o más filas.';
                self.utilPopUps.messageBoxOk(mensaje,"WARNING","Alerta");
			}
		},
		filterParamOdata: function (params) {
			var aFilters = [];
			params.forEach(function (item) {
				if (item.valor1 && !item.valor2) {
					aFilters.push(new Filter(item.key, item.query, item.valor1));
				}
				if (item.valor1 && item.valor2) {
					aFilters.push(new Filter(item.key, item.query, item.valor1, item.valor2));
				}
			});

			return aFilters;
		},
		
		filterParam: function (params) {
			var aFilters = [];
			var sQuery="";
			var modParam = [];
			params.forEach(function(itemX){
				if (itemX.key && itemX.valor && itemX.valor!=="''" && itemX.operator) {
				modParam.push(itemX);
				}
			});
			modParam.forEach(function (item,x) {
				if (item.valor1 && !item.valor2) {
					aFilters.push(new Filter(item.key, item.query, item.valor1));
				}
				if (item.valor1 && item.valor2) {
					aFilters.push(new Filter(item.key, item.query, item.valor1, item.valor2));
				}
				if (item.key && item.valor && item.operator) {
					var modValor = "";
					if(item.type == "Integer"){
						modValor = item.valor;
					}else{
						modValor = "'" +item.valor+"'";
					}
					if(item.operator=="substringof"){
						if(item.parentAnt){
							sQuery = sQuery+item.parentAnt;
						}
						if(x+1==modParam.length){
							sQuery = sQuery+item.operator+"(" +"toupper("+modValor+")" +"," +"toupper(" +item.key +"))";
						}else{
							sQuery = sQuery+item.operator+"(" +"toupper("+modValor+")" +"," +"toupper(" +item.key +"))"+" "+item.condition+" ";
						}
						if(item.parentPost){
							sQuery = sQuery+item.parentPost;
						}
					}else{
						var modCondition="";
						var modOperator="";
						if(item.operator=="="){
							modOperator=item.operator;
						}else{
							modOperator=" "+item.operator+" ";
						}
						if(item.condition){
							
							if(item.condition=="&"){
								modCondition=item.condition;
							}else{
								modCondition=item.condition ? " "+item.condition+" " : "";
							}
							if(item.parentAnt){
								sQuery = sQuery+item.parentAnt;
							}
							if(x+1==modParam.length){
								sQuery = sQuery+item.key+modOperator+modValor;
							}else{
								sQuery = sQuery+item.key+modOperator+ modValor +modCondition;
							}
							if(item.parentPost){
								sQuery = sQuery+item.parentPost;
							}
						}else{
							if(item.parentAnt){
								sQuery = sQuery+item.parentAnt;
							}
							sQuery = sQuery+item.key+modOperator+modValor+modCondition;
							if(item.parentPost){
								sQuery = sQuery+item.parentPost;
							}
						}
					}
				}
			});
			if(sQuery){
				aFilters.push(sQuery);
			}
			return aFilters;
		},
		filterSearchList: function (tablaLista, params) {
			var oBinding = tablaLista.getBinding();
			var aFilters = this.filterParam(params);
			var filter = new Filter({
				filters: aFilters,
				and: true
			});
			oBinding.filter(filter);
		},
		getDateService:function(date) {
			var modFecha = null;
			if(date){
				if(typeof date=="object"){
					modFecha = date;
				}else{
					modFecha = (date.replace("/Date(","")).replace(")/",""); 
					modFecha = new Date(parseInt(modFecha));
				}
			}
			return modFecha;
		},
		validPeriodo:function(dataModel){
			var that = this;
			if(!(dataModel.sPeriodo || dataModel.dPeriodo)){
				return sap.m.MessageToast.show("Seleccione período.", {
					duration: 3000
				});
			}
			var modData;
			var modFechaPeriodoString
			/*if(dataModel.sPeriodo){
				var anio = dataModel.sPeriodo.split("-")[0];
				var mes = dataModel.sPeriodo.split("-")[1];
				var modAnio = parseInt(anio);
				var modMes = parseInt(mes);
				var modFechaPeriodo = dataModel.sPeriodo ? new Date(anio,mes) : null;
				modFechaPeriodo.setMonth(modFechaPeriodo.getMonth()-1);
				modFechaPeriodoString = that.pad(modMes,"0",2)+"-"+that.pad(modAnio,"0",4);
				modData = {
					modFechaPeriodo:modFechaPeriodo,
					modFechaPeriodoString:modFechaPeriodoString,
					modAnio:modAnio,
					modMes:modMes,
					minPeriodo:new Date()
				}
			}*/
			if(dataModel.dPeriodo){
				var fechaStringLocal = ((dataModel.dPeriodo).toLocaleDateString()).replace(/\//gi,"-");
				var anio = that.pad(fechaStringLocal.split("-")[2],"0",4);
				var mes = that.pad(fechaStringLocal.split("-")[1],"0",2);
				modFechaPeriodoString = mes+"-"+anio;
				var modAnio = parseInt(anio);
				var modMes = parseInt(mes);
				modData = {
					modFechaPeriodo:dataModel.dPeriodo,
					modFechaPeriodoString:modFechaPeriodoString,
					modAnio:modAnio,
					modMes:modMes
				}
			}
				return modData;
		},
		reordenarMinfechaSelected:function(self,model,path){
            var table = path.split("/")[1];
            var cabecera = self.getView().getModel(model).getData();
			var items = self.getView().getModel(model).getProperty("/"+table);
			items = items.filter(function (el) {
				return el.EstadoLocal !== 'E';
			});
			var item = self.getView().getModel(model).getProperty(path);
			if(item.iSemana==1){//Es el primero
				if(items.length>1){
					var posterior = items.find(function (el) {
						return el.iSemana==item.iSemana+1;
					});
					var max = new Date(posterior.dFechaInicio);
                    max.setDate(max.getDate()-1);
					self.getView().getModel(model).setProperty(path+"/maxDateInicio",max);
                }
				var fechaHoy = new Date();
				fechaHoy.setHours(0, 0, 0);
                var fechaStringLocal = ((cabecera.dPeriodo).toLocaleDateString()).replace(/\//gi,"-");
				var anio = this.pad(fechaStringLocal.split("-")[2],"0",4);
                var mes = this.pad(fechaStringLocal.split("-")[1],"0",2);
                var modAnio = parseInt(anio);
				var modMes = parseInt(mes);
				var inicioMes = new Date(modAnio,modMes,1);
				inicioMes.setMonth(inicioMes.getMonth()-1);
				var modMinMes;
				if(inicioMes>fechaHoy){
					modMinMes =inicioMes; 
				}else{
					modMinMes = fechaHoy;
				}
                self.getView().getModel(model).setProperty(path+"/minDateInicio",modMinMes);
			}else{
				if(item.iSemana==items.length){//Es el último
				var anterior = items.find(function (el) {
						return el.iSemana==item.iSemana-1;
					});
					var min = new Date(anterior.dFechaFin);
					min.setDate(min.getDate()+1);
					self.getView().getModel(model).setProperty(path+"/minDateInicio",min);
				}else{//Está en el medio
				    var anterior = items.find(function (el) {
						return el.iSemana==item.iSemana-1;
					});
					var posterior = items.find(function (el) {
						return el.iSemana==item.iSemana+1;
					});
				    var min = new Date(anterior.dFechaFin);
					min.setDate(min.getDate()+1);
					var max = new Date(posterior.dFechaInicio);
					max.setDate(max.getDate()-1);
					self.getView().getModel(model).setProperty(path+"/minDateInicio",min);
					self.getView().getModel(model).setProperty(path+"/maxDateInicio",max);
					
				}
			}
			self.getView().getModel(model).refresh(true);
		},
		reordenarUltimoMinFecha:function(self,model,table){
			var aHorarios = self.utilController.unlink(self.getView().getModel(model).getProperty(table));
			aHorarios = aHorarios.filter(function (el) {
				return el.EstadoLocal !== 'E';
			});
			if(aHorarios){
				var tamanio = aHorarios.length;
				if(tamanio>1){
					var anterior = aHorarios.find(function (el) {
						return el.iSemana==aHorarios[aHorarios.length-2].iSemana;
					});
					var actual = aHorarios.find(function (el) {
						return el.iSemana==aHorarios[aHorarios.length-1].iSemana;
					});
					var itemUltimo = actual;
					var itemAnterior = anterior;
					var fechaNewInicio = new Date(itemAnterior.dFechaFin);
						fechaNewInicio.setDate(fechaNewInicio.getDate()+1);
						itemUltimo.minDateInicio = fechaNewInicio;
						var aHorariosNew = self.utilController.unlink(self.getView().getModel(model).getProperty(table));
						aHorariosNew.forEach(function(item){
							if( (item.EstadoLocal !== 'E') && item.iSemana==actual.iSemana){
								item.minDateInicio = fechaNewInicio;
							}
						});
					self.getView().getModel(model).setProperty(table,aHorariosNew);
				}
			}
		},
		reordenarMinFecha2:function(self,model,table){
			var aHorarios = self.getView().getModel(model).getProperty(table);
			var fechaHoy = new Date();
			fechaHoy.setHours(0, 0, 0);
			if(aHorarios){
				aHorarios.forEach(function(horario,x){
                    if(x>0){
						var fechaNewInicio = new Date(aHorarios[x-1].dFechaFin);
						fechaNewInicio.setDate(fechaNewInicio.getDate()+1);
						horario.minDateInicio = fechaNewInicio;
					}
				});
			}
			self.getView().getModel(model).setProperty(table,aHorarios);
		},
		validHorario:function(item,modelData){
			var that = this;
			var fechaHoy = new Date();
			fechaHoy.setHours(0, 0, 0);
				var fechaInicio = that.getDateService(item.dFechaInicio ? item.dFechaInicio : new Date());
			    var dia = fechaInicio.getDate();
				var maxDate = new Date(modelData.iAnioPeriodo,modelData.iMesPeriodo,dia);
				maxDate.setMonth(maxDate.getMonth()-1);
			    maxDate.setDate(maxDate.getDate()+6);
				var finMes = new Date(modelData.iAnioPeriodo,modelData.iMesPeriodo,1);
				finMes.setDate(finMes.getDate()-1);
				var inicioMes = new Date(modelData.iAnioPeriodo,modelData.iMesPeriodo,1);
				inicioMes.setMonth(inicioMes.getMonth()-1);

				var modMinMes;
				if(inicioMes>fechaHoy){
					modMinMes =inicioMes; 
				}else{
					modMinMes = fechaHoy;
				}

				var modMaxDateInicio;
				if(finMes>fechaHoy){
					modMaxDateInicio = finMes;
				}else{
					modMaxDateInicio = fechaHoy;
				}
			    
                var modMaxDateFinSemana; 
			    if(maxDate>fechaHoy){
			    	modMaxDateFinSemana = maxDate;
			    }else{
			    	modMaxDateFinSemana = fechaHoy;
				}

				var modMminDateFin;
				if(fechaInicio>fechaHoy){
					modMminDateFin = fechaInicio;
				}else{
					modMminDateFin = fechaHoy;
				}
				var modMaxDateFin;
				if(modMaxDateFinSemana>modMaxDateInicio){
					modMaxDateFin = modMaxDateInicio;
				}else{
					modMaxDateFin = modMaxDateFinSemana
				}
				var modData = {
					"modMinMes":modMinMes,
					"modMaxDateInicio":modMaxDateInicio,
					"modMminDateFin":modMminDateFin,
					"modMaxDateFin":modMaxDateFin
				}
				return modData;
		}
	};
});
