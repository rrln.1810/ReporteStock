/* global moment:true */
sap.ui.define([
	"../lib/Validation/Validator",
	"./utilController"
], function (Validator,utilController) {
	"use strict";
	return {
		validTypeInput: function (self,objInput,oEvent) {
			var value = self.utilController.unlink(objInput.mProperties).value;
			if (value === undefined || value.trim() === "") {
				this.oMessageManager.registerObject(objInput, true); //registerObject con true es para agregar
			}
			var validator = new Validator();
			validator.validate(objInput,oEvent, this.oMessageManager, this.oMessageProcessor);
		},
		validatorSAPUI5: function (self, nameFieldGroup, oEvent, model,obj) {
			if (!this.oMessageManager) {
				this.oMessageManager = sap.ui.getCore().getMessageManager();
			}
			this.oMessageManager.registerObject(self.getView(), false);
			if (!this.oMessageProcessor) {
				this.oMessageProcessor = new sap.ui.core.message.ControlMessageProcessor();
			}
			this.oMessageManager.registerMessageProcessor(this.oMessageProcessor);
			this.oMessageManager.removeAllMessages();
			var inputs = self.getView().getControlsByFieldGroupId(nameFieldGroup);
			var errorNull = [];
			inputs.forEach(function(item){
				if (oEvent) {
					var path = oEvent.getSource().getBindingContext(model).getPath();
					var bindingInput = item.getBindingContext(model);
					if (bindingInput) {
						var pathInput = bindingInput.getPath();
						if (path === pathInput) {
							this.validTypeInput(self,item,oEvent);
						}
					}
				} else {
					if (item.bOutput === true) {
						this.validTypeInput(self,item,obj);
					}
				}
			}.bind(this));
			/*for (var i in inputs) {
				if (oEvent) {
					var path = oEvent.getSource().getBindingContext(model).getPath();
					var bindingInput = inputs[i].getBindingContext(model);
					if (bindingInput) {
						var pathInput = bindingInput.getPath();
						if (path === pathInput) {
							this.validTypeInput(inputs[i],oEvent);
						}
					}
				} else {
					if (inputs[i].bOutput === true) {
						this.validTypeInput(inputs[i],obj);
					}
				}
			}*/
			var messages = this.oMessageManager.getMessageModel().getData().filter(function (el) {
				return el.type == sap.ui.core.MessageType.Error;
			});
			if (messages.length == 0) {
				return true;
			} else {
				return false;
			}
		}
	};
});