/*global validate,UtilEx,UtilEx,UtilValidate,UtilController,AppLabels,UtilMessage,UtilService*/
sap.ui.define([], function () {
	"use strict";

	return {
		initUtils: function(params){
			return {
				controller : this.controller(params),
				service : this.service(params),
				exception : this.exception(),
				message : this.message(params),
				validate : this.validate(params)
			};
		},
		controller: function (params) {
			return new UtilController(params);
		},
		service: function (params) {
			return new UtilService(params);
		},
		exception: function () {
			return UtilEx;
		},
		message:  function (params) {
			return new UtilMessage(params);
		},
		validate:  function (params) {
			return UtilValidate;
		}
	};
});