sap.ui.define([
], function() {
    "use strict";
    return {
    	iCodeSuccess	: 1,
    	iCodeWarn		: 2,
    	iCodeError		: -1,
    	iCodeException	: -2,
        success: function(sIdTransaction, sMessage, oResults) {
            return {
                iCode			: this.iCodeSuccess,
                sIdTransaction  : sIdTransaction,
                sMessage		: sMessage,
                oResults		: oResults
            };
        },
        warn: function(sIdTransaction, sMessage,oResults) {
            return {
                iCode			: this.iCodeWarn,
                sIdTransaction  : sIdTransaction,
                sMessage		: sMessage,
                oResults		: oResults
            };
        },
        error: function(sIdTransaction, sMessage,oResults) {
            return {
                iCode		: this.iCodeError,
                sIdTransaction  : sIdTransaction,
                sMessage	: sMessage,
                oResults	: oResults
            };
        },
        exception: function(sIdTransaction, sMessage) {
            return {
                iCode		: this.iCodeException,
                sIdTransaction  : sIdTransaction,
                sMessage	: sMessage
            };
        },
        errorServicio:function(error,oHeader){
            return {
                oAuditResponse: {
                    sIdTransaction: oHeader.sIdTransaction,
                    iCode: -1000,
                    sMensaje: 'Error al consultar el servicio (' + error.status + '), vuelva a intentarlo o comuníquese con el área de soporte.'
                }
            };
        }
    };
});