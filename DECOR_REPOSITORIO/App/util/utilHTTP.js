sap.ui.define([
	'../constantes',
	'./utilPopUps',
	'../dataPass',
	'../util/utilResponse'
], function (constantes, utilPopUps,dataPass,utilResponse) {
	"use strict";
	return {
		HandlerErrorRed: function (e, textStatus, errorThrown) {
			var codigoError = "Error: " + e.status;
			utilPopUps.onMessageErrorDialogPress(codigoError);
			sap.ui.core.BusyIndicator.hide();
		},
		parseJwt: function (token) {
			var base64Url = token.split('.')[1];
			var base64 = decodeURIComponent(atob(base64Url).split('').map(function (c) {
				return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
			}).join(''));

			return JSON.parse(base64);
		},
		encode: function (input) {
			var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

			function utf8_encode(string) {
				string = string.replace(/\r\n/g, "\n");
				var utftext = "";
				for (var n = 0; n < string.length; n++) {
					var c = string.charCodeAt(n);
					if (c < 128) {
						utftext += String.fromCharCode(c);
					} else if ((c > 127) && (c < 2048)) {
						utftext += String.fromCharCode((c >> 6) | 192);
						utftext += String.fromCharCode((c & 63) | 128);
					} else {
						utftext += String.fromCharCode((c >> 12) | 224);
						utftext += String.fromCharCode(((c >> 6) & 63) | 128);
						utftext += String.fromCharCode((c & 63) | 128);
					}
				}
				return utftext;
			}

			var output = "";
			var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
			var i = 0;

			input = utf8_encode(input);

			while (i < input.length) {

				chr1 = input.charCodeAt(i++);
				chr2 = input.charCodeAt(i++);
				chr3 = input.charCodeAt(i++);

				enc1 = chr1 >> 2;
				enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
				enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
				enc4 = chr3 & 63;

				if (isNaN(chr2)) {
					enc3 = enc4 = 64;
				} else if (isNaN(chr3)) {
					enc4 = 64;
				}

				output = output +
					keyStr.charAt(enc1) + keyStr.charAt(enc2) +
					keyStr.charAt(enc3) + keyStr.charAt(enc4);

			}

			return output;
		},
		tagAleatorio: function () {
			var tad1 = new Array("M=Ga", "VCaK", "OP=Q", "NNOA", "McXs", "ZZ=Q", "LJGS", "PNk=", "qswf", "ZVs2", "qaSa", "LNms", "CzSQ", "Nlsd",
				"SSd=", "HGas", "MRTG", "KhRt", "lGtR", "BvRt ", "AzsF", "JkTr", "HGas", "QRXG", "KhRt", "PGtR", "Avtt ", "AzsF", "Fkxr");

			var aleat = Math.random() * tad1.length;
			aleat = Math.floor(aleat);
			return tad1[aleat];
		},
		generarIdTransaccion: function () {
			var fecha = new Date();
			var fechaIso = fecha.toISOString();
			var fechaString = fechaIso.toString().replace(/:/g, "").replace(/-/g, "").replace(".", "").replace("Z", "").replace("T", "");
			var randon = Math.floor((Math.random() * 1000000) + 1);
			var idTransaccion = fechaString + "" + randon;
			return idTransaccion;
		},
		validarRespuestaServicio: function (oAuditResponse, mensaje2, callback) {
			if (oAuditResponse.iCode === 1) {
				utilPopUps.onMessageSuccessDialogPress(oAuditResponse.sIdTransaction, mensaje2, callback);
			} else if (oAuditResponse.iCode === 200) {
				utilPopUps.onMessageWarningDialogPressExit(oAuditResponse.sIdTransaction, mensaje2);
			} else if (oAuditResponse.iCode > 1) {
				var mensaje = oAuditResponse.sMessage;
				utilPopUps.onMessageWarningDialogPress2(oAuditResponse.sIdTransaction, mensaje);
			} else if (oAuditResponse.iCode === 0) {
				utilPopUps.onMessageErrorDialogPress(oAuditResponse.sIdTransaction);
			} else {
				utilPopUps.onMessageErrorDialogPress(oAuditResponse.sIdTransaction);
			}
		},
		obtenerFechaIso: function () {
			var d = new Date();
			var fechaIso = d.toISOString();
			return fechaIso.toString();
		},
		
		serviceRootpath: function () {
			var rootPath;
			var userapi = dataPass.sToken;
			if (constantes.ActivarLocal) {
				rootPath = this.urlAmbiente().urlApiGatewayLocal;
				if(!userapi){
					rootPath = constantes.destSecurity;
				}
			} else {
				rootPath = constantes.destApiGateway; //"des-TipoCambio";jQuery.sap.getModulePath("app.tipocambio2");
				if(!userapi){
					rootPath = constantes.destSecurity;
				}
			}
			return rootPath;
		},
		generarHeaders: function () {
			/*var userapi = dataPass.sToken;//self.getView().getModel("userapi");*/
			var request = {};
			/*if (userapi) {*/
				request.sIdTransaccion = this.generarIdTransaccion();
				request.sAplicacion = constantes.IdApp;
				request.sTerminal = "127.0.0.1";
				/*request.sToken = "Bearer " + userapi;*/
			/*}*/
			/*request.Accept = "application/json";
			request.Authorization = "Basic VFRBUElBOlNlaWRvcjA1JA==";*/
			return request;
		},
		urlAmbiente:function(){
			var modData={};
			var urlPortal ="";
			if(!constantes.ActivarLocal){
				var url = window.location.href;
				constantes.ambiente = url.includes("-caja-dev") ? "DEV" : (url.includes("-caja-qas") ? "QAS" : "PRD");
			}
			switch (constantes.ambiente) {
				case "DEV":
					urlPortal = "https://cli-caja-dev.cpp.cfapps.us10.hana.ondemand.com";
					modData.urlInicioPortal = urlPortal+"/site?siteId=c77bb5fb-254e-4ed1-aa00-395f0ddccfeb#Shell-home";
					modData.urlApiGatewayLocal = "https://ci-caja-ms-apigateway-dev.cfapps.us10.hana.ondemand.com";
					modData.urlAppFacturacion = urlPortal+"/site?siteId=c77bb5fb-254e-4ed1-aa00-395f0ddccfeb#Facturacion-display?sap-ui-app-id-hint=saas_approuter_app.facturacionv2&/2/false/false/DNI/NA/iId=";
					modData.urlAppFacturacionCerrarPinPad = urlPortal+"/site?siteId=c77bb5fb-254e-4ed1-aa00-395f0ddccfeb#Facturacion-display?sap-ui-app-id-hint=saas_approuter_app.facturacionv2&/1/false/false/Nombre/1/iId=0000/link=3/0/99/iIdAp=0";
					modData.urlAppCambioSociedad = urlPortal+"/site?siteId=c77bb5fb-254e-4ed1-aa00-395f0ddccfeb#CambioSociedad-Display?sap-ui-app-id-hint=f8f8c403-076b-42b5-80b9-5d7605b3b516";
					break;
				case "QAS":
					urlPortal = "https://ci-caja-qas.cpp.cfapps.us10.hana.ondemand.com";
					modData.urlInicioPortal = urlPortal+"/site?siteId=3293b700-c5f2-40cf-b6de-e40f71c9c3ff#";
					modData.urlApiGatewayLocal = "https://ci-caja-ms-apigateway-qas.cfapps.us10.hana.ondemand.com";
					modData.urlAppFacturacion = urlPortal+"/site?siteId=3293b700-c5f2-40cf-b6de-e40f71c9c3ff#Facturacion-display?sap-ui-app-id-hint=saas_approuter_app.facturacionv2&/2/false/false/DNI/NA/iId=";
					modData.urlAppFacturacionCerrarPinPad = urlPortal+"/site?siteId=3293b700-c5f2-40cf-b6de-e40f71c9c3ff#Facturacion-display?sap-ui-app-id-hint=saas_approuter_app.facturacionv2&/1/false/false/Nombre/1/iId=0000/link=3/0/99/iIdAp=0";
					modData.urlAppCambioSociedad = urlPortal+"/site?siteId=3293b700-c5f2-40cf-b6de-e40f71c9c3ff#CambioSociedad-Display?sap-ui-app-id-hint=f829bc40-a297-4568-89d3-042c9cc01617";
					break;
				case "PRD":
					urlPortal = "https://ci-caja.cpp.cfapps.us10.hana.ondemand.com";
					modData.urlInicioPortal = urlPortal+"/site?siteId=3293b700-c5f2-40cf-b6de-e40f71c9c3ff#";
					modData.urlApiGatewayLocal = "https://ci-caja-ms-apigateway-prd.cfapps.us10.hana.ondemand.com";
					modData.urlAppFacturacion = urlPortal+"/site?siteId=3293b700-c5f2-40cf-b6de-e40f71c9c3ff#Facturacion-display?sap-ui-app-id-hint=saas_approuter_app.facturacionv2&/2/false/false/DNI/NA/iId=";
					modData.urlAppFacturacionCerrarPinPad = urlPortal+"/site?siteId=3293b700-c5f2-40cf-b6de-e40f71c9c3ff#Facturacion-display?sap-ui-app-id-hint=saas_approuter_app.facturacionv2&/1/false/false/Nombre/1/iId=0000/link=3/0/99/iIdAp=0";
					modData.urlAppCambioSociedad = urlPortal+"/site?siteId=3293b700-c5f2-40cf-b6de-e40f71c9c3ff#CambioSociedad-Display?sap-ui-app-id-hint=f829bc40-a297-4568-89d3-042c9cc01617";
					break;
				default:
					break;
			  }
			  return modData;
		},
		httpGet: function (path, context,mockData,masterBusy,detailBusy,mantenerBusy) {
			var self = this;
			self.busyInicio(context,masterBusy,detailBusy);
			return new Promise(function (resolve, reject) {
				var oHeader = self.generarHeaders();
				$.ajax({
					url: self.serviceRootpath() + path,
					method: "GET",      
					headers: oHeader,
					async: true,
					contentType: "application/json",
					success: function (result) {
						self.busyFin(context,masterBusy,detailBusy,mantenerBusy);
						resolve(self.success(result, mockData));
					},
					error: function (error) {
						self.busyFin(context,masterBusy,detailBusy,mantenerBusy);
						resolve(self.error(error,oHeader,mockData));
					}
				});
			});
		},
		httpPost: function (path, data, context,mockData,masterBusy,detailBusy,mantenerBusy) {
			var self = this;
			self.busyInicio(context,masterBusy,detailBusy);
			return new Promise(function (resolve, reject) {
				var oHeader = self.generarHeaders();
				$.ajax({
					url: /*self.serviceRootpath() + */path,
					method: "POST",
					headers: oHeader,
					data: data,
					async: true,
					contentType: "application/json",
					success: function (result) {
						self.busyFin(context,masterBusy,detailBusy,mantenerBusy);
						resolve(self.success(result, mockData));
					},
					error: function (error) {
						self.busyFin(context,masterBusy,detailBusy,mantenerBusy);
						resolve(self.error(error,oHeader,mockData));
					}
				});
			});
		},
		httpPostLocal: function (path, data, context,mockData,masterBusy,detailBusy,mantenerBusy) {
			var self = this;
			self.busyInicio(context,masterBusy,detailBusy);
			return new Promise(function (resolve, reject) {
				var oHeader = self.generarHeaders();
				$.ajax({
					url: self.serviceRootpath() + path,
					method: "POST",
					data: JSON.stringify(data),
					async: true,
					contentType: "application/json",
					success: function (result) {
						self.busyFin(context,masterBusy,detailBusy,mantenerBusy);
						resolve(self.success(result, mockData));
					},
					error: function (error) {
						self.busyFin(context,masterBusy,detailBusy,mantenerBusy);
						resolve(self.error(error,oHeader,mockData));
					}
				});
			});
		},
		httpDelete: function (path, data, context,mockData,masterBusy,detailBusy,mantenerBusy) {
			var self = this;
			self.busyInicio(context,masterBusy,detailBusy);
			return new Promise(function (resolve, reject) {
				var oHeader = self.generarHeaders();
				$.ajax({
					url: self.serviceRootpath() + path,
					method: "DELETE",
					headers: oHeader,
					data: JSON.stringify(data),
					async: true,
					contentType: "application/json",
					success: function (result) {
						self.busyFin(context,masterBusy,detailBusy,mantenerBusy);
						resolve(self.success(result, mockData));
					},
					error: function (error) {
						self.busyFin(context,masterBusy,detailBusy,mantenerBusy);
						resolve(self.error(error,oHeader,mockData));
					},
					complete: function () {
						console.log("--Se ejecutó llamada de red--");
					}
				});
			});
		},
		httpPut: function (path, data, context,mockData,masterBusy,detailBusy,mantenerBusy) {
			var self = this;
			self.busyInicio(context,masterBusy,detailBusy);
			return new Promise(function (resolve, reject) {
				var oHeader = self.generarHeaders();
				$.ajax({
					url: self.serviceRootpath() + path,
					method: "PUT",
					headers: oHeader,
					data: JSON.stringify(data),
					async: true,
					contentType: "application/json",
					success: function (result) {
						self.busyFin(context,masterBusy,detailBusy,mantenerBusy);
						resolve(self.success(result, mockData));
					},
					error: function (error) {
						self.busyFin(context,masterBusy,detailBusy,mantenerBusy);
						resolve(self.error(error,oHeader,mockData));
					}
				});
			});
		},
		success: function (result, mockData) {
			var self = this;
			var modResult;
			var oAuditResponse = result.oAuditResponse;
			if(!constantes.ActivarMockData){
				mockData = null
			}
			if (mockData) {
				modResult = utilResponse.success("17682783782383", "Se consultó correctamente.", mockData);
			} else {
				if (oAuditResponse.iCode === 1) {
					modResult = utilResponse.success(oAuditResponse.sIdTransaction, oAuditResponse.sMessage, result.oData);
				} else if (oAuditResponse.iCode > 1) {
					modResult = utilResponse.warn(oAuditResponse.sIdTransaction, oAuditResponse.sMessage, result.oData);
				} else if (oAuditResponse.iCode < 0 && oAuditResponse.iCode !== -1000) {
					modResult = utilResponse.error(oAuditResponse.sIdTransaction, oAuditResponse.sMessage, result.oData);
					//utilPopUps.onMessageErrorDialogPress(modResult.sIdTransaction);
				} else if (oAuditResponse.iCode === -1000) {
					modResult = utilResponse.exception(oAuditResponse.sIdTransaction, oAuditResponse.sMessage);
				}
			}
			return modResult;
		},
		error: function (error,oHeader,mockData) {
			var modResult;
			if(!constantes.ActivarMockData){
				mockData = null;
			}
			if (mockData) {
				modResult = utilResponse.success("17682783782383", "Se consultó correctamente.", mockData);
			} else {
				modResult = utilResponse.errorServicio(error,oHeader);
			}
			return modResult;
		},
		busyInicio:function(context,masterBusy,detailBusy){
			if(masterBusy){
				context.getView().getModel("modelGlobal").setProperty("/masterBusy",true);
			}
			if(detailBusy){
				context.getView().getModel("modelGlobal").setProperty("/detailBusy",true);
			}
			if( !masterBusy && !detailBusy ){
				sap.ui.core.BusyIndicator.show(0); //?$filter=sCodigoTabla eq 'T00001' or  sCodigoTabla eq 'T00002' or sCodigoTabla eq 'T00003' 
			}
		},
		busyFin:function(context,masterBusy,detailBusy,mantenerBusy){
			if(masterBusy){
				if(!mantenerBusy){
					context.getView().getModel("modelGlobal").setProperty("/masterBusy",false);
				}
			}
			if(detailBusy){
				if(!mantenerBusy){
					context.getView().getModel("modelGlobal").setProperty("/detailBusy",false);
				}
			}
			if( !masterBusy && !detailBusy ){
				if(!mantenerBusy){
					sap.ui.core.BusyIndicator.hide(); //?$filter=sCodigoTabla eq 'T00001' or  sCodigoTabla eq 'T00002' or sCodigoTabla eq 'T00003' 
				}
			}
		}
	};
});