/* global moment:true */
jQuery.sap.require("sap.ui.core.format.DateFormat");
sap.ui.define([
    "sap/ui/model/SimpleType",
	"sap/ui/model/ValidateException",
	"../dataPass"
], function (SimpleType,ValidateException,dataPass) {
	"use strict";
	return {
		typeEmail: SimpleType.extend("Email", {
			formatValue: function (oValue,a,e) {
				return oValue;
			},
			parseValue: function (oValue,type) {
				//parsing step takes place before validating step, value could be altered here
				return oValue;
			},
			validateValue: function (oValue) {
				// The following Regex is only used for demonstration purposes and does not cover all variations of email addresses.
				// It's always better to validate an address by simply sending an e-mail to it.
				var view = dataPass.viewMaster;
				var rexMail = /^\w+[\w-+\.]*\@\w+([-\.]\w+)*\.[a-zA-Z]{2,}$/;//Email
				if (!oValue) {
					throw new ValidateException("Ingrese al menos un caracter.");
				}
				if (!oValue.match(rexMail)) {
					throw new ValidateException("'" + oValue + "' no es un formato de email válido.");
				}
			}
		}),
		typeFecha: SimpleType.extend("Fecha", {
			formatValue: function (oValue,a,e) {
				return oValue;
			},
			parseValue: function (oValue,type) {
				//parsing step takes place before validating step, value could be altered here
				return oValue;
			},
			validateValue: function (oValue) {
				// The following Regex is only used for demonstration purposes and does not cover all variations of email addresses.
				// It's always better to validate an address by simply sending an e-mail to it.
				var view = dataPass.viewMaster;
				//var rexMail = /^\d{2}[./-]\d{2}[./-]\d{4}$/;//Date Format 02-02-2020
				var rexMail = /^\d{4}[./-]\d{2}[./-]\d{2}$/;//Date Format 2020-02-02
				if (!oValue) {
					throw new ValidateException("Llene correctamente la fecha.");
				}
				if (!oValue.match(rexMail)) {
					throw new ValidateException("'" + oValue + "' no es un formato de email válido2.");
				}
			}
		})
	};
});