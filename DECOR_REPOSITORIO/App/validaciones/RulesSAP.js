
/*global validate,UtilEx,UtilEx,UtilValidate,UtilController,AppLabels*/
sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device",
	'sap/ui/core/MessageType',
], function (JSONModel, Device, MessageType) {
	"use strict";

	return {
		//MessageType.Error
		//MessageType.Success
		//MessageType.None

		/*
		Regular Expressions:
		/^\d{2}[./-]\d{2}[./-]\d{4}$/      	//Regular Expressions for Date String
		/[^0-9]/							//Regular Expressions for Numeric
		*/
		GeneralRule: function (oControl,model, oEvent) {
			var rules = [{
				name: "dFechaInicio",
				pattern:/^\d{2}[./-]\d{2}[./-]\d{4}$/,//Regular Expressions for Date String
				required: true,
				stateText: "Falta Llenar Fecha",
				state: MessageType.Error
			}/*,
			{
				name: "dFechaFin",
				required: true,
				stateText: "Falta Llenar Fecha Fin",
				state: MessageType.Error
			}*/];
			var findRule = this.getBinding(oControl,model, rules, oEvent);

			return findRule;
		},
		
		state: function (oControl,model,rule, oEvent) {
			var condition = false;
			/*for (var i in aParam) {
				if (aParam[i].valueParam !== rule.valueCondition[i]) {
					condition = true;
				}
			}*/
			for (var i in rule.campoCondition) {
				var valueOEvent;
				if(oEvent.getSource){
					valueOEvent = oEvent.getSource().getBindingContext(model).getObject()[rule.campoCondition[i]]
				}else{
					valueOEvent = oEvent[rule.campoCondition[i]];
				}
				if(valueOEvent){
					if(rule.condition){
						if(rule.condition === "and"){
							if (valueOEvent === rule.valueCondition[i]) {
								condition = true;
								break;
							}else{
								rule.required = false;
							}
						}
						if(rule.condition === "or"){
							var findCondition = rule.valueCondition.find(function (el) {
								return el == valueOEvent
							});
							if(findCondition){
								condition = true;
								oControl.setRequired(false);
							}else{
								condition = false;
								oControl.setRequired(true);
							}
						}
					}
				}
			}
			if (rule.required && !rule.value /*Si no existe*/ ) {
				if(condition){
					let re = new RegExp(rule.pattern,'i');
					let result = re.test(rule.value);
				    if(result){
                        rule.state = MessageType.Success;
					    rule.stateText = "Entrada Correcta";
				    }else{
				       	rule.state = MessageType.Error;
				    }
				}else{
					let re = new RegExp(rule.pattern,'i');
					let result = re.test(rule.value);
				    if(result){
                        rule.state = MessageType.Success;
					    rule.stateText = "Entrada Correcta";
				    }else{
				       	rule.state = MessageType.Error;
				    }
				}	
				
			} else {
				let re = new RegExp(rule.pattern,'i');
					let result = re.test(rule.value);
				    if(result){
                        rule.state = MessageType.Success;
					    rule.stateText = "Entrada Correcta";
				    }else{
				       	rule.state = MessageType.Error;
				    }
			}
			return rule;
		},
		getBinding: function (oControl,model, rules, oEvent) {
			var preBinding = oControl.mBindingInfos;
			var context, binding, nameCampo, type, value, rule;

			var aPrefijo = ["dateValue", "selectedKey", "value"];

			for (var j in aPrefijo) {
				if (preBinding[aPrefijo[j]]) {
					context = preBinding[aPrefijo[j]];
					type = aPrefijo[j];
					break;
				}
			}

			if (context) {
				binding = context.binding;
				if (type === "dateValue") {
					value = oControl.getDateValue();
				}
				if (type === "value") {
					//value = oControl.getValue();
					value = oControl.getValue().trim(); //lmora2020
				}
				if (type === "selectedKey") {
					value = oControl.getSelectedKey();
				}
				nameCampo = binding.getPath(); //Nombre del campo en el modelo

				var findRule = rules.find(function (el) {
					return el.name == nameCampo
				});
				

				/*var aParam = [];
				if (findRule.campoCondition) {
					for (var i in findRule.campoCondition) {
						var findoParam = oParam.find(function (el) {
							return el.nameParam == findRule.campoCondition[i]
						});
						aParam.push(findoParam);
					}
				}*/
				if(findRule){
					findRule.value = value;
					rule = this.state(oControl,model,findRule, oEvent);
				}
			}

			return rule;
		},

		////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////

		Manifiesto2: function (nameCampo) {
			var rules = [{
				name: "ConsignatarioTipoDocumento",
				required: true,
				stateText: "Se requiere Consignatario",
				state: MessageType.Error
			}, {
				name: "ConsignatarioCodigo",
				required: false,
				stateText: "No Se requiere Consignatario",
				state: MessageType.Success
			}];
			rules = this.state(rules);
			var findRule = rules.find(function (el) {
				return el.name == nameCampo
			});
			return findRule;
		},
		Manifiesto: function (oControl, oEvent) {
			var rules = [{
				name: "/GABL/ConsignatarioTipoDocumento",
				required: true,
				stateText: "Se requiere Tipo Doc. Consignatario",
				condition:"or",
				campoCondition: ["DestinacionCarga"],
				valueCondition: ["28"]
			}, {
				name: "/GABL/ConsignatarioCodigo",
				required: true,
				stateText: "Se requiere Doc. Consignatario",
				condition:"or",
				campoCondition: ["DestinacionCarga"],
				valueCondition: ["28"]
			}];

			var findRule = this.getBinding(oControl, rules, oEvent);

			return findRule;
		}

	};
});