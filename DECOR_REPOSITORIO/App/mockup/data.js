sap.ui.define([

], function () {
	"use strict";
	return {
		fondoFijoSoles:function(){
			var modData = [];
				modData.push({ "sMoneda": 10, "sDenominacion"	: "Billete S/. 10", "iCantidad"		: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 20, "sDenominacion"	: "Billete S/. 20", "iCantidad"		: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 50, "sDenominacion"	: "Billete S/. 50", "iCantidad"		: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 100,"sDenominacion"	: "Billete S/. 100", "iCantidad"	: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 200,"sDenominacion"	: "Billete S/. 200", "iCantidad"	: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 0.10,"sDenominacion"	: "Moneda S/. 0.10", "iCantidad"	: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 0.20, "sDenominacion"	: "Moneda S/. 0.20", "iCantidad"	: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 0.50, "sDenominacion"	: "Moneda S/. 0.50", "iCantidad"	: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 1, "sDenominacion"	: "Moneda S/. 1.00", "iCantidad"	: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 2, "sDenominacion"	: "Moneda S/. 2.00", "iCantidad"	: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 5, "sDenominacion"	: "Moneda S/. 5.00", "iCantidad"	: 0, "fImporte"		: 0 });
			return modData;
		},
		efectivoSoles:function(){
			var modData = [];
				modData.push({ "sMoneda": 10, "sDenominacion"	: "Billete S/. 10", "iCantidad"		: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 20, "sDenominacion"	: "Billete S/. 20", "iCantidad"		: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 50, "sDenominacion"	: "Billete S/. 50", "iCantidad"		: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 100,"sDenominacion"	: "Billete S/. 100", "iCantidad"	: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 200,"sDenominacion"	: "Billete S/. 200", "iCantidad"	: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 0.10,"sDenominacion"	: "Moneda S/. 0.10", "iCantidad"	: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 0.20, "sDenominacion"	: "Moneda S/. 0.20", "iCantidad"	: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 0.50, "sDenominacion"	: "Moneda S/. 0.50", "iCantidad"	: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 1, "sDenominacion"	: "Moneda S/. 1.00", "iCantidad"	: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 2, "sDenominacion"	: "Moneda S/. 2.00", "iCantidad"	: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 5, "sDenominacion"	: "Moneda S/. 5.00", "iCantidad"	: 0, "fImporte"		: 0 });
			return modData;
		},
		efectivoDolares:function(){
			var modData = [];
				modData.push({ "sMoneda": 1, "sDenominacion"	: "Billete $ 1", "iCantidad"		: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 5, "sDenominacion"	: "Billete $ 5", "iCantidad"		: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 10, "sDenominacion"	: "Billete $ 10", "iCantidad"		: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 20, "sDenominacion"	: "Billete $ 20", "iCantidad"		: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 50, "sDenominacion"	: "Billete $ 50", "iCantidad"		: 0, "fImporte"		: 0 });
				modData.push({ "sMoneda": 100, "sDenominacion"	: "Billete $ 100", "iCantidad"		: 0, "fImporte"		: 0 });
			return modData;
		},
		documentos:function(){
			var modData = [];
				modData.push({"iItem":"1", "sDesTipoDocumento":"Boleta", "sDesEstadoDoc":"Rechazado","sNombrePacienteDetalle":"Jimenz Mesta, Jorge", "sDesTipoDocSunat": "DNI","sNumDocIdentidad":"42496234","sRazonSocial":"Jimenz Mesta, Jorge","sNroDoc":"B001-32344432","fTipoCambio":"3.12","sCodMoneda":"PEN","fMonto":"1150.00","iIdMotivo":1,"sMotivoBaja":"Sin Motivo",sCodTipoDocSunat:6,sFormaPago:"Aplicada"});
				modData.push({"iItem":"2", "sDesTipoDocumento":"Factura", "sDesEstadoDoc":"Rechazado","sNombrePacienteDetalle":"Jimenz Mesta, Jorge", "sDesTipoDocSunat": "RUC","sNumDocIdentidad":"42496234","sRazonSocial":"Jimenz Mesta, Jorge","sNroDoc":"B001-32344432","fTipoCambio":"3.12","sCodMoneda":"PEN","fMonto":"3200.00","iIdMotivo":1,"sMotivoBaja":"MOTIVO 1",sCodTipoDocSunat:6,sFormaPago:"Aplicada"});
			return modData;
		},
		traslados:function(){
			var modData = [];
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"3", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"4", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"5", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"6", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"7", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"8", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"9", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"10", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"11", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"12", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"13", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"14", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"15", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"16", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"17", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"18", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"19", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"20", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"21", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"22", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"23", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"24", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"25", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"26", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"27", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"28", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"29", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"30", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"31", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"32", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"33", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"34", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"35", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"36", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"37", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"38", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"39", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"40", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"41", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"42", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"43", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"44", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"45", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"46", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"47", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"48", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"49", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"50", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"51", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"52", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"53", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"54", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"55", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"56", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"57", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"58", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"59", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"60", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"61", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"62", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"63", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"64", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"65", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"66", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"67", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"68", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"69", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"70", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"71", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"72", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","fImportePEN":"200.00","fImporteUSD":"100.00"});


			return modData;
		},
		custodios:function(){
			var modData = [];
				modData.push({"iItem":"1", "sCodigo":"000001", "sFecha":"14-06-2020 11:52:00","sEstado":"Recojo","sConcepto":"Sobrante","sUsuarioCustodia":"RLAZARO","fImportePEN":"200.00","fImporteUSD":"100.00"});
				modData.push({"iItem":"2", "sCodigo":"000002", "sFecha":"14-06-2020 11:52:00","sEstado":"Recojo","sConcepto":"Sobrante","sUsuarioCustodia":"RLAZARO","fImportePEN":"200.00","fImporteUSD":"100.00"});
			return modData;
		},
		resumenTipoDocumento:function(){
			var modData = [];
				modData.push({ "sDenominacion"	: "Facturas Electrónicas","iCantidad": 0,"fImporte": 0 });
				modData.push({ "sDenominacion"	: "Boletas Electrónicas","iCantidad": 0,"fImporte": 0 });
				modData.push({ "sDenominacion"	: "Factura Manual","iCantidad": 0,"fImporte": 0 });
				modData.push({ "sDenominacion"	: "Boleta Manual","iCantidad": 0, "fImporte": 0 });
				modData.push({ "sDenominacion"	: "Nota de Credito Emitida","iCantidad": 0,"fImporte": 0 });
				modData.push({ "sDenominacion"	: "Hab. Fondo de Sencillo","iCantidad": 0, "fImporte": 0 });
				modData.push({ "sDenominacion"	: "Reposición de Fondo","iCantidad": 0,"fImporte": 0 });
				modData.push({ "sDenominacion"	: "Fact. Otras Sedes","iCantidad": 0, "fImporte": 0 });
				modData.push({ "sDenominacion"	: "Bole. Otras Sedes","iCantidad": 0, "fImporte": 0 }); 
				modData.push({ "sDenominacion"	: "Devol. totales efectivo","iCantidad": 0, "fImporte": 0 });
				modData.push({ "sDenominacion"	: "Devol. parciales efectivo","iCantidad": 0, "fImporte": 0 }); 
				modData.push({ "sDenominacion"	: " ","iCantidad": "", "fImporte": "" });   
			return modData;
		},
		resumenMedioPago:function(){
			var modData = [];
				modData.push({ "sDenominacion"	: "Moneda Nacional","iCantidad": 0, "fImporte": 0 });
				modData.push({ "sDenominacion"	: "Traslado Boveda MN","iCantidad": 0, "fImporte": 0 }); 
				modData.push({ "sDenominacion"	: "Moneda Extranjera","iCantidad": 0, "fImporte": 0 });
				modData.push({ "sDenominacion"	: "Traslado Boveda ME","iCantidad": 0, "fImporte": 0 });
				modData.push({ "sDenominacion"	: "Voucher Visa","iCantidad": 0, "fImporte": 0 });
				modData.push({ "sDenominacion"	: "Voucher Master","iCantidad": 0, "fImporte": 0 });
				modData.push({ "sDenominacion"	: "Total Pagos con tarjeta","iCantidad": 0, "fImporte": 0 });
				modData.push({ "sDenominacion"	: "Nota de Credito Aplicada","iCantidad": 0, "fImporte": 0 });
				modData.push({ "sDenominacion"	: "Depósito en cuenta","iCantidad": 0, "fImporte": 0 });
				modData.push({ "sDenominacion"	: "Deposito detracción","iCantidad": 0, "fImporte": 0 });
				modData.push({ "sDenominacion"	: "Web Niubiz","iCantidad": 0, "fImporte": 0 });
				modData.push({ "sDenominacion"	: "Web Izipay","iCantidad": 0, "fImporte": 0 }); 
			return modData;
		},
		/////////////////////////////////////////////////////
		periodos:function(){
			var modData = [
				{Id:1,
				Descripcion:"Periodo1"
				},
				{Id:2,
				Descripcion:"Periodo2"
				}
			];
			return modData;
		},
		areas:function(){
			var modData = [
				{iId:1,
					sNombreArea:"Area1",
				iIdSociedad:1
				},
				{Id:2,
					sNombreArea:"Area2",
				iIdSociedad:2
				}
			];
			return modData;
		},
		sociedades:function(){
			var modData = [
				{iId:1,
					sRazonSocial:"Sociedad1",
				iIdSociedad:1
				},
				{Id:2,
					sRazonSocial:"Sociedad2",
				iIdSociedad:2
				}
			];
			return modData;
		},
		sedes:function(){
			var modData = [
				{iId:1,
					sNombreSede:"Sede1",
				iIdSociedad:1
				},
				{Id:2,
					sNombreSede:"Sede2",
				iIdSociedad:2
				}
			];
			return modData;
		},
		sociedadUsuario:function(){
			var modData = [
				{Id:1,
					iIdSociedad:1,
					sSociedad:"Soceidad1",
					iIdSede:1,
					sSede:"Sede1",
					sUsuario:"David"
				}
			];
			return modData;
		}
	};
});
