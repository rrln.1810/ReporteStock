sap.ui.define([
    'sap/ui/core/mvc/Controller',
    "../formatters/formatter",
    "sap/ui/core/routing/History",
    'sap/ui/model/Filter',
    "sap/ui/model/SimpleType",
    "../dataPass",
    "../model/models",
    "../util/utilPopUps",
    "../util/utilHTTP",
    "../util/utilValidation",
    "../util/utilController",
    "../util/utilUI",
    "../estructuras/enviarDatos",
    "../estructuras/recibirDatos",
    "../service/service",
    "../constantes",
    'sap/ui/model/FilterOperator',
    "../validaciones/Rules",
    "../mockup/data"
], function(Controller, formatter, History, Filter, SimpleType, dataPass, models, utilPopUps, utilHTTP, utilValidation, utilController, utilUI, enviarDatos, recibirDatos, service, constantes, FilterOperator, Rules, data) {
    return Controller.extend('app.reporteStock.controller.BaseController', {
        formatter: formatter,
        Rules: Rules,
        onIniUtils: function() {
            this.utilController = utilController;
            this.constantes = constantes;
            this.service = service;
            this.utilPopUps = utilPopUps;
            this.utilHTTP = utilHTTP;
            this.enviarDatos = enviarDatos;
            this.recibirDatos = recibirDatos;
            this.dataPass = dataPass;
            this.utilValidation = utilValidation;
            this.models = models;
            this.FilterOperator = FilterOperator;
            this.formatter = formatter;
            this.utilUI = utilUI;
        },
        createTabs: function(create) {
            this.utilController.createTabController(dataPass.viewDetail, "view.Frags.Tabs.tab_iconTabFondoFijoSoles", "ContentReporteStock", create);
            this.utilController.createTabController(dataPass.viewDetail, "view.Frags.Tabs.tab_iconTabEfectivoSoles", "ContentReporteStock", create);
            this.utilController.createTabController(dataPass.viewDetail, "view.Frags.Tabs.tab_iconTabEfectivoDolares", "ContentReporteStock", create);
            this.utilController.createTabController(dataPass.viewDetail, "view.Frags.Tabs.tab_iconTabDocumentos", "ContentReporteStock", create);
            this.utilController.createTabController(dataPass.viewDetail, "view.Frags.Tabs.tab_iconTabTraslados", "ContentReporteStock", create);
            this.utilController.createTabController(dataPass.viewDetail, "view.Frags.Tabs.tab_iconTabCustodios", "ContentReporteStock", create);
            this.utilController.createTabController(dataPass.viewDetail, "view.Frags.Tabs.tab_iconTabResumen", "ContentReporteStock", create);
        },
        initArqueo: function(oHorarioCaja) {
            var self = this;
            self.createTabs(true);
            self.consultarMaestros(oHorarioCaja.iId+"&iCodEstadoArqueo="+oHorarioCaja.iEstadoHorario);
            //self.consultarDocumentos(oHorarioCaja.iId);
            self.consultarTraslados(oHorarioCaja.iId);
            self.consultarCustodios(oHorarioCaja.iIdSociedad,oHorarioCaja.iId);
            self.obtenerInformacionUsuario(oHorarioCaja);
            self.obtenerInformacionUsuarioSupervisor(oHorarioCaja);
        }
    });
});