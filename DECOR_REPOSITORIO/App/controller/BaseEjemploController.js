sap.ui.define([
    'sap/ui/core/mvc/Controller',
    "../formatters/formatter",
    "sap/ui/core/routing/History",
    "../dataPass",
    "../util/Util",
    "../util/utilPopUps",
    "../util/utilHTTP",
    "../util/utilValidation",
    "../util/utilController",
    "../service/service",
    "../constantes",
    "sap/ushell/services/Container",
    "../estructuras/enviarDatos",
    "../estructuras/recibirDatos",
], function (Controller, formatter, History, dataPass, Util, utilPopUps, utilHTTP, utilValidation,utilController, service, constantes, Container, enviarDatos, recibirDatos) {
    return Controller.extend('app.tipocambio.controller.BaseController', {
        formatter: formatter,
        onIniUtils: function () {
            this.constantes = constantes;
            this.service = service;
            this.utilPopUps = utilPopUps;
            this.utilHTTP = utilHTTP;
            this.enviarDatos = enviarDatos;
            this.recibirDatos = recibirDatos;
            this.utilValidation = utilValidation;
            this.utilController = utilController;
            this.dataPass = dataPass;
        },
        createTables: function (create) {
            this.utilController.createTableController(this.dataPass.viewDetail, "view.Frags.Tables.tb_tablaTipoCambio", "idContentTipoCambio", create);
        },
        getToken: async function() {
            var self = this;
            var mockData =  self.constantes.sTokenMockData;
            var oParam = [];
            self.dataPass.sToken = null;
            var result = await self.service.serviceGet(self.constantes.userApi, oParam, self, mockData);
            if (result.iCode === 1) {
                var result = result.oResults;
                var infoUsuario = self.constantes.ActivarLocal ? Object.assign(self.utilHTTP.parseJwt(self.constantes.sTokenMockData.replace("Bearer", "").trim()),(self.constantes.oInfoUsuario ? self.constantes.oInfoUsuario : {}) ) : Object.assign(self.utilHTTP.parseJwt(result.sToken ? result.sToken : result),(result.oInfoUsuario ? result.oInfoUsuario : {}) );
                var sToken = self.constantes.ActivarLocal ? self.constantes.sTokenMockData.replace("Bearer", "").trim() : (result.sToken ? result.sToken : result);
                self.dataPass.sToken = sToken;
                self.consultarSociedadUsuario();
            } else {
                self.dataPass.sToken = null;
                return self.utilController.navTo(self, "appDetailNotFound");
            }
        },
        consultarSociedadUsuario: async function () {
            var self = this;
            var oParam = [];
            var result = await self.service.serviceGet(constantes.services.consultarSociedadUsuario, oParam, self);
            if (result.iCode === 1) {
                if (result.oResults.length > 0) { //result.oResults.length>0
                    self.getView().getModel("localModel").setProperty("/sConfiguracionSociedadSede", result.oResults[0].sRazonSocial + " - " + result.oResults[0].sNombreSede);
                    self.getView().getModel("miData").setProperty("/User", result.oResults[0]);
                    self.consultarTipoCambio();
                } else {
                    return self.utilController.navTo(self, "appDetailNotFound");
                }
                
            }else{
                return self.utilController.navTo(self, "appDetailNotFound");
            }
        },
        consultarTipoCambio: async function () {
            var self = this;
            var user = self.getView().getModel("miData").getProperty("/User");
            //var oParam = self.enviarDatos.consultarTipoCambio(user.iIdSociedad);
            var oParam = this.utilController.filterParam([{
                key: "iIdSociedad",
                operator: "eq",
                valor: user.iIdSociedad
            }]);
            var result = await self.service.serviceGetOdata(constantes.services.consultarTipoCambio, oParam, self);
            if (result.iCode === 1) {
                result.oResults.forEach(function (item) {
                    item.fVenta = parseFloat(item.fMonedaVenta);
                    item.fCompra = parseFloat(item.fMonedaVenta);
                    var modFecha = (item.dFecha.replace("/Date(", "")).replace(")/", "");
                    item.dFecha = new Date(parseInt(modFecha));
                });
                self.getView().getModel("localModel").setProperty("/VTipoCambio", result.oResults);
                self.createTables(true);
            }else{
                self.getView().getModel("localModel").setProperty("/VTipoCambio", []);
            }
        },
        onChangeFecha: function () {
            sap.ui.core.BusyIndicator.show(0);
            setTimeout(function () {
                sap.ui.core.BusyIndicator.hide();
            }, 400);
        },
        onChangeTipoCambio: function (oEvent) {
            var value = oEvent.getSource().getValue();
            value = value.replace(/-/g, "");
            var cantDecimals = (value.length - 1) - value.indexOf('.');
            var cantCentenas = (value.length - 1) - cantDecimals;
            var newCentenas, newDecimal, mod;
            if (value.includes(".")) {
                if (cantCentenas >= 6 && cantDecimals >= 5) {
                    newCentenas = value.substring(0, 6);
                    newDecimal = value.substring(cantCentenas + 1, 12).substring(0, 5);
                    mod = newCentenas + "." + newDecimal;
                }
                if (cantCentenas >= 6 && cantDecimals < 5) {
                    newCentenas = value.substring(0, 6);
                    newDecimal = value.substring(cantCentenas + 1, value.length).substring(0, 5);
                    mod = newCentenas + "." + newDecimal;
                }
                if (cantCentenas < 6 && cantDecimals < 5) {
                    newCentenas = value.substring(0, cantCentenas);
                    newDecimal = value.substring(cantCentenas + 1, value.length).substring(0, 5);
                    mod = value;
                }
                if (cantCentenas < 6 && cantDecimals >= 5) {
                    newCentenas = value.substring(0, cantCentenas);
                    newDecimal = value.substring(cantCentenas + 1, 12).substring(0, 5);
                    mod = newCentenas + "." + newDecimal;
                }

            } else {
                if (value.length > 6) {
                    newCentenas = value.substring(0, 6);
                } else {
                    newCentenas = value;
                }
                mod = newCentenas;
            }

            oEvent.getSource().setValue(mod);
            if (mod) {
                this.dataPass.viewDetail.getView().getModel("miData").refresh(true);
            }
        }
    });
});