sap.ui.define([
		"sap/ui/core/mvc/Controller",
		"./BaseController"
	],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
	function (Controller,BaseController) {
		"use strict";

		return BaseController.extend("app.reporteStock.controller.Home", {
			onInit: function () {
				this.onIniUtils();
			}
		});
	});
