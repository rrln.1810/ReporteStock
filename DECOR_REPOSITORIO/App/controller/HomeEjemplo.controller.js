sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/UIComponent",
	"./BaseController"
], function (Controller, UIComponent, BaseController) {
	"use strict";
	return BaseController.extend("app.tipocambio.controller.Home", {
		onInit: function () {
			this.localModel = this.getView().getModel("localModel");
		},
		onRouteMatched: function (oEvent) {},

		onAfterRendering: async function () {
			// var self = this;

			// var oDatosUsuario = this.localModel.getProperty("/oDatosUsuario");

			// var result = await self.consultarUserApi(this);
			//self.localModel.setProperty("/sConfiguracionSociedadSede", result.oResults[0].sRazonSocial + " - " + result.oResults[0].sNombreSede);

		},
	});
});