sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/UIComponent",
    "./BaseController",
    "sap/ui/core/Core"
],
/**
 * @param {typeof sap.ui.core.mvc.Controller} Controller
 */
function (Controller, UIComponent, BaseController,Core) {
    "use strict";

    return BaseController.extend("app.reporteStock.controller.Inicio", {
        onInit: function () {
            this.onIniUtils();
            this.dataPass.viewInicio = this;
            var oRouter = UIComponent.getRouterFor(this);
            oRouter.attachRoutePatternMatched(this.onRouteMatched, this);
        },
        onRouteMatched: function (oEvent) {
            var params = this.utilController.unlink(oEvent.getParameter("arguments"));
            if (oEvent.getParameter("name") === "RouteInicio") {
                this.utilController.createDialogController(this, "view.Frags.Dialogs.dlg_dialogLogin");//Creaci�n de un fragment ligado a un contralador
            }    
        },
        defaultTabs:function(){
            this.getView().byId("ContentReporteStock").setSelectedKey("tabFilterFondoFijoSoles");
        },
        onAfterRendering: function () {
            
		},
        onDlg_dialogLogin:function(){
            this.utilController.createDialogController(this, "view.Frags.Dialogs.dlg_dialogLogin");
        }
    });
});