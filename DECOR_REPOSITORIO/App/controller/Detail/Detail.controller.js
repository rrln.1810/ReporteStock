sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/UIComponent",
    "../BaseController",
    "sap/ui/core/Core"
],
/**
 * @param {typeof sap.ui.core.mvc.Controller} Controller
 */
function (Controller, UIComponent, BaseController,Core) {
    "use strict";

    return BaseController.extend("app.reporteStock.controller.Detail.Detail", {
        onInit: function () {
            this.onIniUtils();
            this.dataPass.viewDetail = this;
            this.defaultTabs();
            //this.getToken();
            var oRouter = UIComponent.getRouterFor(this);
            oRouter.attachRoutePatternMatched(this.onRouteMatched, this);
        },
        defaultTabs:function(){
            this.getView().byId("ContentReporteStock").setSelectedKey("tabFilterFondoFijoSoles");
        },
        onAfterRendering: function () {
		},
        onRouteMatched: function (oEvent) {
            var params = this.utilController.unlink(oEvent.getParameter("arguments"));
            if (oEvent.getParameter("name") === "RouteDetail") {
                this.getView().getModel("localModel").setProperty("/flagArqueo",true);
                
                //if (params.Id != 0) {
                   // this.getView().getModel("localModel").setProperty("/flagReporteArqueo",false);
                   // this.getView().getModel("localModel").setProperty("/flagFacturacionMedioPago",false);
                    //this.mostrarBotones(true);
                    this.getToken();
                    // this.getTokenExterno(786);
                    // this.getView().getModel("localModel").setProperty("/flagReporteArqueo",true);
                    // this.getView().getModel("localModel").setProperty("/flagFacturacionMedioPago",false);
                    // this.getView().getModel("localModel").setProperty("/flagReporteArqueoEstado",3);
                    // this.getView().getModel("localModel").setProperty("/flagCalcularResumen",true);
                //}
            }
            if (oEvent.getParameter("name") === "RouteDetailExterno") {
                    if(params.AppEmisor=='RA'){
                        params.SoloLectura;
                        this.getView().getModel("localModel").setProperty("/flagReporteArqueo",true);
                        this.getView().getModel("localModel").setProperty("/flagFacturacionMedioPago",false);
                        //this.getView().getModel("localModel").setProperty("/flagBtnNavegacionDocumentos",true);
                        
                        this.getView().getModel("localModel").setProperty("/flagReporteArqueoEstado",params.IdEstadoArqueo);
                        this.getView().getModel("localModel").setProperty("/flagCalcularResumen",true);
                            if(params.IdEstadoArqueo==1){//Aperturado
                                this.getTokenExterno(params.Id);
                            }
                            if(params.IdEstadoArqueo==2){//Reaperturado
                                this.getTokenExterno(params.Id);
                            }
                            if(params.IdEstadoArqueo==3){//Arqueo
                                //this.getView().getModel("localModel").setProperty("/flagCalcularResumen",false);
                                this.getTokenExterno(params.Id);
                            }
                            if(params.IdEstadoArqueo==5){//Cerrado
                                this.getView().getModel("localModel").setProperty("/flagCalcularResumen",false);
                                this.getTokenExterno(params.Id);
                            }
                            if(params.IdEstadoArqueo==6){//Contabilizado
                                //this.getView().getModel("localModel").setProperty("/flagBtnNavegacionDocumentos",false);
                                this.getView().getModel("localModel").setProperty("/flagCalcularResumen",false);
                                this.getTokenExterno(params.Id);
                            }
                            if(params.IdEstadoArqueo==7){//Aprobado
                                this.getView().getModel("localModel").setProperty("/flagCalcularResumen",false);
                                this.getTokenExterno(params.Id);
                            }
                            if(params.IdEstadoArqueo==8){//Rechazado
                                this.getTokenExterno(params.Id);
                            }
                            if(params.IdEstadoArqueo==9){//Contabilizado con error
                                //this.getView().getModel("localModel").setProperty("/flagBtnNavegacionDocumentos",false);
                                this.getView().getModel("localModel").setProperty("/flagCalcularResumen",false);
                                this.getTokenExterno(params.Id);
                            }
                            if(params.IdEstadoArqueo==10){//En Proceso Contab
                                //this.getView().getModel("localModel").setProperty("/flagBtnNavegacionDocumentos",false);
                                this.getView().getModel("localModel").setProperty("/flagCalcularResumen",false);
                                this.getTokenExterno(params.Id);
                            }
                    }
                    ///INICIO Actualizar Tab Documentos Roy 06-01-2021
                    if(params.AppEmisor=='FACT'){
                        this.getView().getModel("localModel").setProperty("/flagCalcularResumen",true);
                        this.getView().getModel("localModel").setProperty("/flagReporteArqueo",true);
                        this.getView().getModel("localModel").setProperty("/flagReporteArqueoSorpresivo",true);
                        this.getView().getModel("localModel").setProperty("/flagFacturacionMedioPago",true);
                        //this.getView().getModel("localModel").setProperty("/flagBtnNavegacionDocumentos",true);
                        this.getTokenExterno(params.Id);
                    }
                    ///END Actualizar Tab Documentos Roy 06-01-2021
                    this.getView().getModel("localModel").refresh(true);
                    //https://ci-caja-qas.cpp.cfapps.us10.hana.ondemand.com/site?siteId=3293b700-c5f2-40cf-b6de-e40f71c9c3ff#arqueoCaja-display?sap-ui-app-id-hint=saas_approuter_app.reporteStock&/657/RA
            }
            if (oEvent.getParameter("name") === "RouteDetailExternoSorpresivo") {
                //if(params.IdEstadoArqueo==4){//Sorpresivo
                    this.getView().getModel("localModel").setProperty("/flagReporteArqueo",true);
                    this.getView().getModel("localModel").setProperty("/flagReporteArqueoEstado",4);
                    this.getView().getModel("localModel").setProperty("/flagFacturacionMedioPago",false);
                    //this.getView().getModel("localModel").setProperty("/flagBtnNavegacionDocumentos",false);
                    this.getView().getModel("localModel").setProperty("/flagCalcularResumen",false);
                    this.getTokenExternoSorpresivo(params.Id,params.IdArqueo);
                //}
            }         
        },
        onDlg_dialogArqueoSorpresivo: function () {
            var self = this;
            var modelResumen = self.getView().getModel("Resumen").getData();
            //modelResumen.ResumenTotalFaltante
            /*if(parseFloat(modelResumen.ResumenTotalSobrante)==0 ){
                self.dataPass.flagCustodio=false;
                self.getView().getModel("login").setData({});
                self.utilController.createDialogController(self, "view.Frags.Dialogs.dlg_dialogLogin");
            }else if(parseFloat(modelResumen.ResumenTotalSobrante)>0 ){*/
                self.dataPass.flagCustodio=true;
                self.getView().getModel("arqueoSorpresivo").setData({});
                self.utilController.createDialogController(self, "view.Frags.Dialogs.dlg_dialogArqueoSorpresivo");
            /*}*/
        },
        onDlg_dialogGenerarDocumentoArqueo:function(){
            
            this.getView().getModel("impresion").setData({});
            this.utilController.createDialogController(this, "view.Frags.Dialogs.dlg_dialogGenerarDocumentoArqueo");
        },
        onGuardarArqueoCartuchera: function () {
            
            var self = this;
            var valid = self.utilValidation.validatorSAPUI5(self, ["fieldValidFFS"]);
            if (!valid) { 
                return sap.m.MessageToast.show(self.utilController.i18n("mensajeValidacionError"), {
                    duration: 3000
                });
            }
            var data = self.getView().getModel("FFS").getData();
            var horarioCaja = self.getView().getModel("miData").getProperty("/oHorarioCaja");

            var editable = data.iCodEstadoCartuchera === 1 || data.iCodEstadoCartuchera === 3 ? true : false;
            if(!editable){
                return sap.m.MessageToast.show(self.utilController.i18n("mensajeValidacionTieneArqueoCartuchera"), {
                    duration: 3000
                });
            }
            var aDocumentos = self.getView().getModel("D").getProperty("/aDocumentos");
            if(aDocumentos.length==0){
                return self.utilPopUps.messageBox('No se puede enviar fondo ya que no presenta movimientos. \n Solicite deshabilitación de horario a su Supervisor y anulación de fondo a Caja Central.', 'E');
                /*return sap.m.MessageToast.show("No se puede enviar fondo ya que no presenta movimientos, solicite deshabilitación de horario a su Supervisor y anulación de fondo a Caja Central.", {
                    duration: 3000
                });*/
            }
            if(parseFloat(data.FFSTotalImporte).toFixed(2) == parseFloat(data.fImporteMonedaNacCartuchera).toFixed(2)){
                if(data.iId){
                    var rs = self.utilPopUps.messageBox('¿Desea enviar el fondo fijo?', 'c');
                    rs.then(function(confirmation){
                        if (confirmation) {
                            data = self.getView().getModel("FFS").getData();
                            var oParam = self.enviarDatos.registrarArqueoCartuchera(self,data);
                            var rs2 = self.service.servicePost(self.constantes.services.registrarArqueoCartuchera, oParam, self);
                            rs2.then(function(result){
                                if (result.iCode === 1) {
                                    self.getView().getModel("FFS").setProperty("/iId",result.oResults);
                                    self.consultarBovedaCartuchera(horarioCaja.iId);
                                    self.utilHTTP.validarRespuestaServicio(result, 'Se registró correctamente');
                                }else{
                                    self.utilHTTP.validarRespuestaServicio(result);
                                }
                            });
                        }
                    });
                }else{
                    var rs = self.utilPopUps.messageBox('¿Desea enviar el fondo fijo?', 'c');
                    rs.then(function(confirmation){
                        if (confirmation) {
                            data = self.getView().getModel("FFS").getData();
                            var oParam = self.enviarDatos.registrarArqueoCartuchera(self,data);
                            var rs2 = self.service.servicePost(self.constantes.services.registrarArqueoCartuchera, oParam, self);
                            rs2.then(function(result){
                                if (result.iCode === 1) {
                                    self.getView().getModel("FFS").setProperty("/iId",result.oResults);
                                    self.consultarBovedaCartuchera(horarioCaja.iId);
                                    self.utilHTTP.validarRespuestaServicio(result, 'Se registró correctamente');
                                }else{
                                    self.utilHTTP.validarRespuestaServicio(result);
                                }
                            });
                        }
                    });
                }
            }else if(parseFloat(data.FFSTotalImporte) > parseFloat(data.fImporteMonedaNacCartuchera)){
                return sap.m.MessageToast.show('El total del Importe es mayor al de la cartuchera asignada: '+parseFloat(data.fImporteMonedaNacCartuchera).toFixed(2), {
                    duration: 3000
                });
            }else if(parseFloat(data.FFSTotalImporte) < parseFloat(data.fImporteMonedaNacCartuchera)){
                if(data.iId){
                    var rs = self.utilPopUps.messageBox('¿Desea registrar este arqueo con Reposición de Fondo: '+((parseFloat(data.fImporteMonedaNacCartuchera)-parseFloat(data.FFSTotalImporte)).toFixed(2) )+' ?', 'c');
                    rs.then(function(confirmation){
                        if (confirmation) {
                            var oParam = self.enviarDatos.registrarArqueoCartuchera(self,data);
                            var rs2 = self.service.servicePost(self.constantes.services.registrarArqueoCartuchera, oParam, self);
                            rs2.then(function(result){
                                if (result.iCode === 1) {
                                    self.getView().getModel("FFS").setProperty("/iId",result.oResults);
                                    self.consultarBovedaCartuchera(horarioCaja.iId);
                                    self.utilHTTP.validarRespuestaServicio(result, 'Se registró correctamente');
                                }else{
                                    self.utilHTTP.validarRespuestaServicio(result);
                                }
                            });
                        }
                    });
                }else{
                    var rs = self.utilPopUps.messageBox('¿Desea registrar este arqueo con Reposición de Fondo: '+((parseFloat(data.fImporteMonedaNacCartuchera)-parseFloat(data.FFSTotalImporte)).toFixed(2) )+' ?', 'c');
                    rs.then(function(confirmation){
                        if (confirmation) {
                            var oParam = self.enviarDatos.registrarArqueoCartuchera(self,data);
                            var rs2 = self.service.servicePost(self.constantes.services.registrarArqueoCartuchera, oParam, self);
                            rs2.then(function(result){
                                if (result.iCode === 1) {
                                    self.getView().getModel("FFS").setProperty("/iId",result.oResults);
                                    self.consultarBovedaCartuchera(horarioCaja.iId);
                                    self.utilHTTP.validarRespuestaServicio(result, 'Se registró correctamente');
                                }else{
                                    self.utilHTTP.validarRespuestaServicio(result);
                                }
                            });
                        }
                    });
                }
            }
        },
        actualizarArqueoFinal:function(self){
            var oParam = self.enviarDatos.actualizarArqueoVenta(self);
            var rs2 = self.service.servicePut(self.constantes.services.actualizarArqueoVenta, oParam, self);
            rs2.then(function(result){
                if (result.iCode === 1) {
                    self.utilHTTP.validarRespuestaServicio(result, 'Se actualizó correctamente');
                    ///INICIO IMPRIMIR
                    var modData = self.enviarDatos.mapPDFReporteStockResumen(self);
                    self.utilUI.onGenerarPDFReporteStockResumen(self, modData);
                    var modData1 = self.enviarDatos.mapPDFReporteStockDetalle(self);
                    self.utilUI.onGenerarPDFReporteStockDetalle(self, modData1);
                    ///END IMPRIMIR
                    var oHorarioCaja = self.getView().getModel("miData").getProperty("/oHorarioCaja");
                    self.consultarCaja(null,oHorarioCaja);
                }else{
                    self.utilHTTP.validarRespuestaServicio(result);
                }
            });
        },
        registrarArqueoFinal:function(self){
            var oParam = self.enviarDatos.registrarArqueoVenta(self);
            var rs2 = self.service.servicePost(self.constantes.services.registrarArqueoVenta, oParam, self);
            rs2.then(function(result){
                if (result.iCode === 1) {
                    self.getView().getModel("miData").setProperty("/oHorarioCaja/iEstadoHorario",result.oResults.CodEstadoCaja);
                    self.getView().getModel("miData").setProperty("/oHorarioCaja/sDesEstadoCaja",result.oResults.EstadoCaja);
                    self.getView().getModel("miData").refresh(true)
                    ///INICIO IMPRIMIR
                    var modData = self.enviarDatos.mapPDFReporteStockResumen(self);
                    self.utilUI.onGenerarPDFReporteStockResumen(self, modData);
                    var modData1 = self.enviarDatos.mapPDFReporteStockDetalle(self);
                    self.utilUI.onGenerarPDFReporteStockDetalle(self, modData1);
                    ///END IMPRIMIR
                    self.dataPass.flagValidarCerrarPinpad = true;
                    self.onCerrarPinpad();
                }else{
                    self.utilHTTP.validarRespuestaServicio(result);
                }
            });
        },
        onGuardarArqueoVenta: function () {
            /*var self = this;
            var cerrarPinpad = self.validCierrePinpad();
            if(cerrarPinpad){
                var rs = self.utilPopUps.messageBox('¿Desea Cerrar Pinpad?', 'C');
                rs.then(function(confirmation){
                    if(confirmation){
                        var url = self.utilHTTP.urlAmbiente().urlAppFacturacionCerrarPinPad;
                        window.parent.open(url,'_self');
                    }else{
                        self.validGuardarArqueoVenta(self);
                    }
                    
                });
            }else{*/
                this.validGuardarArqueoVenta();
            /*}*/
        },
        validGuardarArqueoVenta:function(self){
            var self = this;
            self.validDocumentos(function(totalInformativo){
                var restrictivos = totalInformativo.filter(function (el) {
                    return el.iId == 2 || el.iId == 3 || el.iId == 4 || el.iId == 5;
                });
                if(restrictivos.length>0){
                    return self.utilController.createDialogController(self, "view.Frags.Dialogs.dlg_dialogInformativo");
                }else{
                    var modelFFS = self.getView().getModel("FFS").getData();
                    var modelResumen = self.getView().getModel("Resumen").getData();
                    if(modelFFS.iCodEstadoCartuchera ===1 || modelFFS.iCodEstadoCartuchera ===3){
                        return self.utilPopUps.messageBox('Debe presionar el botón enviar fondo fijo antes de realizar el arqueo.', 'E');
                    }
                    var valid = self.utilValidation.validatorSAPUI5(self, ["fieldValidFFS","fieldValidES","fieldValidED"]);
                    if (!valid) { 
                        return sap.m.MessageToast.show(self.utilController.i18n("mensajeValidacionError"), {duration: 3000});
                    }
                    var oArqueo = self.getView().getModel("miData").setProperty("/oArqueo");
                    var sobrante = parseFloat(modelResumen.ResumenTotalSobrante).toFixed(2);
                    var faltante = parseFloat(modelResumen.ResumenTotalFaltante).toFixed(2);
                    if(parseFloat(sobrante)>0){
                            var rs = self.utilPopUps.messageBox('¿Desea cerrar el arqueo con un Sobrante de '+sobrante+' soles?', 'c');
                            rs.then(function(confirmation){
                                if (confirmation) {
                                    self.registrarArqueoFinal(self);
                                }
                            });
                        
                    }else if(parseFloat(faltante)>0){
                        
                            var rs = self.utilPopUps.messageBox('¿Desea cerrar el arqueo con un Faltante de '+faltante+' soles?', 'c');
                            rs.then(function(confirmation){
                                if (confirmation) {
                                    self.registrarArqueoFinal(self);
                                }
                            });
                        
                    }else{
                        
                            var rs = self.utilPopUps.messageBox('¿Desea registrar este arqueo?', 'c');
                            rs.then(function(confirmation){
                                if (confirmation) {
                                    self.registrarArqueoFinal(self);
                                }
                            });
                        
                    }
                }
            });
        },
        onGuardarTabResumen:function(){
            this.actualizarArqueoFinalTabResumen(this);
        }
    });
});