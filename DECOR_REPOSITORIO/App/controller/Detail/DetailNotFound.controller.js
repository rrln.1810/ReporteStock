sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/UIComponent",
    "../BaseController"
],
/**
 * @param {typeof sap.ui.core.mvc.Controller} Controller
 */
function (Controller,UIComponent,BaseController) {
    "use strict";

    return BaseController.extend("app.reporteStock.controller.Detail.DetailNotFound", {
        onInit: function () {
            this.onIniUtils();
            this.dataPass.viewNotFound = this;
            var oRouter = UIComponent.getRouterFor(this);
			oRouter.attachRoutePatternMatched(this.onRouteMatched, this);
        },
        onRouteMatched: function(oEvent){
            var self = this;
            if (oEvent.getParameter("name") === "RouteDetailNotFound") {
				var params = oEvent.getParameter("arguments");
				if (params.Id != 0) {
                }
                var flagNoTieneSociedad = self.getView().getModel("localModel").getProperty("/FlagNotieneSociedad");
                if(flagNoTieneSociedad){
                    var rs = self.utilPopUps.messageBox('El usuario registrado no cuenta con sociedad.', 'A');
                    rs.then(function(confirmation){
                        if (confirmation) { 
                            var url = self.utilHTTP.urlAmbiente().urlAppCambioSociedad;
                            window.parent.open(url,'_self');
                            //window.open("https://ci-caja-qas.cpp.cfapps.us10.hana.ondemand.com/site?siteId=3293b700-c5f2-40cf-b6de-e40f71c9c3ff#CambioSociedad-Display?sap-ui-app-id-hint=f829bc40-a297-4568-89d3-042c9cc01617");
                        }
                    });
                    
                }
			}
        }
    });
});
