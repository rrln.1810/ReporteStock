sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/UIComponent",
    "../../BaseController"
],
/**
 * @param {typeof sap.ui.core.mvc.Controller} Controller
 */
function (Controller,UIComponent,BaseController) {
    "use strict";
    var view;
    return BaseController.extend("app.reporteStock.controller.Frags.Tabs.tab_iconTabResumen", {
        onInit: function () {
            this.onIniUtils();
            view = this.dataPass.viewDetail;
        }
        
    });
});
