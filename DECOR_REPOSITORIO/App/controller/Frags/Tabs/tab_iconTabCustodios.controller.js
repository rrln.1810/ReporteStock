sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/UIComponent",
    "../../BaseController"
],
/**
 * @param {typeof sap.ui.core.mvc.Controller} Controller
 */
function (Controller,UIComponent,BaseController) {
    "use strict";
    var view;
    return BaseController.extend("app.reporteStock.controller.Frags.Tabs.tab_iconTabCustodios", {
        onInit: function () {
            this.onIniUtils();
            view = this.dataPass.viewDetail;
        },
        pressBtnMasCustodio: function () {
            view.getView().getModel("arqueoSorpresivo").setData({});
            view.utilController.createDialogController(view, "view.Frags.Dialogs.dlg_dialogAgregarCustodio");
        },
        pressBtnMenosCustodio:function(){

            var idTable = "tb_tablaCustodios";
            var tablaCustodios = sap.ui.getCore().byId(idTable);
            var indices = tablaCustodios.getSelectedIndices();
            var iIds = [];            
            indices.forEach(function(indice){
                var obj = view.getView().getModel("C").getProperty("/aCustodios/"+indice+"/");
                iIds.push({iId:obj.iId,sEstado : obj.sEstado,sCodEstado:obj.sCodEstado});
            });
            var mensaje="";
            // var aEncontrados = $.grep(iIds, function(element, index) {
            //                             return element.sCodEstado != null && element.sCodEstado == view.constantes.sBovedaCodigoEstadoRecepcionado;
            //                         });
            if(iIds.length==0){
					return sap.m.MessageToast.show("No ha seleccionado ningun item.", {duration: 3000});
				}else{

					//if(aEncontrados.length==0){

							if(iIds.length==1){
								mensaje = '¿Desea eliminar el custodio?';
							}else{
								mensaje = '¿Desea eliminar los items seleccionados?';
							}

						var rs = view.utilPopUps.messageBox(mensaje, 'c');
						rs.then(function(confirmation) {
							if (confirmation) {
								view.dataPass.flagAgregarCustodio=false;
								var oParam = view.enviarDatos.eliminarCustodios(iIds);
								var rs2 = view.service.serviceDelete(view.constantes.services.eliminarCustodio, oParam, view);
								rs2.then(function(result) {
									if (result.iCode === 1) {
										var iIdSociedad = view.getView().getModel("miData").getProperty("/oHorarioCaja/iIdSociedad");
										var iCodApertura = view.getView().getModel("miData").getProperty("/oHorarioCaja/iId");
										view.consultarCustodios(iIdSociedad,iCodApertura);
										sap.ui.getCore().byId(idTable).clearSelection();
										view.utilHTTP.validarRespuestaServicio(result,"Se eliminó correctamente el custodio.");
									} else {
										view.utilHTTP.validarRespuestaServicio(result);
									}
								});
							}
						});

					//}else{
					//	return sap.m.MessageToast.show("No se puede eliminar custodios con estado recepcionado.", {duration: 3000});
					//}

				}
        }
        
    });
});
