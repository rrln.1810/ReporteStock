sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/UIComponent",
    "../../BaseController"
],
/**
 * @param {typeof sap.ui.core.mvc.Controller} Controller
 */
function (Controller,UIComponent,BaseController) {
    "use strict";
    var view;
    return BaseController.extend("app.reporteStock.controller.Frags.Tabs.tab_iconTabDocumentos", {
        onInit: function () {
            this.onIniUtils();
            view = this.dataPass.viewDetail;
        },
        onDlg_dialogModificarDocumento:function(oEvent){
            var oHorarioCaja = view.getView().getModel("miData").getProperty("/oHorarioCaja");
            view.getView().getModel("modificarDocumento").setData({});
            var documento = oEvent.getSource().getBindingContext("D").getObject();   
            var link = view.getView().getModel("localModel").getProperty("/link");
            window.parent.open(view.utilHTTP.urlAmbiente().urlAppFacturacion+documento.iIdDocumento+"/link="+link+"/"+documento.iFactManual+"/"+documento.sCodTipoDocumento+"/iIdAp="+oHorarioCaja.iId,'_self');
        },
        onDlg_dialogDetalleAnulacion:function(oEvent){
            var obj = oEvent.getSource().getBindingContext("D").getObject();
            view.getView().getModel("DetalleAnulacion").setData(obj);
            view.utilController.createDialogController(view, "view.Frags.Dialogs.dlg_dialogDetalleAnulacion");
        }
    });
});
