sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/UIComponent",
    "../../BaseController"
],
/**
 * @param {typeof sap.ui.core.mvc.Controller} Controller
 */
function (Controller,UIComponent,BaseController) {
    "use strict";
    var view;
    return BaseController.extend("app.reporteStock.controller.Frags.Dialogs.dlg_dialogLogin", {
        onInit: function () {
            this.onIniUtils();
            view = this.dataPass.viewInicio;
        },
        onCerrarDlg_dialogLogin:function(){
            sap.ui.getCore().byId("dlg_dialogLogin").destroy();
        },
        onLoginDlg_dialogLogin: async function(){
            var self = this;
            var oParam = {"Usuario":"DMELENDEZ", "Password":"DMELENDEZ9*"};
            var result = await self.service.servicePost(self.constantes.services.login, oParam, self, null);
            if (result.iCode === 1) {
                self.utilHTTP.validarRespuestaServicio(result, 'Se consult� correctamente.');
            } else {
                self.utilHTTP.validarRespuestaServicio(result);
            }
        }
    });
});
