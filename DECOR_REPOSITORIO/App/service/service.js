sap.ui.define([
    '../constantes',
    "../util/utilHTTP",
	"../util/utilResponse"
], function (constantes, utilHTTP, utilResponse) {
	return { 
		servicePost: function  (url,oParam, context,mockData,masterBusy,detailBusy,mantenerBusy) {
			return utilHTTP.httpPost(url,oParam,context,mockData,masterBusy,detailBusy,mantenerBusy);
		},
		servicePostLocal: function  (url,oParam, context,mockData,masterBusy,detailBusy,mantenerBusy) {
			return utilHTTP.httpPostLocal(url,oParam,context,mockData,masterBusy,detailBusy,mantenerBusy);
		},
		servicePut: function  (url,oParam, context,mockData,masterBusy,detailBusy,mantenerBusy) {
			return utilHTTP.httpPut(url.replace('{0}', oParam.iId),oParam,context,mockData,masterBusy,detailBusy,mantenerBusy);
		},
		serviceDelete: function  (url,oParam, context,mockData,masterBusy,detailBusy,mantenerBusy) {
			return utilHTTP.httpDelete(url.replace('{0}', oParam.iId),oParam,context,mockData,masterBusy,detailBusy,mantenerBusy);
		},
		serviceGet: function  (url,oParam, context,mockData,masterBusy,detailBusy,mantenerBusy) {
			var modUrl = oParam.length>0 ?  url+"?"+oParam[0] : url;
			return utilHTTP.httpGet(modUrl,context,mockData,masterBusy,detailBusy,mantenerBusy);
		},
		serviceGetOdata: function  (url,oParam, context,mockData,masterBusy,detailBusy,mantenerBusy) {
			var modUrl = oParam.length>0 ? url+"?$filter="+oParam[0] : url;
			return utilHTTP.httpGet(modUrl,context,mockData,masterBusy,detailBusy,mantenerBusy);
		}
	};
});