/* global moment:true */
jQuery.sap.require("sap.ui.core.format.DateFormat");
sap.ui.define([
	"sap/ui/core/format/NumberFormat"
], function (NumberFormat) {
	"use strict";
	return {
		formatterEstado:function(dato){
            //iEstadoHorario 
            /*
            Aperturado 1
            Reaperturado 2
            Arqueo 3
            Sorpresivo 4
            Cerrado 5
            Contabilizado 6
            Pendiente 0
            Aprobado 7
            Rechazado 8
            Contabilizado con error 9
            */
           /*if(dato==1 || dato==2){
            return "Success";
            }
            else if(dato==8){
				return "Warning";
			}
			
			else(dato==2){
				return "Error";
            }*/
            if(dato==8){
                return "Warning";
            }
		},
		horaApertura: function(inicio, fin){
            var retorno         =   "";

            if (inicio  ||  fin){ 
                var fechaInicio     =   new Date(inicio);
                var fechaFin        =   new Date(fin);
                var horaInicio      =   fechaInicio.getUTCHours()<10?"0"+fechaInicio.getUTCHours():fechaInicio.getUTCHours();
                var minutoInicio    =   fechaInicio.getUTCMinutes()<10?"0"+fechaInicio.getUTCMinutes():fechaInicio.getUTCMinutes();
                var horaFin         =   fechaFin.getUTCHours()<10?"0"+fechaFin.getUTCHours():fechaFin.getUTCHours();
                var minutoFin       =   fechaFin.getUTCMinutes()<10?"0"+fechaFin.getUTCMinutes():fechaFin.getUTCMinutes();

                retorno         =   horaInicio + ":" + minutoInicio + " - " + horaFin + ":" + minutoFin;
            }
            return retorno;
		},
		


		////////////////////////////////
		stateEstado: function(estado){
            var retorno; 

            if (estado  === "Activo"){
                retorno =   "Success";
            } else {
                retorno =   "Error";
            }

            return retorno;
         },

         documentosNombrePaciente: function(sTipoDoc, sRazonSocial, sNombrePaciente, sApellidoPaciente){
            var retorno =   "Validar tipo documento";

            if (sTipoDoc    === "01"){
                retorno =   sRazonSocial;
            } else {
                retorno =   sNombrePaciente  + " " + sApellidoPaciente;
            }

            return retorno;
         },

         motivoImprimir: function(codigo, descripcion){
            var retorno; 

            if (codigo  === "0000"){
                retorno =   descripcion;
            } else {
                retorno =   codigo + " - " + descripcion;
            }

            return retorno;
         },

         iconEstado: function(estado){
            var retorno; 

            if (estado  === "Activo"){
                retorno =   "sap-icon://accept";
            } else {
                retorno =   "sap-icon://sys-cancel-2";
            }

            return retorno;
         },

         excelEnabler: function(lista){
            var retorno; 

            if (lista.length  > 0){
                retorno =   true;
            } else {
                retorno =   false;
            }

            return retorno;
         },
        
        visibleAseguradora: function (value) {
            var retorno =   false; 

            value==="2"?retorno=true:retorno=false;
            
            return retorno; 
        },

        pacienteAsegurado: function (value) {
            var retorno =   ""; 

            if (value   === "2"){
                retorno =   "Si";
            } else {
                retorno =   "No";
            }
            
            return retorno; 
        },

        pacienteIcon: function (tipoNIF, Asegurado) {
            var retorno =   "sap-icon://away"; 

            if (Asegurado   !== undefined){
                if (tipoNIF === "RUC"){
                    retorno =   "sap-icon://business-card";
                } else {
                    if (Asegurado.toString()   === "2"){
                        retorno =   "sap-icon://hr-approval"
                    } else {
                        retorno =   "sap-icon://customer"
                    }
                }
            }

            return retorno; 
        },

        noAplica:function(value){
            return value==""?"No aplica":value;
        },

        importeMonedaLocal: function(importe, moneda, TC){
            var retorno     =       importe;
            if(moneda       ===     "USD"){
                retorno     =       parseFloat(importe) *   parseFloat(TC);
            }

            var oNumberFormat = NumberFormat.getFloatInstance({
                minFractionDigits   : 2,  
                maxFractionDigits   : 2,
                decimals            : 2,
                groupingEnabled     : true,
                groupingSeparator   : ",",
                decimalSeparator    : "."
            });

            var importe = oNumberFormat.format(parseFloat(retorno).toFixed(2))
                
            return importe;
        },

        importeMonedaExtranjera: function(importe, moneda, TC){
            var retorno     =       importe;
            if(moneda       ===     "PEN"){
                retorno     =       parseFloat(importe) /   parseFloat(TC);
            }

            var oNumberFormat = NumberFormat.getFloatInstance({
                minFractionDigits   : 2,  
                maxFractionDigits   : 2,
                decimals            : 2,
                groupingEnabled     : true,
                groupingSeparator   : ",",
                decimalSeparator    : "."
            });

            var importe = oNumberFormat.format(parseFloat(retorno).toFixed(2))
                
            return importe;
        },

        FormatterDinero: function(value){
           if(value!=null){
           	var oNumberFormat = NumberFormat.getFloatInstance({
               minFractionDigits: 2,  
               maxFractionDigits: 2,
                 decimals: 2,
                 groupingEnabled: true,
                 groupingSeparator: ",",
                 decimalSeparator: "."
               });
               
               return oNumberFormat.format(value);
           }else{
           	return "";
           }
           
        },

        FormatterDineroPositivo: function(value){

        var oNumberFormat = NumberFormat.getFloatInstance({
            minFractionDigits: 2,  
            maxFractionDigits: 2,
              decimals: 2,
              groupingEnabled: true,
              groupingSeparator: ",",
              decimalSeparator: "."
            });

            // if (parseFloat(value)   <   0){
            //     value   =   0;
            // }
            
            return oNumberFormat.format(Math.abs(value));
        },

       ISODate: function(value, sTipoPFA){
        
        var retorno =   "";

        if (sTipoPFA    === "PRE-FACTURA"){
            if (value    !== ""){
                var date    =   new Date(value);
                var year    =   date.getFullYear();
                var month   =   date.getMonth()+1;
                var dt      =   date.getDate();
    
                if (dt < 10) {
                    dt = '0' + dt;
                }
                if (month < 10) {
                    month = '0' + month;
                }
    
                retorno =   dt + '/' + month + '/' + year
            }
        }
        
        return  retorno;
       },

        visibleMediosPagos: function(campo4, campo5, tipoPFA){

            var retorno =   false; 

            if (campo4   !==   "true"){
                if (campo5.indexOf(tipoPFA) >   -1){
                    retorno =   true;
                } else {
                    retorno =   false;
                }
            } else {
                retorno =   false;
            }
            return retorno;

        },

        visibleMediosAbono: function(ImporteVueltoRedondeo, campo3, campo2, NCExiste, tipoPFA){
            var tipoPFAOk;
            var retorno =   false; 

            if (tipoPFA === "1" ||  tipoPFA === "PRE-FACTURA"){
                tipoPFAOk   =   "PRE-FACTURA";
            } else {
                tipoPFAOk   =   "PRE-ABONO";
            }

            if (campo2.indexOf(tipoPFAOk) >   -1){
                retorno =   true;
            } else {
                if (NCExiste){
                    retorno =   true;
                } else {
                    retorno =   false;
                }
            }
            
            // Valida que no supere el importe maximo
            if (parseFloat(ImporteVueltoRedondeo)   >   parseFloat(campo3)){
                retorno =   false;
            }
            
            return retorno;

        },

        fechaValida: function(value){
            var retorno =   "";

            if (value){
                if (value.split("/").length >   1){
                    retorno =   value;
                } else if (value.split("").length  === 8){
                    var anio, mes, dia;
                    anio    =   value.split("")[0]  +   value.split("")[1]  +   value.split("")[2]  +   value.split("")[3];
                    mes     =   value.split("")[4]  +   value.split("")[5];
                    dia     =   value.split("")[6]  +   value.split("")[7];
                    retorno =   dia +   "/" +   mes +   "/" +   anio;
                } else {
                    retorno =   "Fecha invÃ¡lida";
                }
            }
            
            return retorno;
        },

        fechaValidaV: function(sTipoPFA){
            var retorno =   true;

            if (sTipoPFA    === "PRE-ABONO" ||  sTipoPFA    === "2"){
                retorno =   false;
            }
            
            return retorno;
        },
        fechaApertura: function(value){
            function pad (n, padString, length) { //Añadir ceros a la izquierda, length tamaño final que debe tener
                var n = n.toString();
                while (n.length < length) {
                    n = padString + n;
                }
                return n;
            }
            var retorno         =   "";

            if (value){ 
                var dFecha = (new Date(value));
                dFecha.setDate(dFecha.getDate()+1);
                var sFecha     =   dFecha.toLocaleDateString();
                var dia       =   pad(sFecha.split("/")[0],"0",2);
                var mes       =   pad(sFecha.split("/")[1],"0",2);
                var anio      =   pad(sFecha.split("/")[2],"0",4);

                retorno         =   dia + "/" + mes + "/" + anio;
            }
            return retorno;
        },

        dosDecimales: function(value){
            return parseFloat(value).toFixed(2);
        },

        footerImprimir:function(PEN, USD, modoAbono){
            var retorno =   true;

            if (PEN !== undefined){
                if (PEN >   0   ||  USD >   0){
                    retorno =   false;
                }
            }

            if (modoAbono   === "Seleccionar"){
                retorno =   false;
            }
            
            return retorno; 
        },

        tipoComprobanteImprimirAbono: function(tipoPFA){
            var retorno =   true;

            if (tipoPFA === "PRE-FACTURA"){
                retorno =   false;
            }

            return retorno;
        },

        tipoComprobanteImprimirAbonoAnulacion: function(tipoPFA, esAnulacion){
            var retorno =   false;

            if (tipoPFA === "PRE-ABONO"){
                retorno =   esAnulacion;
            }

            return retorno;
        },

        tipoComprobanteImprimirFactura: function(tipoPFA){
            var retorno =   true;

            if (tipoPFA === "PRE-ABONO"){
                retorno =   false;
            }

            return retorno;
        },

        iconTabTipoComprobante: function(tipoPFA){
            var retorno =   "1";

            if (tipoPFA === "PRE-ABONO"){
                retorno =   "3";
            }
            return retorno;
        },
        formatoEstadoSCP: function (estado) {
            var retorno;
 
            if (estado  === ""){
                retorno = "None";
            } else if (estado  === 0){
                retorno = "Warning";
            } else if (estado  === 1){
                retorno = "Success";
            } else if (estado  === 2){
                retorno = "Success";
            } else if (estado  === 3){
                 retorno = "Error";
            }
 
            return retorno;
         },
 


	};
});