sap.ui.define([

], function() {
    "use strict";
    return {
        pad: function(n, padString, length) { //Añadir ceros a la izquierda, length tamaño final que debe tener
            var n = n.toString();
            while (n.length < length) {
                n = padString + n;
            }
            return n;
        },
        mapVGenericaCampo: function(self, datos, oFiltro) {
            var that = this;
            var servDatos = datos;
            oFiltro.forEach(function(item) {
                var result = [];
                var nameTable = item;
                var modData = servDatos.filter(function(el) {
                    return el["sCodigoTabla"] == nameTable;
                });
                if (item == oFiltro[0]) {
                    result = that.consultarEfectivoSoles(modData);
                    self.getView().getModel("FFS").setProperty("/aFondoFijoSoles", that.consultarFondoFijoSoles(modData));
                    self.getView().getModel("ES").setProperty("/aEfectivoSoles", that.consultarEfectivoSoles(modData));
                } else if (item == oFiltro[1]) {
                    result = that.consultarEfectivoDolares(modData);
                    self.getView().getModel("ED").setProperty("/aEfectivoDolares", result);
                } else if (item == oFiltro[2]) {
                    result = that.consultarConceptos(modData);
                    self.getView().getModel("localModel").setProperty("/aConceptos", result);
                    var sorpresivo = result.filter(function(el){
                        return el.sCampo2=="Sorpresivo" || !el.iId;
                    });
                    self.getView().getModel("localModel").setProperty("/aConceptosSorpresivo", sorpresivo);
                    var custodio = result.filter(function(el){
                        return el.sCampo2=="Definitivo" || !el.iId;
                    });
                    self.getView().getModel("localModel").setProperty("/aConceptosCustodio", custodio);
                } else if (item == oFiltro[3]) {
                    result = that.consultarTipoTarjeta(modData);
                    self.getView().getModel("localModel").setProperty("/aTipoTarjeta", result);
                }else if (item == oFiltro[4]) {
                    result = that.consultarRoles(modData);
                    self.getView().getModel("localModel").setProperty("/aRoles", result);
                }
            });
            ///INICIO HARDCODE
            self.getView().getModel("localModel").setProperty("/aOpcionesImpresion", that.opcionesImpresion());
            /*self.getView().getModel("Resumen").setProperty("/aResumenTipoDocumento", that.consultarResumenTipoDocumento());
            self.getView().getModel("Resumen").setProperty("/aResumenMedioPago", that.consultarResumenMedioPago());*/
            ///END HARDCODE
        },
        consultarFondoFijoSoles: function(data) {
            var modData = [];
            if(data){
                data.forEach(function(item) {
                    var obj = {
                        "iId": item.iId,
                        "sCodDenominacion": item.sCodigoMaestro ? item.sCodigoMaestro : "",
                        "sDenominacion": item.sCampo4 ? item.sCampo4 : "",
                        "sMoneda": item.sCampo2 ? item.sCampo2 : "",
                        "fValor": item.sCampo1 ? parseFloat(item.sCampo1) : 0,
                        "iCantidad": item.iCantidad ? item.iCantidad : 0,
                        "fImporte": item.fImporte ? item.fImporte : (0).toFixed(2),
                        "editable": true
                    };
                    modData.push(obj);
                });
            }
            return modData;
        },
        consultarEfectivoSoles: function(data) {
            var modData = [];
            if(data){
                data.forEach(function(item) {
                    var obj = {
                        "iId": item.iId,
                        "sCodDenominacion": item.sCodigoMaestro ? item.sCodigoMaestro : "",
                        "sDenominacion": item.sCampo4 ? item.sCampo4 : "",
                        "sMoneda": item.sCampo2 ? item.sCampo2 : "",
                        "fValor": item.sCampo1 ? parseFloat(item.sCampo1) : 0,
                        "iCantidad": item.iCantidad ? item.iCantidad : 0,
                        "fImporte": item.fImporte ? item.fImporte : (0).toFixed(2)
                    };
                    modData.push(obj);
                });
            }
            return modData;
        },
        consultarEfectivoDolares: function(data) {
            var modData = [];
            if(data){
                data.forEach(function(item) {
                    var obj = {
                        //"iId":item.iId,
                        "sCodDenominacion": item.sCodigoMaestro ? item.sCodigoMaestro : "",
                        "sDenominacion": item.sCampo4 ? item.sCampo4 : "",
                        "sMoneda": item.sCampo2 ? item.sCampo2 : "",
                        "fValor": item.sCampo1 ? parseFloat(item.sCampo1) : 0,
                        "iCantidad": item.iCantidad ? item.iCantidad : 0,
                        "fImporte": item.fImporte ? item.fImporte : (0).toFixed(2)
                    };
                    modData.push(obj);
                });
            }
            return modData;
        },
        consultarDocumentos: function(self,data) {
            var documentos = data.aDocumentos;
            var pagos = data.aPagos;
            var modData = [];
            if(documentos){
                documentos.forEach(function(item, x) {
                    var modPagos = pagos.filter(function(el){
                        return el.iIdDocumento == item.iIdDocumento;
                    });
                    modPagos = self.utilController.removeDuplicatesGeneral(modPagos,"sFormaPago");
                    var formaPago = "";
                    if(modPagos.length>0){
                        if(modPagos.length==1){
                            formaPago = modPagos[0].sFormaPago;
                        }else{
                            formaPago = "Múltiple";
                        }
                    }
                    
                    var tipoDoc = item.sCodTipoDocSunat;
                    var cliente;
                    var observacion, motivo;
                    if( (item.sCodTipoDocumento==3 || item.sCodTipoDocumento==1) && item.sDesEstadoDoc=="Anulado" ){
                        motivo = item.sMotivoBaja;
                        observacion = item.sObservacionAnulacion;
                    }
                    if(item.sCodTipoDocumento==7 && item.sDesEstadoDoc=="Anulado"){
                        motivo = item.sMotivoNC;
                        observacion = item.sObservacionNC;
                    }

                    if (tipoDoc == 6 || (item.sCodTipoDocumento==1 && tipoDoc == 0)) {
                        cliente = item.sRazonSocial;
                    } else {
                        cliente = item.sApellidoPaciente || item.sNombrePaciente ? (item.sApellidoPaciente ? item.sApellidoPaciente:"") + ", " +  (item.sNombrePaciente ? item.sNombrePaciente:"") : "";
                        //cliente = item.sNombrePacienteDetalle ? item.sNombrePacienteDetalle : ""; //item.sNombrePaciente ? item.sNombrePaciente + " " + (item.sApellidoPaciente ? item.sApellidoPaciente : "") : "";
                    }
                        var sNroDocPartido;
                        var sSerie; 
                        var sCorrelativo
                    if(item.sNroDoc){
                         sNroDocPartido  =  item.sNroDoc.split("-");
                    }
                    if(sNroDocPartido){
                             sCorrelativo  = sNroDocPartido[0] ? sNroDocPartido[0] : "";
                        sSerie  = sNroDocPartido[1] ? sNroDocPartido[1] : "";
                    }else{
                            sCorrelativo = "";
                           sSerie  = "";
                    }
                   
                    var obj = {
                        //"iId":item.iId,
                        "iItem": x + 1,
                        "sTipoDocumento": item.sDesTipoDocumento  ? ( item.iFactManual==1 ? (item.sDesTipoDocumento + " Manual") : item.sDesTipoDocumento   )  : "",
                        "sEstado": item.sDesEstadoDoc ? item.sDesEstadoDoc : "",
                        "sPaciente": item.sNombrePacienteDetalle,//item.sNombrePaciente ? item.sNombrePaciente + " " + (item.sApellidoPaciente ? item.sApellidoPaciente : "") : "",
                        "sTipoDocumentoIdentidad": item.sDesTipoDocSunat ? item.sDesTipoDocSunat : "",
                        "sNumeroIdentidad": item.sNumDocIdentidad ? item.sNumDocIdentidad : "",
                        "sCliente": cliente,
                        "sNumeroSunat": item.sNroDoc ? item.sNroDoc : "",
                        "fTipoCambio": item.fTipoCambio ? parseFloat(item.fTipoCambio).toFixed(2) : "0",
                        "fTipoCambioOriginal": item.fTipoCambio ? parseFloat(item.fTipoCambio) : 0,
                        "fAjustadoRedondeoPEN": item.fAjustadoRedondeoPEN ? parseFloat(item.fAjustadoRedondeoPEN) : "0",
                        "fAjustadoRedondeoUSD": item.fAjustadoRedondeoUSD ? parseFloat(item.fAjustadoRedondeoUSD) : "0",
                        "fAjustadoRedondeoEfectivoPEN": item.fAjustadoRedondeoEfectivoPEN ? parseFloat(item.fAjustadoRedondeoEfectivoPEN) : "0",
                        "fAjustadoRedondeoEfectivoUSD": item.fAjustadoRedondeoEfectivoUSD ? parseFloat(item.fAjustadoRedondeoEfectivoUSD) : "0",
                        "fAjusteRedondeoDepositoPEN" : item.fAjusteRedondeoDepositoPEN ?  parseFloat(item.fAjusteRedondeoDepositoPEN) : "0", 
                        "fImporteRedondeoVuelto": item.fImporteRedondeoVuelto ? parseFloat(item.fImporteRedondeoVuelto) : "0",
                        "sMoneda": item.sCodMoneda ? item.sCodMoneda : "",
                        "fImporte": item.fMonto ? parseFloat(item.fMonto) : "0",//parseFloat(item.fMonto).toFixed(2) : "0",
                        "fImporteOriginal": item.fMonto ? parseFloat(item.fMonto) : 0,
                        "iIdMotivo": item.iIdMotivo ? item.iIdMotivo : null,
                        "sMotivoAnulacion": item.sMotivoBaja ? item.sMotivoBaja : "",
                        "sCorrelativo" : sCorrelativo,
                        "sSerie" : sSerie,
                        "dFechaCreacion": item.dFechaCreacion,
                        "fBase": item.fBase,
                        "fIgv": item.fIgv,
                        "fMonto": item.fMonto,
                        "iCodArea": item.iCodArea,
                        "iCodSedeEmisor": item.iCodSedeEmisor,
                        "iCodSociedad": item.iCodSociedad,
                        "iId": item.iId,
                        "iIdApertura": item.iIdApertura,
                        "iIdDocumento": item.iIdDocumento,
                        "sApellidoPaciente": item.sApellidoPaciente,
                        "sCodTipoDocSunat": item.sCodTipoDocSunat,
                        "sCodTipoDocumento": item.sCodTipoDocumento,
                        "sDesCodArea": item.sDesCodArea,
                        "sDesSedeEmisor": item.sDesSedeEmisor,
                        "sDesTipoDocSunat": item.sDesTipoDocSunat,
                        "sDesTipoDocumento": item.sDesTipoDocumento,
                        "sNombrePaciente": item.sNombrePaciente,
                        "sNroDoc": item.sNroDoc,
                        "sNumComprobanteSunat": item.sNumComprobanteSunat,
                        "sNumDocIdentidad": item.sNumDocIdentidad,
                        "sRazonSocial": item.sRazonSocial,
                        "sUsuarioCreador": item.sUsuarioCreador,
                        "iFactManual": item.iFactManual,
                        "sDesEstadoDoc": item.sDesEstadoDoc,
                        "fImporteVuelto": item.fImporteVuelto ? parseFloat(item.fImporteVuelto).toFixed(2) : 0,
                        "sCodSunat": item.sCodSunat,
                        "sModoAbono": item.sModoAbono,
                        "sFormaPago": formaPago,//item.sFormaPago,

                        
                        
                        /*"sMotivoNC": item.sMotivoNC,
                        "sObservacionAnulacion": item.sObservacionAnulacion,
                        "sObservacionNC": item.sObservacionNC,
                        "sCodUsuarioLogin": item.sCodUsuarioLogin,
                        "dFechaModificacion": item.dFechaModificacion,*/
                        
                        "sUsuarioAnulador": item.sUsuarioSupervisor,//item.sCodUsuarioLogin,
                        "sFechaAnulacion": item.sFechaAnulacion,//item.dFechaModificacion ? (new Date(item.dFechaModificacion.replace("Z", ""))).toLocaleString() : "",
                        "sMotivo": motivo,
                        "sObservacion": observacion,
                        "sMonedaVuelto" : item.sMonedaVuelto,
                        "sReferencia"   : item.sReferencia,
                        "sReferenciaNC" : item.sReferenciaNC,
                        "sCodTipoDocumentoOrigen": item.sCodTipoDocumentoOrigen,
                        "sDesTipoDocumentoOrigen": item.sDesTipoDocumentoOrigen,
                        "sReferenciaOrigen": item.sReferenciaOrigen


                    };
                    modData.push(obj);
                });
            }
            return modData;
        },
        consultarPagos: function(data) {
            var modData = [];
            if(data){
                data.forEach(function(item) {
                    var obj = {
                        "iId": item.iId,
                        "dFechaOperacion": item.dFechaOperacion,
                        "fImporte": item.fImporte ? parseFloat(item.fImporte).toFixed(2) : 0,
                        "iCodApertura": item.iCodApertura,
                        "iIdDocumento": item.iIdDocumento,
                        "iIdUsuarioCaja": item.iIdUsuarioCaja,
                        "sBanco": item.sBanco,
                        "sCodBanco": item.sCodBanco,
                        "sCodFormaPago": item.sCodFormaPago,
                        "sCodTipoTarjeta": item.sCodTipoTarjeta,
                        "sFechaOperacion": item.sFechaOperacion,
                        "sFormaPago": item.sFormaPago,
                        "sFormaTarjeta": item.sFormaTarjeta,
                        "sIdTransaccionPago": item.sIdTransaccionPago,
                        "sMoneda": item.sMoneda,
                        "sNumReferencia": item.sNumReferencia,
                        "sNumTarjetaCheque": item.sNumTarjetaCheque,
                        "sNumeroOperacion": item.sNumeroOperacion,
                        "sTipoPost": item.sTipoPost,
                        "sTipoTarjeta": item.sTipoTarjeta,
                        "iIdDocumentoSunat": item.iIdDocumentoSunat,
                        "fTipoCambio": item.fTipoCambio ? parseFloat(item.fTipoCambio).toFixed(2) : 0,
                        "sReferencia" : item.sNumReferencia,
                        "sTarjetaCheque" : item.sNumTarjetaCheque.split("*****")[1],
                        "sEstadoPINPAD":item.sEstadoPINPAD
                    };
                    modData.push(obj);
                });
            }
            return modData;
        },
        consultarTraslados: function(data,ctx) {
            var modData = [];
            var self = ctx;
            data = data.filter(function (el) {
				return el.sNombreEstado == "Aprobado";
			});
			
            if(data){
                data.forEach(function(item, x) {
                    var importePEN = 0;
                    var importeUSD = 0;
                    if (item.sMoneda == "PEN") {
                        importePEN = item.sTotalImporte;
                    }
                    if (item.sMoneda == "USD") {
                        importeUSD = item.sTotalImporte;
                    }
                     var sFecha = item.sFechaSinFormato ? (new Date(item.sFechaSinFormato.replace("Z", ""))).toLocaleString() : "";
                    var obj = {
                        "iId": item.iId,
                        "iItem": x + 1,
                        "sCodigo": item.sCodigo ? item.sCodigo : "",
                        "sFecha": sFecha.length>0 ? this.utilController.formatFechaStringToDDMMAAAAHHMMSS(item.sFechaSinFormato) : "",//sFecha ,
                        //"sFecha": sFecha.length>0 ? this.utilController.formatFechaDDMMAAAAHHMMSS(new Date(item.sFechaSinFormato)) : "",//sFecha ,
                        //"sFecha": item.sFecha ? (new Date(item.sFecha.replace("Z", ""))).toLocaleString() : "",
                        "dFecha": item.sFecha ? Date.parse(new Date(sFecha) ): "",
                        "fImportePEN": importePEN ? parseFloat(importePEN).toFixed(2) : "0",
                        "fImporteUSD": importeUSD ? parseFloat(importeUSD).toFixed(2) : "0",
                        "fImportePENOriginal": importePEN ? parseFloat(importePEN) : 0,
                        "fImporteUSDOriginal": importeUSD ? parseFloat(importeUSD) : 0,
                        "iIdEstado": item.iIdEstado ? item.iIdEstado : "",
                        "sMoneda": item.sMoneda ? item.sMoneda : ""
                    };
                    modData.push(obj);
                }.bind(self));
            }
            return modData;
        },
        consultarCustodios: function(data,self) {
                
            var modData = [];
            var ctx = self;
            if(data){
                data.forEach(function(item, x) {
                    ;
                    //var sFecha = item.sFecha ? (new Date(item.sFecha.replace("Z", ""))).toLocaleString() : "";
                    var sFecha = item.sFecha ? item.sFecha : "";
                    var obj = {
                        "iId": item.iId,
                        "iItem": x + 1,
                        "sCodigo": item.sCodigo ? item.sCodigo : "",
                        "sCodEstado" : item.sCodEstado ? item.sCodEstado : null, 
                        "sFecha": sFecha.length>0 ? this.utilController.formatFechaStringToDDMMAAAAHHMMSS(item.sFecha) : "",//sFecha ,
                        "dFecha": item.sFecha ? Date.parse(new Date(sFecha) ): "",
                        "sEstado": item.sEstado ? item.sEstado : "",
                        "sMotivoRechazo" : item.sMotivoRechazo ? item.sMotivoRechazo : "",
                        "sConcepto": item.sConcepto ? item.sConcepto : "",
                        "sUsuarioCustodia": item.sUsuarioCajero ? item.sUsuarioCajero : "",
                        "fImportePEN": item.sImportePEN ? item.sImportePEN : "0.00",
                        "fImporteUSD": item.sImporteUSD ? item.sImporteUSD : "0.00",
                        "fImportePENOriginal": item.sImportePEN ? item.sImportePEN : "0.00",
                        "fImporteUSDOriginal": item.sImporteUSD ? item.sImporteUSD : "0.00"
                    };
                    modData.push(obj);
                }.bind(self));
            }
            return modData;
        },
        mapConsultarArqueoUsuario: function(self, data) {
             
            var that = this;
            var oArqueo = data.oArqueo;
            var aDenominacion = data.oArqueoDenominacion;
            var aDenominacionResumen = data.oArqueoResumen;
            var modelES = self.getView().getModel("ES");
            var modelED = self.getView().getModel("ED");
            var modelD = self.getView().getModel("D");
            var modelResumen = self.getView().getModel("Resumen");
            var modelLocal = self.getView().getModel("localModel");
            var ES = modelES.getProperty("/aEfectivoSoles");
            var ED = modelED.getProperty("/aEfectivoDolares");
            
            var ESTotalCantidad = 0;
            var ESTotalImporte = 0;
            ES.forEach(function(item, x) {
                var obj = aDenominacion.find(function(el) {
                    return el.sCodDenominacion == item.sCodDenominacion;
                });
                if (obj) {
                    modelD.setProperty("/TotalEfectivoPEN",parseFloat(parseFloat(obj.fTotalEfectivoPEN).toFixed(2)));
                    item.iCantidad = obj.iCantidad ? parseInt(obj.iCantidad) : 0;
                    item.fImporte = obj.fImporte ? parseFloat(obj.fImporte).toFixed(2) : 0;
                    modelES.setProperty("/aEfectivoSoles/" + x + "/iId", obj.iId);
                    modelES.setProperty("/aEfectivoSoles/" + x + "/iCantidad", item.iCantidad);
                    modelES.setProperty("/aEfectivoSoles/" + x + "/fImporte", item.fImporte);
                }
                ESTotalCantidad = ESTotalCantidad + item.iCantidad;
                ESTotalImporte = (parseFloat(ESTotalImporte) + parseFloat(item.fImporte)).toFixed(2);
            });
            modelES.setProperty("/ESTotalCantidad", ESTotalCantidad);
            modelES.setProperty("/ESTotalImporte", ESTotalImporte);
            var EDTotalCantidad = 0;
            var EDTotalImporte = 0;
            ED.forEach(function(item, x) {
                var obj = aDenominacion.find(function(el) {
                    return el.sCodDenominacion == item.sCodDenominacion;
                });
                if (obj) {
                    modelD.setProperty("/TotalEfectivoUSD",parseFloat(parseFloat(obj.fTotalEfectivoUSD).toFixed(2)));
                    item.iCantidad = obj.iCantidad ? parseInt(obj.iCantidad) : 0;
                    item.fImporte = obj.fImporte ? parseFloat(obj.fImporte).toFixed(2) : 0;
                    modelED.setProperty("/aEfectivoDolares/" + x + "/iId", obj.iId);
                    modelED.setProperty("/aEfectivoDolares/" + x + "/iCantidad", item.iCantidad);
                    modelED.setProperty("/aEfectivoDolares/" + x + "/fImporte", item.fImporte);
                }
                EDTotalCantidad = EDTotalCantidad + item.iCantidad;
                EDTotalImporte = (parseFloat(EDTotalImporte) + parseFloat(item.fImporte)).toFixed(2);
            });
            
            var listaTipoDocumento = that.aListaTipoDocumento();
            var RTDTotalCantidad = 0;
            var RTDTotalImporte = 0;
            var aICorrelativosTipoDocumentoNegativo = [5,14];
            listaTipoDocumento.forEach(function(item, x) {
                var obj = aDenominacionResumen.find(function(el) {
                    return item.sCodDenominacion && el.sCodDenominacionDoc == item.sCodDenominacion;
                });              
                if (obj) {
                        console.log(obj.fImporte)
                    item.iCantidad = obj.iCantidad ? parseInt(obj.iCantidad) : 0;
                    item.fImporte = obj.fImporte ? parseFloat(obj.fImporte) : 0;
                    if(obj.iCorrelativo){
                           
                        RTDTotalCantidad = RTDTotalCantidad + item.iCantidad;
                       

                            if(aICorrelativosTipoDocumentoNegativo.includes(obj.iCorrelativo)){
                                RTDTotalImporte = (parseFloat(RTDTotalImporte) - parseFloat(item.fImporte)).toFixed(2);
                            }else{
                                 RTDTotalImporte = (parseFloat(RTDTotalImporte) + parseFloat(item.fImporte)).toFixed(2);     
                            }
                        
                    }
                }
            });
            var listaMedioPago = that.aListaMedioPago();
            var RMPTotalCantidad = 0;
            var RMPTotalImporte = 0;
            var aICorrelativosMedioPagoNegativo = [29,30,31,32,33];
            listaMedioPago.forEach(function(item, x) {
                var obj = aDenominacionResumen.find(function(el) {
                    return item.sCodDenominacion && el.sCodDenominacionDoc == item.sCodDenominacion;
                });
                if (obj) {
                    item.iCantidad = obj.iCantidad ? parseInt(obj.iCantidad) : 0;
                    item.fImporte = obj.fImporte ? parseFloat(obj.fImporte) : 0;
                    if(obj.iCorrelativo){
                             RMPTotalCantidad = RMPTotalCantidad + item.iCantidad;
                            if(aICorrelativosMedioPagoNegativo.includes(obj.iCorrelativo)){
                                RMPTotalImporte = (parseFloat(RMPTotalImporte) - parseFloat(item.fImporte)).toFixed(2);
                            }else{
                                RMPTotalImporte = (parseFloat(RMPTotalImporte) + parseFloat(item.fImporte)).toFixed(2);    
                            }
                       
                        
                    }
                }
            });
            
             
            modelResumen.setProperty("/aResumenTipoDocumento",listaTipoDocumento);
            modelResumen.setProperty("/aResumenMedioPago",listaMedioPago);

            modelResumen.setProperty("/RTDTotalCantidad",RTDTotalCantidad);
            modelResumen.setProperty("/RTDTotalImporte",parseFloat(RTDTotalImporte)); //RTDTotalImporte);
            modelResumen.setProperty("/RMPTotalCantidad",RMPTotalCantidad);
            modelResumen.setProperty("/RMPTotalImporte",parseFloat(RMPTotalImporte));//RMPTotalImporte);

            modelResumen.setProperty("/ResumenTotalAjusteRedondeo",parseFloat(parseFloat(oArqueo.fAjusteRedondeo).toFixed(2)));
            modelResumen.setProperty("/ResumenTotalSobrante",parseFloat(parseFloat(oArqueo.fSobrante).toFixed(2)));
            modelResumen.setProperty("/ResumenTotalFaltante",parseFloat(parseFloat(oArqueo.fFaltante).toFixed(2)));
            modelResumen.setProperty("/ResumenReposicionFondo",parseFloat(parseFloat(oArqueo.fReposicionFondo).toFixed(2)));

            modelED.setProperty("/EDTotalCantidad", EDTotalCantidad);
            modelED.setProperty("/EDTotalImporte", EDTotalImporte);
            modelLocal.setProperty("/oArqueoDenominacion",aDenominacion);
            
            modelLocal.setProperty("/oArqueoResumen",aDenominacionResumen);
            modelLocal.setProperty("/oArqueo",oArqueo);
            
            //INICIO ROY 05-07-2021
            var flagReporteArqueoEstado = self.getView().getModel("localModel").getProperty("/flagReporteArqueoEstado");
             
            if( flagReporteArqueoEstado && !(flagReporteArqueoEstado==1 || flagReporteArqueoEstado==2 || flagReporteArqueoEstado==3 || flagReporteArqueoEstado==8) ){//Aperturado //Reaperturado //Rechazado
                self.getView().getModel("C").setProperty("/aCustodios", data.aArqueoCustodio);//ROY 05-07-2021
                self.getView().getModel("T").setProperty("/aTraslados", data.aArqueoTraslado);//ROY 05-07-2021
            }
            //END ROY 05-07-2021
        },
        consultarArqueoDocumentos : function(data){
              var modData = []
                if(data){
                     data.forEach(function(item){
                        var sNroDocPartido;
                        var sSerie; 
                        var sCorrelativo
                            if(item.sNumeroSunat){
                                 sNroDocPartido  =  item.sNumeroSunat.split("-");
                            }
                            if(sNroDocPartido){
                                     sCorrelativo  = sNroDocPartido[0] ? sNroDocPartido[0] : "";
                                sSerie  = sNroDocPartido[1] ? sNroDocPartido[1] : "";
                            }else{
                                    sCorrelativo = "";
                                   sSerie  = "";
                            }  
                            item["sSerie"] =   sSerie;
                            item["sCorrelativo"] =  sCorrelativo
                     })  
                     modData = data; 
                }

                return modData;



        },
        mapConsultarArqueoUsuarioFotografia: function(self, data) {
                
            var that = this;
            var oArqueo = data.oArqueo;
            var aDenominacion = data.oArqueoDenominacion;
            var aDenominacionResumen = data.oArqueoResumen;
            var modelES = self.getView().getModel("ES");
            var modelED = self.getView().getModel("ED");
            var modelD = self.getView().getModel("D");
            var modelLocal = self.getView().getModel("localModel");
            var modelLocal = self.getView().getModel("localModel");
            var modelResumen = self.getView().getModel("Resumen");
            var ES = modelES.getProperty("/aEfectivoSoles");
            var ED = modelED.getProperty("/aEfectivoDolares");
            var flagReporteArqueoEstado = self.getView().getModel("localModel").getProperty("/flagReporteArqueoEstado");
                if(flagReporteArqueoEstado==4){
                        modelLocal.setProperty("/flagSoloLectura",true);
                        self.getView().getModel("miData").setProperty("/oHorarioCaja/iEstadoHorario",flagReporteArqueoEstado);// Sorpresivo 4
                        self.getView().getModel("miData").setProperty("/oHorarioCaja/sDesEstadoCaja","Sorpresivo");// Sorpresivo 4
                }
            modelD.setProperty("/TotalEfectivoPEN", aDenominacion.length>0 ? parseFloat(parseFloat(aDenominacion[0].fTotalEfectivoPEN).toFixed(2)):0);
            modelD.setProperty("/TotalEfectivoUSD", aDenominacion.length>0 ? parseFloat(parseFloat(aDenominacion[0].fTotalEfectivoUSD).toFixed(2)):0);
            var ESTotalCantidad = 0;
            var ESTotalImporte = 0;
            ES.forEach(function(item, x) {
                var obj = aDenominacion.find(function(el) {
                    return el.sCodDenominacion == item.sCodDenominacion;
                });
                if (obj) {
                    item.iCantidad = obj.iCantidad ? parseInt(obj.iCantidad) : 0;
                    item.fImporte = obj.fImporte ? parseFloat(obj.fImporte).toFixed(2) : 0;
                    modelES.setProperty("/aEfectivoSoles/" + x + "/iId", obj.iId);
                    modelES.setProperty("/aEfectivoSoles/" + x + "/iCantidad", item.iCantidad);
                    modelES.setProperty("/aEfectivoSoles/" + x + "/fImporte", item.fImporte);
                }
                ESTotalCantidad = ESTotalCantidad + item.iCantidad;
                ESTotalImporte = (parseFloat(ESTotalImporte) + parseFloat(item.fImporte)).toFixed(2);
            });
            modelES.setProperty("/ESTotalCantidad", ESTotalCantidad);
            modelES.setProperty("/ESTotalImporte", ESTotalImporte);
            var EDTotalCantidad = 0;
            var EDTotalImporte = 0;
            ED.forEach(function(item, x) {
                var obj = aDenominacion.find(function(el) {
                    return el.sCodDenominacion == item.sCodDenominacion;
                });
                if (obj) {
                    item.iCantidad = obj.iCantidad ? parseInt(obj.iCantidad) : 0;
                    item.fImporte = obj.fImporte ? parseFloat(obj.fImporte).toFixed(2) : 0;
                    modelED.setProperty("/aEfectivoDolares/" + x + "/iId", obj.iId);
                    modelED.setProperty("/aEfectivoDolares/" + x + "/iCantidad", item.iCantidad);
                    modelED.setProperty("/aEfectivoDolares/" + x + "/fImporte", item.fImporte);
                }
                EDTotalCantidad = EDTotalCantidad + item.iCantidad;
                EDTotalImporte = (parseFloat(EDTotalImporte) + parseFloat(item.fImporte)).toFixed(2);
            });
            
            modelED.setProperty("/EDTotalCantidad", EDTotalCantidad);
            modelED.setProperty("/EDTotalImporte", EDTotalImporte);
            modelLocal.setProperty("/oArqueoDenominacion",aDenominacion);
            
            modelLocal.setProperty("/oArqueoResumen",aDenominacionResumen);
            modelLocal.setProperty("/oArqueo",oArqueo);
 
            var documentos = self.recibirDatos.consultarArqueoDocumentos(data.aArqueoDocumento) ;

            self.getView().getModel("C").setProperty("/aCustodios", data.aArqueoCustodio);
            self.getView().getModel("D").setProperty("/aDocumentos", documentos);

            var FFSTotalCantidad = 0;
            var FFSTotalImporte = 0;
            var aFondoFijoSoles = self.getView().getModel("FFS").getProperty("/aFondoFijoSoles");
            aFondoFijoSoles.forEach(function(item, x) {
                var obj = data.aArqueoFFS.find(function(el) {
                    return item.sCodDenominacion && el.sCodDenominacion == item.sCodDenominacion;
                });
                if (obj) {
                    item.iCantidad = obj.iCantidad ? parseInt(obj.iCantidad) : 0;
                    item.fImporte = obj.fImporte ? parseFloat(obj.fImporte).toFixed(2) : 0;
                    //if(obj.iCorrelativo){
                        FFSTotalCantidad = FFSTotalCantidad + item.iCantidad;
                        FFSTotalImporte = (parseFloat(FFSTotalImporte) + parseFloat(item.fImporte)).toFixed(2);
                    //}
                }
            });
            self.getView().getModel("FFS").setProperty("/aFondoFijoSoles", aFondoFijoSoles);
            self.getView().getModel("FFS").setProperty("/FFSTotalCantidad", FFSTotalCantidad);
            self.getView().getModel("FFS").setProperty("/FFSTotalImporte", FFSTotalImporte);
            

            self.getView().getModel("FFS").setProperty("/sNumeroFondoAsignado",data.aArqueoFFS.length>0 ? data.aArqueoFFS[0].sNumeroFondoAsignado : "")
            self.getView().getModel("T").setProperty("/aTraslados", data.aArqueoTraslado);

            self.getView().getModel("D").setProperty("/visibleBoletaElectronica", false);
            self.getView().getModel("D").setProperty("/visibleFacturaElectronica", false);
            self.getView().getModel("D").setProperty("/visibleBoletaManual", false);
            self.getView().getModel("D").setProperty("/visibleFacturaManual", false);
            self.getView().getModel("D").setProperty("/visibleBoletaCredito", false);
            self.getView().getModel("D").setProperty("/visibleFacturaCredito", false);
            self.getView().getModel("D").setProperty("/visibleNotaCreditoEmitida", false);
            self.getView().getModel("D").setProperty("/visibleBoletaOtraSede", false);
            self.getView().getModel("D").setProperty("/visibleAnulado", false);
            self.getView().getModel("D").setProperty("/visibleFacturaOtraSede", false);
            self.getView().getModel("D").setProperty("/visibleDevolucionTotalEfectivo", false);
            self.getView().getModel("D").setProperty("/visibleNotaCreditoEmitidaPagaNo", false);
            self.getView().getModel("D").setProperty("/visibleComprobantesOtrasSedes", false);
            self.getView().getModel("D").setProperty("/visibleNotaCreditoAplicada", false);
            self.getView().getModel("D").setProperty("/visibleOtros", false);
            self.getView().getModel("D").setProperty("/visibleOtrosComprobantes", false);
            documentos.forEach(function(item){
                item.sNroDoc = item.sNumeroSunat;
            });


            var modDoc = that.distribuirDocumentos(self,documentos);//Nuevo Método
            var cantidadBoletas = 0;
            var importeBoletas = 0;
            var cantidadFacturasTabDoc = 0;
            var importeFacturasTabDoc = 0;
            var cantidadBoletManual = 0;
            var importeBoletManual = 0;
            var cantidadFactManual = 0;
            var importeFactManual = 0;
            var cantidadBoletaCreditoTabDoc = 0;
            var importeBoletaCreditoTabDoc = 0;
            var cantidadFacturaCreditoTabDoc = 0;
            var importeFacturaCreditoTabDoc = 0;
            var cantidadAnuladoTabDoc = 0;
            var importeAnuladoTabDoc = 0;
            var cantidadNCAplicada = 0;
            var importeNCAplicada = 0;
            var cantidadNCPagaNoTabDoc = 0;
            var importeNCPagaNoTabDoc = 0;
            var cantidadComprobantesOtrasSedesTabDoc = 0;
            var importeComprobantesOtrasSedesTabDoc = 0;
            var totalCantidadOtros = 0;
            var totalImporteOtros = 0;
            
            self.getView().getModel("D").setProperty("/aDocumentosBoletaElectronica", modDoc.aDocumentosBoletaElectronica);
            modDoc.aDocumentosBoletaElectronica.forEach(function(item){
                cantidadBoletas=cantidadBoletas+item.iCantidad;
                importeBoletas=importeBoletas+parseFloat(item.fImporte);
            });
            self.getView().getModel("D").setProperty("/TotalCantidadBoletaElectronica", cantidadBoletas);
            self.getView().getModel("D").setProperty("/TotalImporteBoletaElectronica",parseFloat(importeBoletas)), //parseFloat(importeBoletas).toFixed(2));
            self.getView().getModel("D").setProperty("/visibleBoletaElectronica", modDoc.aDocumentosBoletaElectronica.length>0 ? true : false);
            

            self.getView().getModel("D").setProperty("/aDocumentosFacturaElectronica", modDoc.aDocumentosFacturaElectronica);
            modDoc.aDocumentosFacturaElectronica.forEach(function(item){
                cantidadFacturasTabDoc=cantidadFacturasTabDoc+item.iCantidad;
                importeFacturasTabDoc=importeFacturasTabDoc+parseFloat(item.fImporte);
            });
            self.getView().getModel("D").setProperty("/TotalCantidadFacturaElectronica", cantidadFacturasTabDoc);
            self.getView().getModel("D").setProperty("/TotalImporteFacturaElectronica",parseFloat(importeFacturasTabDoc)), //parseFloat(importeFacturas).toFixed(2));
            self.getView().getModel("D").setProperty("/visibleFacturaElectronica", modDoc.aDocumentosFacturaElectronica.length>0 ? true : false);
            
            self.getView().getModel("D").setProperty("/aDocumentosBoletaManual", modDoc.aDocumentosBoletaManual);
            modDoc.aDocumentosBoletaManual.forEach(function(item){
                cantidadBoletManual=cantidadBoletManual+item.iCantidad;
                importeBoletManual=importeBoletManual+parseFloat(item.fImporte);
            });
            self.getView().getModel("D").setProperty("/TotalCantidadBoletaManual", cantidadBoletManual);
            self.getView().getModel("D").setProperty("/TotalImporteBoletaManual", parseFloat(importeBoletManual)), //parseFloat(importeBoletManual).toFixed(2));
            self.getView().getModel("D").setProperty("/visibleBoletaManual", modDoc.aDocumentosBoletaManual.length>0 ? true : false);
            
            self.getView().getModel("D").setProperty("/aDocumentosFacturaManual", modDoc.aDocumentosFacturaManual);
            modDoc.aDocumentosFacturaManual.forEach(function(item){
                cantidadFactManual=cantidadFactManual+item.iCantidad;
                importeFactManual=importeFactManual+parseFloat(item.fImporte);
            });
            self.getView().getModel("D").setProperty("/TotalCantidadFacturaManual", cantidadFactManual);
            self.getView().getModel("D").setProperty("/TotalImporteFacturaManual",  parseFloat(importeFactManual)), //parseFloat(importeFactManual).toFixed(2));
            self.getView().getModel("D").setProperty("/visibleFacturaManual", modDoc.aDocumentosFacturaManual.length>0 ? true : false);

            self.getView().getModel("D").setProperty("/aDocumentosBoletaCredito", modDoc.aDocumentosBoletaCredito);
            modDoc.aDocumentosBoletaCredito.forEach(function(item){
                cantidadBoletaCreditoTabDoc=cantidadBoletaCreditoTabDoc+item.iCantidad;
                importeBoletaCreditoTabDoc=importeBoletaCreditoTabDoc+parseFloat(item.fImporte);
            });
            self.getView().getModel("D").setProperty("/TotalCantidadBoletaCredito", cantidadBoletaCreditoTabDoc);//cantidadBoletaCredito);
            self.getView().getModel("D").setProperty("/TotalImporteBoletaCredito", parseInt(importeBoletaCreditoTabDoc));//parseFloat(importeBoletaCredito)),//parseFloat(importeBoletaCredito).toFixed(2));
            self.getView().getModel("D").setProperty("/visibleBoletaCredito", modDoc.aDocumentosBoletaCredito.length>0 ? true : false);

            self.getView().getModel("D").setProperty("/aDocumentosFacturaCredito", modDoc.aDocumentosFacturaCredito);
            modDoc.aDocumentosFacturaCredito.forEach(function(item){
                cantidadFacturaCreditoTabDoc=cantidadFacturaCreditoTabDoc+item.iCantidad;
                importeFacturaCreditoTabDoc=importeFacturaCreditoTabDoc+parseFloat(item.fImporte);
            });
            self.getView().getModel("D").setProperty("/TotalCantidadFacturaCredito", cantidadFacturaCreditoTabDoc);//cantidadFacturaCredito);
            self.getView().getModel("D").setProperty("/TotalImporteFacturaCredito",parseFloat(importeFacturaCreditoTabDoc))//parseFloat(importeFacturaCredito)), // parseFloat(importeFacturaCredito).toFixed(2));
            self.getView().getModel("D").setProperty("/visibleFacturaCredito", modDoc.aDocumentosFacturaCredito.length>0 ? true : false);

            self.getView().getModel("D").setProperty("/aDocumentosAnulado", modDoc.aDocumentosAnulado);
            modDoc.aDocumentosAnulado.forEach(function(item){
                cantidadAnuladoTabDoc=cantidadAnuladoTabDoc+item.iCantidad;
                importeAnuladoTabDoc=importeAnuladoTabDoc+parseFloat(item.fImporte);
            });
            self.getView().getModel("D").setProperty("/TotalCantidadAnulado", cantidadAnuladoTabDoc)//cantidadAnulado);
            self.getView().getModel("D").setProperty("/TotalImporteAnulado", parseFloat(importeAnuladoTabDoc))//parseFloat(importeAnulado)), //parseFloat(importeAnulado).toFixed(2));
            self.getView().getModel("D").setProperty("/visibleAnulado", modDoc.aDocumentosAnulado.length>0 ? true : false);

            self.getView().getModel("D").setProperty("/aDocumentosNotaCreditoAplicada", modDoc.aDocumentosNotaCreditoAplicada);
            modDoc.aDocumentosNotaCreditoAplicada.forEach(function(item){
                cantidadNCAplicada=cantidadNCAplicada+item.iCantidad;
                importeNCAplicada=importeNCAplicada+parseFloat(item.fImporte);
            });
            self.getView().getModel("D").setProperty("/TotalCantidadNotaCreditoAplicada", cantidadNCAplicada);
            self.getView().getModel("D").setProperty("/TotalImporteNotaCreditoAplicada", parseFloat(importeNCAplicada)), //parseFloat(importeNCAplicada).toFixed(2));
            self.getView().getModel("D").setProperty("/visibleNotaCreditoAplicada", modDoc.aDocumentosNotaCreditoAplicada.length>0 ? true : false);

            self.getView().getModel("D").setProperty("/aDocumentosNotaCreditoEmitidaPagaNo", modDoc.aDocumentosNotaCreditoEmitidaPagaNo);
            modDoc.aDocumentosNotaCreditoEmitidaPagaNo.forEach(function(item){
                cantidadNCPagaNoTabDoc=cantidadNCPagaNoTabDoc+item.iCantidad;
                importeNCPagaNoTabDoc=importeNCPagaNoTabDoc+parseFloat(item.fImporte);
            });
            self.getView().getModel("D").setProperty("/TotalCantidadNotaCreditoEmitidaPagaNo", cantidadNCPagaNoTabDoc);
            self.getView().getModel("D").setProperty("/TotalImporteNotaCreditoEmitidaPagaNo", parseFloat(importeNCPagaNoTabDoc)), // parseFloat(importeNCPagaNoTabDoc).toFixed(2));
            self.getView().getModel("D").setProperty("/visibleNotaCreditoEmitidaPagaNo", modDoc.aDocumentosNotaCreditoEmitidaPagaNo.length>0 ? true : false);

            self.getView().getModel("D").setProperty("/aDocumentosComprobantesOtrasSedes", modDoc.aDocumentosComprobantesOtrasSedes);
            modDoc.aDocumentosComprobantesOtrasSedes.forEach(function(item){
                cantidadComprobantesOtrasSedesTabDoc=cantidadComprobantesOtrasSedesTabDoc+item.iCantidad;
                importeComprobantesOtrasSedesTabDoc=importeComprobantesOtrasSedesTabDoc+parseFloat(item.fImporte);
            });
            self.getView().getModel("D").setProperty("/TotalCantidadComprobantesOtrasSedes", cantidadComprobantesOtrasSedesTabDoc)//cantidadComprobantesOtrasSedes);
            self.getView().getModel("D").setProperty("/TotalImporteComprobantesOtrasSedes", parseFloat(importeComprobantesOtrasSedesTabDoc)) //parseFloat(importeComprobantesOtrasSedes)), //parseFloat(importeComprobantesOtrasSedes).toFixed(2));
            self.getView().getModel("D").setProperty("/visibleComprobantesOtrasSedes", modDoc.aDocumentosComprobantesOtrasSedes.length>0 ? true : false);
            ///GNM++++
            //self.utilController.orderByAscMejoraxMultipleCondiciones(modDoc.aDocumentosComprobantesOtrasSedes,"sTipoDocumento","sCorrelativo","sSerie");
            ///GNM+++

            
            self.getView().getModel("D").setProperty("/aDocumentosOtrosComprobantes", modDoc.aDocumentosOtrosComprobantes);
            modDoc.aDocumentosOtrosComprobantes.forEach(function(item){
                totalCantidadOtros=totalCantidadOtros+item.iCantidad;
                totalImporteOtros=totalImporteOtros+parseFloat(item.fImporte);
            });
            self.getView().getModel("D").setProperty("/TotalCantidadOtrosComprobantes", totalCantidadOtros);
            self.getView().getModel("D").setProperty("/TotalImporteOtrosComprobantes", parseFloat(totalImporteOtros)), //parseFloat(totalImporteOtros).toFixed(2));
            self.getView().getModel("D").setProperty("/visibleOtrosComprobantes", modDoc.aDocumentosOtrosComprobantes.length>0 ? true : false);
            ///GNM++++
            //self.utilController.orderByAscMejoraxMultipleCondiciones(modDoc.aDocumentosOtrosComprobantes,"sTipoDocumento","sCorrelativo","sSerie");
            ///GNM+++
            


            
            var listaTipoDocumento = that.aListaTipoDocumento();
            var RTDTotalCantidad = 0;
            var RTDTotalImporte = 0;
            var aICorrelativosTipoDocumentoNegativo = [5,14];
            listaTipoDocumento.forEach(function(item, x) {
                var obj = aDenominacionResumen.find(function(el) {
                    return item.sCodDenominacion && el.sCodDenominacionDoc == item.sCodDenominacion;
                });              
                if (obj) {
                        console.log(obj.fImporte)
                    item.iCantidad = obj.iCantidad ? parseInt(obj.iCantidad) : 0;
                    item.fImporte = obj.fImporte ? parseFloat(obj.fImporte) : 0;
                    if(obj.iCorrelativo){
                           
                        RTDTotalCantidad = RTDTotalCantidad + item.iCantidad;
                       

                            if(aICorrelativosTipoDocumentoNegativo.includes(obj.iCorrelativo)){
                                RTDTotalImporte = (parseFloat(RTDTotalImporte) - parseFloat(item.fImporte)).toFixed(2);
                            }else{
                                 RTDTotalImporte = (parseFloat(RTDTotalImporte) + parseFloat(item.fImporte)).toFixed(2);     
                            }
                        
                    }
                }
            });
            var listaMedioPago = that.aListaMedioPago();
            var RMPTotalCantidad = 0;
            var RMPTotalImporte = 0;
            var aICorrelativosMedioPagoNegativo = [29,30,31,32,33];
            listaMedioPago.forEach(function(item, x) {
                var obj = aDenominacionResumen.find(function(el) {
                    return item.sCodDenominacion && el.sCodDenominacionDoc == item.sCodDenominacion;
                });
                if (obj) {
                    item.iCantidad = obj.iCantidad ? parseInt(obj.iCantidad) : 0;
                    item.fImporte = obj.fImporte ? parseFloat(obj.fImporte) : 0;
                    if(obj.iCorrelativo){
                             RMPTotalCantidad = RMPTotalCantidad + item.iCantidad;
                            if(aICorrelativosMedioPagoNegativo.includes(obj.iCorrelativo)){
                                RMPTotalImporte = (parseFloat(RMPTotalImporte) - parseFloat(item.fImporte)).toFixed(2);
                            }else{
                                RMPTotalImporte = (parseFloat(RMPTotalImporte) + parseFloat(item.fImporte)).toFixed(2);    
                            }
                       
                        
                    }
                }
            });
            
             
            modelResumen.setProperty("/aResumenTipoDocumento",listaTipoDocumento);
            modelResumen.setProperty("/aResumenMedioPago",listaMedioPago);

            modelResumen.setProperty("/RTDTotalCantidad",RTDTotalCantidad);
            modelResumen.setProperty("/RTDTotalImporte",parseFloat(RTDTotalImporte)); //RTDTotalImporte);
            modelResumen.setProperty("/RMPTotalCantidad",RMPTotalCantidad);
            modelResumen.setProperty("/RMPTotalImporte",parseFloat(RMPTotalImporte));//RMPTotalImporte);

            modelResumen.setProperty("/ResumenTotalAjusteRedondeo",parseFloat(parseFloat(oArqueo.fAjusteRedondeo).toFixed(2)));
            modelResumen.setProperty("/ResumenTotalSobrante",parseFloat(parseFloat(oArqueo.fSobrante).toFixed(2)));
            modelResumen.setProperty("/ResumenTotalFaltante",parseFloat(parseFloat(oArqueo.fFaltante).toFixed(2)));
            modelResumen.setProperty("/ResumenReposicionFondo",parseFloat(parseFloat(oArqueo.fReposicionFondo).toFixed(2)));
        },
        consultarBovedaCartuchera: function(self, data) {
            
            var tabla = "/aFondoFijoSoles";
            var aDenominacion = data.aCartucheraDetalle.length ? data.aCartucheraDetalle : [];
            var modelFFS = self.getView().getModel("FFS");
            var modelMiData = self.getView().getModel("miData");
            var FFS = modelFFS.getProperty(tabla);
            var FFSTotalCantidad = 0;
            var FFSTotalImporte = 0;
            ///INICIO CABECERA
            modelFFS.setProperty("/iId", data.oCartuchera.iId);
            modelFFS.setProperty("/fImporteMonedaNac", data.oCartuchera.fImporteMonedaNac);
            modelFFS.setProperty("/iCodApertura", data.oCartuchera.iCodApertura);
            modelFFS.setProperty("/sDesApertura", data.oCartuchera.sDesApertura);
            //GNM+++
            modelFFS.setProperty("/sNumeroFisico", data.oCartuchera.sNumeroFisico);
            //GNM+++
            modelFFS.setProperty("/sNumeroFondoAsignado", data.oCartuchera.sCodCartuchera);
            modelFFS.setProperty("/fImporteMonedaNacCartuchera", data.oCartuchera.fImporteMonedaNacCartuchera);
            modelFFS.setProperty("/fImporteMonedaNacArqueo", data.oCartuchera.fImporteMonedaNacArqueo);
           //modelFFS.setProperty("/fImporteMonedaNacReposicion", data.oCartuchera.fImporteMonedaNacReposicion);
           
            modelFFS.setProperty("/fImporteMonedaNacReposicion", data.oCartuchera.iCodEstadoCartuchera ==3 ? 0:data.oCartuchera.fImporteMonedaNacReposicion);
            modelFFS.setProperty("/iCantidadReposicion", data.oCartuchera.iCantidadReposicion);
            modelFFS.setProperty("/iCantidadArqueoCartuchera", data.oCartuchera.iCantidadArqueoCartuchera);
            modelFFS.setProperty("/iCodEstadoCartuchera", data.oCartuchera.iCodEstadoCartuchera);
            modelFFS.setProperty("/sMotivoRechazo", data.oCartuchera.sMotivoRechazo);
            modelFFS.setProperty("/visibleMotivoRechazo", data.oCartuchera.iCodEstadoCartuchera ==3 ? true:false);
            modelFFS.setProperty("/colorMotivoRechazo", data.oCartuchera.iCodEstadoCartuchera ==3 ? "rojo":"negro");

            if ((data.oCartuchera.sCodTipoMov == 1 || data.oCartuchera.sCodTipoMov == 2) && (data.oCartuchera.iCodEstadoCartuchera == 0 || data.oCartuchera.iCodEstadoCartuchera == 2)){
                modelMiData.setProperty("/footerFondoFijoSoles", false);
            }else{
                modelMiData.setProperty("/footerFondoFijoSoles", true);
            }
            
            ///END CABECERA
            ///INICIO DETALLE
            var editable = data.oCartuchera.iCodEstadoCartuchera === 1 || data.oCartuchera.iCodEstadoCartuchera === 3 ? true : false;
            FFS.forEach(function(item, x) {
                var obj = aDenominacion.find(function(el) {
                    return parseFloat(el.fValor) == parseFloat(item.fValor); //el.sCodDenominacion == item.sCodDenominacion;
                });
                if (obj) {
                    item.iCantidad = obj.iCantidadCartuchera ? parseInt(obj.iCantidadCartuchera) : 0;
                    item.fImporte = obj.fImporteCartuchera ? parseFloat(obj.fImporteCartuchera).toFixed(2) : 0;
                    modelFFS.setProperty(tabla + "/" + x + "/iId", item.iId);
                    modelFFS.setProperty(tabla + "/" + x + "/iCantidad", item.iCantidad);
                    modelFFS.setProperty(tabla + "/" + x + "/fImporte", item.fImporte);
                    modelFFS.setProperty(tabla + "/" + x + "/editable", editable);
                }
                FFSTotalCantidad = FFSTotalCantidad + item.iCantidad;
                FFSTotalImporte = (parseFloat(FFSTotalImporte) + parseFloat(item.fImporte)).toFixed(2);
            });
            modelFFS.setProperty("/FFSTotalCantidad", FFSTotalCantidad);
            modelFFS.setProperty("/FFSTotalImporte", FFSTotalImporte);
            ///END DETALLE
            modelFFS.refresh(true);
        },
        consultarConceptos: function(data) {
            var modData = [];
            modData.push({ "iId": null, "sDescripcion": "Seleccione Concepto" });
            data.forEach(function(item) {
                var obj = {
                    "iId": item.iId,
                    "sCodigoEstado": item.sCodigoEstado,
                    "sDescripcion": item.sCampo1,
                    "sCampo2": item.sCampo2
                };
                modData.push(obj);
            });
            return modData;
        },
        consultarTipoTarjeta: function(data) {
            var modData = [];
            modData.push({ "iId": null, "sDescripcion": "Seleccione Tipo Tarjeta" });
            data.forEach(function(item) {
                var obj = {
                    "iId": item.iId,
                    "sCodigoEstado": item.sCodigoEstado,
                    "sDescripcion": item.sCampo1
                };
                modData.push(obj);
            });
            return modData;
        },
        consultarRoles: function(data) {
            var modData = [];
            data.forEach(function(item) {
                var obj = {
                    "iId": item.iId,
                    "sCodigo": item.sCodigoMaestro,
                    "sCodigoEstatico": item.sCampo3,
                    "sDescripcion": item.sCampo1
                };
                modData.push(obj);
            });
            return modData;
        },
        consultarCaja: function(data,ctx) {
            var modData = [];
            var self = ctx;
            
            if(data){
                data.forEach(function(item) {
                    var obj = {
                        iId: item.iId,
                        dFechaApertura: item.dFechaApertura ? new Date(item.dFechaApertura.replace("Z","")) : null,
                        iEstadoHorario: item.iEstadoHorario,
                        iIdArea: item.iIdArea,
                        iIdAsignacion: item.iIdAsignacion,
                        iIdSede: item.iIdSede,
                        iIdSociedad: item.iIdSociedad,
                        iIdUsuarioCajero: item.iIdUsuarioCajero,
                        iSemana: item.iSemana,
                        sArea: item.sArea,
                        sDesEstadoCaja: item.sDesEstadoCaja,
                        //sFechaApertura: item.dFechaApertura ? ( new Date(item.dFechaApertura.replace("Z","")) ).toLocaleDateString() : null,
                        sFechaApertura: item.dFechaApertura ? ( this.utilController.formatFechaDDMMAAAA(new Date(item.dFechaApertura.replace("Z",""))))  : null,
                        sFechaAperturaAAAA_MM_DD: item.dFechaApertura ? item.dFechaApertura.split("T")[0]  : null,
                        sHoraFin: item.sHoraFin,
                        sHoraInicio: item.sHoraInicio,
                        sPeriodo: item.sPeriodo,
                        sSede: item.sSede,
                        sSociedad: item.sSociedad,
                        sUsuarioCajero: item.sUsuarioCajero,
                        iIdUsuarioSupervisor : item.iIdUsuarioSupervisor,
                        //sNombreSupervisor: item.sNombreSupervisor,
                        sUsuarioSupervisorEstadoAperturado: item.sUsuarioSupervisorEstadoAperturado,
                        sDesMotivoRechazo: item.sDesMotivoRechazo,
                        flagOrderBy: new Date(item.sHoraInicio.replace("Z","")).getTime()//(item.dFechaApertura ? new Date(item.dFechaApertura.replace("Z","")) : 0) + (item.sHoraFin ? new Date(item.sHoraFin.replace("Z","")) : 0)
                    };
                    modData.push(obj);
                }.bind(self));
            }
            
            modData = self.utilController.orderByNew(modData,"flagOrderBy","DESC");
            return modData;
        },
        ////INICIO MAESTROS HARDCODE
        consultarResumenTipoDocumento: function(self,noCalcular) {
            var that = this;
            var modData = [];
            ////INICIO VALIDACIONES/////////////////////////////////////////////////////////////////////////////////
            var horarioCaja = self.getView().getModel("miData").getProperty("/oHorarioCaja");
            var modelResumen = self.getView().getModel("Resumen");
            var data = self.getView().getModel("FFS").getData();
            var iCantidadArqueoCartuchera = parseFloat(data.fImporteMonedaNacCartuchera)>0 ? 1 : 0; 
            var fImporteMonedaNacArqueo = data.fImporteMonedaNacCartuchera ? data.fImporteMonedaNacCartuchera : 0; 
            var iCantidadReposicion = parseFloat(data.fImporteMonedaNacReposicion)>0 ? 1 : 0;
            var fImporteMonedaNacReposicion = data.fImporteMonedaNacReposicion ? data.fImporteMonedaNacReposicion : 0; 
            var D = self.getView().getModel("D").getData();
            var documentos = D.aDocumentos;
            var pagos = D.aPagos;
            var cantidadFacturas = 0;
            var importeFacturas = 0;
            var cantidadBoletas = 0;
            var importeBoletas = 0;
            var cantidadFacturasPagaNo = 0;
            var importeFacturasPagaNo = 0;
            var cantidadBoletasPagaNo = 0;
            var importeBoletasPagaNo = 0;
            var cantidadNC = 0;
            var importeNC = 0;
            var cantidadNCPagaNo=0;
            var importeNCPagaNo=0;
            var cantidadNCPagaNoTabDoc=0;
            var importeNCPagaNoTabDoc=0;
            var cantidadFactOtrasSedes = 0;
            var importeFactOtrasSedes = 0;
            var cantidadFactOtrasSedesPagaNo = 0;
            var importeFactOtrasSedesPagaNo = 0;
            var cantidadBoletOtrasSedesPagaNo = 0;
            var importeBoletOtrasSedesPagaNo = 0;
            var cantidadBoletOtrasSedes = 0;
            var importeBoletOtrasSedes = 0;
            var cantidadFactManual = 0;
            var importeFactManual=0;
            var cantidadBoletManual=0;
            var importeBoletManual=0;
            var cantidadBoletManualPagaNo=0;
            var importeBoletManualPagaNo=0;
            var cantidadFactManualPagaNo=0;
            var importeFactManualPagaNo=0;
            var cantidadDevTotalEfectivo=0;
            var importeDevTotalEfectivo=0;
            var cantidadDevParcialEfectivo=0;
            var importeDevParcialEfectivo=0;
            var cantidadNCAplicada=0;
            var importeNCAplicada=0;
            var totalAjusteRedonde=0;
            //
            var aDocumentosBoletaCredito = [] ; 
            var cantidadBoletaCredito = 0 ; 
            var importeBoletaCredito = 0 ; 
            var aDocumentosFacturaCredito = [] ; 
            var cantidadFacturaCredito = 0 ; 
            var importeFacturaCredito = 0 ; 
            var aDocumentosAnulado = [] ;
            var cantidadAnulado = 0;
            var importeAnulado = 0;
            var cantidadComprobantesOtrasSedes=0;
            var importeComprobantesOtrasSedes=0;
            ///INICIO DOCUMENTOS
            if(documentos.length>0){
                
                documentos.forEach(function(doc){
                    var pago = D.aPagos.find(function(el){
                        return el.iIdDocumento == doc.iIdDocumento;
                    });

                    totalAjusteRedonde = totalAjusteRedonde + parseFloat(doc.fAjustadoRedondeoPEN) + parseFloat(doc.fImporteRedondeoVuelto);
                    

                    if( doc.sDesSedeEmisor==horarioCaja.sSede ){
                        if(doc.sCodTipoDocumento==1 && doc.iFactManual!==1 && !(doc.sDesEstadoDoc == "Anulado" || doc.sDesEstadoDoc == "Baja" || doc.sDesEstadoDoc == "Al crédito")){//facturas electrónicas
                            cantidadFacturas = cantidadFacturas + 1;
                            importeFacturas = importeFacturas + parseFloat(doc.fImporte);
                            doc.flagDocumento = "FacturaElectronica";
                        }

                        if(doc.sCodTipoDocumento==3 && doc.iFactManual!==1 && !(doc.sDesEstadoDoc=="Anulado" || doc.sDesEstadoDoc == "Baja" || doc.sDesEstadoDoc == "Al crédito")){//boletas electrónicas
                            cantidadBoletas = cantidadBoletas + 1;
                            importeBoletas = importeBoletas + parseFloat(doc.fImporte);
                            doc.flagDocumento = "BoletaElectronica";
                        }

                        if(doc.sCodTipoDocumento==1 && doc.iFactManual!==1 && doc.sDesEstadoDoc == "Al crédito"){//facturas electrónicas PAGA NO
                            cantidadFacturasPagaNo = cantidadFacturasPagaNo + 1;
                            importeFacturasPagaNo = importeFacturasPagaNo + parseFloat(doc.fImporte);
                            doc.flagDocumento = "FacturaElectronicaPagaNo";
                        }

                        if(doc.sCodTipoDocumento==3 && doc.iFactManual!==1 && doc.sDesEstadoDoc == "Al crédito"){//boletas electrónicas PAGA NO
                            cantidadBoletasPagaNo = cantidadBoletasPagaNo + 1;
                            importeBoletasPagaNo = importeBoletasPagaNo + parseFloat(doc.fImporte);
                            doc.flagDocumento = "BoletaElectronicaPagaNo";
                        }
                       
                    }
                    //if(doc.sCodTipoDocumento==1 && doc.iFactManual==1 && (doc.sDesEstadoDoc!=="Al crédito")){//Factura Manual
                    //if(doc.sCodTipoDocumento==1 && doc.iFactManual==1 && doc.sDesSedeEmisor==horarioCaja.sSede && (doc.sDesEstadoDoc!=="Al crédito")){//Factura Manual
                    if(doc.sCodTipoDocumento==1 && doc.iFactManual==1 && doc.sDesSedeEmisor==horarioCaja.sSede && !(doc.sDesEstadoDoc == "Anulado" || doc.sDesEstadoDoc == "Al crédito")){//Factura Manual
                        cantidadFactManual = cantidadFactManual + 1;
                        importeFactManual = importeFactManual + parseFloat(doc.fImporte);
                        doc.flagDocumento = "FacturaManual";
                    }

                    //if(doc.sCodTipoDocumento==3 && doc.iFactManual==1 && (doc.sDesEstadoDoc!=="Al crédito")){//Boleta Manual
                    //if(doc.sCodTipoDocumento==3 && doc.iFactManual==1 && doc.sDesSedeEmisor==horarioCaja.sSede &&  (doc.sDesEstadoDoc!=="Al crédito")){//Boleta Manual
                    if(doc.sCodTipoDocumento==3 && doc.iFactManual==1 && doc.sDesSedeEmisor==horarioCaja.sSede && !(doc.sDesEstadoDoc == "Anulado" || doc.sDesEstadoDoc == "Al crédito")){//Boleta Manual
                        cantidadBoletManual = cantidadBoletManual + 1;
                        importeBoletManual = importeBoletManual + parseFloat(doc.fImporte);
                        doc.flagDocumento = "BoletaManual";
                    }

                    if(doc.sCodTipoDocumento==1 && doc.iFactManual==1 && doc.sDesSedeEmisor==horarioCaja.sSede && doc.sDesEstadoDoc=="Al crédito"){//Factura Manual PAGA NO
                        cantidadFactManualPagaNo = cantidadFactManualPagaNo + 1;
                        importeFactManualPagaNo = importeFactManualPagaNo + parseFloat(doc.fImporte);
                        doc.flagDocumento = "FacturaManualPagaNo";
                    }

                    if(doc.sCodTipoDocumento==3 && doc.iFactManual==1 && doc.sDesSedeEmisor==horarioCaja.sSede && doc.sDesEstadoDoc=="Al crédito"){//Boleta Manual PAGA NO
                        cantidadBoletManualPagaNo = cantidadBoletManualPagaNo + 1;
                        importeBoletManualPagaNo = importeBoletManualPagaNo + parseFloat(doc.fImporte);
                        doc.flagDocumento = "BoletaManualPagaNo";
                    }
                    //if(doc.sCodTipoDocumento==7 && doc.sDesEstadoDoc=="Al crédito"){//notas de crédito emitida
                    if(doc.sCodTipoDocumento==7 && doc.sDesEstadoDoc=="Devolución total"){//notas de crédito emitida
                        cantidadNC = cantidadNC + 1;
                        importeNC = importeNC + parseFloat(doc.fImporte);
                        doc.flagDocumento = "NotaCreditoEmitida";
                    }
                    //if(doc.sCodTipoDocumento==7 && doc.sDesEstadoDoc=="Al crédito"){//notas de crédito emitida PAGA NO
                    if(pago){
                        if(doc.sCodTipoDocumento==7 && pago.sFormaPago.includes("Paga No") && doc.sDesEstadoDoc!=="Anulado"){
                                cantidadNCPagaNo = cantidadNCPagaNo + 1;
                                importeNCPagaNo = importeNCPagaNo + parseFloat(doc.fImporte);
                                doc.flagDocumento = "NotaCreditoEmitidaPagaNo";
                        }

                        if(doc.sCodTipoDocumento==7 && pago.sFormaPago.includes("Paga No") && doc.sDesEstadoDoc=="Al crédito"){
                            cantidadNCPagaNoTabDoc = cantidadNCPagaNoTabDoc + 1;
                            importeNCPagaNoTabDoc = importeNCPagaNoTabDoc + parseFloat(doc.fImporte);
                            doc.flagDocumento = "NotaCreditoEmitidaPagaNoTabDoc";
                        }

                    }
                    

                    //if(doc.sCodTipoDocumento==1 && doc.sDesSedeEmisor!=horarioCaja.sSede && !(doc.sDesEstadoDoc == "Anulado" || doc.sDesEstadoDoc == "Baja" || doc.sDesEstadoDoc == "Al crédito")){//facturas otras sede
                    if(doc.sCodTipoDocumento==1  && doc.sDesSedeEmisor!=horarioCaja.sSede && !(doc.sDesEstadoDoc == "Anulado" || doc.sDesEstadoDoc == "Baja" || doc.sDesEstadoDoc == "Al crédito")){//facturas otras sede

                        cantidadFactOtrasSedes = cantidadFactOtrasSedes + 1;
                        importeFactOtrasSedes = importeFactOtrasSedes + parseFloat(doc.fImporte);
                        doc.flagDocumento = "FacturaOtraSede";
                    }

                    //if(doc.sCodTipoDocumento==3 && doc.sDesSedeEmisor!=horarioCaja.sSede && !(doc.sDesEstadoDoc == "Anulado" || doc.sDesEstadoDoc == "Baja" || doc.sDesEstadoDoc == "Al crédito")){//boletas otras sede
                    if(doc.sCodTipoDocumento==3  && doc.sDesSedeEmisor!=horarioCaja.sSede && !(doc.sDesEstadoDoc == "Anulado" || doc.sDesEstadoDoc == "Baja" || doc.sDesEstadoDoc == "Al crédito")){//boletas otras sede
                    
                        cantidadBoletOtrasSedes = cantidadBoletOtrasSedes + 1;
                        importeBoletOtrasSedes = importeBoletOtrasSedes + parseFloat(doc.fImporte);
                        doc.flagDocumento = "BoletaOtraSede";
                    }
                    
                    if(doc.sCodTipoDocumento==1 && doc.sDesSedeEmisor!=horarioCaja.sSede && doc.sDesEstadoDoc == "Al crédito"){//facturas otras sede PAGA NO 
                        cantidadFactOtrasSedesPagaNo = cantidadFactOtrasSedesPagaNo + 1;
                        importeFactOtrasSedesPagaNo = importeFactOtrasSedesPagaNo + parseFloat(doc.fImporte);
                        doc.flagDocumento = "FacturaOtraSedePagaNo";
                    }
                    
                    if(doc.sCodTipoDocumento==3 && doc.sDesSedeEmisor!=horarioCaja.sSede && doc.sDesEstadoDoc == "Al crédito"){//boletas otras sede PAGA NO
                        cantidadBoletOtrasSedesPagaNo = cantidadBoletOtrasSedesPagaNo + 1;
                        importeBoletOtrasSedesPagaNo = importeBoletOtrasSedesPagaNo + parseFloat(doc.fImporte);
                        doc.flagDocumento = "BoletaOtraSedePagaNo";
                    }
                    
                    if(doc.sCodTipoDocumento==7 && doc.sDesEstadoDoc=="Devolución total" && doc.sModoAbono == "Efectivo"){//Devolución totales efectivo
                        cantidadDevTotalEfectivo = cantidadDevTotalEfectivo + 1;
                        importeDevTotalEfectivo = importeDevTotalEfectivo + parseFloat(doc.fImporte);
                        doc.flagDocumento = "DevolucionTotalEfectivo";
                    }

                    /*if(doc.sDesEstadoDoc=="Pagado" && doc.sFormaPago.includes("Nota de crédito") && doc.sModoAbono == "Efectivo"){//Devolución parciales efectivo
                        cantidadDevParcialEfectivo = cantidadDevParcialEfectivo + 1;
                        importeDevParcialEfectivo = importeDevParcialEfectivo + parseFloat(doc.fImporteVuelto);//se debe traer el vuelto
                        doc.flagDocumento = "DevolucionParcialEfectivo";
                    }*/

                    if(doc.sCodTipoDocumento==7 && doc.sDesEstadoDoc=="Aplicada"){//Nota de crédito aplicada
                        cantidadNCAplicada = cantidadNCAplicada + 1;
                        importeNCAplicada = importeNCAplicada + parseFloat(doc.fImporte);
                        doc.flagDocumento = "NotaCreditoAplicada";
                    }




                    if(( doc.sCodTipoDocumento==1 || doc.sCodTipoDocumento==3 || doc.sCodTipoDocumento==7 ) && doc.sDesEstadoDoc=="Anulado"){//Anulados
                        cantidadAnulado = cantidadAnulado + 1;
                        importeAnulado = importeAnulado + parseFloat(doc.fImporte);
                        doc.flagDocumento = "Anulado";
                    }
                    //falta  para anulaciones
                });
            }
            ///END DOCUMENTOS
            ////END VALIDACIONES////////////////////////////////////////////////////////////////////////////////////
            var totalCantidadContado = 0;
            var totalImporteContado = 0;
            var totalCantidadCredito = 0;
            var totalImporteCredito = 0;
            var aICorrelativosNegativo = [];

            modData = that.aListaTipoDocumento();
            var RTDTotalCantidad=0;
            var RTDTotalImporte=0;
            var aICorrelativosNegativo =[];
            var aICorrelativosNegativo1 =[];
            var aICorrelativosNegativo2 =[];
            aICorrelativosNegativo1.push(5)
            aICorrelativosNegativo2.push(14);

            aICorrelativosNegativo = aICorrelativosNegativo1.concat(aICorrelativosNegativo2);
            modData.forEach(function(item){
                if(item.sDenominacion=="Facturas Electrónicas"){
                    item.iCantidad = cantidadFacturas;
                    item.fImporte = parseFloat(importeFacturas);
                }
                if(item.sDenominacion=="Boletas Electrónicas"){
                    item.iCantidad = cantidadBoletas;
                    item.fImporte = parseFloat(importeBoletas);
                }
                if(item.sDenominacion=="Factura Manual"){
                    item.iCantidad = cantidadFactManual;
                    item.fImporte = parseFloat(importeFactManual);
                }
                if(item.sDenominacion=="Boleta Manual"){
                    item.iCantidad = cantidadBoletManual;
                    item.fImporte = parseFloat(importeBoletManual);
                }
                if(item.sDenominacion=="Nota de Crédito Emitida"){
                    item.iCantidad = cantidadNC;
                    item.fImporte = parseFloat(importeNC);
                }
                if(item.sDenominacion=="Fact. Otras Sedes"){
                    item.iCantidad = cantidadFactOtrasSedes;
                    item.fImporte = parseFloat(importeFactOtrasSedes);
                }
                if(item.sDenominacion=="Bole. Otras Sedes"){
                    item.iCantidad = cantidadBoletOtrasSedes;
                    item.fImporte = parseFloat(importeBoletOtrasSedes);
                }
                if(item.sDenominacion=="Hab. Fondo de Sencillo"){//
                    item.iCantidad = iCantidadArqueoCartuchera;
                    item.fImporte = parseFloat(fImporteMonedaNacArqueo);
                }
                //////PRIMER SUB TOTAL/////////////////
                
                if(item.iCorrelativo){
                    if(item.iCorrelativo>=1 && item.iCorrelativo<=8){        
                        totalCantidadContado = totalCantidadContado + item.iCantidad;
                        if(aICorrelativosNegativo1.includes(item.iCorrelativo)){
                            //totalImporteContado = totalImporteContado - parseFloat(item.fImporte)
                            totalImporteContado = totalImporteContado - parseFloat(item.fImporte)       
                            }else{
                            //totalImporteContado = totalImporteContado + parseFloat(item.fImporte)
                            totalImporteContado = totalImporteContado + parseFloat(item.fImporte)       
                            }
                        
                    }
                }
                //////PRIMER SUB TOTAL/////////////////
                if(item.sDenominacion=="Total Contado"){
                    item.iCantidad = totalCantidadContado;
                    item.fImporte = parseFloat(totalImporteContado);
                }
                if(item.sDenominacion=="Facturas Electrónicas Paga no"){
                    item.iCantidad = cantidadFacturasPagaNo;
                    item.fImporte = parseFloat(importeFacturasPagaNo);
                }
                if(item.sDenominacion=="Boletas Electrónicas Paga no"){
                    item.iCantidad = cantidadBoletasPagaNo;
                    item.fImporte = parseFloat(importeBoletasPagaNo);
                }
                if(item.sDenominacion=="Factura Manual Paga no"){
                    item.iCantidad = cantidadFactManualPagaNo;
                    item.fImporte = parseFloat(importeFactManualPagaNo);
                }
                if(item.sDenominacion=="Boleta Manual Paga no"){
                    item.iCantidad = cantidadBoletManualPagaNo;
                    item.fImporte = parseFloat(importeBoletManualPagaNo);
                }
                if(item.sDenominacion=="Nota de Crédito Emitida Paga no"){
                    item.iCantidad = cantidadNCPagaNo;
                    item.fImporte = parseFloat(importeNCPagaNo);
                }
                if(item.sDenominacion=="Fact. Otras Sedes Paga no"){
                    item.iCantidad = cantidadFactOtrasSedesPagaNo;
                    item.fImporte = parseFloat(importeFactOtrasSedesPagaNo);
                }
                if(item.sDenominacion=="Bole. Otras Sedes Paga no"){
                    item.iCantidad = cantidadBoletOtrasSedesPagaNo;
                    item.fImporte = parseFloat(importeBoletOtrasSedesPagaNo);
                }
                //////SEGUNDO SUB TOTAL//////////////////////////
                
                    if(item.iCorrelativo){
                        if(item.iCorrelativo>=10 && item.iCorrelativo<=16){
                            totalCantidadCredito = totalCantidadCredito + item.iCantidad;
                            if(aICorrelativosNegativo2.includes(item.iCorrelativo)){
                               //totalImporteCredito = totalImporteCredito - parseFloat(item.fImporte)
                               totalImporteCredito = totalImporteCredito - parseFloat(item.fImporte) 
                            }else{
                               //totalImporteCredito = totalImporteCredito + parseFloat(item.fImporte)
                               totalImporteCredito = totalImporteCredito + parseFloat(item.fImporte)  
                            }
                            
                        }
                    }
                //////SEGUNDO SUB TOTAL//////////////////////////
                if(item.sDenominacion=="Total Crédito"){
                    item.iCantidad = totalCantidadCredito;
                    item.fImporte = parseFloat(totalImporteCredito);
                }
                ///TOTAL GENERAL//////////////////
                if(item.iCorrelativo){
                    var importeRTD = item.fImporte ? item.fImporte : 0;
                    var cantidadRTD = item.iCantidad ? item.iCantidad : 0;
                    RTDTotalCantidad=RTDTotalCantidad+cantidadRTD;
                    if(aICorrelativosNegativo.includes(item.iCorrelativo)){
                        //RTDTotalImporte = (parseFloat(RTDTotalImporte) - parseFloat(importeRTD)).toFixed(2);
                        RTDTotalImporte = (parseFloat(RTDTotalImporte) - parseFloat(importeRTD))
                    }else{
                        //RTDTotalImporte = (parseFloat(RTDTotalImporte) + parseFloat(importeRTD)).toFixed(2);
                        RTDTotalImporte = (parseFloat(RTDTotalImporte) + parseFloat(importeRTD));    
                    }
                }
                ///TOTAL GENERAL///////////////////
            });
            if(!noCalcular){
                modelResumen.setProperty("/RTDTotalCantidad",RTDTotalCantidad);
                modelResumen.setProperty("/RTDTotalImporte",parseFloat(RTDTotalImporte)); //RTDTotalImporte);
            }
            var modDoc = that.distribuirDocumentos(self,documentos);//Nuevo Método

            var totalCantidadOtros=0;
            var totalImporteOtros=0;
            var cantidadBoletaCreditoTabDoc=0;
            var importeBoletaCreditoTabDoc=0;
            var cantidadFacturaCreditoTabDoc=0;
            var importeFacturaCreditoTabDoc=0;
            var cantidadAnuladoTabDoc=0;
            var importeAnuladoTabDoc=0;
            var cantidadComprobantesOtrasSedesTabDoc=0;
            var importeComprobantesOtrasSedesTabDoc=0;
            var cantidadFacturasTabDoc =0;
            var importeFacturasTabDoc = 0;



            modDoc.aDocumentosOtrosComprobantes.forEach(function(item,x){
                         totalCantidadOtros = totalCantidadOtros + (x+1);
                        // totalImporteOtros = totalImporteOtros + parseFloat(item.fImporte);
                        totalImporteOtros = totalImporteOtros + parseFloat(item.fImporte);
                 });
                 modDoc.aDocumentosOtrosComprobantes = self.utilController.orderBy(modDoc.aDocumentosOtrosComprobantes,"sNroDoc")
            
            //-------- PESTAÑA DE DOCUMENTOS - ARQUEO DE CAJA		


            modDoc.aDocumentosFacturaElectronica.forEach(function(item,x){
                cantidadFacturasTabDoc = cantidadFacturasTabDoc + (x+1);
                //importeBoletaCredito = importeBoletaCredito + parseFloat(item.fImporte);
                importeFacturasTabDoc = importeFacturasTabDoc + parseFloat(item.fImporte);
            });

            modDoc.aDocumentosBoletaCredito.forEach(function(item,x){
                cantidadBoletaCreditoTabDoc = cantidadBoletaCreditoTabDoc + (x+1);
                //importeBoletaCredito = importeBoletaCredito + parseFloat(item.fImporte);
                importeBoletaCreditoTabDoc = importeBoletaCreditoTabDoc + parseFloat(item.fImporte);
            });
            
            modDoc.aDocumentosFacturaCredito.forEach(function(item,x){
                cantidadFacturaCreditoTabDoc = cantidadFacturaCreditoTabDoc + (x+1);
                //importeFacturaCredito = importeFacturaCredito + parseFloat(item.fImporte);
                importeFacturaCreditoTabDoc = importeFacturaCreditoTabDoc + parseFloat(item.fImporte);
            });

            modDoc.aDocumentosAnulado.forEach(function(item,x){
                cantidadAnuladoTabDoc = cantidadAnuladoTabDoc + (x+1);
                //importeAnulado = importeAnulado + parseFloat(item.fImporte);
                importeAnuladoTabDoc = importeAnuladoTabDoc + parseFloat(item.fImporte);
            });

            modDoc.aDocumentosComprobantesOtrasSedes.forEach(function(item,x){
                cantidadComprobantesOtrasSedesTabDoc = cantidadComprobantesOtrasSedesTabDoc + (x+1);
                //importeComprobantesOtrasSedes = importeComprobantesOtrasSedes + parseFloat(item.fImporte);
                importeComprobantesOtrasSedesTabDoc = importeComprobantesOtrasSedesTabDoc + parseFloat(item.fImporte);
            });
            

            // var totalCantidadOtros=0;
            // var totalImporteOtros=0;
            // var aDocumentosOtros = documentos.filter(function(el){
            //     return (
            //     !el.flagDocumento ||
            //     el.flagDocumento == "FacturaElectronicaPagaNo" || 
            //     el.flagDocumento == "BoletaElectronicaPagaNo" ||
            //     el.flagDocumento == "FacturaManualPagaNo" ||
            //     el.flagDocumento == "BoletaManualPagaNo" ||
            //     el.flagDocumento == "NotaCreditoEmitidaPagaNo" ||
            //     el.flagDocumento == "FacturaOtraSedePagaNo" ||
            //     el.flagDocumento == "BoletaOtraSedePagaNo")
            // });
            // aDocumentosOtros.forEach(function(item,x){
            //         totalCantidadOtros = totalCantidadOtros + (x+1);
            //         totalImporteOtros = totalImporteOtros + parseFloat(item.fImporte);
            // });

            /*iCantidadArqueoCartuchera
            fImporteMonedaNacArqueo
            iCantidadReposicion
            fImporteMonedaNacReposicion
            cantidadFacturasPagaNo
            importeFacturasPagaNo
            cantidadBoletasPagaNo
            importeBoletasPagaNo
            cantidadFactManualPagaNo
            importeFactManualPagaNo
            cantidadBoletManualPagaNo
            importeBoletManualPagaNo
            cantidadNCPagaNo
            importeNCPagaNo
            cantidadFactOtrasSedesPagaNo
            importeFactOtrasSedesPagaNo
            cantidadBoletOtrasSedesPagaNo
            importeBoletOtrasSedesPagaNo
            cantidadDevParcialEfectivo
            importeDevParcialEfectivo*/
            
            
            
            
            
            
            
            
                self.getView().getModel("D").setProperty("/aDocumentosBoletaElectronica", modDoc.aDocumentosBoletaElectronica);
                self.getView().getModel("D").setProperty("/TotalCantidadBoletaElectronica", cantidadBoletas);
                self.getView().getModel("D").setProperty("/TotalImporteBoletaElectronica",parseFloat(importeBoletas)), //parseFloat(importeBoletas).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleBoletaElectronica", modDoc.aDocumentosBoletaElectronica.length>0 ? true : false);

                self.getView().getModel("D").setProperty("/aDocumentosFacturaElectronica", modDoc.aDocumentosFacturaElectronica);
                self.getView().getModel("D").setProperty("/TotalCantidadFacturaElectronica", cantidadFacturasTabDoc);
                self.getView().getModel("D").setProperty("/TotalImporteFacturaElectronica",parseFloat(importeFacturasTabDoc)), //parseFloat(importeFacturas).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleFacturaElectronica", modDoc.aDocumentosFacturaElectronica.length>0 ? true : false);

                self.getView().getModel("D").setProperty("/aDocumentosBoletaManual", modDoc.aDocumentosBoletaManual);
                self.getView().getModel("D").setProperty("/TotalCantidadBoletaManual", cantidadBoletManual);
                self.getView().getModel("D").setProperty("/TotalImporteBoletaManual", parseFloat(importeBoletManual)), //parseFloat(importeBoletManual).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleBoletaManual", modDoc.aDocumentosBoletaManual.length>0 ? true : false);

                self.getView().getModel("D").setProperty("/aDocumentosFacturaManual", modDoc.aDocumentosFacturaManual);
                self.getView().getModel("D").setProperty("/TotalCantidadFacturaManual", cantidadFactManual);
                self.getView().getModel("D").setProperty("/TotalImporteFacturaManual",  parseFloat(importeFactManual)), //parseFloat(importeFactManual).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleFacturaManual", modDoc.aDocumentosFacturaManual.length>0 ? true : false);


                //aDocumentosBoletaCredito : 
                

                self.getView().getModel("D").setProperty("/aDocumentosBoletaCredito", modDoc.aDocumentosBoletaCredito);
                self.getView().getModel("D").setProperty("/TotalCantidadBoletaCredito", cantidadBoletaCreditoTabDoc);//cantidadBoletaCredito);
                self.getView().getModel("D").setProperty("/TotalImporteBoletaCredito", parseFloat(importeBoletaCreditoTabDoc));//parseFloat(importeBoletaCredito)),//parseFloat(importeBoletaCredito).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleBoletaCredito", modDoc.aDocumentosBoletaCredito.length>0 ? true : false);
                
                //aDocumentosFacturaCredito : 

                self.getView().getModel("D").setProperty("/aDocumentosFacturaCredito", modDoc.aDocumentosFacturaCredito);
                self.getView().getModel("D").setProperty("/TotalCantidadFacturaCredito", cantidadFacturaCreditoTabDoc);//cantidadFacturaCredito);
                self.getView().getModel("D").setProperty("/TotalImporteFacturaCredito",parseFloat(importeFacturaCreditoTabDoc))//parseFloat(importeFacturaCredito)), // parseFloat(importeFacturaCredito).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleFacturaCredito", modDoc.aDocumentosFacturaCredito.length>0 ? true : false);

                //aDocumentosAnulado
                
                self.getView().getModel("D").setProperty("/aDocumentosAnulado", modDoc.aDocumentosAnulado);
                self.getView().getModel("D").setProperty("/TotalCantidadAnulado", cantidadAnuladoTabDoc)//cantidadAnulado);
                self.getView().getModel("D").setProperty("/TotalImporteAnulado", parseFloat(importeAnuladoTabDoc))//parseFloat(importeAnulado)), //parseFloat(importeAnulado).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleAnulado", modDoc.aDocumentosAnulado.length>0 ? true : false);
                
                self.getView().getModel("D").setProperty("/aDocumentosNotaCreditoAplicada", modDoc.aDocumentosNotaCreditoAplicada);
                self.getView().getModel("D").setProperty("/TotalCantidadNotaCreditoAplicada", cantidadNCAplicada);
                self.getView().getModel("D").setProperty("/TotalImporteNotaCreditoAplicada", parseFloat(importeNCAplicada)), //parseFloat(importeNCAplicada).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleNotaCreditoAplicada", modDoc.aDocumentosNotaCreditoAplicada.length>0 ? true : false);

                self.getView().getModel("D").setProperty("/aDocumentosNotaCreditoEmitidaPagaNo", modDoc.aDocumentosNotaCreditoEmitidaPagaNo);
                self.getView().getModel("D").setProperty("/TotalCantidadNotaCreditoEmitidaPagaNo", cantidadNCPagaNoTabDoc);
                self.getView().getModel("D").setProperty("/TotalImporteNotaCreditoEmitidaPagaNo", parseFloat(importeNCPagaNoTabDoc)), // parseFloat(importeNCPagaNoTabDoc).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleNotaCreditoEmitidaPagaNo", modDoc.aDocumentosNotaCreditoEmitidaPagaNo.length>0 ? true : false);
                
                

                self.getView().getModel("D").setProperty("/aDocumentosComprobantesOtrasSedes", modDoc.aDocumentosComprobantesOtrasSedes);
                self.getView().getModel("D").setProperty("/TotalCantidadComprobantesOtrasSedes", cantidadComprobantesOtrasSedesTabDoc)//cantidadComprobantesOtrasSedes);
                self.getView().getModel("D").setProperty("/TotalImporteComprobantesOtrasSedes", parseFloat(importeComprobantesOtrasSedesTabDoc)) //parseFloat(importeComprobantesOtrasSedes)), //parseFloat(importeComprobantesOtrasSedes).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleComprobantesOtrasSedes", modDoc.aDocumentosComprobantesOtrasSedes.length>0 ? true : false);
                
                self.getView().getModel("D").setProperty("/aDocumentosOtrosComprobantes", modDoc.aDocumentosOtrosComprobantes);
                self.getView().getModel("D").setProperty("/TotalCantidadOtrosComprobantes", totalCantidadOtros);
                self.getView().getModel("D").setProperty("/TotalImporteOtrosComprobantes", parseFloat(totalImporteOtros)), //parseFloat(totalImporteOtros).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleOtrosComprobantes", modDoc.aDocumentosOtrosComprobantes.length>0 ? true : false);
                
                /*
                self.getView().getModel("D").setProperty("/aDocumentosNotaCreditoEmitida", aDocumentosNotaCreditoEmitida);
                self.getView().getModel("D").setProperty("/TotalCantidadNotaCreditoEmitida", cantidadNC);
                self.getView().getModel("D").setProperty("/TotalImporteNotaCreditoEmitida", parseFloat(importeNC).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleNotaCreditoEmitida", aDocumentosNotaCreditoEmitida.length>0 ? true : false);

                self.getView().getModel("D").setProperty("/aDocumentosFacturaOtraSede", aDocumentosFacturaOtraSede);
                self.getView().getModel("D").setProperty("/TotalCantidadFacturaOtraSede", cantidadFactOtrasSedes);
                self.getView().getModel("D").setProperty("/TotalImporteFacturaOtraSede", parseFloat(importeFactOtrasSedes).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleFacturaOtraSede", aDocumentosFacturaOtraSede.length>0 ? true : false);

                self.getView().getModel("D").setProperty("/aDocumentosBoletaOtraSede", aDocumentosBoletaOtraSede);
                self.getView().getModel("D").setProperty("/TotalCantidadBoletaOtraSede", cantidadBoletOtrasSedes);
                self.getView().getModel("D").setProperty("/TotalImporteBoletaOtraSede", parseFloat(importeBoletOtrasSedes).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleBoletaOtraSede", aDocumentosBoletaOtraSede.length>0 ? true : false);
                                
                self.getView().getModel("D").setProperty("/aDocumentosDevolucionTotalEfectivo", aDocumentosDevolucionTotalEfectivo);
                self.getView().getModel("D").setProperty("/TotalCantidadDevolucionTotalEfectivo", cantidadDevTotalEfectivo);
                self.getView().getModel("D").setProperty("/TotalImporteDevolucionTotalEfectivo", parseFloat(importeDevTotalEfectivo).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleDevolucionTotalEfectivo", aDocumentosDevolucionTotalEfectivo.length>0 ? true : false);
                
                


                
                self.getView().getModel("D").setProperty("/aDocumentosFacturaElectronicaPagaNo", aDocumentosFacturaElectronicaPagaNo);
                self.getView().getModel("D").setProperty("/TotalCantidadFacturaElectronicaPagaNo", cantidadFacturasPagaNo);
                self.getView().getModel("D").setProperty("/TotalImporteFacturaElectronicaPagaNo", parseFloat(importeFacturasPagaNo).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleFacturaElectronicaPagaNo", aDocumentosFacturaElectronicaPagaNo.length>0 ? true : false);

                self.getView().getModel("D").setProperty("/aDocumentosBoletaElectronicaPagaNo", aDocumentosBoletaElectronicaPagaNo);
                self.getView().getModel("D").setProperty("/TotalCantidadBoletaElectronicaPagaNo", cantidadBoletasPagaNo);
                self.getView().getModel("D").setProperty("/TotalImporteBoletaElectronicaPagaNo", parseFloat(importeBoletasPagaNo).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleBoletaElectronicaPagaNo", aDocumentosBoletaElectronicaPagaNo.length>0 ? true : false);

                self.getView().getModel("D").setProperty("/aDocumentosFacturaManualPagaNo", aDocumentosFacturaManualPagaNo);
                self.getView().getModel("D").setProperty("/TotalCantidadFacturaManualPagaNo", cantidadFactManualPagaNo);
                self.getView().getModel("D").setProperty("/TotalImporteFacturaManualPagaNo", parseFloat(importeFactManualPagaNo).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleFacturaManualPagaNo", aDocumentosFacturaManualPagaNo.length>0 ? true : false);

                self.getView().getModel("D").setProperty("/aDocumentosBoletaManualPagaNo", aDocumentosBoletaManualPagaNo);
                self.getView().getModel("D").setProperty("/TotalCantidadBoletaManualPagaNo", cantidadBoletManualPagaNo);
                self.getView().getModel("D").setProperty("/TotalImporteBoletaManualPagaNo", parseFloat(importeBoletManualPagaNo).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleBoletaManualPagaNo", aDocumentosBoletaManualPagaNo.length>0 ? true : false);

                

                self.getView().getModel("D").setProperty("/aDocumentosFacturaOtraSedePagaNo", aDocumentosFacturaOtraSedePagaNo);
                self.getView().getModel("D").setProperty("/TotalCantidadFacturaOtraSedePagaNo", cantidadFactOtrasSedesPagaNo);
                self.getView().getModel("D").setProperty("/TotalImporteFacturaOtraSedePagaNo", parseFloat(importeFactOtrasSedesPagaNo).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleFacturaOtraSedePagaNo", aDocumentosFacturaOtraSedePagaNo.length>0 ? true : false);

                self.getView().getModel("D").setProperty("/aDocumentosBoletaOtraSedePagaNo", aDocumentosBoletaOtraSedePagaNo);
                self.getView().getModel("D").setProperty("/TotalCantidadBoletaOtraSedePagaNo", cantidadBoletOtrasSedesPagaNo);
                self.getView().getModel("D").setProperty("/TotalImporteBoletaOtraSedePagaNo", parseFloat(importeBoletOtrasSedesPagaNo).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleBoletaOtraSedePagaNo", aDocumentosBoletaOtraSedePagaNo.length>0 ? true : false);
               

                self.getView().getModel("D").setProperty("/aDocumentosOtros", aDocumentosOtros);
                self.getView().getModel("D").setProperty("/TotalCantidadOtros", totalCantidadOtros);
                self.getView().getModel("D").setProperty("/TotalImporteOtros", parseFloat(totalImporteOtros).toFixed(2));
                self.getView().getModel("D").setProperty("/visibleOtros", aDocumentosOtros.length>0 ? true : false);
                */
               ////INICIO SETEAR iId en el TAB RESUMEN/////////////////////////////////////
               var  aDenominacionResumen =  self.getView().getModel("localModel").getProperty("/oArqueoResumen");
               modData.forEach(function(item) {
                       if(aDenominacionResumen){
                            var obj = aDenominacionResumen.find(function(el) {
                            return el.sCodDenominacionDoc == item.sCodDenominacion;
                                       });
                                   if (obj) {
                                       item.iId = obj.iId;
                                   }     
                       }
                 
               });
               ////END SETEAR iId en el TAB RESUMEN////////////////////////////////////////
                self.getView().getModel("D").refresh(true);
                // Logica para ponerle el color : 
                /*
                const aItems = sap.ui.getCore().byId("idTbResumenTipoDocumento").getItems();
                var indexColorBlue = [5,21]
                aItems.forEach(function(element,index){
                        
                      console.log(element) 
                      if((indexColorBlue).includes(index)){
                              element.addStyleClass("blueBackground"); 
                      }
                      
                });
                */
                self.utilController.verificarYpintarObligatoriedadCamposValidarPOS_Delivery(self,modDoc,modDoc.aDocumentosBoletaElectronica,pagos,"idTbBoletaElectronicaTabDoc","redErrorStyleBackground");
                self.utilController.verificarYpintarObligatoriedadCamposValidarPOS_Delivery(self,modDoc,modDoc.aDocumentosFacturaElectronica,pagos,"idTbFacturaElectronicaTabDoc","redErrorStyleBackground");
                self.utilController.verificarYpintarObligatoriedadCamposValidarPOS_Delivery(self,modDoc,modDoc.aDocumentosBoletaManual,pagos,"idTbBoletaManualTabDoc","redErrorStyleBackground");
                self.utilController.verificarYpintarObligatoriedadCamposValidarPOS_Delivery(self,modDoc,modDoc.aDocumentosFacturaManual,pagos,"idTbFacturaManualTabDoc","redErrorStyleBackground");
                self.utilController.verificarYpintarObligatoriedadCamposValidarPOS_Delivery(self,modDoc,modDoc.aDocumentosBoletaCredito,pagos,"idTbBoletaCreditoTabDoc","redErrorStyleBackground");
                self.utilController.verificarYpintarObligatoriedadCamposValidarPOS_Delivery(self,modDoc,modDoc.aDocumentosFacturaCredito,pagos,"idTbFacturaCreditoTabDoc","redErrorStyleBackground");
                //self.utilController.verificarYpintarObligatoriedadCamposValidarPOS_Delivery(self,aDocumentosAnulado,pagos,"idTbAnuladoTabDoc","redErrorStyleBackground");
                self.utilController.verificarYpintarObligatoriedadCamposValidarPOS_Delivery(self,modDoc,modDoc.aDocumentosNotaCreditoAplicada,pagos,"idTbNotaCreditoAplicadaTabDoc","redErrorStyleBackground");
                self.utilController.verificarYpintarObligatoriedadCamposValidarPOS_Delivery(self,modDoc,modDoc.aDocumentosNotaCreditoEmitidaPagaNo,pagos,"idTbNotaCreditoEmitidaPagaNoTabDoc","redErrorStyleBackground");
                self.utilController.verificarYpintarObligatoriedadCamposValidarPOS_Delivery(self,modDoc,modDoc.aDocumentosComprobantesOtrasSedes,pagos,"idTbComprobantesOtrasSedesTabDoc","redErrorStyleBackground");
                self.utilController.verificarYpintarObligatoriedadCamposValidarPOS_Delivery(self,modDoc,modDoc.aDocumentosOtrosComprobantes,pagos,"idTbOtrosComprobantesTabDoc","redErrorStyleBackground");

            return modData;
        },
        consultarResumenMedioPago: function(self) {
            var modData = [];
            /////INICIO VALIDACIONES/////////////////////////////////////
            var ES = self.getView().getModel("ES").getData();
            var ED = self.getView().getModel("ED").getData();
            var T = self.getView().getModel("T").getData();
            var C = self.getView().getModel("C").getData();
            var FFS = self.getView().getModel("FFS").getData(); 
            var modelResumen = self.getView().getModel("Resumen");
            var D = self.getView().getModel("D").getData();
            var documentos = D.aDocumentos;
            var pagos = D.aPagos;
            var totalAjusteRedonde=0;
            var ultimoTipoCambio = 0;
            var cantidadNCAplicada=0;
            var importeNCAplicada=0;
            var cantidadNC=0;
            var importeNC=0;
            var cantidadNCPagaNo=0;
            var importeNCPagaNo=0;
            var cantidadFacturasPagaNo=0;
            var importeFacturasPagaNo=0;
            var cantidadBoletasPagaNo=0;
            var importeBoletasPagaNo=0;
            var cantidadDevTotalAbonoCuenta=0;
            var importeDevTotalAbonoCuenta=0;
            var cantidadDevTotalExtornoTarjeta=0;
            var importeDevTotalExtornoTarjeta=0;
            var cantidadDevTotalAnulacionPagaNo=0;
            var importeDevTotalAnulacionPagaNo=0;
            var cantidadDevTotalAbonoCheque=0;
            var importeDevTotalAbonoCheque=0;
            var cantidadDevTotalSoles=0;
            var importeDevTotalSoles=0;
            var cantidadDevTotalDolares=0;
            var importeDevTotalDolares=0;
            var aVueltoPEN = 0;
            var aVueltoUSD = 0;
            var sTotalEfectivoPEN = 0;
            var sTotalEfectivoUSD = 0;
            
            var orderDocumentos =self.utilController.orderById(documentos); 
            
            //ultimoTipoCambio = orderDocumentos ? (orderDocumentos.length > 0 ? orderDocumentos[orderDocumentos.length-1].fTipoCambio : 0) : 0;
            ultimoTipoCambio = self.getView().getModel("D").getProperty("/fUltimoTipoCambio");
            ///INICIO DOCUMENTOS
            if(documentos.length>0){
                documentos.forEach(function(doc){
                    var pago = D.aPagos.find(function(el){
                        return el.iIdDocumento == doc.iIdDocumento;
                    });
                    if(doc.sDesEstadoDoc!="Anulado"){
                       totalAjusteRedonde = totalAjusteRedonde + parseFloat(doc.fAjustadoRedondeoPEN) + parseFloat(doc.fImporteRedondeoVuelto);      
                    }
                           
                    /*
                    if(doc.sCodTipoDocumento==7 && doc.sDesEstadoDoc=="Aplicada"){//Nota de crédito aplicada
                        cantidadNCAplicada = cantidadNCAplicada + 1;
                        importeNCAplicada = importeNCAplicada + parseFloat(doc.fImporte);
                        //doc.flagDocumento = "NotaCreditoAplicada";
                    }
                    */

                    if(doc.sCodTipoDocumento==1 && doc.sDesEstadoDoc == "Al crédito"){//facturas electrónicas PAGA NO
                        cantidadFacturasPagaNo = cantidadFacturasPagaNo + 1;
                        importeFacturasPagaNo = importeFacturasPagaNo + parseFloat(doc.fImporte);
                        //doc.flagDocumento = "FacturaElectronicaPagaNo";
                    }

                    if(doc.sCodTipoDocumento==3 && doc.sDesEstadoDoc == "Al crédito"){//boletas electrónicas PAGA NO
                        cantidadBoletasPagaNo = cantidadBoletasPagaNo + 1;
                        importeBoletasPagaNo = importeBoletasPagaNo + parseFloat(doc.fImporte);
                        //doc.flagDocumento = "BoletaElectronicaPagaNo";
                    }
                    if(doc.sCodTipoDocumento==7 && doc.sDesEstadoDoc=="Al crédito"){//notas de crédito emitida
                        cantidadNC = cantidadNC + 1;
                        importeNC = importeNC + parseFloat(doc.fImporte);
                    }
                  
                    if(doc.sCodTipoDocumento==7 && doc.sDesEstadoDoc=="Devolución total" && doc.sModoAbono == self.constantes.DesAbonoCuenta){//Dev total Abono cuenta
                        if(doc.sMoneda=="PEN"){
                            cantidadDevTotalAbonoCuenta = cantidadDevTotalAbonoCuenta + 1;
                            importeDevTotalAbonoCuenta = importeDevTotalAbonoCuenta + parseFloat(pago.fImporte);//parseFloat(doc.fImporte);
                        }else{
                            cantidadDevTotalAbonoCuenta = cantidadDevTotalAbonoCuenta + 1;
                            importeDevTotalAbonoCuenta = importeDevTotalAbonoCuenta + parseFloat(pago.fImporte)*parseFloat(ultimoTipoCambio);//parseFloat(doc.fImporte)*parseFloat(ultimoTipoCambio);
                        }
                        //doc.flagDocumento = "DevolucionTotalAbonoCuenta";
                    }
                   
                    if(doc.sCodTipoDocumento==7 && doc.sDesEstadoDoc=="Devolución total" && doc.sModoAbono == self.constantes.DesExtornoTarjeta){//Dev total Extorno tarjeta para nota de crédito
                         
                        if(doc.sMoneda=="PEN"){
                            cantidadDevTotalExtornoTarjeta = cantidadDevTotalExtornoTarjeta + 1;
                            importeDevTotalExtornoTarjeta = importeDevTotalExtornoTarjeta + parseFloat(pago.fImporte);//parseFloat(doc.fImporte);
                        }else{
                            cantidadDevTotalExtornoTarjeta = cantidadDevTotalExtornoTarjeta + 1;
                            importeDevTotalExtornoTarjeta = importeDevTotalExtornoTarjeta + parseFloat(pago.fImporte)*parseFloat(ultimoTipoCambio);//parseFloat(doc.fImporte)*parseFloat(ultimoTipoCambio);
                        }
                        //doc.flagDocumento = "DevolucionTotalAbonoCuenta";
                    }
                   
                    if((doc.sCodTipoDocumento==1 || doc.sCodTipoDocumento==3) && doc.sDesEstadoDoc !="Anulado"  && doc.sModoAbono == self.constantes.DesExtornoTarjeta){//Dev total Extorno tarjeta para facturas y boletas
                          
                        if(doc.sMonedaVuelto=="PEN"){
                            cantidadDevTotalExtornoTarjeta = cantidadDevTotalExtornoTarjeta + 1;
                            importeDevTotalExtornoTarjeta = importeDevTotalExtornoTarjeta + parseFloat(doc.fImporteVuelto);//parseFloat(doc.fImporte);
                        }else{
                            cantidadDevTotalExtornoTarjeta = cantidadDevTotalExtornoTarjeta + 1;
                            importeDevTotalExtornoTarjeta = importeDevTotalExtornoTarjeta + parseFloat(pago.fImporteVuelto)*parseFloat(ultimoTipoCambio);//parseFloat(doc.fImporte)*parseFloat(ultimoTipoCambio);
                        }
                        //doc.flagDocumento = "DevolucionTotalAbonoCuenta";
                    }
                    
                    if(doc.sCodTipoDocumento==7 && doc.sDesEstadoDoc=="Devolución total" && doc.sModoAbono == self.constantes.DesAnulacionPagaNo){//Dev total Abono cuenta
                        
                        if(doc.sMoneda=="PEN"){
                            cantidadDevTotalAnulacionPagaNo = cantidadDevTotalAnulacionPagaNo + 1;
                            importeDevTotalAnulacionPagaNo = importeDevTotalAnulacionPagaNo+ parseFloat(pago.fImporte);//parseFloat(doc.fImporte);
                        }else{
                            cantidadDevTotalAnulacionPagaNo = cantidadDevTotalAnulacionPagaNo + 1;
                            importeDevTotalAnulacionPagaNo = importeDevTotalAnulacionPagaNo + parseFloat(pago.fImporte)*parseFloat(ultimoTipoCambio);//parseFloat(doc.fImporte)*parseFloat(ultimoTipoCambio);
                        }
                        //doc.flagDocumento = "DevolucionTotalAbonoCuenta";
                    }

                    if(doc.sCodTipoDocumento==7 && doc.sDesEstadoDoc=="Devolución total" && doc.sModoAbono == self.constantes.DesPagoCheque){//Dev total Cheque
                        
                         if(doc.sMoneda=="PEN"){
                            cantidadDevTotalAbonoCheque = cantidadDevTotalAbonoCheque + 1;
                            importeDevTotalAbonoCheque = importeDevTotalAbonoCheque + parseFloat(pago.fImporte); //parseFloat(doc.fImporte);
                        }else{
                            cantidadDevTotalAbonoCheque = cantidadDevTotalAbonoCheque + 1;
                            importeDevTotalAbonoCheque = importeDevTotalAbonoCheque + parseFloat(pago.fImporte)*parseFloat(ultimoTipoCambio); //parseFloat(doc.fImporte)*parseFloat(ultimoTipoCambio);
                        }
                        //doc.flagDocumento = "DevolucionTotalAbonoCheque";
                    }
                    
                    if(doc.sCodTipoDocumento==7 && doc.sDesEstadoDoc=="Devolución total" && doc.sModoAbono == "Efectivo"){//Dev total Tab Efectivo Soles
                        if(doc.sMoneda=="PEN"){
                            cantidadDevTotalSoles = cantidadDevTotalSoles + 1;
                            importeDevTotalSoles = importeDevTotalSoles + parseFloat(pago.fImporte);
                        }else{
                            cantidadDevTotalDolares = cantidadDevTotalDolares + 1;
                            importeDevTotalDolares = importeDevTotalDolares + parseFloat(pago.fImporte);
                        }
                        //doc.flagDocumento = "DevolucionTotalAbonoCuenta";
                    }
                        
                        
                    //Calcular Vueltos en Total Efectivo Soles y Dolares
                        if(doc.sMonedaVuelto=="PEN" && doc.sModoAbono=="Efectivo" && doc.sDesEstadoDoc!="Anulado"){
                            aVueltoPEN += parseFloat(doc.fImporteVuelto);
                        }  else if(doc.sMonedaVuelto=="USD" && doc.sModoAbono=="Efectivo" && doc.sDesEstadoDoc!="Anulado"){
                            aVueltoUSD += parseFloat(doc.fImporteVuelto);   
                        }
                        
                    if(pago){
                        if(doc.sCodTipoDocumento==7 && pago.sFormaPago.includes("Paga No") && doc.sDesEstadoDoc!=="Anulado"){
                                cantidadNCPagaNo = cantidadNCPagaNo + 1;
                                importeNCPagaNo = importeNCPagaNo + parseFloat(doc.fImporte);
                                //doc.flagDocumento = "NotaCreditoEmitidaPagaNo";
                        }

                        
                        
                        //---
                        if((doc.sCodTipoDocumento==1||doc.sCodTipoDocumento==3 ) && doc.sModoAbono==self.constantes.DesAbonoCuenta  && doc.sDesEstadoDoc!="Anulado"){//Dev total Abono cuenta
                        if(pago.sMoneda=="PEN"){
                            cantidadDevTotalAbonoCuenta = cantidadDevTotalAbonoCuenta + 1;    
                            importeDevTotalAbonoCuenta = importeDevTotalAbonoCuenta + parseFloat(doc.fImporteVuelto);
                        }else{
                            cantidadDevTotalAbonoCuenta = cantidadDevTotalAbonoCuenta + 1;
                            importeDevTotalAbonoCuenta = importeDevTotalAbonoCuenta + parseFloat(doc.fImporteVuelto)*parseFloat(ultimoTipoCambio);
                        }
                        //doc.flagDocumento = "DevolucionTotalAbonoCuenta";
                         }

                         if((doc.sCodTipoDocumento==1||doc.sCodTipoDocumento==3 ) && doc.sModoAbono==self.constantes.DesPagoCheque  && doc.sDesEstadoDoc!="Anulado"){//Dev total Cheque
                        if(pago.sMoneda=="PEN"){
                            cantidadDevTotalAbonoCheque = cantidadDevTotalAbonoCheque + 1;
                            importeDevTotalAbonoCheque = importeDevTotalAbonoCheque + parseFloat(doc.fImporteVuelto);
                        }else{
                            cantidadDevTotalAbonoCheque = cantidadDevTotalAbonoCheque + 1;
                            importeDevTotalAbonoCheque = importeDevTotalAbonoCheque + parseFloat(doc.fImporteVuelto)*parseFloat(ultimoTipoCambio);
                        }
                        }//descomentado de validación ROY 22-03-2021
        

                    }
                        

                });
            }
            ///END DOCUMENTOS
            ///INICIO TRASLADOS
            var cantidadTrasladosMN = 0;
            var importeTrasladosMN = 0; 
            var cantidadTrasladosME = 0;
            var importeTrasladosME = 0; 

            var cantidadTrasladosUSD = 0;
            var importeTrasladosUSD = 0; 
            
            if(T.aTraslados.length>0){
                T.aTraslados.forEach(function(traslado){
                    if(traslado.sMoneda=="PEN"){//Traslado Boveda MN
                        cantidadTrasladosMN = cantidadTrasladosMN + 1;
                        importeTrasladosMN = importeTrasladosMN + parseFloat(traslado.fImportePEN );
                    }
                    if(traslado.sMoneda=="USD"){//Traslado Boveda ME
                        cantidadTrasladosME = cantidadTrasladosME + 1;
                        importeTrasladosME = importeTrasladosME + (parseFloat(traslado.fImporteUSD)*parseFloat(ultimoTipoCambio));
                    }
                    if(traslado.sMoneda=="USD"){//Traslado Boveda ME
                        cantidadTrasladosUSD = cantidadTrasladosUSD + 1;
                        importeTrasladosUSD = importeTrasladosUSD + parseFloat(traslado.fImporteUSD);
                    }
                });
            }
            //END TRASLADOS
            ///INICIO PAGOS
            var cantidadVoucherVisa=0;
            var importeVoucherVisa=0;
            var cantidadVoucherMaster=0;
            var importeVoucherMaster=0;
            var cantidadTotalVoucherConTarjeta=0;
            var importeTotalVoucherConTarjeta=0;
           
            var cantidadDepositoCuenta=0;
            var importeDepositoCuenta=0;
            var cantidadDepositoDetraccion=0;
            var importeDepositoDetraccion=0;
            var cantidadWebNiubiz=0;
            var importeWebNiubiz=0;
            var cantidadWebIzipay=0;
            var importeWebIzipay=0;

            var cantidadVoucherAmericanExpress=0;
            var importeVoucherAmericanExpress=0;
            var cantidadVoucherDinersClub=0;
            var importeVoucherDinersClub=0;

            var cantidadME=parseInt(ED.EDTotalCantidad);
            

            //var importeME=parseFloat(ED.EDTotalImporte)*ultimoTipoCambio;
            var importeME=parseFloat(ED.EDTotalImporte)*ultimoTipoCambio;
            
            var fImporte = 0;
            if(pagos){
                /////INICIO VALIDACION PINPAD//////////////////////////////////////////////////////////////////////////////////////////////////////
                   /*
                   var aDocumentoNroReferencia = self.getView().getModel("localModel").getProperty("/aDocumentosNumReferencia") ? self.getView().getModel("localModel").getProperty("/aDocumentosNumReferencia") : [];
                   //1. Pagos del incio.
                    var pagoPinpad = pagos.filter(function(el){
                        return el.sFormaPago == "Devolución total";
                    });
                    var docPagoResumen = [];
                    if(pagoPinpad.length>0){
                        pagoPinpad.forEach(function(item){
                            var obj = documentos.find(function(el){
                                return el.iIdDocumento == item.iIdDocumento
                                && el.sModoAbono ==self.constantes.DesFormaPagoTarjeta;
                            });
                            if(obj){
                                docPagoResumen.push(obj);
                            }
                        });
                    }
                    var pagoReturn = [];
                    if(docPagoResumen.length>0){
                        docPagoResumen.forEach(function(item){
                            var obj = pagos.find(function(el){
                                return el.iIdDocumento == item.iIdDocumento;
                                });

                            if(obj){
                                  pagoReturn.push(obj); 
                            }
                          
                        });
                    }
                    if(pagoReturn.length>0){
                        pagoReturn.forEach(function(item){
                            
                            var obj = {
                                fImporte : item.fImporte?parseFloat(item.fImporte):0,
                                sNumReferencia : item.sNumReferencia
                                };
                                if(obj){
                                    aValidacionPinpad.push(obj);
                                }
                        }); 
                         
                    }

                    var docSunatPinpad = [];
                    if(aValidacionPinpad.length>0){
                        aValidacionPinpad.forEach(function(item){
                                var obj = aDocumentoNroReferencia.find(function(el){
                                    return item.sNumReferencia.includes(el.sNroDoc) ;
                            });
                            if(obj){
                                 docSunatPinpad.push(obj);   
                            }
                                
                        });
                    }
                    var pagoReturnAgain = [];
                    if(docSunatPinpad.length>0){
                        docSunatPinpad.forEach(function(item){
                            var obj = pagos.find(function(el){
                                return el.iIdDocumento == item.iIdDocumento;
                            });
                            if(obj){
                                    obj.sNroDocValid = item.sNroDoc;
                                    pagoReturnAgain.push(obj); 
                            }
                        });
                    }
                    if(aValidacionPinpad.length>0){
                        aValidacionPinpad.forEach(function(item){
                            var obj = pagoReturnAgain.find(function(el){
                                return item.sNumReferencia.includes(el.sNroDocValid);
                            });
                            if(obj){
                        
                                item.sTipoTarjeta = obj.sTipoTarjeta;
                                item.sFormaPago = obj.sFormaPago;
                            }
                        });
                    }
                    console.log(aValidacionPinpad);
                /////END VALIDACION PINPAD//////////////////////////////////////////////////////////////////////////////////////////////////////
                */
                pagos.forEach(function(pago){

                    

                    var docSunat = documentos.find(function(el){
                        return el.iIdDocumento == pago.iIdDocumento;
                    });
                    if(docSunat){
                        if(pago.sTipoTarjeta==self.constantes.DesVoucherVisa && pago.sFormaPago==self.constantes.DesFormaPagoTarjeta && !(docSunat.sDesEstadoDoc=="Anulado" || docSunat.sDesEstadoDoc=="Baja" )){//Voucher Visa
                            cantidadVoucherVisa = cantidadVoucherVisa + 1;
                            importeVoucherVisa = importeVoucherVisa + parseFloat(pago.fImporte);
                        }
                        if(pago.sTipoTarjeta==self.constantes.DesVoucherMaster && pago.sFormaPago==self.constantes.DesFormaPagoTarjeta && !(docSunat.sDesEstadoDoc=="Anulado" || docSunat.sDesEstadoDoc=="Baja")){//Voucher Master  pago.sTipoTarjeta.toUpperCase()
                            cantidadVoucherMaster = cantidadVoucherMaster + 1;
                            importeVoucherMaster = importeVoucherMaster + parseFloat(pago.fImporte);
                        }
                        
                        if(pago.sFormaPago==self.constantes.DesFormaPagoTarjeta && !(docSunat.sDesEstadoDoc=="Anulado" || docSunat.sDesEstadoDoc=="Baja") ){//Total Voucher Con Tarjeta
                            cantidadTotalVoucherConTarjeta = cantidadTotalVoucherConTarjeta + 1;
                            importeTotalVoucherConTarjeta = importeTotalVoucherConTarjeta + parseFloat(pago.fImporte);
                        }

                        if(pago.sFormaPago==self.constantes.DesFormaPagoTarjeta && pago.sTipoTarjeta==self.constantes.DesVoucherAmericanExpress && !(docSunat.sDesEstadoDoc=="Anulado" || docSunat.sDesEstadoDoc=="Baja")){//Voucher American Express
                            console.log(docSunat);
                            cantidadVoucherAmericanExpress = cantidadVoucherAmericanExpress + 1;
                            importeVoucherAmericanExpress = importeVoucherAmericanExpress + parseFloat(pago.fImporte);
                        }
                        if(pago.sFormaPago==self.constantes.DesFormaPagoTarjeta && pago.sTipoTarjeta==self.constantes.DesVoucherDinersClub && !(docSunat.sDesEstadoDoc=="Anulado" || docSunat.sDesEstadoDoc=="Baja")){//Voucher Diners Club
                            cantidadVoucherDinersClub = cantidadVoucherDinersClub + 1;
                            importeVoucherDinersClub = importeVoucherDinersClub + parseFloat(pago.fImporte);
                        }
                        
                        if(pago.sFormaPago==self.constantes.DesDepositoCuenta && !(docSunat.sDesEstadoDoc=="Anulado" || docSunat.sDesEstadoDoc=="Baja")){//Depósito en cuenta
                          
                            if(pago.sMoneda=="USD"){
                                cantidadDepositoCuenta = cantidadDepositoCuenta + 1;
                                //importeDepositoCuenta = importeDepositoCuenta + (parseFloat(pago.fImporte)*parseFloat(pago.fTipoCambio)) + parseFloat(docSunat.fAjustadoRedondeoEfectivoPEN); 
                                importeDepositoCuenta = importeDepositoCuenta + (parseFloat(pago.fImporte)*parseFloat(pago.fTipoCambio)) + parseFloat(docSunat.fAjusteRedondeoDepositoPEN) ;
                            }else{
                                cantidadDepositoCuenta = cantidadDepositoCuenta + 1;
                                importeDepositoCuenta = importeDepositoCuenta + parseFloat(pago.fImporte);
                            }
                            
                        }
                       
                        if(pago.sFormaPago=="Nota de crédito" && !(docSunat.sDesEstadoDoc=="Anulado" || docSunat.sDesEstadoDoc=="Baja")){//Nota de Crédito Aplicada
                            if(pago.sMoneda=="USD"){
                                cantidadNCAplicada = cantidadNCAplicada + 1;
                                importeNCAplicada = importeNCAplicada + (parseFloat(pago.fImporte)*parseFloat(pago.fTipoCambio)) + parseFloat(docSunat.fAjustadoRedondeoEfectivoPEN);   
                            }else{
                                cantidadNCAplicada = cantidadNCAplicada + 1;
                                importeNCAplicada = importeNCAplicada + parseFloat(pago.fImporte);
                            }
                        }

                        if(pago.sFormaPago==self.constantes.DesDepositoDetraccion && !(docSunat.sDesEstadoDoc=="Anulado" || docSunat.sDesEstadoDoc=="Baja")){//Depósito detracción
                            cantidadDepositoDetraccion = cantidadDepositoDetraccion + 1;
                            importeDepositoDetraccion = importeDepositoDetraccion + parseFloat(pago.fImporte);
                        }
                        if( (pago.sFormaPago==self.constantes.DesPosDelivery || pago.sFormaPago==self.constantes.DesPosDelivery) && !(docSunat.sDesEstadoDoc=="Anulado" || docSunat.sDesEstadoDoc=="Baja")){//Web Niubiz //POS Delivery
                            cantidadWebNiubiz = cantidadWebNiubiz + 1;
                            importeWebNiubiz = importeWebNiubiz + parseFloat(pago.fImporte);
                        }
                        if(pago.sFormaPago==self.constantes.DesWebIzipay && !(docSunat.sDesEstadoDoc=="Anulado" || docSunat.sDesEstadoDoc=="Baja")){//Web Izipay
                            cantidadWebIzipay = cantidadWebIzipay + 1;
                            importeWebIzipay = importeWebIzipay + parseFloat(pago.fImporte);
                        }

                        if(pago.sFormaPago== "Efectivo" && docSunat.sDesEstadoDoc!=="Anulado"){
                            if(pago.sMoneda=="PEN"){
                                sTotalEfectivoPEN += parseFloat(pago.fImporte)
                            }
                            if(pago.sMoneda=="USD"){
                                sTotalEfectivoUSD += parseFloat(pago.fImporte);
                            }
                        }

                    }
                });
            }
            // self.getView().getModel("localModel").setProperty("/aValidacionPinpad",aValidacionPinpad); 

            /*
            var RMP= modelResumen.getProperty("/aResumenMedioPago");
            RMP = RMP.filter(function (el) {
                return el.iCorrelativo;
            });
            */

            
            /////END VALIDACIONES//////////////////////////////////////////////////
            var totalCantidadEfectivo = 0;
            var totalImporteEfectivo = 0;
            var totalCantidadPagoTarjeta = 0;
            var totalImportePagoTarjeta = 0;
            var totalCantidadTotalCaja = 0;
            var totalImporteTotalCaja = 0;
            var aICorrelativosNegativo = [];
            var fImporteMonedaNacReposicion = self.getView().getModel("FFS").getProperty("/fImporteMonedaNacReposicion")
            
            
            var totalVoucherVisa = parseFloat(importeVoucherVisa);
            var totalVoucherMaster = parseFloat(importeVoucherMaster);
            var totalVoucherDinersClub = parseFloat(importeVoucherDinersClub);
            var totalVoucherAmericanExpress = parseFloat(importeVoucherAmericanExpress);
            

            var totalPOSDelivery = parseFloat(importeWebNiubiz);

            var totalWebIzipay = parseFloat(importeWebIzipay);

            



            var aValidacionPinpad =  self.getView().getModel("D").getProperty("/aValidacionPinpad"); 
            if(aValidacionPinpad.length>0){
                    aValidacionPinpad.forEach(function(item){
                            if( !(item.sFormaPago == self.constantes.DesPosDelivery || item.sFormaPago == self.constantes.DesWebIzipay) ){
                                if(item.sTipoTarjeta==self.constantes.DesVoucherVisa){
                                    totalVoucherVisa = totalVoucherVisa - item.fImporte; 
                                    cantidadVoucherVisa = cantidadVoucherVisa - 1;
                                }
                                if(item.sTipoTarjeta==self.constantes.DesVoucherMaster){
                                    totalVoucherMaster = totalVoucherMaster - item.fImporte; 
                                    cantidadVoucherMaster = cantidadVoucherMaster - 1;
                                }
                                if(item.sTipoTarjeta==self.constantes.DesVoucherDinersClub){
                                    //totalVoucherVisa = totalVoucherVisa - item.fImporte; 
                                    //cantidadVoucherDinersClub = cantidadVoucherDinersClub -1;
                                    totalVoucherDinersClub = totalVoucherDinersClub - item.fImporte;
                                    cantidadVoucherDinersClub = cantidadVoucherDinersClub -1;
                                }
                                if(item.sTipoTarjeta==self.constantes.DesVoucherAmericanExpress){
                                    //totalVoucherVisa = totalVoucherVisa - item.fImporte;
                                    //cantidadVoucherAmericanExpress = cantidadVoucherAmericanExpress - 1;
                                    totalVoucherAmericanExpress = totalVoucherAmericanExpress - item.fImporte;
                                    cantidadVoucherAmericanExpress = cantidadVoucherAmericanExpress - 1;
                                }
                            }
                            

                            if(item.sFormaPago == self.constantes.DesPosDelivery){
                                totalPOSDelivery = totalPOSDelivery - item.fImporte;
                                cantidadWebNiubiz = cantidadWebNiubiz - 1;
                            }

                            if(item.sFormaPago == self.constantes.DesWebIzipay){
                                totalWebIzipay = totalWebIzipay - item.fImporte;
                                cantidadWebIzipay = cantidadWebIzipay - 1;
                            }
                            
                    })
            }
          
            
            modData = this.aListaMedioPago();
            var RMPTotalCantidad=0;
            var RMPTotalImporte=0;
            var aICorrelativosNegativo = [29,30,31,32,33];
            modData.forEach(function(item){
                if(item.sDenominacion=="Devol. Fondo de Sencillo"){
                    item.iCantidad = FFS.FFSTotalCantidad;
                    item.fImporte = parseFloat(FFS.FFSTotalImporte);
                }
                if(item.sDenominacion=="Moneda Nacional"){
                    item.iCantidad = ES.ESTotalCantidad;
                    item.fImporte = parseFloat(ES.ESTotalImporte);
                }
                if(item.sDenominacion=="Traslado Boveda MN"){
                    item.iCantidad = cantidadTrasladosMN;
                    item.fImporte = parseFloat(importeTrasladosMN);
                }
                if(item.sDenominacion=="Moneda Extranjera"){
                    item.iCantidad = cantidadME;
                    item.fImporte = parseFloat(importeME);
                }
                if(item.sDenominacion=="Traslado Boveda ME"){
                    item.iCantidad = cantidadTrasladosME;
                    item.fImporte = parseFloat(importeTrasladosME);
                }
                /////PRIMER SUBTOTAL////////////////////////////////
                if(item.iCorrelativo){
                    if(item.iCorrelativo>=1 && item.iCorrelativo<=5){
                        totalCantidadEfectivo = totalCantidadEfectivo + item.iCantidad;
                        //totalImporteEfectivo = totalImporteEfectivo + parseFloat(item.fImporte)
                        totalImporteEfectivo = totalImporteEfectivo + parseFloat(item.fImporte)
                    }
                }
                /////PRIMER SUBTOTAL////////////////////////////////
                if(item.sDenominacion=="Total Efectivo"){
                    item.iCantidad = totalCantidadEfectivo;
                    item.fImporte = parseFloat(totalImporteEfectivo);
                }
                if(item.sDenominacion=="Voucher Visa"){
                    item.iCantidad = cantidadVoucherVisa;
                    item.fImporte = parseFloat(totalVoucherVisa);
                }
                if(item.sDenominacion=="Voucher Master"){
                    item.iCantidad = cantidadVoucherMaster;
                    item.fImporte = parseFloat(totalVoucherMaster);
                }
                if(item.sDenominacion=="Voucher Diners"){
                    item.iCantidad = cantidadVoucherDinersClub;
                    item.fImporte = parseFloat(totalVoucherDinersClub);
                }
                if(item.sDenominacion=="Voucher Amex"){
                    item.iCantidad = cantidadVoucherAmericanExpress;
                    item.fImporte = parseFloat(totalVoucherAmericanExpress);
                }
                if(item.sDenominacion=="POS Delivery"){
                    item.iCantidad = cantidadWebNiubiz;
                    item.fImporte = parseFloat(totalPOSDelivery);
                }
                if(item.sDenominacion=="Web Izipay"){
                    item.iCantidad = cantidadWebIzipay;
                    item.fImporte = parseFloat(totalWebIzipay);
                }
                /////SEGUNDO SUBTOTAL////////////////////////////
                    if(item.iCorrelativo){
                        if(item.iCorrelativo>=11 && item.iCorrelativo<=16){
                            totalCantidadPagoTarjeta = totalCantidadPagoTarjeta + item.iCantidad;
                            //totalImportePagoTarjeta = totalImportePagoTarjeta + parseFloat(item.fImporte)
                            totalImportePagoTarjeta = totalImportePagoTarjeta + parseFloat(item.fImporte)
                        }
                    }
                if(item.sDenominacion=="Total Pagos Con Tarjeta"){
                    item.iCantidad = totalCantidadPagoTarjeta;
                    item.fImporte = parseFloat(totalImportePagoTarjeta);
                }
                /////SEGUNDO SUBTOTAL////////////////////////////
                if(item.sDenominacion=="Depósito en Cuenta"){
                    item.iCantidad = cantidadDepositoCuenta;
                    item.fImporte = parseFloat(importeDepositoCuenta);
                }
                if(item.sDenominacion=="Nota de Crédito Aplicada"){
                    item.iCantidad = cantidadNCAplicada;
                    item.fImporte = parseFloat(importeNCAplicada);
                }
                //////TERCER SUB TOTAL/////////////////////////////
                    if(item.iCorrelativo){
                        if(item.iCorrelativo>=21 && item.iCorrelativo<=22){
                            totalCantidadTotalCaja = totalCantidadTotalCaja + item.iCantidad;
                        // totalImporteTotalCaja = totalImporteTotalCaja + parseFloat(item.fImporte)
                        totalImporteTotalCaja = totalImporteTotalCaja + parseFloat(item.fImporte)
                        }
                    }
                if(item.sDenominacion=="Total Caja"){
                    totalImporteTotalCaja = totalImporteTotalCaja + parseFloat(totalImporteEfectivo) + parseFloat(totalImportePagoTarjeta);
                    item.iCantidad = totalCantidadTotalCaja;
                    item.fImporte = parseFloat(totalImporteTotalCaja);
                }
                ///////TERCER SUB TOTAL///////////////////////////////////////////////////////////
                if(item.sDenominacion=="Facturas Paga No"){
                    item.iCantidad = cantidadFacturasPagaNo;
                    item.fImporte = parseFloat(importeFacturasPagaNo);
                }
                if(item.sDenominacion=="Boletas Paga No"){
                    item.iCantidad = cantidadBoletasPagaNo;
                    item.fImporte = parseFloat(importeBoletasPagaNo);
                }
                if(item.sDenominacion=="Notas de Crédito Paga No"){
                    item.iCantidad = cantidadNCPagaNo;
                    item.fImporte = parseFloat(importeNCPagaNo);
                }
                if(item.sDenominacion=="Dev Total Cheque"){
                    item.iCantidad = cantidadDevTotalAbonoCheque;
                    item.fImporte = parseFloat(importeDevTotalAbonoCheque);
                }
                if(item.sDenominacion=="Dev Total Abono Cuenta"){
                    item.iCantidad = cantidadDevTotalAbonoCuenta;
                    item.fImporte = parseFloat(importeDevTotalAbonoCuenta);
                }
                if(item.sDenominacion=="Dev Total Extorno Tarjeta"){
                      
                    item.iCantidad = cantidadDevTotalExtornoTarjeta;
                    item.fImporte = parseFloat(importeDevTotalExtornoTarjeta);
                }

                if(item.sDenominacion=="Dev Anulación Paga No"){
                    item.iCantidad = cantidadDevTotalAnulacionPagaNo;
                    item.fImporte = parseFloat(importeDevTotalAnulacionPagaNo);
                }
                
                    if(item.iCorrelativo){
                        var importeRMP = item.fImporte ? item.fImporte : 0;
                        var cantidadRMP = item.iCantidad ? item.iCantidad : 0;
                        RMPTotalCantidad=RMPTotalCantidad+cantidadRMP;
                        if(aICorrelativosNegativo.includes(item.iCorrelativo)){
                            //RMPTotalImporte = (parseFloat(RMPTotalImporte) - parseFloat(importeRMP)).toFixed(2);   
                            RMPTotalImporte = (parseFloat(RMPTotalImporte) - parseFloat(importeRMP));    
                        }else{
                            //RMPTotalImporte = (parseFloat(RMPTotalImporte) + parseFloat(importeRMP)).toFixed(2);
                            RMPTotalImporte = (parseFloat(RMPTotalImporte) + parseFloat(importeRMP));    
                        }
                    
                    }
            });
            modelResumen.setProperty("/RMPTotalCantidad",RMPTotalCantidad);
            modelResumen.setProperty("/RMPTotalImporte",parseFloat(RMPTotalImporte));//RMPTotalImporte);

            var importeCustodio = 0;

            if(C.aCustodios.length>0){
                C.aCustodios.forEach(function(item){
                    if(item.sEstado=="Recepcionado"){
                       // importeCustodio = importeCustodio + parseFloat(item.sImportePEN) + (parseFloat(item.sImporteUSD)*parseFloat(ultimoTipoCambio));
                         importeCustodio = importeCustodio + parseFloat(item.fImportePEN) + (parseFloat(item.fImporteUSD)*parseFloat(ultimoTipoCambio));
                    }
                });
            }
            var RTDTotalImporte = modelResumen.getProperty("/RTDTotalImporte");
            
            //ReposicionFondo
            modelResumen.setProperty("/ResumenTotalAjusteRedondeo",parseFloat(totalAjusteRedonde)); //parseFloat(totalAjusteRedonde).toFixed(2));
            ////GUILLE
                var valor = 0;
                valor = parseFloat(RMPTotalImporte) + parseFloat(totalAjusteRedonde)  - parseFloat(RTDTotalImporte);
                if(valor>0){
                    modelResumen.setProperty("/ResumenTotalSobrante",valor)
                    modelResumen.setProperty("/ResumenTotalFaltante",0)
                }else if(valor == 0){
                    modelResumen.setProperty("/ResumenTotalSobrante",valor)
                    modelResumen.setProperty("/ResumenTotalFaltante",valor)
                }else if(valor < 0 ){
                    modelResumen.setProperty("/ResumenTotalFaltante",Math.abs(valor));
                    modelResumen.setProperty("/ResumenTotalSobrante",0)
                }

            ////GUILLE
            //modelResumen.setProperty("/ResumenTotalSobrante",(RMPTotalImporte-RTDTotalImporte)>=0 ? self.formatter.FormatterDinero(((parseFloat(RMPTotalImporte)-parseFloat(RTDTotalImporte)) - importeCustodio)) : self.formatter.FormatterDinero(0));//((parseFloat(RMPTotalImporte)-parseFloat(RTDTotalImporte)) - importeCustodio).toFixed(2) : 0);
            //modelResumen.setProperty("/ResumenTotalSobrante",(RMPTotalImporte-RTDTotalImporte)>=0 ? ((parseFloat(RMPTotalImporte)+parseFloat(totalAjusteRedonde)) - ( parseFloat(importeCustodio) + parseFloat(RTDTotalImporte)  )) : 0);//((parseFloat(RMPTotalImporte)-parseFloat(RTDTotalImporte)) - importeCustodio).toFixed(2) : 0);
            //modelResumen.setProperty("/ResumenTotalFaltante",(RMPTotalImporte+totalAjusteRedonde-RTDTotalImporte)<0 ? (Math.abs(parseFloat(RTDTotalImporte)-parseFloat(RMPTotalImporte)-parseFloat(totalAjusteRedonde))) : 0); //(Math.abs(parseFloat(RTDTotalImporte)-parseFloat(RMPTotalImporte)-parseFloat(totalAjusteRedonde))).toFixed(2) : 0);
            //modelResumen.setProperty("/ResumenTotalSobrante",(RMPTotalImporte+totalAjusteRedonde-RTDTotalImporte)>=0 ? ((parseFloat(RMPTotalImporte)+parseFloat(totalAjusteRedonde)) - ( parseFloat(RTDTotalImporte)  )) : 0);//((parseFloat(RMPTotalImporte)-parseFloat(RTDTotalImporte)) - importeCustodio).toFixed(2) : 0);
            modelResumen.setProperty("/ResumenReposicionFondo",parseFloat(fImporteMonedaNacReposicion));
            var valor = 0;
            valor = parseFloat(RMPTotalImporte) + parseFloat(totalAjusteRedonde)  - parseFloat(RTDTotalImporte);
            if(valor>0){
                       modelResumen.setProperty("/ResumenTotalSobrante",valor)
                       modelResumen.setProperty("/ResumenTotalFaltante",0)

            }else if(valor == 0){
                    modelResumen.setProperty("/ResumenTotalSobrante",valor)
                    modelResumen.setProperty("/ResumenTotalFaltante",valor)
            }else if(valor < 0 ){
                    modelResumen.setProperty("/ResumenTotalFaltante",Math.abs(valor));
                    modelResumen.setProperty("/ResumenTotalSobrante",0)
            }
            var totalEfectivoPEN = 0;
            var totalEfectivoUSD = 0;
            var fImporteMonedaNacReposicion = self.getView().getModel("FFS").getProperty("/fImporteMonedaNacReposicion");
            //documentos.forEach((a) => aVueltoPEN += parseFloat(a.fImporteVuelto));
           
               
            totalEfectivoPEN = sTotalEfectivoPEN - ( aVueltoPEN + parseFloat(importeDevTotalSoles) + parseFloat(importeTrasladosMN) ) + parseFloat(fImporteMonedaNacReposicion);
            totalEfectivoUSD = sTotalEfectivoUSD - ( aVueltoUSD + parseFloat(importeDevTotalDolares) + parseFloat(importeTrasladosUSD) );
            //self.getView().getModel("D").setProperty("/TotalEfectivoPEN", parseFloat(totalEfectivoPEN).toFixed(2)  ) ;
            //self.getView().getModel("D").setProperty("/TotalEfectivoUSD", parseFloat(totalEfectivoUSD).toFixed(2) );
                
            self.getView().getModel("D").setProperty("/TotalEfectivoPEN", (parseFloat(totalEfectivoPEN))) ;
            self.getView().getModel("D").setProperty("/TotalEfectivoUSD", (parseFloat(totalEfectivoUSD))) ;
            self.getView().getModel("D").refresh(true);
            ////INICIO SETEAR iId en el TAB RESUMEN/////////////////////////////////////
            var  aDenominacionResumen =  self.getView().getModel("localModel").getProperty("/oArqueoResumen");
                modData.forEach(function(item) {
                    if(aDenominacionResumen){
                        var obj = aDenominacionResumen.find(function(el) {
                                  return el.sCodDenominacionDoc == item.sCodDenominacion;
                        });
                        if (obj) {
                            item.iId =  obj.iId;
                        }    
                    }
                });
            ////END SETEAR iId en el TAB RESUMEN////////////////////////////////////////
            modelResumen.refresh(true);
            return modData;
        },
        aListaTipoDocumento:function(){
            var modData = [];
            modData.push({ "iCorrelativo": null, "sTipoAgrupador" : null, "sCodDenominacion": null, "sDenominacion": "Comprobantes Contado", "iCantidad": "", "fImporte": null });
            modData.push({ "iCorrelativo": 1,"sTipoAgrupador" : "I1", "sCodDenominacion": "TD1", "sDenominacion": "Facturas Electrónicas", "iCantidad": 0, "fImporte": 0});
            modData.push({ "iCorrelativo": 2,"sTipoAgrupador" : "I1", "sCodDenominacion": "TD2", "sDenominacion": "Boletas Electrónicas", "iCantidad": 0, "fImporte": 0});
            modData.push({ "iCorrelativo": 3,"sTipoAgrupador" : "I1", "sCodDenominacion": "TD3", "sDenominacion": "Factura Manual", "iCantidad": 0, "fImporte": 0});
            modData.push({ "iCorrelativo": 4,"sTipoAgrupador" : "I1", "sCodDenominacion": "TD4", "sDenominacion": "Boleta Manual", "iCantidad": 0, "fImporte": 0});
            modData.push({ "iCorrelativo": 5,"sTipoAgrupador" : "I2", "sCodDenominacion": "TD5", "sDenominacion": "Nota de Crédito Emitida", "iCantidad": 0, "fImporte": 0}); 
            modData.push({ "iCorrelativo": 6,"sTipoAgrupador" : "I1", "sCodDenominacion": "TD6", "sDenominacion": "Fact. Otras Sedes", "iCantidad": 0, "fImporte":  0});
            modData.push({ "iCorrelativo": 7,"sTipoAgrupador" : "I1", "sCodDenominacion": "TD7", "sDenominacion": "Bole. Otras Sedes", "iCantidad": 0, "fImporte": 0});
            modData.push({ "iCorrelativo": 8,"sTipoAgrupador" : "I1", "sCodDenominacion": "TD8", "sDenominacion": "Hab. Fondo de Sencillo", "iCantidad": 0, "fImporte": 0});
            modData.push({ "iCorrelativo": null,"sTipoAgrupador" : null, "sCodDenominacion": "TD10", "sDenominacion": "Total Contado", "iCantidad": 0, "fImporte": 0});
            
            modData.push({ "iCorrelativo": null,"sTipoAgrupador" : null, "sCodDenominacion": null, "sDenominacion": "", "iCantidad": "", "fImporte": null });
            modData.push({ "iCorrelativo": null,"sTipoAgrupador" : null, "sCodDenominacion": null, "sDenominacion": "", "iCantidad": "", "fImporte": null });
            modData.push({ "iCorrelativo": null,"sTipoAgrupador" : null, "sCodDenominacion": null, "sDenominacion": "", "iCantidad": "", "fImporte": null });
            modData.push({ "iCorrelativo": null,"sTipoAgrupador" : null, "sCodDenominacion": null, "sDenominacion": "", "iCantidad": "", "fImporte": null });
            
            modData.push({ "iCorrelativo": null,"sTipoAgrupador" : null, "sCodDenominacion": null, "sDenominacion": "Comprobantes al Crédito", "iCantidad": "", "fImporte": null });
            modData.push({ "iCorrelativo": 10,"sTipoAgrupador" : "I1", "sCodDenominacion": "TD20", "sDenominacion": "Facturas Electrónicas Paga no", "iCantidad": 0, "fImporte": 0});
            modData.push({ "iCorrelativo": 11,"sTipoAgrupador" : "I1", "sCodDenominacion": "TD21", "sDenominacion": "Boletas Electrónicas Paga no", "iCantidad": 0, "fImporte": 0});
            modData.push({ "iCorrelativo": 12,"sTipoAgrupador" : "I1", "sCodDenominacion": "TD22", "sDenominacion": "Factura Manual Paga no", "iCantidad": 0, "fImporte": 0});
            modData.push({ "iCorrelativo": 13,"sTipoAgrupador" : "I1", "sCodDenominacion": "TD23", "sDenominacion": "Boleta Manual Paga no", "iCantidad": 0, "fImporte": 0});
            modData.push({ "iCorrelativo": 14,"sTipoAgrupador" : "I2", "sCodDenominacion": "TD24", "sDenominacion": "Nota de Crédito Emitida Paga no", "iCantidad": 0, "fImporte": 0});
            modData.push({ "iCorrelativo": 15,"sTipoAgrupador" : "I1", "sCodDenominacion": "TD25", "sDenominacion": "Fact. Otras Sedes Paga no", "iCantidad": 0, "fImporte": 0});
            modData.push({ "iCorrelativo": 16,"sTipoAgrupador" : "I1", "sCodDenominacion": "TD26", "sDenominacion": "Bole. Otras Sedes Paga no", "iCantidad": 0, "fImporte": 0});
            modData.push({ "iCorrelativo": null,"sTipoAgrupador" : null, "sCodDenominacion": "TD27", "sDenominacion": "Total Crédito", "iCantidad": 0, "fImporte": 0});

            modData.push({ "iCorrelativo": null,"sTipoAgrupador" : null, "sCodDenominacion": null, "sDenominacion": "", "iCantidad": "", "fImporte": null });
            modData.push({ "iCorrelativo": null,"sTipoAgrupador" : null, "sCodDenominacion": null, "sDenominacion": "", "iCantidad": "", "fImporte": null });
            modData.push({ "iCorrelativo": null,"sTipoAgrupador" : null, "sCodDenominacion": null, "sDenominacion": "", "iCantidad": "", "fImporte": null });
            return modData;
        },
        aListaMedioPago:function(){
            var modData = [];
            modData.push({ "iCorrelativo": 1,"sTipoAgrupador" : "P1", "sCodDenominacion": "MP1", "sDenominacion": "Devol. Fondo de Sencillo", "iCantidad": 0, "fImporte": 0}); //parseFloat(FFS.FFSTotalImporte).toFixed(2) });
            //modData.push({ "iCorrelativo": 2, "sCodDenominacion": "MP1", "sDenominacion": "Reposición de Fondo", "iCantidad": 0, "fImporte": self.formatter.FormatterDinero(parseFloat(fImporteMonedaNacReposicion))});// parseFloat(fImporteMonedaNacReposicion).toFixed(2) });
            modData.push({ "iCorrelativo": 2,"sTipoAgrupador" : "P1", "sCodDenominacion": "MP2", "sDenominacion": "Moneda Nacional", "iCantidad": 0, "fImporte": 0});//parseFloat(ES.ESTotalImporte).toFixed(2) });
            modData.push({ "iCorrelativo": 3,"sTipoAgrupador" : "P1", "sCodDenominacion": "MP3", "sDenominacion": "Traslado Boveda MN", "iCantidad": 0, "fImporte": 0});//parseFloat(importeTrasladosMN).toFixed(2) });
            modData.push({ "iCorrelativo": 4,"sTipoAgrupador" : "P1", "sCodDenominacion": "MP4", "sDenominacion": "Moneda Extranjera", "iCantidad": 0, "fImporte": 0});//parseFloat(importeME).toFixed(2) });
            modData.push({ "iCorrelativo": 5,"sTipoAgrupador" : "P1", "sCodDenominacion": "MP5", "sDenominacion": "Traslado Boveda ME", "iCantidad": 0, "fImporte": 0});//parseFloat(importeTrasladosME).toFixed(2) });
          
            modData.push({ "iCorrelativo": null,"sTipoAgrupador" : null, "sCodDenominacion": "MP10", "sDenominacion": "Total Efectivo", "iCantidad": 0, "fImporte": 0}); //parseFloat(totalImporteEfectivo).toFixed(2) });
            
            
            modData.push({ "iCorrelativo": null,"sTipoAgrupador" : null, "sCodDenominacion": null, "sDenominacion": "", "iCantidad": "", "fImporte": null });
            
          
            modData.push({ "iCorrelativo": 11,"sTipoAgrupador" : "P1", "sCodDenominacion": "MP11", "sDenominacion": "Voucher Visa", "iCantidad": 0, "fImporte": 0}); //parseFloat(totalVoucherVisa).toFixed(2) });
            modData.push({ "iCorrelativo": 12,"sTipoAgrupador" : "P1", "sCodDenominacion": "MP12", "sDenominacion": "Voucher Master", "iCantidad": 0, "fImporte": 0});//parseFloat(totalVoucherMaster).toFixed(2) });
            modData.push({ "iCorrelativo": 13,"sTipoAgrupador" : "P1", "sCodDenominacion": "MP13", "sDenominacion": "Voucher Diners", "iCantidad": 0, "fImporte": 0});//parseFloat(totalVoucherDinersClub).toFixed(2) });
            modData.push({ "iCorrelativo": 14,"sTipoAgrupador" : "P1", "sCodDenominacion": "MP14", "sDenominacion": "Voucher Amex", "iCantidad": 0, "fImporte": 0});//parseFloat(totalVoucherAmericanExpress).toFixed(2) });
            modData.push({ "iCorrelativo": 15,"sTipoAgrupador" : "P1", "sCodDenominacion": "MP15", "sDenominacion": "POS Delivery", "iCantidad": 0, "fImporte": 0});//parseFloat(totalPOSDelivery).toFixed(2) });
            modData.push({ "iCorrelativo": 16,"sTipoAgrupador" : "P1", "sCodDenominacion": "MP16", "sDenominacion": "Web Izipay", "iCantidad": 0, "fImporte": 0});//parseFloat(totalWebIzipay).toFixed(2) });
            
            modData.push({ "iCorrelativo": null,"sTipoAgrupador" : null, "sCodDenominacion": "MP20", "sDenominacion": "Total Pagos Con Tarjeta", "iCantidad": 0, "fImporte": 0});//parseFloat(totalImportePagoTarjeta).toFixed(2) });

            modData.push({ "iCorrelativo": null,"sTipoAgrupador" : null, "sCodDenominacion": null, "sDenominacion": "", "iCantidad": "", "fImporte": null });

            modData.push({ "iCorrelativo": 21,"sTipoAgrupador" : "P1", "sCodDenominacion": "MP21", "sDenominacion": "Depósito en Cuenta", "iCantidad": 0, "fImporte": 0});//parseFloat(importeDepositoCuenta).toFixed(2) });
            modData.push({ "iCorrelativo": 22,"sTipoAgrupador" : "P1", "sCodDenominacion": "MP22", "sDenominacion": "Nota de Crédito Aplicada", "iCantidad": 0, "fImporte": 0});//parseFloat(importeNCAplicada).toFixed(2) });
            
             
            modData.push({ "iCorrelativo": null,"sTipoAgrupador" : null, "sCodDenominacion": "MP25", "sDenominacion": "Total Caja", "iCantidad": 0, "fImporte":0}); //parseFloat(totalImporteTotalCaja).toFixed(2) });

            modData.push({ "iCorrelativo": null,"sTipoAgrupador" : null, "sCodDenominacion": null, "sDenominacion": "", "iCantidad": "", "fImporte": null });
            
            modData.push({ "iCorrelativo": 27,"sTipoAgrupador" : "P1", "sCodDenominacion": "MP30", "sDenominacion": "Facturas Paga No", "iCantidad": 0, "fImporte": 0});//parseFloat(importeFacturasPagaNo).toFixed(2) });
            modData.push({ "iCorrelativo": 28,"sTipoAgrupador" : "P1", "sCodDenominacion": "MP31", "sDenominacion": "Boletas Paga No", "iCantidad": 0, "fImporte": 0});//parseFloat(importeBoletasPagaNo).toFixed(2) });
            modData.push({ "iCorrelativo": 29,"sTipoAgrupador" : "P2", "sCodDenominacion": "MP32", "sDenominacion": "Notas de Crédito Paga No", "iCantidad": 0, "fImporte": 0});//parseFloat(importeNCPagaNo).toFixed(2) });
            modData.push({ "iCorrelativo": 30,"sTipoAgrupador" : "P2", "sCodDenominacion": "MP33", "sDenominacion": "Dev Total Cheque", "iCantidad": 0, "fImporte": 0});//parseFloat(importeDevTotalAbonoCheque).toFixed(2) });
            modData.push({ "iCorrelativo": 31,"sTipoAgrupador" : "P2", "sCodDenominacion": "MP34", "sDenominacion": "Dev Total Abono Cuenta", "iCantidad": 0, "fImporte": 0});//parseFloat(importeDevTotalAbonoCuenta).toFixed(2) });
            modData.push({ "iCorrelativo": 32,"sTipoAgrupador" : "P2", "sCodDenominacion": "MP35", "sDenominacion": "Dev Total Extorno Tarjeta", "iCantidad": 0, "fImporte": 0});
            modData.push({ "iCorrelativo": 33,"sTipoAgrupador" : null, "sCodDenominacion": "MP36", "sDenominacion": "Dev Anulación Paga No", "iCantidad": 0, "fImporte": 0});
            
            return modData;
        },
        distribuirDocumentos:function(self,documentos){
            var modDoc = {};
            modDoc.aDocumentosNotaCreditoEmitida = documentos.filter(function(el){
                return el.flagDocumento == "NotaCreditoEmitida";
            });
            modDoc.aDocumentosNotaCreditoEmitida = self.utilController.orderBy(modDoc.aDocumentosNotaCreditoEmitida,"sNroDoc");
        
            modDoc.aDocumentosFacturaOtraSede = documentos.filter(function(el){
                return el.flagDocumento == "FacturaOtraSede";
            });
            modDoc.aDocumentosFacturaOtraSede = self.utilController.orderBy(modDoc.aDocumentosFacturaOtraSede,"sNroDoc");

            modDoc.aDocumentosBoletaOtraSede = documentos.filter(function(el){
                return el.flagDocumento == "BoletaOtraSede";
            });
            modDoc.aDocumentosBoletaOtraSede = self.utilController.orderBy(modDoc.aDocumentosBoletaOtraSede,"sNroDoc");

            modDoc.aDocumentosDevolucionTotalEfectivo = documentos.filter(function(el){
                return el.flagDocumento == "DevolucionTotalEfectivo";
            });
            modDoc.aDocumentosDevolucionTotalEfectivo = self.utilController.orderBy(modDoc.aDocumentosDevolucionTotalEfectivo,"sNroDoc");

            modDoc.aDocumentosDevolucionParcialEfectivo = documentos.filter(function(el){
                return el.flagDocumento == "DevolucionParcialEfectivo";
            });
            modDoc.aDocumentosDevolucionParcialEfectivo = self.utilController.orderBy(modDoc.aDocumentosDevolucionParcialEfectivo,"sNroDoc");

            modDoc.aDocumentosFacturaElectronicaPagaNo = documentos.filter(function(el){
                return el.flagDocumento == "FacturaElectronicaPagaNo";
            });
            modDoc.aDocumentosFacturaElectronicaPagaNo = self.utilController.orderBy(modDoc.aDocumentosFacturaElectronicaPagaNo,"sNroDoc");

            modDoc.aDocumentosBoletaElectronicaPagaNo = documentos.filter(function(el){
                return el.flagDocumento == "BoletaElectronicaPagaNo";
            });
            modDoc.aDocumentosBoletaElectronicaPagaNo = self.utilController.orderBy(modDoc.aDocumentosBoletaElectronicaPagaNo,"sNroDoc");

            modDoc.aDocumentosFacturaManualPagaNo = documentos.filter(function(el){
                return el.flagDocumento == "FacturaManualPagaNo";
            });
            modDoc.aDocumentosFacturaManualPagaNo = self.utilController.orderBy(modDoc.aDocumentosFacturaManualPagaNo,"sNroDoc");

            modDoc.aDocumentosBoletaManualPagaNo = documentos.filter(function(el){
                return el.flagDocumento == "BoletaManualPagaNo";
            });
            modDoc.aDocumentosBoletaManualPagaNo = self.utilController.orderBy(modDoc.aDocumentosBoletaManualPagaNo,"sNroDoc");
            
            modDoc.aDocumentosFacturaOtraSedePagaNo = documentos.filter(function(el){
                return el.flagDocumento == "FacturaOtraSedePagaNo";
            });
            modDoc.aDocumentosFacturaOtraSedePagaNo = self.utilController.orderBy(modDoc.aDocumentosFacturaOtraSedePagaNo,"sNroDoc");

            modDoc.aDocumentosBoletaOtraSedePagaNo = documentos.filter(function(el){
                return el.flagDocumento == "BoletaOtraSedePagaNo";
            });
            modDoc.aDocumentosBoletaOtraSedePagaNo = self.utilController.orderBy(modDoc.aDocumentosBoletaOtraSedePagaNo,"sNroDoc");
            

            //-------- PESTAÑA DE DOCUMENTOS - ARQUEO DE CAJA
            //1.Boletas electrónicas
            modDoc.aDocumentosBoletaElectronica = documentos.filter(function(el){
                return el.flagDocumento == "BoletaElectronica";
            });

            modDoc.aDocumentosBoletaElectronicaxDevolucionParcialEfectivo = documentos.filter(function(el){
                return el.flagDocumento == "DevolucionParcialEfectivo"
                       && el.sCodTipoDocumento == "03"
                       && el.iFactManual ==  0;
            });
            if(modDoc.aDocumentosBoletaElectronicaxDevolucionParcialEfectivo.length>0){
                   modDoc.aDocumentosBoletaElectronica = modDoc.aDocumentosBoletaElectronica.concat( modDoc.aDocumentosBoletaElectronicaxDevolucionParcialEfectivo)  
            }
           

            //modDoc.aDocumentosBoletaElectronica = self.utilController.orderBy(modDoc.aDocumentosBoletaElectronica,"sNroDoc");
             modDoc.aDocumentosBoletaElectronica =
 self.utilController.orderByAscMejoraxMultipleCondiciones(modDoc.aDocumentosBoletaElectronica,"sTipoDocumento","sCorrelativo","sSerie");

            //2.Facturas electrónicas
            modDoc.aDocumentosFacturaElectronica = documentos.filter(function(el){
                return el.flagDocumento == "FacturaElectronica"
            });

            modDoc.aDocumentosFacturaElectronicaxDevolucionParcialEfectivo = documentos.filter(function(el){
                return el.flagDocumento == "DevolucionParcialEfectivo"
                       && el.sCodTipoDocumento == "01"
                       && el.iFactManual ==  0;
            });
            if(modDoc.aDocumentosFacturaElectronicaxDevolucionParcialEfectivo.length>0){
                   modDoc.aDocumentosFacturaElectronica = modDoc.aDocumentosFacturaElectronica.concat( modDoc.aDocumentosFacturaElectronicaxDevolucionParcialEfectivo)  
            }

            
            //modDoc.aDocumentosFacturaElectronica = self.utilController.orderBy(modDoc.aDocumentosFacturaElectronica,"sNroDoc");

            modDoc.aDocumentosFacturaElectronica =
 self.utilController.orderByAscMejoraxMultipleCondiciones(modDoc.aDocumentosFacturaElectronica,"sTipoDocumento","sCorrelativo","sSerie");

            //3.Boletas manuales
            modDoc.aDocumentosBoletaManual = documentos.filter(function(el){
                return el.flagDocumento == "BoletaManual";
            });
            //modDoc.aDocumentosBoletaManual = self.utilController.orderBy(modDoc.aDocumentosBoletaManual,"sNroDoc");
            modDoc.aDocumentosBoletaManual =
 self.utilController.orderByAscMejoraxMultipleCondiciones(modDoc.aDocumentosBoletaManual,"sTipoDocumento","sCorrelativo","sSerie");

            //4.Facturas manuales
            modDoc.aDocumentosFacturaManual = documentos.filter(function(el){
                return el.flagDocumento == "FacturaManual";
            });
            //modDoc.aDocumentosFacturaManual = self.utilController.orderBy(modDoc.aDocumentosFacturaManual,"sNroDoc");
            modDoc.aDocumentosFacturaManual =
 self.utilController.orderByAscMejoraxMultipleCondiciones(modDoc.aDocumentosFacturaManual,"sTipoDocumento","sCorrelativo","sSerie");

            //5.Boletas al crédito
            modDoc.aDocumentosBoletaCredito = documentos.filter(function(el){
                return el.flagDocumento == "BoletaElectronicaPagaNo" ||  el.flagDocumento == "BoletaManualPagaNo";
            });
            //modDoc.aDocumentosBoletaCredito = self.utilController.orderBy(modDoc.aDocumentosBoletaCredito,"sNroDoc");
            modDoc.aDocumentosBoletaCredito =
 self.utilController.orderByAscMejoraxMultipleCondiciones(modDoc.aDocumentosBoletaCredito,"sTipoDocumento","sCorrelativo","sSerie");

            //6.Facturas al crédito  
            modDoc.aDocumentosFacturaCredito = documentos.filter(function(el){
                return el.flagDocumento == "FacturaManualPagaNo" ||  el.flagDocumento == "FacturaElectronicaPagaNo";
            });
            //modDoc.aDocumentosFacturaCredito = self.utilController.orderBy(modDoc.aDocumentosFacturaCredito,"sNroDoc");
            modDoc.aDocumentosFacturaCredito =
 self.utilController.orderByAscMejoraxMultipleCondiciones(modDoc.aDocumentosFacturaCredito,"sTipoDocumento","sCorrelativo","sSerie");

            //7.Documentos anulados
            modDoc.aDocumentosAnulado = documentos.filter(function(el){
                return el.flagDocumento == "Anulado";
            });
            //modDoc.aDocumentosAnulado = self.utilController.orderBy(modDoc.aDocumentosAnulado,"sNroDoc");
            modDoc.aDocumentosAnulado =
 self.utilController.orderByAscMejoraxMultipleCondiciones(modDoc.aDocumentosAnulado,"sTipoDocumento","sCorrelativo","sSerie");
            
            //8.Notas de crédito aplicada
            modDoc.aDocumentosNotaCreditoAplicada = documentos.filter(function(el){
                return el.flagDocumento == "NotaCreditoAplicada";
            });
            //modDoc.aDocumentosNotaCreditoAplicada = self.utilController.orderBy(modDoc.aDocumentosNotaCreditoAplicada,"sNroDoc");
            modDoc.aDocumentosNotaCreditoAplicada =
 self.utilController.orderByAscMejoraxMultipleCondiciones(modDoc.aDocumentosNotaCreditoAplicada,"sTipoDocumento","sCorrelativo","sSerie");

            //9.Notas de crédito Paga No
            modDoc.aDocumentosNotaCreditoEmitidaPagaNo = documentos.filter(function(el){
                return el.flagDocumento == "NotaCreditoEmitidaPagaNoTabDoc";
            });
            //modDoc.aDocumentosNotaCreditoEmitidaPagaNo = self.utilController.orderBy(modDoc.aDocumentosNotaCreditoEmitidaPagaNo,"sNroDoc");
            modDoc.aDocumentosNotaCreditoEmitidaPagaNo =
 self.utilController.orderByAscMejoraxMultipleCondiciones(modDoc.aDocumentosNotaCreditoEmitidaPagaNo,"sTipoDocumento","sCorrelativo","sSerie");

            //10.Comprobantes de otras sedes
            modDoc.aDocumentosComprobantesOtrasSedes = documentos.filter(function(el){
                return el.flagDocumento == "BoletaOtraSedePagaNo" ||  
                el.flagDocumento == "FacturaOtraSedePagaNo"  ||  
                el.flagDocumento == "BoletaOtraSede" ||
                el.flagDocumento == "FacturaOtraSede";
            });
            //modDoc.aDocumentosComprobantesOtrasSedes = self.nsultutilController.orderBy(modDoc.aDocumentosComprobantesOtrasSedes,"sNroDoc");
        modDoc.aDocumentosComprobantesOtrasSedes =
 self.utilController.orderByAscMejoraxMultipleCondiciones(modDoc.aDocumentosComprobantesOtrasSedes,"sTipoDocumento","sCorrelativo","sSerie");
            
            //11.Otros Comprobantes
            modDoc.aDocumentosOtrosComprobantes = documentos.filter(function(el){
                return !(el.flagDocumento == "BoletaElectronica" ||
                el.flagDocumento == "FacturaElectronica" ||
                el.flagDocumento == "BoletaManual" ||
                el.flagDocumento == "FacturaManual" ||
                el.flagDocumento == "BoletaElectronicaPagaNo" ||
                el.flagDocumento == "BoletaManualPagaNo" ||
                el.flagDocumento == "FacturaManualPagaNo" ||
                el.flagDocumento == "FacturaElectronicaPagaNo" ||
                el.flagDocumento == "Anulado" ||
                el.flagDocumento == "NotaCreditoAplicada" ||
                el.flagDocumento == "NotaCreditoEmitidaPagaNoTabDoc" ||
                el.flagDocumento == "BoletaOtraSedePagaNo" ||
                el.flagDocumento == "FacturaOtraSedePagaNo" ||
                el.flagDocumento == "BoletaOtraSede" ||
                el.flagDocumento == "FacturaOtraSede" ||
                el.flagDocumento  == "DevolucionParcialEfectivo")
            });
            //modDoc.aDocumentosOtrosComprobantes = self.utilController.orderBy(modDoc.aDocumentosOtrosComprobantes,"sNroDoc");
            modDoc.aDocumentosOtrosComprobantes =
 self.utilController.orderByAscMejoraxMultipleCondiciones(modDoc.aDocumentosOtrosComprobantes,"sTipoDocumento","sCorrelativo","sSerie");

            return modDoc;
        },
        opcionesImpresion: function() {
            var modData = [{ "iId": null, "sDescripcion": "Seleccione Impresión" }, { "iId": 1, "sDescripcion": "Resumen" }, { "iId": 2, "sDescripcion": "Detalle" }];
            return modData;
        },
        ////END MAESTROS HARDCODE
        ////////////////////////////////////////////////////////////////////////////////////////////////////

    };
});