sap.ui.define([

], function() {
    "use strict";
    return {
        consultarTipoCambio:function(oHorarioCaja){
            var modData = {
                "Filter":[]
            };
            var aFilter = [];
            aFilter.push({ key: "iIdSociedad", operator: "IN", aValue: [oHorarioCaja.iIdSociedad] });
            aFilter.push({ key: "sFechaAux", operator: "<=", value: oHorarioCaja.sFechaAperturaAAAA_MM_DD });
            modData.Filter =aFilter;
            return modData;
        },
        getDateService: function(date) {
            var modFecha = null;
            if (date) {
                modFecha = (date.replace("/Date(", "")).replace(")/", "");
                modFecha = new Date(parseInt(modFecha));
            }
            return modFecha;
        },
        registrarArqueoCartuchera: function(self, data) {
            var miData = self.getView().getModel("miData").getData();
            var FFS = data.aFondoFijoSoles;
            var modData = {};
            ;
            modData.oCartuchera = {
                "iIdSociedad": miData.oHorarioCaja.iIdSociedad,
                "sSociedad": miData.oHorarioCaja.sSociedad,
                "iIdSede": miData.oHorarioCaja.iIdSede,
                "sSede": miData.oHorarioCaja.sSede,
                "iIdArea": miData.oHorarioCaja.iIdArea,
                "sArea": miData.oHorarioCaja.sArea,
                "iIdUsuarioCajero": miData.oHorarioCaja.iIdUsuarioCajero,
                "sUsuarioCajero": miData.oHorarioCaja.sUsuarioCajero,
                "sCodigoCartuchera": data.sNumeroFondoAsignado,
                "iCodApertura": data.iCodApertura,
                "sDesApertura": data.sDesApertura,
                //GNM+++
                "sNumeroFisico": data.sNumeroFisico,
                //GNM+++
                "dFecha": miData.oHorarioCaja.dFechaApertura,
                "fDiferencia": parseFloat(data.fImporteMonedaNacCartuchera) - parseFloat(data.FFSTotalImporte)

            };
            modData.aCartucheraDetalle = [];
            FFS.forEach(function(item) {
                var obj = {
                    "sCodDenominacion": item.sCodDenominacion,
                    "sDenominacion": item.sDenominacion,
                    "sMoneda": item.sMoneda,
                    "fValor": item.fValor,
                    "iCantidadCartuchera": item.iCantidad,
                    "fImporteCartuchera":item.fImporte ? parseFloat(parseFloat(item.fImporte).toFixed(2)) : 0
                }
                modData.aCartucheraDetalle.push(obj);
            });
            return modData;
        },
        registrarArqueoSorpresivo: function(self) {
            var miData = self.getView().getModel("miData").getData();
            var FFS = self.getView().getModel("FFS").getData();
            var aFFS = FFS.aFondoFijoSoles;
            var D = self.getView().getModel("D").getData();
            var aTraslados = self.getView().getModel("T").getProperty("/aTraslados");
            var aCustodios = self.getView().getModel("C").getProperty("/aCustodios");
            var aDocumentos = D.aDocumentos;
            
            /*D.aDocumentosBoletaElectronica.concat(D.aDocumentosFacturaElectronica);
            
            aDocumentos.concat(D.aDocumentosBoletaManual);
            aDocumentos.concat(D.aDocumentosFacturaManual);
            aDocumentos.concat(D.aDocumentosBoletaCredito);
            aDocumentos.concat(D.aDocumentosFacturaCredito);
            aDocumentos.concat(D.aDocumentosAnulado);
            aDocumentos.concat(D.aDocumentosNotaCreditoAplicada);
            aDocumentos.concat(D.aDocumentosNotaCreditoEmitidaPagaNo);
            aDocumentos.concat(D.aDocumentosComprobantesOtrasSedes);
            aDocumentos.concat(D.aDocumentosOtrosComprobantes);*/
            
            var ES = self.getView().getModel("ES").getProperty("/aEfectivoSoles");
            var ED = self.getView().getModel("ED").getProperty("/aEfectivoDolares");
            var resumen = self.getView().getModel("Resumen").getData();
            var RTD = resumen.aResumenTipoDocumento;
            var RMP = resumen.aResumenMedioPago;

            var aDenominaciones = ES.concat(ED);
            var aResumen = RTD.concat(RMP);
            aResumen = aResumen.filter(function(el) {
                return el["sCodDenominacion"];
            });
            var modData = {};
            modData.aArqueoDenominacion = [];
            modData.aArqueoResumen = [];
            modData.aArqueoDocumento = [];
            modData.aArqueoTraslado = [];
            modData.aArqueoCustodio = [];
            modData.aArqueoFFS = [];
            modData.oArqueo = {
                "iIdSociedad": miData.oHorarioCaja.iIdSociedad,
                "sSociedad": miData.oHorarioCaja.sSociedad,
                "iIdSede": miData.oHorarioCaja.iIdSede,
                "sSede": miData.oHorarioCaja.sSede,
                "iIdArea": miData.oHorarioCaja.iIdArea,
                "sArea": miData.oHorarioCaja.sArea,
                "iIdUsuarioSolicitud": miData.oHorarioCaja.iIdUsuarioSupervisor,//miData.oHorarioCaja.iIdUsuarioCajero, //Preguntar
                "sUsuarioSolicitud": miData.oHorarioCaja.sUsuarioSupervisorEstadoAperturado,//miData.oHorarioCaja.sUsuarioCajero, //Preguntar
                "iIdUsuarioCaja": miData.oHorarioCaja.iIdUsuarioCajero,
                "sUsuarioCaja": miData.oHorarioCaja.sUsuarioCajero,
                "sCodApertura": miData.oHorarioCaja.iId,
                //"dFechaApertura": miData.oHorarioCaja.sFechaApertura,
                "dFechaApertura": miData.oHorarioCaja.dFechaApertura,
                "fAjusteRedondeo": resumen.ResumenTotalAjusteRedondeo ? parseFloat(parseFloat(resumen.ResumenTotalAjusteRedondeo).toFixed(2)) : 0,
                "fSobrante": resumen.ResumenTotalSobrante ? parseFloat(parseFloat(resumen.ResumenTotalSobrante).toFixed(2)) : 0,
                "fFaltante": resumen.ResumenTotalFaltante ? parseFloat(parseFloat(resumen.ResumenTotalFaltante).toFixed(2)) : 0,
                "sHoraInicio":miData.oHorarioCaja.sHoraInicio,
                "sHoraFin":miData.oHorarioCaja.sHoraFin,
                "fReposicionFondo":resumen.ResumenReposicionFondo ? parseFloat(parseFloat(resumen.ResumenReposicionFondo).toFixed(2)) : 0

            };
            aDenominaciones.forEach(function(item) {
                modData.aArqueoDenominacion.push({
                    "sCodDenominacion": item.sCodDenominacion,
                    "sDenominacion": item.sDenominacion,
                    "sMoneda": item.sMoneda,
                    "fValor": item.fValor,
                    "iCantidad": item.iCantidad,
                    "fImporte": item.fImporte ? parseFloat(parseFloat(item.fImporte).toFixed(2)): 0,
                    "fTotalEfectivoUSD": D.TotalEfectivoUSD ? parseFloat(parseFloat(D.TotalEfectivoUSD).toFixed(2)): 0,
                    "fTotalEfectivoPEN": D.TotalEfectivoPEN ? parseFloat(parseFloat(D.TotalEfectivoPEN).toFixed(2)): 0
                })
            });
            aResumen.forEach(function(item) {
                modData.aArqueoResumen.push({
                    "sCodTipoIngreso": "1",
                    "sTipoIngreso": "2",
                    "sCodDenominacionDoc": item.sCodDenominacion,
                    "sDenominacionDoc": item.sDenominacion,
                    "iCantidad": item.iCantidad,
                    "fImporte": item.fImporte ? parseFloat(parseFloat(item.fImporte).toFixed(2)): 0,
                    "iCorrelativo": item.iCorrelativo
                })
            });
            
            aDocumentos.forEach(function(item){
                modData.aArqueoDocumento.push({
                    //"iId": item.iId,
                    "iItem" :   item.iItem,
                    "iIdDocumento": item.iIdDocumento,
                    "sTipoDocumento": item.sTipoDocumento,
                    "sNumeroSunat": item.sNumeroSunat,
                    "sPaciente": item.sPaciente,
                    "sCliente": item.sCliente,
                    "sEstado": item.sEstado,
                    "sTipoDocumentoIdentidad": item.sTipoDocumentoIdentidad,
                    "sNumeroIdentidad": item.sNumeroIdentidad,
                    "sMoneda": item.sMoneda,
                    "fTipoCambio": item.fTipoCambio,
                    "fImporte": item.fImporte,
                    "sMotivoAnulacion": item.sMotivoAnulacion,
                    "sFormaPago": item.sFormaPago,
                    "sUsuarioAnulador": item.sUsuarioAnulador,
                    "sFechaAnulacion": item.sFechaAnulacion,
                    "sMotivo": item.sMotivo,
                    "sObservacion": item.sObservacion,
                    "flagDocumento":item.flagDocumento
                    
                })
            });
            aFFS.forEach(function(item) {
                modData.aArqueoFFS.push({
                    "sCodDenominacion": item.sCodDenominacion,
                    "sDenominacion": item.sDenominacion,
                    "sMoneda": item.sMoneda,
                    "fValor": item.fValor,
                    "iCantidad": item.iCantidad,
                    "fImporte": item.fImporte ? parseFloat(parseFloat(item.fImporte).toFixed(2)): 0,
                    "sNumeroFondoAsignado": FFS.sNumeroFondoAsignado
                })
            });
            aTraslados.forEach(function(item) {
                modData.aArqueoTraslado.push({
                    "iItem": item.iItem,
                    "sCodigo": item.sCodigo,
                    "sFecha": item.sFecha,
                    "fImportePEN": item.fImportePEN ? parseFloat(parseFloat(item.fImportePEN).toFixed(2)): 0,
                    "fImporteUSD": item.fImporteUSD ? parseFloat(parseFloat(item.fImporteUSD).toFixed(2)): 0
                })
            });
            aCustodios.forEach(function(item) {
                modData.aArqueoCustodio.push({
                    "iItem": item.iItem,
                    "sCodigo": item.sCodigo,
                    "sFecha": item.sFecha,
                    "sEstado": item.sEstado,
                    "sConcepto": item.sConcepto,
                    "sUsuarioCustodia": item.sUsuarioCustodia,
                    "fImportePEN": item.fImportePEN ? parseFloat(parseFloat(item.fImportePEN).toFixed(2)): 0,
                    "fImporteUSD": item.fImporteUSD ? parseFloat(parseFloat(item.fImporteUSD).toFixed(2)): 0
                })
            });
            return modData;
        },
        registrarArqueoVenta: function(self) {
            
            var miData = self.getView().getModel("miData").getData();
            var FFS = self.getView().getModel("FFS").getProperty("/aFondoFijoSoles");
            var ES = self.getView().getModel("ES").getProperty("/aEfectivoSoles");
            var ED = self.getView().getModel("ED").getProperty("/aEfectivoDolares");
            var resumen = self.getView().getModel("Resumen").getData();
            var FFS = self.getView().getModel("FFS").getData();
            var aFFS = FFS.aFondoFijoSoles;
            var D = self.getView().getModel("D").getData();
            var aTraslados = self.getView().getModel("T").getProperty("/aTraslados");
            var aCustodios = self.getView().getModel("C").getProperty("/aCustodios");
            var aDocumentos = D.aDocumentos;
            var RTD = resumen.aResumenTipoDocumento;
            var RMP = resumen.aResumenMedioPago;

            var aDenominaciones = ES.concat(ED);
            var aResumen = RTD.concat(RMP);
            aResumen = aResumen.filter(function(el) {
                return el["sCodDenominacion"];
            });
            var modData = {};
            modData.aArqueoDenominacion = [];
            modData.aArqueoResumen = [];
            modData.aArqueoTraslado = [];
            modData.aArqueoCustodio = [];
            modData.oArqueo = {
                "iIdSociedad": miData.oHorarioCaja.iIdSociedad,
                "sSociedad": miData.oHorarioCaja.sSociedad,
                "iIdSede": miData.oHorarioCaja.iIdSede,
                "sSede": miData.oHorarioCaja.sSede,
                "iIdArea": miData.oHorarioCaja.iIdArea,
                "sArea": miData.oHorarioCaja.sArea,
                "iIdUsuarioSolicitud": miData.oHorarioCaja.iIdUsuarioCajero, //Preguntar
                "sUsuarioSolicitud": miData.oHorarioCaja.sUsuarioCajero, //Preguntar
                "iIdUsuarioCaja": miData.oHorarioCaja.iIdUsuarioCajero,
                "sUsuarioCaja": miData.oHorarioCaja.sUsuarioCajero,
                "sCodApertura": miData.oHorarioCaja.iId,
                //"dFechaApertura": miData.oHorarioCaja.sFechaApertura,
                "dFechaApertura": miData.oHorarioCaja.dFechaApertura,
                "fAjusteRedondeo": resumen.ResumenTotalAjusteRedondeo ? parseFloat(parseFloat(resumen.ResumenTotalAjusteRedondeo).toFixed(2)): 0 ,
                "fSobrante": resumen.ResumenTotalSobrante ? parseFloat(parseFloat(resumen.ResumenTotalSobrante).toFixed(2)): 0, // resumen.ResumenTotalAjusteRedondeo ? parseFloat(resumen.ResumenTotalAjusteRedondeo): 0,
                "fFaltante": resumen.ResumenTotalFaltante ? parseFloat(parseFloat(resumen.ResumenTotalFaltante).toFixed(2)): 0,
                "sHoraInicio":miData.oHorarioCaja.sHoraInicio,
                "sHoraFin":miData.oHorarioCaja.sHoraFin,
                "fReposicionFondo": resumen.ResumenReposicionFondo ? parseFloat(parseFloat(resumen.ResumenReposicionFondo).toFixed(2)): 0
            };
            aDenominaciones.forEach(function(item) {
                modData.aArqueoDenominacion.push({
                    "sCodDenominacion": item.sCodDenominacion,
                    "sDenominacion": item.sDenominacion,
                    "sMoneda": item.sMoneda,
                    "fValor": item.fValor,
                    "iCantidad": item.iCantidad,
                    "fImporte": item.fImporte ? parseFloat(parseFloat(item.fImporte).toFixed(2)): 0,
                    "fTotalEfectivoUSD": D.TotalEfectivoUSD ? parseFloat(parseFloat(D.TotalEfectivoUSD).toFixed(2)): 0,
                    "fTotalEfectivoPEN": D.TotalEfectivoPEN ? parseFloat(parseFloat(D.TotalEfectivoPEN).toFixed(2)): 0
                })
            });
            aResumen.forEach(function(item) {
                modData.aArqueoResumen.push({
                    "sTipoAgrupador"  :item.sTipoAgrupador,
                    "sCodTipoIngreso": "1",
                    "sTipoIngreso": "2",
                    "sCodDenominacionDoc": item.sCodDenominacion,
                    "sDenominacionDoc": item.sDenominacion,
                    "iCantidad": item.iCantidad,
                    "fImporte": item.fImporte ? parseFloat(parseFloat(item.fImporte).toFixed(2)): 0,
                    "iCorrelativo": item.iCorrelativo
                })
            });
            /*aDocumentos.forEach(function(item){
                modData.aArqueoDocumento.push({
                    //"iId": item.iId,
                    "iItem" :   item.iItem,
                    "iIdDocumento": item.iIdDocumento,
                    "sTipoDocumento": item.sTipoDocumento,
                    "sNumeroSunat": item.sNumeroSunat,
                    "sPaciente": item.sPaciente,
                    "sCliente": item.sCliente,
                    "sEstado": item.sEstado,
                    "sTipoDocumentoIdentidad": item.sTipoDocumentoIdentidad,
                    "sNumeroIdentidad": item.sNumeroIdentidad,
                    "sMoneda": item.sMoneda,
                    "fTipoCambio": item.fTipoCambio,
                    "fImporte": item.fImporte,
                    "sMotivoAnulacion": item.sMotivoAnulacion,
                    "sFormaPago": item.sFormaPago,
                    "sUsuarioAnulador": item.sUsuarioAnulador,
                    "sFechaAnulacion": item.sFechaAnulacion,
                    "sMotivo": item.sMotivo,
                    "sObservacion": item.sObservacion,
                    "flagDocumento":item.flagDocumento
                    
                })
            });
            aFFS.forEach(function(item) {
                modData.aArqueoFFS.push({
                    "sCodDenominacion": item.sCodDenominacion,
                    "sDenominacion": item.sDenominacion,
                    "sMoneda": item.sMoneda,
                    "fValor": item.fValor,
                    "iCantidad": item.iCantidad,
                    "fImporte": item.fImporte ? parseFloat(item.fImporte): 0,
                    "sNumeroFondoAsignado": FFS.sNumeroFondoAsignado
                })
            });
            aTraslados.forEach(function(item) {
                modData.aArqueoTraslado.push({
                    "iItem": item.iItem,
                    "sCodigo": item.sCodigo,
                    "sFecha": item.sFecha,
                    "fImportePEN": item.fImportePEN ? parseFloat(item.fImportePEN): 0,
                    "fImporteUSD": item.fImporteUSD ? parseFloat(item.fImporteUSD): 0
                })
            });
            aCustodios.forEach(function(item) {
                modData.aArqueoCustodio.push({
                    "iItem": item.iItem,
                    "sCodigo": item.sCodigo,
                    "sFecha": item.sFecha,
                    "sEstado": item.sEstado,
                    "sConcepto": item.sConcepto,
                    "sUsuarioCustodia": item.sUsuarioCustodia,
                    "fImportePEN": item.fImportePEN ? parseFloat(item.fImportePEN): 0,
                    "fImporteUSD": item.fImporteUSD ? parseFloat(item.fImporteUSD): 0
                })
            });*/
            /*modData.Detalle={
            	"iIdSociedad":miData.iIdSociedad,
            	"sSociedad":miData.sSociedad,
            	"sNombreColaborador":miData.sNombreColaborador,
            	"sIdUsuario":miData.sIdUsuario,
            	"iIdSede":miData.iIdSede,
            	"sSede":miData.sSede,
            	"iIdArea":miData.iIdArea,
            	"sArea":miData.sArea,
            	"iIdSupervisor":miData.iIdSupervisor,
            	"sSupervisor":miData.sSupervisor,
            	"iIdEstado":miData.iIdEstado,
            	"sEstado":miData.sEstado
            };
            modData.aFondoFijoSoles=[];
            modData.aEfectivoSoles=[];
            modData.aEfectivoDolares=[];
            FFS.forEach(function(item){
            	var obj ={
            		"sCodDenominacion":item.sCodDenominacion,
            		"sDenominacion":item.sDenominacion,
            		"sMoneda":item.sMoneda,
            		"fValor":item.fValor,
            		"iCantidadCartuchera":item.iCantidad,
            		"fImporteCartuchera":item.fImporte
            	}
            	modData.aFondoFijoSoles.push(obj);
            });
            ES.forEach(function(item){
            	var obj ={
            		"sCodDenominacion":item.sCodDenominacion,
            		"sDenominacion":item.sDenominacion,
            		"sMoneda":item.sMoneda,
            		"fValor":item.fValor,
            		"iCantidadCartuchera":item.iCantidad,
            		"fImporteCartuchera":item.fImporte
            	}
            	modData.aEfectivoSoles.push(obj);
            });
            ED.forEach(function(item){
            	var obj ={
            		"sCodDenominacion":item.sCodDenominacion,
            		"sDenominacion":item.sDenominacion,
            		"sMoneda":item.sMoneda,
            		"fValor":item.fValor,
            		"iCantidadCartuchera":item.iCantidad,
            		"fImporteCartuchera":item.fImporte
            	}
            	modData.aEfectivoDolares.push(obj);
            });*/
            aTraslados.forEach(function(item) {
                modData.aArqueoTraslado.push({
                    "iItem": item.iItem,
                    "sCodigo": item.sCodigo,
                    "sFecha": item.sFecha,
                    "fImportePEN": item.fImportePEN ? parseFloat(parseFloat(item.fImportePEN).toFixed(2)): 0,
                    "fImporteUSD": item.fImporteUSD ? parseFloat(parseFloat(item.fImporteUSD).toFixed(2)): 0
                })
            });
            aCustodios.forEach(function(item) {
                modData.aArqueoCustodio.push({
                    "iItem": item.iItem,
                    "sCodigo": item.sCodigo,
                    "sFecha": item.sFecha,
                    "sEstado": item.sEstado,
                    "sConcepto": item.sConcepto,
                    "sUsuarioCustodia": item.sUsuarioCustodia,
                    "fImportePEN": item.fImportePEN ? parseFloat(parseFloat(item.fImportePEN).toFixed(2)): 0,
                    "fImporteUSD": item.fImporteUSD ? parseFloat(parseFloat(item.fImporteUSD).toFixed(2)): 0
                })
            });
            return modData;
        },
        actualizarArqueoVenta: function(self) {
            
            var miData = self.getView().getModel("miData").getData();
            var usuario = self.getView().getModel("localModel").getProperty("/Usuario");
            var FFS = self.getView().getModel("FFS").getProperty("/aFondoFijoSoles");
            var ES = self.getView().getModel("ES").getProperty("/aEfectivoSoles");
            var ED = self.getView().getModel("ED").getProperty("/aEfectivoDolares");
            var resumen = self.getView().getModel("Resumen").getData();
            var oArqueo = self.getView().getModel("localModel").getProperty("/oArqueo");
            var RTD = resumen.aResumenTipoDocumento;
            var RMP = resumen.aResumenMedioPago;

            var aDenominaciones = ES.concat(ED);
            var aResumen = RTD.concat(RMP);
            var modData = {};
            modData.iId = oArqueo.iId;
            modData.aArqueoDenominacion = [];
            modData.aArqueoResumen = [];
            modData.oArqueo = {
                "iId": oArqueo.iId,
                "fAjusteRedondeo": resumen.ResumenTotalAjusteRedondeo ? parseFloat(resumen.ResumenTotalAjusteRedondeo): 0,
                "fSobrante": resumen.ResumenTotalSobrante ? parseFloat(resumen.ResumenTotalSobrante): 0,
                "fFaltante": resumen.ResumenTotalFaltante ? parseFloat(resumen.ResumenTotalFaltante): 0
            };
            aDenominaciones.forEach(function(item) {
                if(item.iId){
                    modData.aArqueoDenominacion.push({
                        "iId": item.iId,
                        "iCantidad": item.iCantidad,
                        "fImporte": item.fImporte ? parseFloat(item.fImporte): 0
                    });
                }
            });
            aResumen.forEach(function(item) {
                if(item.iId){
                    modData.aArqueoResumen.push({
                        "iId": item.iId,
                        "iCantidad": item.iCantidad,
                        "fImporte": item.fImporte ? parseFloat(item.fImporte): 0
                    });
                }
            });
            return modData;
        },
        registrarCustodio: function(self) {
            var miData = self.utilController.unlink(self.getView().getModel("miData").getData());
            var custodio = self.utilController.unlink(self.getView().getModel("arqueoSorpresivo").getData());
            
            var modData = {
                oBovedaCustodia: {
                    "sCodApertura": miData.oHorarioCaja.iId,
                    "iIdSociedad": miData.oHorarioCaja.iIdSociedad,
                    "sSociedad": miData.oHorarioCaja.sSociedad,
                    "iIdSede": miData.oHorarioCaja.iIdSede,
                    "sSede": miData.oHorarioCaja.sSede,
                    "iIdArea": miData.oHorarioCaja.iIdArea,
                    "sArea": miData.oHorarioCaja.sArea,
                    "iIdUsuarioCajero": miData.oHorarioCaja.iIdUsuarioCajero,
                    "sUsuarioCajero": miData.oHorarioCaja.sUsuarioCajero,
                    "sNombreCajero": miData.oHorarioCaja.sNombreUsuarioCajero,
                    "iIdConcepto": custodio.iIdConcepto,
                    "sCodConcepto": custodio.sCodConcepto,
                    "sConcepto": custodio.sConcepto,
                    "sFecha": self.utilController.formatFechaAAAAMMDDHora(new Date()),//this.parseDate(miData.oHorarioCaja.sFechaApertura),
                    "sImportePEN": custodio.fImportePEN ? custodio.fImportePEN : null,
                    "sImporteUSD": custodio.fImporteUSD ? custodio.fImporteUSD : null,
                    "sObservacion": custodio.sObservacion ? custodio.sObservacion : ""
                }
            };
            return modData;
        },
        parseDate: function (date) {
            var tmDate = new Date();
            var hour = tmDate.getTimezoneOffset() / 60;
            var aDate = date.split("/");
            return aDate[2] + "-" + aDate[1] + "-" + aDate[0] + " " + hour + ":00";
        },      
        actualizarDocumento: function(data) {
            var modData = {
                "iId": data.iId,
                "iIdTipoTarjeta": data.iIdTipoTarjeta,
                "sNumeroTarjeta": data.sNumeroTarjeta,
                "sNumeroReferencia": data.sNumeroReferencia,
                "iIdTransaccion": data.iIdTransaccion,
                "sIdTransaccion": data.sIdTransaccion
            };
            return modData;
        },
        mapPDFCabecera:function(self){
            var miData = self.utilController.unlink(self.getView().getModel("miData").getData());
            var FFS = self.utilController.unlink(self.getView().getModel("FFS").getData());
            var usuario = self.dataPass.Usuario;
            var modData = {};
            modData.Titulo = "ReporteStock"
            modData.sSociedad = miData.oHorarioCaja.sSociedad;
            modData.sNombreColaborador = miData.oHorarioCaja.sUsuarioCajero;
            modData.sIdUsuario = miData.oHorarioCaja.sNumDocumentoCajero; //miData.sIdUsuario;
            modData.sSede = miData.oHorarioCaja.sSede;
            modData.sArea = miData.oHorarioCaja.sArea;
            modData.sSupervisor = miData.oHorarioCaja.sNombreSupervisor;
            modData.sEstado = miData.oHorarioCaja.sDesEstadoCaja; //miData.sEstado;
            modData.sFirmaCajaCentral = "Firma y Sello de Cajero/Adminisionista"; //miData.sFirmaCajaCentral;
            modData.sFirmaNombre = miData.oHorarioCaja.sNombreUsuarioCajero; //miData.sFirmaNombre;
            modData.sDNI = miData.oHorarioCaja.sNumDocumentoCajero;//usuario.sNumDocumento; //miData.sDNI;
            modData.sTipoDocumento = miData.oHorarioCaja.sTipoDocumentoCajero; //miData.sDNI;
            modData.sFirmaCajaGeneral = "FIRMA CAJA GENERAL"; //miData.sFirmaCajaGeneral;
            modData.sFechaApertura = miData.oHorarioCaja.sFechaApertura;
            modData.sRangoFecha = self.formatter.horaApertura(miData.oHorarioCaja.sHoraInicio,miData.oHorarioCaja.sHoraFin);
            modData.sNumeroFondoAsignado = FFS.sNumeroFondoAsignado;
            return modData;
        },
        mapPDFDocumentos: function(self,modData){
            var D = self.utilController.unlink(self.getView().getModel("D").getData());
            modData.aDocumentos = self.utilController.convertArrayToStringPDF(D.aDocumentos);
            modData.aDocumentosBoletaElectronica = self.utilController.convertArrayToStringPDF(D.aDocumentosBoletaElectronica);
            modData.aDocumentosFacturaElectronica = self.utilController.convertArrayToStringPDF(D.aDocumentosFacturaElectronica);
            modData.aDocumentosBoletaManual = self.utilController.convertArrayToStringPDF(D.aDocumentosBoletaManual);
            modData.aDocumentosFacturaManual = self.utilController.convertArrayToStringPDF(D.aDocumentosFacturaManual);
            modData.aDocumentosBoletaCredito = self.utilController.convertArrayToStringPDF(D.aDocumentosBoletaCredito);
            modData.aDocumentosFacturaCredito = self.utilController.convertArrayToStringPDF(D.aDocumentosFacturaCredito);
            modData.aDocumentosAnulado = self.utilController.convertArrayToStringPDF(D.aDocumentosAnulado);
            modData.aDocumentosNotaCreditoEmitidaPagaNo = self.utilController.convertArrayToStringPDF(D.aDocumentosNotaCreditoEmitidaPagaNo);
            modData.aDocumentosComprobantesOtrasSedes = self.utilController.convertArrayToStringPDF(D.aDocumentosComprobantesOtrasSedes);
            modData.aDocumentosOtrosComprobantes = self.utilController.convertArrayToStringPDF(D.aDocumentosOtrosComprobantes);



            modData.aDocumentosNotaCreditoEmitida = self.utilController.convertArrayToStringPDF(D.aDocumentosNotaCreditoEmitida);
            modData.aDocumentosBoletaOtraSede = self.utilController.convertArrayToStringPDF(D.aDocumentosBoletaOtraSede);
            modData.aDocumentosFacturaOtraSede = self.utilController.convertArrayToStringPDF(D.aDocumentosFacturaOtraSede);
            modData.aDocumentosDevolucionTotalEfectivo = self.utilController.convertArrayToStringPDF(D.aDocumentosDevolucionTotalEfectivo);
            modData.aDocumentosNotaCreditoAplicada = self.utilController.convertArrayToStringPDF(D.aDocumentosNotaCreditoAplicada);
            modData.aDocumentosOtros = self.utilController.convertArrayToStringPDF(D.aDocumentosOtros);
            modData.TotalImporteBoletaElectronica = self.formatter.FormatterDinero(D.TotalImporteBoletaElectronica);//(parseFloat(D.TotalImporteBoletaElectronica)).toFixed(2);
            modData.TotalImporteFacturaElectronica = self.formatter.FormatterDinero(D.TotalImporteFacturaElectronica);//(parseFloat(D.TotalImporteFacturaElectronica)).toFixed(2);
            modData.TotalImporteBoletaManual = self.formatter.FormatterDinero(D.TotalImporteBoletaManual);//(parseFloat(D.TotalImporteBoletaManual)).toFixed(2);
            modData.TotalImporteFacturaManual = self.formatter.FormatterDinero(D.TotalImporteFacturaManual);//(parseFloat(D.TotalImporteFacturaManual)).toFixed(2);

            modData.TotalImporteBoletaCredito = self.formatter.FormatterDinero(D.TotalImporteBoletaCredito);//(parseFloat(D.TotalImporteBoletaCredito)).toFixed(2);
            modData.TotalImporteFacturaCredito = self.formatter.FormatterDinero(D.TotalImporteFacturaCredito);//(parseFloat(D.TotalImporteFacturaCredito)).toFixed(2);
            modData.TotalImporteAnulado = self.formatter.FormatterDinero(D.TotalImporteAnulado);//(parseFloat(D.TotalImporteAnulado)).toFixed(2);
            modData.TotalImporteNotaCreditoEmitidaPagaNo = self.formatter.FormatterDinero(D.TotalImporteNotaCreditoEmitidaPagaNo);//(parseFloat(D.TotalImporteNotaCreditoEmitidaPagaNo)).toFixed(2);
            modData.TotalImporteComprobantesOtrasSedes = self.formatter.FormatterDinero(D.TotalImporteComprobantesOtrasSedes);//(parseFloat(D.TotalImporteComprobantesOtrasSedes)).toFixed(2);
            modData.TotalImporteOtrosComprobantes = self.formatter.FormatterDinero(D.TotalImporteOtrosComprobantes);//(parseFloat(D.TotalImporteOtrosComprobantes)).toFixed(2);


            modData.TotalImporteNotaCreditoEmitida = self.formatter.FormatterDinero(D.TotalImporteNotaCreditoEmitida);//(parseFloat(D.TotalImporteNotaCreditoEmitida)).toFixed(2);
            modData.TotalImporteBoletaOtraSede = self.formatter.FormatterDinero(D.TotalImporteBoletaOtraSede);//(parseFloat(D.TotalImporteBoletaOtraSede)).toFixed(2);
            modData.TotalImporteFacturaOtraSede = self.formatter.FormatterDinero(D.TotalImporteFacturaOtraSede);//(parseFloat(D.TotalImporteFacturaOtraSede)).toFixed(2);
            modData.TotalImporteDevolucionTotalEfectivo = self.formatter.FormatterDinero(D.TotalImporteDevolucionTotalEfectivo);//(parseFloat(D.TotalImporteDevolucionTotalEfectivo)).toFixed(2);
            modData.TotalImporteNotaCreditoAplicada = self.formatter.FormatterDinero(D.TotalImporteNotaCreditoAplicada);//(parseFloat(D.TotalImporteNotaCreditoAplicada)).toFixed(2);
            modData.TotalImporteOtros = self.formatter.FormatterDinero(D.TotalImporteOtros);//(parseFloat(D.TotalImporteOtros)).toFixed(2);
            
            ///INICIO ÚLTIMO TIPO DOCUMENTO////
            modData.ultimoTipoDocumento  = modData.aDocumentosBoletaElectronica.length>0 ? self.utilController.i18n("txtD1"):modData.ultimoTipoDocumento;
            modData.ultimoTipoDocumento  = modData.aDocumentosFacturaElectronica.length>0 ? self.utilController.i18n("txtD2"):modData.ultimoTipoDocumento;
            modData.ultimoTipoDocumento  = modData.aDocumentosBoletaManual.length>0 ? self.utilController.i18n("txtD3"):modData.ultimoTipoDocumento;
            modData.ultimoTipoDocumento  = modData.aDocumentosFacturaManual.length>0 ? self.utilController.i18n("txtD4"):modData.ultimoTipoDocumento;
            modData.ultimoTipoDocumento  = modData.aDocumentosBoletaCredito.length>0 ? self.utilController.i18n("txtD5"):modData.ultimoTipoDocumento;
            modData.ultimoTipoDocumento  = modData.aDocumentosFacturaCredito.length>0 ? self.utilController.i18n("txtD6"):modData.ultimoTipoDocumento;
            modData.ultimoTipoDocumento  = modData.aDocumentosAnulado.length>0 ? self.utilController.i18n("txtD7"):modData.ultimoTipoDocumento;
            modData.ultimoTipoDocumento  = modData.aDocumentosNotaCreditoAplicada.length>0 ? self.utilController.i18n("txtD8"):modData.ultimoTipoDocumento;
            modData.ultimoTipoDocumento  = modData.aDocumentosNotaCreditoEmitidaPagaNo.length>0 ? self.utilController.i18n("txtD9"):modData.ultimoTipoDocumento;
            modData.ultimoTipoDocumento  = modData.aDocumentosComprobantesOtrasSedes.length>0 ? self.utilController.i18n("txtD10"):modData.ultimoTipoDocumento;
            modData.ultimoTipoDocumento  = modData.aDocumentosOtrosComprobantes.length>0 ? self.utilController.i18n("txtD11"):modData.ultimoTipoDocumento;			
			///END ÚLTIMO TIPO DOCUMENTO////
            return modData;
        },
        mapPDFFFS:function(self,modData){
            var FFS = self.utilController.unlink(self.getView().getModel("FFS").getData());
            modData.FFSTotalCantidad = FFS.FFSTotalCantidad;
            modData.FFSTotalImporte = self.formatter.FormatterDinero(FFS.FFSTotalImporte);
            modData.aFondoFijoSoles = self.utilController.convertArrayToStringPDF(FFS.aFondoFijoSoles);
            return modData;
        },
        mapPDFES:function(self,modData){
            var ES = self.utilController.unlink(self.getView().getModel("ES").getData());
            modData.ESTotalCantidad = ES.ESTotalCantidad;
            modData.ESTotalImporte = self.formatter.FormatterDinero(ES.ESTotalImporte);
            modData.aEfectivoSoles = self.utilController.convertArrayToStringPDF(ES.aEfectivoSoles);
            return modData;
        },
        mapPDFED:function(self,modData){
            var ED = self.utilController.unlink(self.getView().getModel("ED").getData());
            modData.EDTotalCantidad = ED.EDTotalCantidad;
            modData.EDTotalImporte = self.formatter.FormatterDinero(ED.EDTotalImporte);
            modData.aEfectivoDolares = self.utilController.convertArrayToStringPDF(ED.aEfectivoDolares);
            return modData;
        },
        mapPDFTraslados:function(self,modData){
            var T = self.utilController.unlink(self.getView().getModel("T").getData());
            modData.aTraslados = self.utilController.convertArrayToStringPDF(T.aTraslados);
            return modData;
        },
        mapPDFCustodios:function(self,modData){
            var C = self.utilController.unlink(self.getView().getModel("C").getData());
            modData.aCustodios = self.utilController.convertArrayToStringPDF(C.aCustodios);
            return modData;
        },
        mapPDFResumen:function(self,modData){
            var resumen = self.utilController.unlink(self.getView().getModel("Resumen").getData());
            resumen = resumen ? resumen : [];
            modData.RTDTotalCantidad = resumen.RTDTotalCantidad;
            modData.RTDTotalImporte = self.formatter.FormatterDinero(resumen.RTDTotalImporte);
            modData.aResumenTipoDocumento = self.utilController.convertArrayToStringPDF(resumen.aResumenTipoDocumento);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            modData.RMPTotalCantidad = resumen.RMPTotalCantidad;
            modData.RMPTotalImporte = self.formatter.FormatterDinero(resumen.RMPTotalImporte);
            modData.aResumenMedioPago = self.utilController.convertArrayToStringPDF(resumen.aResumenMedioPago);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            modData.ResumenTotalAjusteRedondeo = self.formatter.FormatterDinero(resumen.ResumenTotalAjusteRedondeo); //modData.RTDTotalImporte
            modData.ResumenTotalSobrante = self.formatter.FormatterDinero(resumen.ResumenTotalSobrante);
            modData.ResumenTotalFaltante = self.formatter.FormatterDinero(resumen.ResumenTotalFaltante);
            modData.ResumenReposicionFondo = self.formatter.FormatterDinero(resumen.ResumenReposicionFondo);
            return modData;
        },
        mapPDFReporteStockResumen: function(self) {
            var modData = {};
            ///INICIO CABECERA
            modData = this.mapPDFCabecera(self);
            ///END CABECERA
            ///INICIO FONDO FIJO SOLES
            modData = this.mapPDFFFS(self,modData);
            ///END FONDO FIJO SOLES
            ///INICIO EFECTIVO SOLES
            modData = this.mapPDFES(self,modData);
            ///END EFECTIVO SOLES
            ///INICIO EFECTIVO DOLARES
            modData = this.mapPDFED(self,modData);
            ///END EFECTIVO DOLARES
            ///INICIO DOCUMENTOS
            modData = this.mapPDFDocumentos(self,modData);
            ///END DOCUMENTOS
            ///INICIO TRASLADOS
            modData = this.mapPDFTraslados(self,modData);
            ///END TRASLADOS
            ///INICIO CUSTODIOS
            modData = this.mapPDFCustodios(self,modData);
            ///END CUSTODIOS
            ///INICIO RESUMEN
            modData = this.mapPDFResumen(self,modData);
            ///END RESUMEN
            modData = self.utilController.convertObjectToStringPDF(modData);
            return modData;
        },
        mapPDFReporteStockDetalle: function(self) {
            var modData = {};
            ///INICIO CABECERA
            modData = this.mapPDFCabecera(self);
            ///END CABECERA
            ///INICIO FONDO FIJO SOLES
            modData = this.mapPDFFFS(self,modData);
            ///END FONDO FIJO SOLES
            ///INICIO EFECTIVO SOLES
            modData = this.mapPDFES(self,modData);
            ///END EFECTIVO SOLES
            ///INICIO EFECTIVO DOLARES
            modData = this.mapPDFED(self,modData);
            ///END EFECTIVO DOLARES
            ///INICIO DOCUMENTOS
            modData = this.mapPDFDocumentos(self,modData);
            ///END DOCUMENTOS
            ///INICIO TRASLADOS
            modData = this.mapPDFTraslados(self,modData);
            ///END TRASLADOS
            ///INICIO CUSTODIOS
            modData = this.mapPDFCustodios(self,modData);
            ///END CUSTODIOS
            ///INICIO RESUMEN
            modData = this.mapPDFResumen(self,modData);
            ///END RESUMEN
            modData = self.utilController.convertObjectToStringPDF(modData);
            return modData;
        },
        login: function(self) {
            var login = self.getView().getModel("login").getData(); //debe ser un usuario con rol de caja central
            var miData = self.getView().getModel("miData").getData();

            var sClave = login.sUsuario + ":" + login.sPassword;
            var modData = {
                sClave: btoa(sClave), //self.utilHTTP.encode(sClave),//"am9zZS52aWxsYW51ZXZhOlBlcnUxMjM0sNTYuLg==",//usuario + ":" + clave
                iIdUsuarioCajero: miData.oHorarioCaja.iIdUsuarioCajero,
                iIdSociedad: miData.oHorarioCaja.iIdSociedad,
                iIdSede: miData.oHorarioCaja.iIdSede,
                iIdArea: miData.oHorarioCaja.iIdArea
            }
            return modData;
        },
        loginSupervisor: function(self) {
            var login = self.getView().getModel("login").getData(); //debe ser un usuario con rol de caja central
            var miData = self.getView().getModel("miData").getData();
            var oHorarioCaja = self.getView().getModel("miData").getProperty("/oHorarioCaja");
            var sClave = login.sUsuario + ":" + login.sPassword;
            
            var modData = {
                sClave: btoa(sClave), //self.utilHTTP.encode(sClave),//"am9zZS52aWxsYW51ZXZhOlBlcnUxMjM0sNTYuLg==",//usuario + ":" + clave
                iIdUsuarioCajero: miData.oHorarioCaja.iIdUsuarioCajero,
                iIdSociedad: miData.oHorarioCaja.iIdSociedad,
                iIdSede: miData.oHorarioCaja.iIdSede,
                iIdArea: miData.oHorarioCaja.iIdArea,
                sDiaApertura: (oHorarioCaja.dFechaApertura).getTime()
            }
            return modData;
        },
        consultarDocumentoSunatMasivo : function(self,aValidacionPinpad,mapResponsePago) {
            console.log(aValidacionPinpad);
            var modData = {};
            var aFilter = []
            var aNumReferencia = ["F054-00000285","BO08-88888818"];
            aValidacionPinpad.forEach(element => {
                aNumReferencia.push(element.sNumReferencia)  
            });
            aFilter.push({ key: "sNroDoc", operator: "IN", aValue: aNumReferencia })
            modData.Filter = aFilter;
            modData.pagos = mapResponsePago;
            return modData;

        },

        consultarTipoCambioMasivo : function(self){
               console.log(self);
            var modData = {};
            var aFilter = []
            var aFecha = [];
            
            aFilter.push({ key: "sNroDoc", operator: "IN", aValue: aFecha })
            modData.Filter = aFilter;
            return modData; 
        },
        eliminarCustodios: function (data) {
			var modData = {
				aItems:[]
            };
            data.forEach(function(item){
                var obj = {
                    "iId":item.iId
                }
                modData.aItems.push(obj)
            });
			return modData;
		},

    };
});