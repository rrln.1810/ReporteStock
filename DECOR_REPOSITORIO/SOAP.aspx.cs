﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
namespace DECOR_REPOSITORIO
{
    public partial class SOAP : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string pRetval = "{\"oAuditResponse\":{\"iCode\": {0}, \"sMessage\":\"{1}\",\"sIdTransaction\":\"{2}\" },\"oData\":{3}}";
            string json = "";
            try{
                //******INICIO SERVICIO LOGIN**********************************************************************************************//
                if (Request.Params["Accion"]=="Login")//POST
                {
                    LOGIN_STOCKWs.ZSSD_INPUT_LOGIN_STOCK inputLOGIN_STOCK = new LOGIN_STOCKWs.ZSSD_INPUT_LOGIN_STOCK();
                    inputLOGIN_STOCK.USUARIO = Request.Params["Usuario"];
                    inputLOGIN_STOCK.PASSWORD = Request.Params["Password"];
                    LOGIN_STOCKWs.ZRFC_LOGIN_STOCK inputLogin = new LOGIN_STOCKWs.ZRFC_LOGIN_STOCK();
                    inputLogin.I_DATOS = inputLOGIN_STOCK;
                    LOGIN_STOCKWs.ZWS_LOGIN_STOCKClient cliente = new LOGIN_STOCKWs.ZWS_LOGIN_STOCKClient();
                    cliente.ClientCredentials.UserName.UserName = "TTAPIA";
                    cliente.ClientCredentials.UserName.Password = "Seidor05$";
                    LOGIN_STOCKWs.ZRFC_LOGIN_STOCKResponse responseWs = cliente.ZRFC_LOGIN_STOCK(inputLogin);
                    json = JsonConvert.SerializeObject(responseWs);
                }
                //******END SERVICIO LOGIN**********************************************************************************************//
                pRetval = pRetval.Replace("{0}", "1");
                pRetval = pRetval.Replace("{1}", "OK");
                pRetval = pRetval.Replace("{2}", Request.Headers["sIdTransaccion"]);
                pRetval = pRetval.Replace("{3}", json);
                Response.ContentType = "application/json; charset=UTF-8";
                Response.Write(pRetval);
            }
            catch (WebException wex)
            {
                pRetval = pRetval.Replace("{0}", "-1");
                pRetval = pRetval.Replace("{1}", wex.Message + " // " + wex.InnerException);
                pRetval = pRetval.Replace("{2}", Request.Headers["sIdTransaccion"]);
                pRetval = pRetval.Replace("{3}", "[]");
                Response.ContentType = "application/json; charset=UTF-8";
                Response.Write(pRetval);
            }
            catch (Exception ex)
            {
                pRetval = pRetval.Replace("{0}", "-2");
                pRetval = pRetval.Replace("{1}", ex.Message + " // " + ex.InnerException);
                pRetval = pRetval.Replace("{2}", Request.Headers["sIdTransaccion"]);
                pRetval = pRetval.Replace("{3}", "[]");
                Response.ContentType = "application/json; charset=UTF-8";
                Response.Write(pRetval);
            }
            

        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
