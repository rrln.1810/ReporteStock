﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.IO;
using System.Drawing;

namespace DECOR_REPOSITORIO
{
    public class Archivos
    {
        public static FtpWebResponse subirFotos(String nombreArchivo,byte[] fileContents)
        {
            try
            {
                return UtilFTP.crearArchivo(Constantes.ftpPathUbicaciones + "/" + nombreArchivo + ".jpg", fileContents);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                throw ex;
                
            }
        }

        public static byte[] leerFotos(String nombreArchivo)
        {
            try
            {
                return UtilFTP.leerArchivo(Constantes.ftpPathUbicaciones + "/" + nombreArchivo + ".jpg");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                throw ex;

            }
        }

        public static System.Drawing.Image ResizeImage(System.Drawing.Image file, int width, int height, bool onlyResizeIfWider)
        {

            using (System.Drawing.Image image = file)
            {
                // Prevent using images internal thumbnail
                image.RotateFlip(RotateFlipType.Rotate180FlipNone);
                image.RotateFlip(RotateFlipType.Rotate180FlipNone);

                if (onlyResizeIfWider == true)
                {
                    if (image.Width <= width)
                    {
                        width = image.Width;
                    }
                }
                int newHeight = image.Height * width / image.Width;
                if (newHeight > height)
                {
                    // Resize with height instead
                    width = image.Width * height / image.Height;
                    newHeight = height;
                }
                System.Drawing.Image NewImage = image.GetThumbnailImage(width, newHeight, null, IntPtr.Zero);
                return NewImage;
            }
        }

        public byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, imageIn.RawFormat);
                return ms.ToArray();
            }
        }
    }
}
