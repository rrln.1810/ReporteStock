﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net;

namespace DECOR_REPOSITORIO
{
    public class Directorios
    {
        public static bool DirectorioTransportistasCreado = false;
        public static void crearDirectorioUbicaciones()
        {
            try
            {
                if (Directorios.DirectorioTransportistasCreado == false)
                {
                    UtilFTP.crearDirectorio(Constantes.ftpPathUbicaciones);
                }
                Directorios.DirectorioTransportistasCreado = true;
            }
            catch (WebException e)
            {
                Console.WriteLine(e.Message.ToString());
                String status = ((FtpWebResponse)e.Response).StatusDescription;
                Console.WriteLine(status);
                Directorios.DirectorioTransportistasCreado = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }
    }
}
